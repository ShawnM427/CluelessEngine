#version 410

#define MAX_BONES 255

layout (location = 0) in vec3 inPosition;
layout (location = 1) in vec4 inColor; // byte type
layout (location = 2) in vec2 inTexture;
layout (location = 3) in vec3 inNormal;
layout (location = 4) in vec3 inTangent;
layout (location = 5) in vec3 inBiTangent;
layout (location = 6) in ivec4 inBoneIds;
layout (location = 7) in vec4 inBoneWeights;


layout (location = 0) out vec4 outColor;
layout (location = 1) out vec2 outTexture;
layout (location = 2) out vec3 outNormal;
layout (location = 3) out vec3 outWorldPos;
layout (location = 4) out vec3 outTangent;
layout (location = 5) out vec3 outBiTangent;

uniform mat4 xWorld;
uniform mat4 xView;
uniform mat4 xProjection;

layout (std140) uniform xBones {
	mat4 BoneTransforms[MAX_BONES];
};

void main() {
	mat4 
	    boneTransform =  BoneTransforms[inBoneIds.x] * inBoneWeights.x;
	    boneTransform += BoneTransforms[inBoneIds.y] * inBoneWeights.y;
	    boneTransform += BoneTransforms[inBoneIds.z] * inBoneWeights.z;
	    boneTransform += BoneTransforms[inBoneIds.w] * inBoneWeights.w;

	vec4 modelPos = boneTransform * vec4(inPosition, 1.0);
    vec4 pos = xView * xWorld * modelPos;
	outWorldPos = pos.xyz;
	pos = xProjection * pos;
	gl_Position = pos;

    outColor = inColor / 255.0f;
    outTexture = inTexture;
    mat3 mv = mat3(xView) * mat3(xWorld) * mat3(boneTransform );
	outNormal = mv * inNormal;
	outTangent = mv * inTangent;
	outBiTangent = mv * inBiTangent;
}

