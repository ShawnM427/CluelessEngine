#version 430

uniform samplerCube xSampler;

layout (location = 1) in vec3 fragUvt;

layout (location = 0) out vec3 out_color;
layout (location = 1) out vec3 out_normal;
layout (location = 2) out vec4 emisive_color;

void main()
{
    vec4 in_color = texture(xSampler, fragUvt);

	out_color.rgb = in_color.rgb;
	emisive_color = in_color;
	out_normal = vec3(0.5f);
}
