#version 410

layout (location = 0) in vec4 vsColor;
layout (location = 1) in vec2 vsTexCoords;
layout (location = 2) in vec3 vsNormal;
layout (location = 3) in vec3 vsWorldSpace;
layout (location = 4) in vec3 vsTangent;
layout (location = 5) in vec3 vsBiTangent;

layout (location = 0) out vec4 frag_color;
layout (location = 1) out vec3 normal_color;
layout (location = 2) out vec4 emisive_color;
layout (location = 3) out vec4 specular_color;
layout (location = 4) out vec3 world_space;

uniform sampler2D xDiffuse;
uniform sampler2D xSpecular;
uniform sampler2D xEmissive;
uniform sampler2D xNormal;

uniform mat4 xWorld;
uniform mat4 xModel;
uniform mat4 xView;

void main() {
	frag_color = texture2D(xDiffuse, vsTexCoords);
	mat3 tangentSpace = mat3(normalize(vsTangent), normalize(vsBiTangent), normalize(vsNormal));
	// Re-normalize
	vec3 normalMap = texture(xNormal, vsTexCoords).xyz * 2.0 - 1.0;
	normal_color = normalize(tangentSpace * normalMap);
	emisive_color = texture(xEmissive, vsTexCoords);
	specular_color = texture(xSpecular, vsTexCoords);
	world_space = vsWorldSpace;
}
