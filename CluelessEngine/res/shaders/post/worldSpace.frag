#version 430

uniform sampler2D xWorldSpace;

uniform vec2      xTexSize;

layout(location = 0) out vec3 out_color;

void main()
{
    vec2 texUV = gl_FragCoord.xy / xTexSize;    
	vec3 in_worldSpace = texture(xWorldSpace, texUV).rgb; 
	
	out_color = in_worldSpace/ 10.0f;
}
