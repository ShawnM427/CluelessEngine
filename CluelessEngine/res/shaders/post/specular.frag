#version 430

uniform sampler2D xSpecular;

uniform vec2      xTexSize;

layout(location = 0) out vec3 out_color;

void main()
{
    vec2 texUV = gl_FragCoord.xy / xTexSize;    
	vec4 in_specular = texture(xSpecular, texUV); 
	
	out_color = in_specular.rgb  * in_specular.a;;
}
