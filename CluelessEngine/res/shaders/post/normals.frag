#version 430

uniform sampler2D xNormals;

uniform vec2      xTexSize;

layout(location = 0) out vec3 out_color;

void main()
{
    vec2 texUV = gl_FragCoord.xy / xTexSize;    
	vec3 in_normal = texture(xNormals, texUV).rgb; 
	
	out_color.rgb = (in_normal + 1.0f) / 2.0f;
}
