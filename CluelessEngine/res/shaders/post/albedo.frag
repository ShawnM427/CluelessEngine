#version 430

uniform sampler2D xSampler;

uniform vec2      xTexSize;

layout(location = 0) out vec3 out_color;

void main()
{
    vec2 texUV = gl_FragCoord.xy / xTexSize;    
	vec3 in_albedo = texture(xSampler, texUV).rgb; 
	
	out_color.rgb = in_albedo;
}
