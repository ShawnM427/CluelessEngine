#version 420

layout (location = 0) in vec3 in_position;
layout (location = 1) in vec2 in_uv;
layout (location = 2) in vec3 in_normal;
	
out vec3 position;
out vec2 uv;
out vec3 normal;

uniform mat4 xModel;
uniform mat4 xView;
uniform mat4 xProjection;
	
void main() {
    // Right-left matrix multiplication
	normal = mat3(xView * xModel) * in_normal;
	vec4 viewspacePos = xView * xModel * vec4(in_position, 1.0f);
	position = viewspacePos.xyz;
	uv = in_uv;
	gl_Position =  xProjection * viewspacePos;
}