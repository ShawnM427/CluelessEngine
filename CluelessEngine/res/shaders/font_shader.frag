#version 410

uniform sampler2D xSampler;

layout (location = 0) in vec4 fragColor;
layout (location = 1) in vec2 fragUv;
	
out vec4 frag_color;
	
void main() {
	frag_color = fragColor * texture2D(xSampler, fragUv).a;
}