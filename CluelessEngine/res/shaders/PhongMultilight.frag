#version 420

#define MAX_LIGHTS 4
#define MAX_MATERIALS 1
#define MAX_TEXTURES 16

in vec3 position;
in vec2 uv;
in vec3 normal;

out vec4 outColor;

uniform sampler2D AlbedoTexs[MAX_TEXTURES];
uniform mat4 xView;

// Light Settings
struct LightData {
	vec4  Position;
	vec3  Ambient;
	float ConstantAttenuation;
	vec3  Diffuse;
	float LinearAttenuation;
	vec3  Specular;
	float QuadraticAttenuation;
};

// Material settings
struct Material {
	vec4      ObjectColor;
	float     TextureContribution;
	float     SpecularPower;
	uint      TextureId;
};

// Material Settings
layout (std140) uniform xMaterials {
	Material Materials[MAX_MATERIALS];
};
layout (std140) uniform xLights {
	LightData Lights[MAX_LIGHTS];
};

vec3 calcPhong(LightData light, vec3 norm, uint matId);
vec4 calcMaterialMultiplier(uint matId);
	
void main() {
	// Re-normalize
	vec3 norm = normalize(normal);
	uint matId = 0;

	for(int ix = 0; ix < MAX_LIGHTS; ix++) {
		outColor.rgb += calcPhong(Lights[ix], norm, matId);
	}

	vec4 colorTexture = calcMaterialMultiplier(matId);
	outColor.rgb *= colorTexture.rgb;
	//outColor.rgb = norm;
	outColor.a = colorTexture.a;
}

vec4 calcMaterialMultiplier(uint matId) {
	return mix(Materials[matId].ObjectColor, 
	           texture(AlbedoTexs[Materials[matId].TextureId], uv), 
			   Materials[matId].TextureContribution);
}

vec3 calcPhong(LightData light, vec3 norm, uint matId) {
	vec4 lightPos = light.Position * xView;
	vec3 lightVec = lightPos.xyz - position;
	float dist = length(lightVec);

	vec3 lightDir = lightVec / dist;

	float nDotL = dot(norm, lightDir);
	
	if (nDotL > 0.0) {
		// Calculate falloff (attenuation)
		float att = 1.0 / (
			(light.ConstantAttenuation) + 
			(light.LinearAttenuation * dist) + 
			(light.QuadraticAttenuation * dist * dist)
			);

		// Calculate diffuse
		outColor.rgb += light.Diffuse * nDotL * att;

		// Blinn-Phong half vector
		float nDotHV = max(dot(norm, normalize(lightDir + normalize(-position))), 0.0);

		return light.Specular * pow(nDotHV, Materials[matId].SpecularPower) * att;
	}
}