#version 440
		uniform sampler2D xSamplers[8];
		struct GsVert {
			vec2  TexCoord;
			vec4  Color;
			float TexId;
		};
		in GsVert GsToFS;
		out vec4 FinalColor;
		void main() {
            //FinalColor = vec4(1.0, 1.0, 1.0, 1.0);
			//FinalColor = Input.Color; 
			FinalColor = GsToFS.Color * texture2D(xSamplers[int(GsToFS.TexId)], GsToFS.TexCoord);
		}