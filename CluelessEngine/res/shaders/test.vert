#version 440
uniform mat4 xWorld;
struct ProgVert {
	vec3  Position;
	vec4  Color;
	float Size;
	float TexId;
};
struct VsVert {
	vec4  Position;
	vec4  Color;
	float Size;
	float TexId;
};
in ProgVert Input;
out VsVert VsToGs;
void main() {
	VsToGs.Position = xWorld * vec4(Input.Position, 1);
	VsToGs.Color = Input.Color;
	VsToGs.TexId = Input.TexId;
	VsToGs.Size = Input.Size;
	gl_Position = VsToGs.Position;
}