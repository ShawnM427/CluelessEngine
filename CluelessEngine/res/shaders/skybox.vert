#version 410
layout (location = 0) in vec3 vertexPosition;
layout (location = 2) in vec2 vertexTexture;
	
layout (location = 1) out vec3 fragmentTexture;

uniform mat4 xTransform;
	
void main() {
	gl_Position = xTransform * vec4(vertexPosition, 1);
	fragmentTexture = vertexPosition;
}