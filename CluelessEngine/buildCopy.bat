echo off
set OutputPath=%1
set %PROCESSOR_ARCHITECTURE=%2
xcopy "res\*" "%OutputPath%res\"  /e /c /f /y /s
xcopy "dependencies\%PROCESSOR_ARCHITECTURE%\*" "%OutputPath%"  /e /c /f /y /s