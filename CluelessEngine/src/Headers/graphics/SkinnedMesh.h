#pragma once

#include "cms/Structures.h"
#include "graphics/UniformBuffer.h"
#include "graphics/Mesh.h"

#include <assimp\scene.h>

namespace clueless { namespace graphics {
	
	class SkinnedMesh :
		public IResource 
	{
	public:
		SkinnedMesh(uint16_t numBones);
		virtual ~SkinnedMesh();
		
		void Unload();

		void LoadFromScene(const aiScene *scene, int meshIndex, const ContentToken& materialToken);
		
		void Draw(const glm::mat4& world, const glm::mat4& view, const glm::mat4& projection) const;

		uint16_t GetBoneCount() const { return myBoneCount; }
		glm::mat4* GetBoneData() { return myBuffer->GetData<glm::mat4>(); }

	private:
		UniformBuffer *myBuffer;
		Mesh          *myMesh;
		ContentToken   myMaterialToken;

		uint16_t       myBoneCount;
	};

} }