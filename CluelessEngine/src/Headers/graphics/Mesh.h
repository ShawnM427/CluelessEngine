#pragma once
#ifndef MESH_H
#define MESH_H

#include <utils/common.h>
#include <graphics/common.h>
#include <vector>

#include "glm_math.h"

#include "graphics/VertexDeclaration.h"
#include "MeshData.h"
#include "Shader.h"
#include "Texture.h"
#include "Material.h"

namespace clueless { namespace graphics {
	
	class Model;

    /**
        Represents an indexed mesh that can be rendered. This is meant to be a single-material mesh part
    */
    class Mesh
    {
		friend class Model;
        public:
			// Stores any mesh offset for this mesh part
			glm::mat4 Transform;

            /**
                Create a new mesh
                @param data The vertex and index data to create the mesh from
                @param vDecl The layout of the vertex data in this mesh
            */
            Mesh(const MeshData& data, GLenum primitiveMode = GL_TRIANGLES, GLenum usage = GL_STATIC_DRAW); 
			//Mesh(const Mesh& other) = delete;
            /**
                Cleans up this mesh and it's buffers
            */
            virtual ~Mesh();
						
            void Update(void* vertexData, int numVertices, USHORT* indexDara, int numIndices);

			void Unload();

			void NoMaterialDraw() const;

            /**
                Renders this mesh part
                @see https://www.khronos.org/opengl/wiki/Primitive#Provoking_vertex
            */
            void Draw() const;
            /**
                Renders this mesh part
                @see https://www.khronos.org/opengl/wiki/Primitive#Provoking_vertex
            */
            void Draw(const int indexCount) const;

        protected:
            // Stores the handles to the buffers
            GLuint myVbo, myIbo, myVao;
            // Store the vertex and index counts
            uint   myVertexCount, myIndexCount;

			// Stores the size of a single vertex
            uint myVertexSize;

			// Stores the usage type (ex: static_draw, stream_draw, etc...)
			GLenum myUsage;
			// Stores the primitive mode
			GLenum myPrimitiveMode;

			// Stores which warnings have been raised for this mesh
			uint8_t myWarnFlags;

			enum WarnFlags {
				/* Warning for slow streaming when using glBufferSubData */
				SLOW_STREAMING = 1,
				/* Warning for updating a buffer without STREAM_DRAW usage */
				USAGE          = 2
			};

    };

} }

#endif // MESH_H
