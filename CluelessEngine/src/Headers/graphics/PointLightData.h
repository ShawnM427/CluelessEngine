#pragma once

#include "glm_math.h"

namespace clueless { namespace graphics {

	struct PointLightData {
		glm::vec4 Position;
		glm::vec3 Diffuse;
		float     Attenuation;
		glm::vec3 Specular;
		float     Padding;
	};

} }