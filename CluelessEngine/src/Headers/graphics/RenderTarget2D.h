#ifndef RENDERTARGET2D_H
#define RENDERTARGET2D_H

#include <graphics/common.h>

namespace clueless { namespace graphics {

    class RenderTarget2D
    {
        public:
            RenderTarget2D(const uint32_t width, const uint32_t height, const GLenum wrapMode = GL_CLAMP);

            void Cleanup();

            void Bind() const;
            static void Unbind();

            GLuint GetTextureHandle() const { return myRenderedTexture; }

        private:
            void     __Create(const uint32_t width, const uint32_t height, const GLenum wrapMode);

            uint32_t myWidth, myHeight;
            GLuint   myFrameBufferHandle;
            GLuint   myRenderedTexture;
            GLuint   myDepthRenderBuffer;
    };

} }

#endif // RENDERTARGET2D_H
