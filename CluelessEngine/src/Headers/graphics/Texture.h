/**
    Defines the Texture Class
    @date   July 27, 2017
    @author Shawn Matthews - 100412327
    @author Shaun McKinnon - 100642799
    @author Paul Puig      - 100656910
*/
#pragma once
#ifndef TEXTURE_H
#define TEXTURE_H

#include "graphics/common.h"

#include "globals.h"
#include "utils/logger.h"

namespace clueless { namespace graphics {

	struct TextureDefinition {
		uint   Width;
		uint   Height;
		uchar* Data;
		GLenum FilterMode;
		GLenum InternalPixelType;
		GLenum PixelFormat;
		GLenum PixelType;
		GLenum WrapMode;
		bool   EnableAnisotropy;

		TextureDefinition() {}
		TextureDefinition(
			const uint width, 
			const uint height, 
			uchar *data,
			const GLenum filterMode = GL_NEAREST,
			const GLenum internalPixelType = GL_RGBA,
			const GLenum pixelFormat = GL_RGBA,
			const GLenum pixelType = GL_UNSIGNED_BYTE,
			const GLenum wrapMode = GL_CLAMP,
			const bool   enableAnisotropy = true) :
			Width(width), Height(height), Data(data), FilterMode(filterMode), InternalPixelType(internalPixelType),
			PixelFormat(pixelFormat), PixelType(pixelType), WrapMode(wrapMode), EnableAnisotropy(enableAnisotropy) {}
	};

    /**
        Manages creating and loading textures
    */
    class Texture
    {
        public:
            //destructor
            ~Texture();
            //constructors
            Texture() {}
            Texture(const char* fileName, GLenum filterMode = GL_NEAREST, GLenum wrapMode = GL_CLAMP, bool enableAnisotropy = true);
            Texture(const uint width, const uint height, unsigned char *data,
                                  const GLenum filterMode = GL_NEAREST,
                                  const GLenum internalPixelType = GL_RGBA,
                                  const GLenum pixelFormat = GL_RGBA,
                                  const GLenum pixelType = GL_UNSIGNED_BYTE,
                                  const GLenum wrapMode = GL_CLAMP,
                                  const bool   enableAnisotropy = true);
			Texture(const TextureDefinition& definition);
			Texture(const Texture& other) {
				FILE_LOG(logDEBUG) << "Texture copy detected. Should be removed!\n" << LOG_CALLSTACK();
			}

			/*
				Unloads this resource
			*/
			void Unload();

            /**
                Binds this texture to the given texture slot
                @param textureSlot The texture slot to bind to (EX: GL_TEXTURE0)
            */
            void Bind(uint8_t textureSlot);  
            /**
                Gets the GL handle to the underlying texture
            */
            inline GLuint GetHandle() const { return myTextureHandle; }

			static void Unbind(const GLenum location);

            inline uint GetWidth() const { return myHeight; }
            inline uint GetHeight() const { return myHeight; }

        private:
            // Stores the GL handle to the underlying texture
            GLuint myTextureHandle;

            // Stores the dimensions of the texture
            uint   myWidth, myHeight; 

            void __Create(const uint width, const uint height, unsigned char *data,
                            const GLenum filterMode,
                            const GLenum internalPixelType,
                            const GLenum pixelFormat,
                            const GLenum pixelType,
                            const GLenum wrapMode,
                            const bool   enableAnisotropy = true);
    };

}}

#endif // TEXTURE_H
