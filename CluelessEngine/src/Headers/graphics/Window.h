#pragma once
#ifndef WINDOW_H
#define WINDOW_H

#include <functional>

#include "glm_math.h"

#include "graphics/common.h"
#include "ui/input/InputManager.h"
#include "globals.h"

namespace clueless { namespace graphics { class Window; } }


namespace clueless {
	
    namespace graphics {

        class Window
        {
			friend clueless::ui::input::InputManager;

        public:
            Window(const uint width, const uint height, const char* title = "Clueless Engine", const bool fullscreen = false, const bool vsyncEnabled = true);

            ~Window();

            void Init();

            void Resize(const uint newWidth, const uint newHeight);
			
            void InitRenderLoop();

            void Update();
            void SwapBuffers() const;

            void Close();

            bool Closing() const;

            inline uint GetWidth() const { return myWidth; }
            inline uint GetHeight() const { return myHeight; }
            inline double GetFrameDelta() const { return myGameTime.delta; }
            inline double GetFrameTime() const { return myGameTime.currentFrame; }

			glm::vec2 GetPosition() const; 

            void SetVSyncEnabled(const bool value);
            bool GetVSyncEnabled() const { return isVsyncEnabled; }

			void TriggerResize() {
				_handleGlfwResize(myWidth, myHeight);
			}

			void AddResizeCallback(std::function<void(void*, int, int)> callback) { myResizeCallbacks.push_back(callback); }
            void SetResizeCallback(std::function<void(void*, int, int)> callback) { myResizeCallback = callback; }
            void SetClosingCallback(std::function<void(void*, bool&)> callback) { myClosingCallback = callback; }

            void SetKeyEventCallback(std::function<void(void*, int, int, int, int)> callback) { myKeyPressEventback = callback; }
            void SetTextEventCallback(std::function<void(void*, uint, int)> callback) { myTextEventCallback = callback; }

            void SetMouseMoveCallback(std::function<void(void*, double, double)> callback) { myMouseMoveCallback = callback; }
            void SetMousePressCallback(std::function<void(void*, int, int, int)> callback) { myMousePressCallback = callback; }
            void SetMouseScrollCallback(std::function<void(void*, double, double)> callback) { myMouseScrollCallback = callback; }

            void SetJoystickConnectionCallback(std::function<void(void*, int, int)> callback) { myJoystickConnectionCallback = callback; }

            void SetPathDropCallback(std::function<void(void*, int, char**)> callback) { myPathDropCallback = callback; }

            bool IsMousePressed(const int button) const;
            bool IsKeyPressed(const int button) const;

            const char* GetClipboardString() const;
            void SetClipboardString(const char* text);

			inline void SetUserParam(void* value) { myUserParam = value; }
			inline void* GetUserParam() const { return myUserParam; }

			inline GLFWwindow* GetHandle() const { return myWindowHandle; }

        private:
			struct WindowTime {
				double currentFrame, lastFrame, delta;
			};

            friend class clueless::ui::input::InputManager;

            GLFWwindow* myWindowHandle;
            const char* myTitle;
            uint        myWidth, myHeight;
            bool        isFullscreen, isVsyncEnabled;
			WindowTime  myGameTime;

            void*       myUserParam;

			std::vector<std::function<void(void*, int, int)>>   myResizeCallbacks;

            std::function<void(void*, int, int)>           myResizeCallback;
            std::function<void(void*, bool&)>              myClosingCallback;

            std::function<void(void*, int, char**)>        myPathDropCallback;

            std::function<void(void*, double, double)>     myMouseMoveCallback;
            std::function<void(void*, int, int, int, int)> myKeyPressEventback;
            std::function<void(void*, uint, int)>          myTextEventCallback;
            std::function<void(void*, int, int, int)>      myMousePressCallback;
            std::function<void(void*, double, double)>     myMouseScrollCallback;
            std::function<void(void*, int, int)>           myJoystickConnectionCallback;

            void _handleGlfwResize(const uint newWidth, const uint newHeight);
            void _handleGlfwMouseButton(const int button, const int action, const int mods);
            void _handleGlfwKeyEvent(const int key, const int scancode, const int action, const int mods);
            void _handleGlfwTextInput(const uint codepoint, const int mods);
            void _handleGlfwMouseMove(const double xPos, const double yPos);
            void _handleGlfwScroll(const double xPos, const double yPos);
            void _handleGlfwDrop(int numPaths, char** paths);
            void _handleGlfwClose();

            static void __handleGlfwKeyEvent(GLFWwindow* window, int key, int scancode, int action, int mods);
            static void __handleGlfwTextEvent(GLFWwindow* window, uint codepoint, int mods);
            static void __handleGlfwMouseButton(GLFWwindow* window, int button, int action, int mods);
            static void __handleGlfwResize(GLFWwindow* window, int width, int height);
            static void __handleGlfwError(int error, const char* message);
            static void __handleGlfwMouseMove(GLFWwindow* window, double xPos, double yPos);
            static void __handleGlfwMouseScroll(GLFWwindow* window, double xPos, double yPos);
            static void __handleGlfwWindowClose(GLFWwindow* window);
            static void __handleGlfwDrop(GLFWwindow* window, int numPaths, char** paths);
            static void CALLBACK __handleGlDebug(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam);
        };
    }
}

#endif // WINDOW_H
