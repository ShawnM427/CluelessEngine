/**
    Defines the GeoUtils namespace
    @date   July 27, 2017
    @author Shawn Matthews - 100412327
    @author Shaun McKinnon - 100642799
    @author Paul Puig      - 100656910
*/
#pragma once
#ifndef GEOUTILS_H
#define GEOUTILS_H

#include "glm_math.h"

#include <graphics/MeshData.h>

namespace clueless { namespace graphics { namespace geo_utils {

	uint pack(glm::vec4 color);

	enum TexturingMode {
		TextureModeFace,
		TextureModeUV
	};

    MeshData CreateSprite(const glm::vec2 halfSize, const float minTexX, const float minTexY, const float maxTexX, const float maxTexY);
    MeshData CreateCircleFan(const int segments, const float radius, const glm::vec4 color, const glm::vec2 offset, float texSize = -1.0f);
	MeshData CreateTextureCube(const glm::vec3 size, const glm::vec3 offset, const glm::vec4 color, const TexturingMode mode = TextureModeFace, bool inverted = false);

}}}

#endif // GEOUTILS_H
