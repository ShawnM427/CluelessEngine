#pragma once
#ifndef FONTRENDERER_H
#define FONTRENDERER_H

#include <unordered_map>

#include <graphics/common.h>
#include <graphics/Texture.h>
#include <graphics/VertexLayouts.h>
#include <graphics/MeshData.h>
#include <graphics/Mesh.h>

#include "ui/TextureFont.h"

#include "glm_math.h"

namespace clueless { namespace graphics {
    class FontRenderer 
    {
        public:
            static void Render(const clueless::ui::TextureFont& font, const char* text, const glm::vec2 position, const glm::vec4 color, const float scale = 1.0f);
            static MeshData Bake(const clueless::ui::TextureFont& font, const char* text, const glm::vec2 position, const glm::vec4 color, const float scale = 1.0f);

        protected:

        private:
            static const int BATCH_SIZE;

            static Mesh*     myMesh;
			static Shader*   myShader;
			
            static VertexPositionColorTexture*  myVertexData;
            static uint16_t*                    myIndexData;

			static void __Init();

            static void __GenerateData(const clueless::ui::TextureFont& font, const char* text, const glm::vec2 position, const glm::vec4 color, VertexPositionColorTexture* vertices, uint16_t* indices, int& length, const float scale);
    };
}}
#endif // FONTRENDERER_H
