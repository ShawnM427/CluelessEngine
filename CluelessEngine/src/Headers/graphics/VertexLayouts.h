/**
    Defines the Vertex Layout
    @date   July 27, 2017
    @author Shawn Matthews - 100412327
    @author Shaun McKinnon - 100642799
    @author Paul Puig      - 100656910
*/
#pragma once
#ifndef VERTEXLAYOUTS_H_INCLUDED
#define VERTEXLAYOUTS_H_INCLUDED

#include <graphics/VertexDeclaration.h>

#include "glm_math.h"

/*
    Shader Vertex Layout:
        0 - vec3 Position
        1 - vec4 Specular Color
        2 - vec2 Texture UV
        3 - vec3 Normal
        4 - vec3 Binormal
*/

namespace clueless { namespace graphics {

	union GpuColor {
		uint        Value;
		char        Bytes[4];
		struct {
			unsigned char R, G, B, A;
		};
		struct {
			unsigned char X, Y, Z, W;
		};
	};

    /**
        Represents a vertex with a given position and color
    */
    struct VertexPositionColor {
        glm::vec3 Position;
		GpuColor  Color;
    };


    /**
        Represents how the VertexPositionColor is sent to the GPU
    */
    const VertexDeclaration VertPosColDecl = VertexDeclaration(2,
        VertexElement(0, GL_FLOAT, 3, VType_Position),
        VertexElement(1, GL_UNSIGNED_BYTE, 4, VType_Color)
    );


    /**
        Represents a vertex with a given position and texture
    */
    struct VertexPositionTexture {
        glm::vec3 Position;
        glm::vec2 Texture;
    };


    /**
        Represents how the VertexPositionTexture is sent to the GPU
    */
    const VertexDeclaration VertPosTexDecl = VertexDeclaration(2,
        VertexElement(0, GL_FLOAT, 3, VType_Position),
        VertexElement(2, GL_FLOAT, 2, VType_Texture)
    );


    /**
        Represents a vertex with a given position, color, and texture
    */
    struct VertexPositionColorTexture {
        glm::vec3 Position;
		GpuColor  Color;
        glm::vec2 Texture;
    };

	struct VertexPositionNormalTexture {
		glm::vec3 Position;
		glm::vec2 Texture;
		glm::vec3 Normal;
	};

	/**
	Represents how the VertexPositionColorTexture is sent to the GPU
	*/
	const VertexDeclaration VertPosNormTex = VertexDeclaration(3,
		VertexElement(0, GL_FLOAT, 3, VType_Position),
		VertexElement(2, GL_FLOAT, 2, VType_Normal),
		VertexElement(3, GL_FLOAT, 3, VType_Normal)
	);

    /**
        Represents how the VertexPositionColorTexture is sent to the GPU
    */
    const VertexDeclaration VertPosColTexDecl = VertexDeclaration(3,
        VertexElement(0, GL_FLOAT, 3, VType_Position),
        VertexElement(1, GL_UNSIGNED_BYTE, 4, VType_Color),
        VertexElement(2, GL_FLOAT, 2, VType_Texture)
    );


	struct VertexPositionColorNormal {
		glm::vec3 Position;
		GpuColor  Color;
		glm::vec3 Normal;
	};
	const VertexDeclaration VertPosColNorm = VertexDeclaration(3,
		VertexElement(0, GL_FLOAT, 3, VType_Position),
		VertexElement(1, GL_UNSIGNED_BYTE, 4, VType_Color),
		VertexElement(3, GL_FLOAT, 3, VType_Normal)
	);

	struct VertexPositionColorTextureNormal {
		glm::vec3 Position;
		GpuColor  Color;
		glm::vec2 Texture;
		glm::vec3 Normal;
	};

	/**
	Represents how the VertexPositionColorTexture is sent to the GPU
	*/
	const VertexDeclaration VertPosColTexNormDecl = VertexDeclaration(4,
		VertexElement(0, GL_FLOAT, 3, VType_Position),
		VertexElement(1, GL_UNSIGNED_BYTE, 4, VType_Color),
		VertexElement(2, GL_FLOAT, 2, VType_Texture),
		VertexElement(3, GL_FLOAT, 3, VType_Normal)
	);


	struct FullVertex {
		glm::vec3 Position;
		glm::vec2 Texture;
		glm::vec3 Normal;
		glm::vec3 Tangent;
		glm::vec3 BiTangent;
	};

	const VertexDeclaration FullVertexDecl = VertexDeclaration(5,
		VertexElement(0, GL_FLOAT, 3, VType_Position),
		VertexElement(2, GL_FLOAT, 2, VType_Texture),
		VertexElement(3, GL_FLOAT, 3, VType_Normal),
		VertexElement(4, GL_FLOAT, 3, VType_Tangent),
		VertexElement(5, GL_FLOAT, 3, VType_BinTan)
	);


	struct SkinnedVert {
		glm::vec3  Position;
		glm::vec2  Texture;
		glm::vec3  Normal;
		glm::vec3  Tangent;
		glm::vec3  BiTangent;
		glm::ivec4 BoneIDs;
		glm::vec4  BoneWeights;
	};

	const VertexDeclaration SkinnedVertDecl = VertexDeclaration(7,
		VertexElement(0, GL_FLOAT, 3, VType_Position),
		VertexElement(2, GL_FLOAT, 2, VType_Texture),
		VertexElement(3, GL_FLOAT, 3, VType_Normal),
		VertexElement(4, GL_FLOAT, 3, VType_Tangent),
		VertexElement(5, GL_FLOAT, 3, VType_BinTan),
		VertexElement(6, GL_INT,   4, VType_User1),
		VertexElement(7, GL_FLOAT, 4, VType_User2)
	);
	

}}

#endif // VERTEXLAYOUTS_H_INCLUDED
