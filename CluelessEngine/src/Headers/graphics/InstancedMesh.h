#pragma once
#ifndef INSTANCEDMESH_H
#define INSTANCEDMESH_H

#include <graphics/common.h>

#include "glm_math.h"

#include "graphics/VertexDeclaration.h"
#include "MeshData.h"


namespace clueless { namespace graphics {

    struct InstanceData {
        glm::mat4 Transform;
        glm::vec4 Color;
    };

    /**
        Represents an indexed mesh that can be rendered. This is meant to be a single-material mesh part
    */
    class InstancedMesh
    {
        public:
            /**
                Create a new mesh
                @param data The vertex and index data to create the mesh from
                @param vDecl The layout of the vertex data in this mesh
                @param numInstance The number of instances to store
                @param attribLocation The location of the start of the mat4 in the shader
            */
            InstancedMesh(const MeshData& data, const VertexDeclaration vDecl, const uint numInstances, const int attribLocation);
			InstancedMesh(const InstancedMesh& other) = delete;
            /**
                Cleans up this mesh and it's buffers
            */
            virtual ~InstancedMesh();

            /**
                Renders this mesh part
                @param primitiveMode The mode in which to render primitives of this mesh
                @see https://www.khronos.org/opengl/wiki/Primitive#Provoking_vertex
            */
            void Draw(const GLenum primitiveMode, const int count) const;

            /**
                Updates our internal instance buffer
                @param count The number of instances to update
            */
            void UpdateInstances(const int count);

            /**
                Gets a handle to our buffer, and also returns the number of instances
                @param count Will return the number of instances in the mesh
            */
            InstanceData* GetInstanceData(int& count);

        protected:
            // Stores the handles to the buffers
            GLuint     myVbo, myIbo, myVao, myInstanceBuffer;

            // Store the vertex and index counts
            uint       myVertexCount, myIndexCount, myInstanceCount;

            InstanceData* myInstanceData;

    };
}}

#endif // INSTANCEDMESH_H
