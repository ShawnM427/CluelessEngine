#pragma once
#ifndef GRAPHICS_COMMON
#define GRAPHICS_COMMON

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#define DEBUG_SEVERITY_HIGH         0x9146
#define DEBUG_SEVERITY_MEDIUM       0x9147
#define DEBUG_SEVERITY_LOW          0x9148
#define DEBUG_SEVERITY_NOTIFICATION 0x826B

#define INDEX_TYPE GLushort

#endif // GRAPHICS_COMMON
