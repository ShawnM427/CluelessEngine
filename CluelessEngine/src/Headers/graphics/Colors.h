#pragma once

#include "glm_math.h"

#define WHITE   glm::vec4(1.0f, 1.0f, 1.0f, 1.0f)
#define BLACK   glm::vec4(0.0f, 0.0f, 0.0f, 1.0f)
#define RED     glm::vec4(1.0f, 0.0f, 0.0f, 1.0f)
#define GREEN   glm::vec4(0.0f, 1.0f, 0.0f, 1.0f)
#define BLUE    glm::vec4(0.0f, 0.0f, 1.0f, 1.0f)
#define CYAN    glm::vec4(0.0f, 1.0f, 1.0f, 1.0f)
#define MAGENTA glm::vec4(1.0f, 0.0f, 1.0f, 1.0f)
#define YELLOW  glm::vec4(1.0f, 0.0f, 1.0f, 1.0f)