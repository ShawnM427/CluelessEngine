#pragma once

#include <memory>

#include "glad/glad.h"

namespace clueless { namespace graphics {

		class UniformBuffer {
		public:
			UniformBuffer();
			~UniformBuffer();

			void Init();
			void Unload();

			void Bind(GLuint bindingPoint);

			void Update();

			template <typename T>
			void Init(GLuint bindPoint, size_t count = 1);

			template <typename T>
			void Alloc(size_t count = 1);

			template <typename T>
			void SetData(const T& data);

			template <typename T>
			void SetData(const T* data, size_t size);

			GLuint ElementCount() const { return myDataCount; }

			template <typename T>
			T* GetData(size_t index);

			template <typename T>
			T* GetData();

		private:
			void   *myData;
			GLuint  myHandle;
			GLuint  myDataSize;
			GLuint  myDataCount;
		};

		template <typename T>
		void UniformBuffer::Init(GLuint bindPoint, size_t count) {
			Init();
			Alloc<T>(count);
			Bind(bindPoint);
		}

		template <typename T>
		void UniformBuffer::Alloc(size_t count) {
			if (myDataSize != sizeof(T)) {
				if (myData)
					free(myData);
				myData = malloc(sizeof(T) * count);
				myDataSize = sizeof(T) * count;
				myDataCount = count;
			}
			Update();
		}

		template <typename T>
		void UniformBuffer::SetData(const T& data) {
			if (myDataSize != sizeof(T)) {
				if (myData)
					free(myData);
				myData = malloc(sizeof(T));
				myDataSize = sizeof(T);
				memcpy(myData, &data, sizeof(T));
				myDataCount = 1;
			}
			Update();
		}

		template <typename T>
		void UniformBuffer::SetData(const T* data, size_t size) {
			if (myDataSize != sizeof(T) * size) {
				if (myData)
					free(myData);
				myData = malloc(sizeof(T) * size);
				myDataSize = sizeof(T) * size;
				memcpy(myData, &data, sizeof(T) * size);
				myDataCount = size;
			}
			Update();
		}

		template <typename T>
		T* UniformBuffer::GetData(size_t index) {
			if (index < myDataCount) {
				return reinterpret_cast<T*>(myData) + index;
			}
		}

		template <typename T>
		T* UniformBuffer::GetData() {
			return reinterpret_cast<T*>(myData);
		}

} }