#pragma once

#include "utils/Stack.h"

namespace clueless { namespace graphics { namespace renderer {

	template <typename T>
	class TransformStack {
		private:
			Stack<T> myQueue;

		public:
			TransformStack();
			~TransformStack();

			void Push(T& transform, bool overwrite = true);
			void Pop();
			const T& Current() const;
	};

} } }

#include "graphics/renderer/TransformStack.cpp"