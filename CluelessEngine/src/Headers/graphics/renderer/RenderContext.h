#pragma once

#include "utils/Stack.h"
#include "RenderState.h"

namespace clueless { namespace graphics { namespace renderer {

	class RenderContext {
	public:

	private:
		static RenderContext __mySingleton;
		
		Stack<RenderState> myStateStack;
	};

} } }