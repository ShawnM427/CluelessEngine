#pragma once

namespace clueless { namespace graphics { namespace renderer {

	struct TextureUnit {
		unsigned short Texture1D;
		unsigned short Texture2D;
		unsigned short Texture3D;
		unsigned short TextureCube;
	};

	struct RenderState {
		TextureUnit TextureUnits[32];
		
		int BoundProgramId;
	};

} } }