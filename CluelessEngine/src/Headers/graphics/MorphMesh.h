#pragma once

#include "MeshData.h"
#include "Mesh.h"
#include "anim/Enums.h"

#include "anim/AnimationMath.h"

namespace clueless { namespace graphics {

	class MorphMesh {
		public:
			MorphMesh();
			virtual ~MorphMesh();

			void AllocateKeys(int count);
			void InsertKey(float time, MeshData data);

			void SetLoopMode(anim::LoopType value);

			void pauseLoop(bool result);
			void Update(float deltaTime);

			void Draw() const;

		private:
			struct MorphKey {
				MeshData Data;
				float    Time;
			};

			bool pause = false;
			MorphKey      *myKeys;
			int            myNumKeys;
			int            myActiveKeys;
			int            myCurrentKey;
			int            myNextKey;
			float          myTime;
			float          myTotalTime;
			float          mySpeedMult;
			anim::LoopType  myLoopType;

			template <typename T, bool normalize = false>
			void __TryMix(int position, float tVal, void* from, void* to, void* result, const VertexDeclaration* vDecl){
				if (position != -1) {
					if (normalize)
						vDecl->Set(position, result, glm::normalize(anim::ease(vDecl->Get<T>(position, from), vDecl->Get<T>(position, to), tVal)));
					else
						vDecl->Set(position, result, anim::ease(vDecl->Get<T>(position, from), vDecl->Get<T>(position, to), tVal));
				}
			}

			MeshData       myActiveData;
			Mesh          *myMesh;
	};

} }