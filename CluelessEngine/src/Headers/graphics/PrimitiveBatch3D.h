#pragma once

#include "glm_math.h"

#include "VertexLayouts.h"
#include "utils/Stack.h"
#include "graphics/Shader.h"

#include "Colors.h"

#define RENDER3D_LINES_SIZE 1000
#define RENDER3D_TRIS_SIZE  15000

namespace clueless { namespace graphics {

	class PrimitiveBatch3D {
		public:
			static glm::mat4 ViewProj;

			static void Init();

			static void PushTransform(const glm::mat4& transform);
			static void PushOverride(const glm::mat4& transform);
			static void PopTransform();

			
			static void PushLine(const glm::vec3& pos1, const glm::vec3& pos2, const glm::vec4& color = WHITE);
			static void PushLine(const glm::vec3& pos1, const glm::vec3& pos2, const glm::vec4& fromColor, const glm::vec4& toColor);
			static void PushTriangle(const glm::vec3& pos1, const glm::vec3& pos2, const glm::vec3& pos3, const glm::vec4& color = WHITE);
			
			static void PushSphere(const glm::vec3& center, const glm::vec3& size, const glm::vec4& color = WHITE);

			static void Flush();

		private:
			struct PrimitiveVert3D {
				glm::vec3 Position;
				glm::vec4 Color;
			};

			static glm::mat4 myCurrent;

			static PrimitiveVert3D *myTriVertices;
			static PrimitiveVert3D *myLineVertices;

			static uint32_t         myBufferCounts[2];
			static GLuint           myVertexBuffers[2];
			static GLuint           myVaos[2];

			static Shader          *myShader;
			static Stack<glm::mat4> myTransformStack;
	};

} }