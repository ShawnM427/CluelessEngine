#pragma once

#include "graphics/common.h"
#include "globals.h"

namespace clueless { namespace graphics {

	class CubeMap {
		public:
			//destructor
			~CubeMap();
			//constructors
			CubeMap() {}
			CubeMap(const char* fileName, GLenum filterMode = GL_LINEAR, GLenum wrapMode = GL_CLAMP_TO_EDGE);
			CubeMap(const char** fileNames, GLenum filterMode = GL_LINEAR, GLenum wrapMode = GL_CLAMP_TO_EDGE);

			void Unload();

			void Bind(GLenum slot) const;

		private:
			// Stores the GL handle to the underlying texture
			GLuint myTextureHandle;
			
			void __LoadSide(const char* fileName, GLenum side,
				const GLenum internalPixelType,
				const GLenum pixelFormat,
				const GLenum pixelType);

			void __Create(const char** fileNames,
				const GLenum filterMode,
				const GLenum internalPixelType,
				const GLenum pixelFormat,
				const GLenum pixelType,
				const GLenum wrapMode);
	};

} }