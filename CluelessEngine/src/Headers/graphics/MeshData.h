#pragma once
#ifndef MESHDATA_H_INCLUDED
#define MESHDATA_H_INCLUDED

#include "graphics/common.h"
#include "VertexDeclaration.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

namespace clueless { namespace graphics {
    /**
        Represents a set of geometry with vertices and indices
    */
    struct MeshData {
        /**  Stores a pointer to the vertex data for the mesh  */
        void   *Vertices;
        /**  Stores the number of vertices in the mesh part  */
		uint16_t  VertexCount;
        /**  Stores a pointer to the index data for the mesh  */
		uint16_t *Indices;
        /**  Stores the number of indices in this mesh  */
		uint16_t  IndexCount;
		/** Stores the vertex declaration used by this mesh data */
		const VertexDeclaration *VDecl;

        /** Default constructor  */
		MeshData() {};

        /**
            Creates a new mesh data with the given geometry

            @param vertexData The vertex data pointer for the mesh
            @param vertexCount The number of vertices in the mesh
            @param indices The index data pointer for the mesh
            @param numIndices The number of indices in the mesh, this is used to calculate the number of vertices actually drawn
        */
		MeshData(void *vertexData, uint16_t vertexCount, uint16_t *indices, uint16_t numIndices, const VertexDeclaration *vDecl) :
			Vertices(vertexData), VertexCount(vertexCount), Indices(indices), IndexCount(numIndices), VDecl(vDecl) { };

		static MeshData LoadFromScene(const aiScene* scene, int meshIndex);
    };
} }

#endif // MESHDATA_H_INCLUDED
