/**
    Defines VertexDeclaration Class
    @date   July 27, 2017
    @author Shawn Matthews - 100412327
    @author Shaun McKinnon - 100642799
    @author Paul Puig      - 100656910
*/
#pragma once
#ifndef VERTEXDECLARATION_H
#define VERTEXDECLARATION_H

#include <cstdarg>
#include <vector>

#include "graphics/common.h"
#include "globals.h"

namespace clueless { namespace graphics {

	enum VType {
		VType_Unknown  = 0,
		VType_Position = 1,
		VType_Normal   = 2,
		VType_Texture  = 3,
		VType_Tangent  = 4,
		VType_BinTan   = 5,
		VType_Color    = 6,
		VType_User1    = 7,
		VType_User2    = 8,
		VType_User3    = 9,
	};

    /**
        Represents a single element of a vertex (ex: position or color)
    */
    struct VertexElement {
		static const VertexElement Null;

        /**  The binding location of the attribute to the vertex shader  */
        uint      Location;
        /**  The data type of the attribute  */
        GLenum    Type;
        /**  Whether or not this value is noramlized before sending to the GPU  */
        GLboolean Normalized;
        /**  Stores the number of components for this attribute  */
        uint      Size;
        /**  Stores the offset from the start of the vertex  */
        uint      Offset;
		/**  Stores the usage hint for the element */
		VType     Usage;

        /**
            Creates a new vertex element

            @param location  The location
            @param type      The data type of the attribute
            @param size      The number of elements in the attribute
            @param normalize Whether or not the attribute is normalized
        */
        VertexElement(uint location, GLenum type, uint size, VType usage, GLboolean normalized = GL_FALSE) :
            Location(location), Type(type), Normalized(normalized), Size(size), Offset(0), Usage(usage) { }

	private:
		VertexElement() : Location(-1), Type(GL_NONE), Normalized(false), Size(0), Offset(-1), Usage(VType_Unknown) {}
    };

    /**
        Defines how a single vertex is layed out within a mesh
    */
    class VertexDeclaration
    {
        public:
            uint ElementSize;

            /**
                Create a new vertex declaration with a varying number of arguments
                @param count The number of paramaters you are passing to the function
                @param ...   A list of vertex elements that define the vertex
            */
            VertexDeclaration(const int count, ...);
            virtual ~VertexDeclaration();

            /**
                Applies this vertex definition to a VAO
            */
            void Apply() const;

			const VertexElement Introspect(VType usage) const;

			template <typename T>
			T Get(const VType usage, const void* data) const {
				for (int ix = 0; ix < myElementCount; ix++) {
					if (myElements[ix].Usage == usage) {
						return *reinterpret_cast<T*>((char*)data + myElements[ix].Offset);
					}
				}
				return T();
			}

			template <typename T>
			T Get(const int offset, const void* data) const {
				return *reinterpret_cast<T*>((char*)data + offset);
			}

			template <typename T>
			void Set(const VType usage, void* data, const T value) const {
				for (int ix = 0; ix < myElementCount; ix++) {
					if (myElements[ix].Usage == usage) {
						memcpy((char*)data + myElements[ix].Offset, &value, sizeof(T));
					}
				}
			}

			template <typename T>
			void Set(const int offset, void* data, const T value) const {
				memcpy((char*)data + offset, &value, sizeof(T));
			}

        private:
            /**  Stores the elements in this vertex layout  */
            VertexElement* myElements;
			int myElementCount;
    };
}}

#endif // VERTEXDECLARATION_H
