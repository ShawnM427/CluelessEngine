#pragma once

#include <string>
#include <vector>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "Mesh.h"
#include "Shader.h"

namespace clueless { namespace graphics {

	class Model  {
		friend class Mesh;
		public:
			Model();
			~Model();

			void Draw();

			void Unload();

			void AddMesh(Mesh mesh) {
				myMeshes.push_back(mesh);
			}
			
			int GetMeshCount() const {
				return myMeshes.size();
			}

			Mesh& GetMesh(int index) {
				return myMeshes[index];
			}

			//static Model LoadFromFile(const std::string& fileName);
			static Model* LoadFromNode(const aiScene *scene, aiNode *node);

		private:
			static void __HandleNode(const aiScene *scene, aiNode* node, Model& model);

			std::vector<Mesh> myMeshes;
			std::string       myName;
	};
} }