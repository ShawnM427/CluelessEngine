#pragma once

#include "glm_math.h"
#include "LinearMath/btTransform.h"

namespace clueless {

	struct Transform {
		glm::vec3 Position;
		glm::quat Orientation;
		glm::vec3 Scale;

		Transform() : Position(glm::vec3()), Orientation(glm::quat()), Scale(glm::vec3(1.0f)) { }

		void CopyFrom(const btTransform& other);
		void CopyTo(btTransform& other);

		operator glm::mat4() const;		
		glm::mat4 operator *(const Transform& other);

	};
}

namespace glm {
	float distance(const clueless::Transform& left, const clueless::Transform& right);
}