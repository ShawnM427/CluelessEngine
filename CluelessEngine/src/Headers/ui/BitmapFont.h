#pragma once
#ifndef BITMAP_FONT_H
#define BITMAP_FONT_H

#include <unordered_map>

#include <graphics/common.h>
#include <graphics/Texture.h>
#include <graphics/VertexLayouts.h>
#include <graphics/MeshData.h>
#include <graphics/Mesh.h>

#include "TextureFont.h"

#include "glm_math.h"

namespace clueless { namespace ui {
	class BitmapFont : public TextureFont
    {
        public:
			const int CHAR_COUNT = 128;

			BitmapFont(const char* fontFileName);
            ~BitmapFont();
			
			void GetCharMetrics(int codepoint, int *xAdvance, int *xOff, int *yOff, int* gWidth, int *gHeight) const;
			int  GetKerning(int char1, int char2) const;
			void GetCharBounds(int inChar, float *x, float *y, float *x2, float* y2) const;
			int  GetLineHeight() const {
				return myLineHeight;
			}
			int  GetCharXAdvance(const char codepoint) const;

            #ifdef DEBUG
            void DBG_DumpKernings() const;
            #endif

        protected:

        private:			
            struct Glyph {
                int  X, Y;
                int  Width, Height;
                int  XOff, YOff;
                int  XAdvance;
            };

            char*  myFontName;
            short  myFontSize;
            int    myLineHeight;
            int    myLineBase;
            float  myScaleX;
            float  myScaleY;

            Glyph* myGlyphs;
            char** myKernings;

            void __LoadTextureFile(const char* fileName);
    };
}}
#endif // FONTRENDERER_H
