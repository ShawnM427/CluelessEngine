/*
	Provides joystick implementations of the input sources

	@date September 09th, 2017
*/


#pragma once
#ifndef JOYSTICKS_H
#define JOYSTICKS_H

#include "InputSources.h"

namespace clueless { namespace ui { namespace input {

	/*
	Represents an axial input source that wraps around a joystick or gamepad axis
	*/
	struct JoystickAxisSource : AxisInputSource {
		/*
		Create a new axial input from a given joystick and axis ID

		@param joystick The ID of the joystick to poll
		@param axis     The ID of the axis to poll
		W
		@see http://www.glfw.org/docs/latest/input_guide.html#joystick
		*/
		JoystickAxisSource(int joystick, int axis) : myJoystick(joystick), myAxis(axis) {}

	protected:
		/*
		Actually gets the input from the InputManager

		@returns The axial value as returned from the InputManager
		*/
		float __GetInput() {
			return InputManager::GetJoystickAxis(myJoystick, myAxis);
		}

		/* Stores ID of joystick and axis, again, see http://www.glfw.org/docs/latest/input_guide.html#joystick */
		int myJoystick;
		int myAxis;
	};

	/*
		Represents a joystick axis source that is normalized to be within a given range
	*/
	struct NormalizedJoystickAxisSource : NormalizedAxisSource {
		/*
			Create a new joystick axial input that is normalized to within the given range

			@param joystick The ID of the joystick to poll
			@param axis     The ID of the axis to poll
			@param sourceMin The minimum value the source input can have (for xBox axes it's -1.0)
			@param sourceMax The maximum value the source input can have (for xBox axis it's 1.0)
			@param outMin    The minimum value for the mapped output
			@param outMax    The maximum value for the mapped output
		*/
		NormalizedJoystickAxisSource(int joystick, int axis, float sourceMin = -1.0f, float sourceMax = 1.0f, float outMin = 0.0f, float outMax = 1.0f) :
			NormalizedAxisSource(new JoystickAxisSource(joystick, axis), sourceMin, sourceMax, outMin, outMax) {}

		// Delete copy constructor
		NormalizedJoystickAxisSource(const JoystickAxisSource& other) = delete;

		// Destructor for cleaning up the axis source we created in constructor
		~NormalizedJoystickAxisSource() {
			// We need to delete the input source to perform any cleanup, will
			delete myInput;
		}
	};

	/*
		Represents a joystick button source that will retreive 
	*/
	struct JoystickButtonSource : ButtonInputSource {
		/*
			Creates a new joystick button source from the given joystick and button index

			@param joystick The ID of the joystick to poll
			@param button   The ID of the button to poll
		*/
		JoystickButtonSource(int joystick, int button) : myButton(button), myJoystick(joystick) {}

		protected:
			/*
				Actually polls the InputManager for the button state

				@returns The state of the joystick button
			*/
			ButtonState __GetInput() {
				return InputManager::GetJoystickButton(myJoystick, myButton);
			}

			int myJoystick;
			int myButton;
	};
} } }

#endif