#pragma once
#ifndef CONTROLLERMAPPING_H
#define CONTROLLERMAPPING_H

#include <map>
#include <string>
#include <iostream>

namespace clueless { namespace ui { namespace input {

	/*
		Represents a Control Input Item, which stores a human-readable name as well as a input location
	*/
    struct CtrlInputItem {
		/* Gets or sets the location (ID) of the control item in the mapping */
        int         Location;
		/* Gets a human-readable name for the control for use in UI and end-user menus */
        const char* ReadableName;

		/* Create a default control item with -1 for location and with the readable name "< Unknown >"*/
        CtrlInputItem() : Location(-1), ReadableName("< Unknown >") { }
		/*
			Create a new control mapping item with the given button/axis location and name

			@param loc The ID of the button or axis
			@param name The human-readable name associated with this item (EX: Left Stick)
		*/
        CtrlInputItem(int loc, const char* name) : Location(loc + 1), ReadableName(name) { }
    };

	/*
		Represents the layout of a controller or joystick
	*/
    class ControllerMapping
    {
        public:
			/*
				Creates a new empty controller mapping
			*/
            ControllerMapping();
			/*
				Creates a new empty controller mapping with a given name

				@param name A human-readable name for this controller mapping (EX: "xBox Controller")
			*/
            ControllerMapping(const char* name);

			/*
				Adds a new axis to this controller mapping with the given lookup name

				@param name The internal name for this axis
				@param axis The mapping item to assign to this internal name
			*/
            void AddAxisLookup(std::string name, const CtrlInputItem axis) { myAxisLookups[name] = axis; }
			/*
				Adds a new axis to this controller mapping with the given lookup name, axis, and readable name

				@param name			The internal name for this axis
				@param axis			The location (ID) of the axis to add
				@param readableName The human-readable name for the axis (EX: "Left Stick")
			*/
            void AddAxisLookup(std::string name, const int axis, const char* readableName) { myAxisLookups[name] = CtrlInputItem(axis, readableName); }
			/*
				Adds a new axis to this controller mapping with the given lookup name and axis. This will set the readable name
				to the internal name

				@param name			The internal name for this axis
				@param axis			The location (ID) of the axis to add
			*/
            void AddAxisLookup(std::string name, const int axis) { myAxisLookups[name] = CtrlInputItem(axis, name.c_str()); }
			/*
				Gets the location of an axis on the joystick from the given internal name

				@param name The internal name to look up
			*/
            int GetAxisLoc(std::string name) { return myAxisLookups[name].Location - 1; }

			/*
				Adds a new button to this controller mapping with the given lookup name

				@param name   The internal name for this button
				@param button The mapping item to assign to this internal name
			*/
            void AddButtonLookup(std::string name, const CtrlInputItem button) { myButtonLookups[name] = button; }
			/*
				Adds a new button to this controller mapping with the given lookup name

				@param name         The internal name for this button
				@param button       The location (ID) of the axis to add
				@param readableName The human-readable name for the axis (EX: "Left Stick")
			*/
            void AddButtonLookup(std::string name, const int button, const char* readableName) { myButtonLookups[name] = CtrlInputItem(button, readableName); }
			/*
				Adds a new button to this controller mapping with the given lookup name. This will set the readable name
				to the internal name

				@param name         The internal name for this button
				@param button       The location (ID) of the axis to add
			*/
            void AddButtonLookup(std::string name, const int button) { myButtonLookups[name] = CtrlInputItem(button, name.c_str()); }
			/*
				Gets the joystick button location for the given lookup name

				@param name The internal name of the button to get
			*/
            int GetButtonLoc(std::string name) { return myButtonLookups[name].Location -1; }

			/*
				Prints this mapping to a given output stream

				@param stream The stream to output to
			*/
            void Print(std::ostream& stream);

			/*
				Looks up a list of known mappings and returns the first one found. If none are found, will

				@param name The name of the mapping to search more
			*/
            static const ControllerMapping& GetKnownMapping(const char* name);


        private:
			// Stores a map of axis and button inputs 
            std::map<std::string, CtrlInputItem> myAxisLookups;
            std::map<std::string, CtrlInputItem> myButtonLookups;

            const char* myName;

			// Internal static for registering new controller mapping types
            static void __AddMap(const char* name, const ControllerMapping newMapping);

			// Stores all the registered controller mappings
            static std::map<std::string, ControllerMapping> myKnownMaps;
    };

}}}

#endif // INPUTMAPPING_H
