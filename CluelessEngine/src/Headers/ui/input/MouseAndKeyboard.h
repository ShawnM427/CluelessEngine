#pragma once
#ifndef INPUT_MOUSE_AND_KB_H
#define INPUT_MOUSE_AND_KB_H

#include "InputSources.h"

namespace clueless { namespace ui{ namespace input {

	/*
	Represents a button input source that get's it's state from the keyboard
	*/
	struct KeyboardButtonSource : ButtonInputSource {
		/*
		Create a new keyboard source for the given keyboard key

		@param key The keyboard key to watch for

		@see       http://www.glfw.org/docs/latest/group__keys.html
		*/
		KeyboardButtonSource(int key) : myKey(key) {}

	protected:
		/*
		Actually gets the button state from the input manager

		@returns ButtonPressed when the key is first pressed, ButtonHeld if it is being held, and ButtonReleased if released
		*/
		ButtonState __GetInput() {
			return InputManager::GetKeyState(myKey);
		}

		// Stores the keyboard key to watch, see http://www.glfw.org/docs/latest/group__keys.html
		int myKey;
	};

	/*
	Represents an axial input source that maps a key to an axial input
	*/
	struct KeyboardAxisSource : AxisInputSource {
		/*
		Create a new axis source that watches a keyboard key

		@param key       The GLFW key to watch for
		@param lowValue  The value to return when the key is released
		@param highValue The value to return when the key is pressed

		@see             http://www.glfw.org/docs/latest/group__keys.html
		*/
		KeyboardAxisSource(int key, float lowValue = 0.0f, float highValue = 1.0f) :
			myKey(key), myLowValue(lowValue), myHighValue(highValue) {}

	protected:
		/*
		Actually gets the axial state from the button press

		@returns highValue when the key is pressed, lowValue when not
		*/
		float __GetInput() {
			return InputManager::GetKeyState(myKey) == ButtonReleased ? myLowValue : myHighValue;
		}

		/* Stores the keyboard key to watch, see http://www.glfw.org/docs/latest/group__keys.html */
		int myKey;

		// Stores the high and low value
		float myLowValue;
		float myHighValue;
	};

	struct MouseDeltaXAxisSource : AxisInputSource {
		float __GetInput() {
			myAccumulator *= 0.9f;
			myAccumulator += (float)InputManager::GetMouseDelta().x;
			return myAccumulator;
		}
		private:
			float myAccumulator;
	};

	struct MouseDeltaYAxisSource : AxisInputSource {
		float __GetInput() {
			myAccumulator *= 0.9f;
			myAccumulator += (float)InputManager::GetMouseDelta().y;
			return myAccumulator;
		}
		private:
			float myAccumulator;
	};

	struct MouseButtonSource : ButtonInputSource {
		MouseButtonSource(int button) : myButton(button) {}
		ButtonState __GetInput() {
			return InputManager::GetMouseButton(myButton);
		}

	protected:
		int myJoystick;
		int myButton;
	};

} } }

#endif