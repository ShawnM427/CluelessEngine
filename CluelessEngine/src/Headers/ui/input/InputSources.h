/*
	Provides an abstraction layer upon input devices for seperating input logic from gameplay logic

	@date September 09th, 2017
*/
#pragma once

#include "ui/input/InputManager.h"
#include "memory"

namespace clueless { namespace ui { namespace input {

	/*
		Represents an input source that works on an axis range
	*/
	struct AxisInputSource {
		/*
			Gets or sets whether this control is enabled
		*/
		bool Enabled = true;

		/*
			Gets the axial input from this input source, as long as it is enabled.
			If it is not enabled, will return 0.0f

			@returns The axial input, or 0.0 if disabled
		*/
		float GetInput() {
			return Enabled ? __GetInput() : 0.0f;
		}

		protected:
			/*
				Virtual method for children classes to implement for actually getting the axial
				input. MUST be overriden in child classes.
				@returns The axial input
			*/
			virtual float __GetInput() = 0;
	};

	/*
		Represents an input source that produces button states
	*/
	struct ButtonInputSource {
		/*
		Gets or sets whether this control is enabled
		*/
		bool Enabled = true;

		/*
			Gets the button state input from this input source, as long as it is enabled.
			If it is not enabled, will return ButtonReleased

			@returns The button state, or ButtonReleased if disabled
		*/
		ButtonState GetInput() {
			return Enabled ? __GetInput() : ButtonReleased;
		}

		protected:
			/*
				Virtual method for children classes to implement for actually getting the state
				input. MUST be overriden in child classes.
				@returns The button state
			*/
			virtual ButtonState __GetInput() = 0;
	};


	/*
		Represents an axial input that can wrap other axial inputs to provide support for range normalization.
		For instance, we can use this to wrap a -1.0 - 1.0 range on a joystick axis to 0.0 - 1.0
	*/
	struct NormalizedAxisSource : AxisInputSource {
		/*
			Create a new normalized axis source around the given input

			@param input     The source input to wrap around
			@param sourceMin The minimum value the source input can have (for xBox axes it's -1.0)
			@param sourceMax The maximum value the source input can have (for xBox axis it's 1.0)
			@param outMin    The minimum value for the mapped output
			@param outMax    The maximum value for the mapped output
		*/
		NormalizedAxisSource(AxisInputSource* input, float sourceMin = -1.0f, float sourceMax = 1.0f, float outMin = 0.0f, float outMax = 1.0f) :
			myInput(input), mySourceMin(sourceMin), mySourceMax(sourceMax), myOutMin(outMin), myOutMax(outMax) {}


		protected:
			/*
				Actually performs the input range mapping

				@returns The axial value as returned mapped from inRange to outRange
			*/
			float __GetInput() {
				return myOutMin + ((myOutMax - myOutMin) / (mySourceMax - mySourceMin)) * (myInput->GetInput() - mySourceMin);
			}

			// Stores a pointer to the axial source
			AxisInputSource* myInput;

			// Stores the mins and maxes for range mapping
			float mySourceMin, mySourceMax;
			float myOutMin, myOutMax;
	};
} } }

#include "Manipulation.h"
#include "Joysticks.h"
#include "MouseAndKeyboard.h"