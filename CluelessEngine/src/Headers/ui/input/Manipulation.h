/*
	Provides manipulation tools for input sources

	@date September 09th, 2017
*/

#pragma once
#ifndef INPUT_MANIP_H
#define INPUT_MANIP_H

#include "InputSources.h"

namespace clueless { namespace ui { namespace input {

	/*
	 Maps a button input to an axial input source
	*/
	struct ButtonToAxis : AxisInputSource {
		/*
			Maps a button input to an axis
			@param input The input source to create a trigger for
			@param highValue The value when the button is pressed
			@param lowValue The value when the button is released
		*/
		ButtonToAxis(ButtonInputSource* input, float highValue = 1.0f, float lowValue = 0.0f) :
			myInput(input), myHighValue(highValue), myLowValue(lowValue) {}


	protected:
		/*
			Polls the input source for a state, then maps that state to either lowValue or highValue

			@returns lowValue when released, highValue when pressed or held;
		*/
		float __GetInput() {
			return myInput->GetInput() == ButtonReleased ? myLowValue : myHighValue;
		}

		// We expect whatever gave us the input 
		ButtonInputSource* myInput;

		// The values for when the button is pressed or released
		float myLowValue,  myHighValue;
	};

	/*
		Maps an axis input to a button trigger
	*/
	struct AxisToButton : ButtonInputSource {
		/*
			Maps an axis input to a button trigger

			@param input The input source to create a trigger for
			@param minValue The minimum value to trigger on
			@param maxValue The maximum value to trigger on
		*/
		AxisToButton(AxisInputSource* input, float minValue, float maxValue = 1.0f) :
			myInput(input), myMinValue(minValue), myMaxValue(maxValue), myLastState(ButtonReleased) {}


		protected:
			/*
				Handles polling the input source and calculating the button state

				@returns ButtonPressed when axis first passes threshold (positive), ButtonHeld when above the threshold, and ButtonReleased when below
			*/
			ButtonState __GetInput() {
				float value = myInput->GetInput();
				if (value >= myMinValue && value <= myMaxValue)
					myLastState = myLastState == ButtonReleased ? ButtonPressed : ButtonHeld;
				else
					myLastState = ButtonReleased;

				return myLastState;
			}
			// We expect the pointer to be managed by the caller
			AxisInputSource* myInput;

			// States and ranges
			ButtonState myLastState;
			float myMinValue, myMaxValue;
	};

	/*
		Input class that can be used to accumulate multiple input axis into one,
		with support for weights

		Note: This accumulator expects all input sources to be managed and destroyed externally
	*/
	struct AxisAccumulator : AxisInputSource {
		/*
			Create a new Axial Accumulator from a list of inputs. This WILL NOT be a weighted system

			@param inputs   An array of pointers to Axis Input Sources
			@param count    The number of inputs in the array to use (starting from 0)
			@param minValue The minimum value for the output
			@param maxValue The maximum vlaue for the output
		*/
		AxisAccumulator(AxisInputSource** inputs, int count, float minValue = -1.0f, float maxValue = 1.0f) {
			myInputs = inputs;
			myInputCount = count;
			myWeights = new float[count];
			std::fill(myWeights, myWeights + count, 1.0f);
			myTotalWeight = count;

			myMinValue = minValue;
			myMaxValue = maxValue;
		}

		/*
			Create a new Axial Accumulator from a list of inputs, each one with it's own weight. Negative weights are allowed, but if the sum of all weights
			is 0, will cause the weights to not be correctly caluclated (will default to totalWeight = 1, meaning all inputs will map to their range * their weight)

			Note: Minimum and maximum outputs are hard values, and the calculated output will be clamped to these values, please keep this in mind

			@param inputs   An array of pointers to Axis Input Sources
			@param count    The number of inputs in the array to use (starting from 0)
			@param minValue The minimum value for the output
			@param maxValue The maximum vlaue for the output
		*/
		AxisAccumulator(AxisInputSource** inputs, float* weights, int count, float minValue = -1.0f, float maxValue = 1.0f) {
			myInputs = inputs;
			myInputCount = count;
			myWeights = new float[count];
			memcpy(myWeights, weights, count * sizeof(float));
			myTotalWeight = 0.0f;
			for (int ix = 0; ix < count; ix++)
				myTotalWeight += myWeights[ix];

			if (myTotalWeight == 0.0f)
				myTotalWeight = 1.0f;

			myMinValue = minValue;
			myMaxValue = maxValue;
		}

		protected:
			/*
				Actually performs the polling and calculates the output

				@returns The weighted sum of all the input sources
			*/
			float __GetInput() {
				float sum = 0;
				for (int ix = 0; ix < myInputCount; ix++) {
					sum += myInputs[ix]->GetInput() * myWeights[ix] / myTotalWeight;
				}
				sum = sum < myMinValue ? myMinValue : (sum > myMaxValue ? myMaxValue : sum);
				return sum;
			}

			// Stores the arrays and utilities for the arrays
			AxisInputSource** myInputs;
			float*		      myWeights;
			int				  myInputCount;
			float			  myTotalWeight;

			// Stores min and max for clamping
			float             myMinValue;
			float             myMaxValue;
	};

} } }

#endif