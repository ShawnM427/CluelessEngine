#pragma once

#include "graphics/Texture.h"
#include "TextureFont.h"

#include <stb_truetype.h>

namespace clueless { namespace ui {
	
	class TrueTypeFont : public TextureFont {
		public:
			TrueTypeFont(const char* fileName, uint32_t size);
			~TrueTypeFont();

			GlyphInfo GetGlyph(int codePoint, float offsetX, float offsetY) const;
			int  GetKerning(int char1, int char2) const;
			int  GetLineHeight() const;

			void Unload();

		private:
			const uint32_t ATLAS_WIDTH = 1024;
			const uint32_t ATLAS_HEIGHT = 1024;
			const uint32_t FONT_OVERSAMPLE_X = 2;
			const uint32_t FONT_OVERSAMPLE_Y = 2;
			const uint32_t FIRST_CHAR = ' ';
			const uint32_t CHAR_COUNT = '~' - ' ';

			stbtt_packedchar* myCharInfo;
			unsigned char*    myFontData;
			uint32_t          myFontSize;
			stbtt_fontinfo    myFontInfo;
			float             myPixelHeightScale;
			float             myEmToPixel;
			int               myAscent, 
				              myDescent, 
				              myLineGap;
	}; 
} }