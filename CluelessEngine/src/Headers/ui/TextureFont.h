#pragma once

#include "graphics/Texture.h"
#include "glm_math.h"
#include "cms/Structures.h"

namespace clueless { namespace ui {

	struct GlyphInfo {
		glm::vec3 Positions[4];
		glm::vec2 UVs[4];
		float OffsetX, OffsetY;
	};

	class TextureFont : 
		public IResource {
		public:
			virtual GlyphInfo GetGlyph(int codePoint, float offsetX, float offsetY) const = 0;
			virtual int  GetKerning(int char1, int char2) const = 0;
			virtual int  GetLineHeight() const = 0;

			virtual clueless::graphics::Texture* GetTexture() const { return myTexture;  }

		protected:
			clueless::graphics::Texture *myTexture;
	};

} }