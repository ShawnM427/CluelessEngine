
#include "game_logic/GameSystem.h"
#include "game_logic/puzzles/Circuits.h"
#include "game_logic/GameObject.h"
#include <imgui/imgui.h>
#include <imgui/imgui_impl.h>
#include <imgui/imgui_internal.h>
#include "Event.h"
#include "audio/SoundInstance.h"
#include <map>


namespace clueless {
	namespace game_logic {
		namespace systems {

			using namespace audio;
			class ImGuiPuzzleSystem:public GameSystem
			{

			private:
				Circuits * puzzle = new Circuits();

			public:
				std::map<std::string, SoundInstance*> *soundSet;

				static bool result;
				static std::map<std::string, GameObject *> myObjects;
				SoundInstance* sound = nullptr;
				SoundInstance* alarmSound = nullptr;
				Transform trans;
				static bool openPuzzle;
				static bool switch1;

				ImColor color = ImColor(255, 0, 0, 255);
				static bool switch2;
				static bool switch3;
				static bool switch4;
				static bool switch5;

				static int puzzleNum;
				void puzzle1(ImDrawList* drawList, ImRect bb);
				void puzzle2(ImDrawList* drawList, ImRect bb);
				void puzzle3(ImDrawList* drawList, ImRect bb);

	


				
				ImGuiPuzzleSystem();
				~ImGuiPuzzleSystem();



				void Init();
				void FixedUpdate(GameObject * object);
				void AddGameObject(std::string name, GameObject* object);
				void PrePhysics();
				void PostRender();
				void Reset();



			};




		}
	}
}
