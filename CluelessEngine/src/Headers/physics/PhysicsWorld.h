#pragma once

#include "btBulletDynamicsCommon.h"
#include "RigidBody.h"
#include "glm_math.h"

#define COL_PLAYER 1
#define COL_BALL   2
#define COL_MONKEY 3
#define COL_INTERACTIVE 4
#define COL_NONE 5
#define COL_TOOL 6
#define COL_TOOL_USE 7
#define COL_DEBRIS 8

namespace clueless { namespace physics {
	
	class PhysicsWorld {
		public:
			friend class RigidBody;

			PhysicsWorld();
			~PhysicsWorld();

			void AddRigidBody(RigidBody *body);
			void RemoveRigidBody(RigidBody *body);

			void DebugDraw();

			void SetGravity(glm::vec3 value);
			void SetGravity(float x, float y, float z);

			btDynamicsWorld *GetInternalWorld() const { return myWorld; }

			void Update();

		private:
			btDynamicsWorld *myWorld;
			btDefaultCollisionConfiguration *myCollisionConfig;
			btCollisionDispatcher *myDispatcher;
			btBroadphaseInterface *myBroadphase;
			btSequentialImpulseConstraintSolver *mySolver;
	};

} }