
#pragma once
#ifndef uint
	typedef unsigned int  uint;
#endif
typedef unsigned char uchar;
typedef unsigned long ulong;

#define M_PI    3.141592653589
#define M_PI_2  3.141592653589 / 2.0
#define M_PI_4  3.141592653589 / 4.0

#define M_PI_F  3.141592653589f
#define M_PI_2F 3.141592653589f / 2.0f
#define M_PI_4F 3.141592653589f / 4.0f