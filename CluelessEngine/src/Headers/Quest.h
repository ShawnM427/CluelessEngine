
#include <string>
#include <vector>
#include "Task.h"
#include "Event.h"




enum QuestLevel
{
	MAINQUEST,
	SIDEQUEST,
	EVENT

};

class Quest{

private:
	//quest details
	std::string id;
	std::vector<std::string> requirements;
	std::string myTitle;
	std::string myDetails;
	std::string myCompleteDetails;
	QuestLevel myQuestLevel;

	//quest status
	bool myComplete = false;
	bool myActive = false;
	bool myVisible = false;

	//quests things to do
	std::vector<Task*> myTasks;
	//quest listener
	void __CompleteQuest();
public:
	Quest();
	Quest(std::string title,std::string details,std::string completeDetails,
		QuestLevel questImportance);
	~Quest();
	void AddTask(Task* task);
	std::vector<Task*> getTasks();
	void SetId(std::string id);
	void setTitle(std::string title);
	std::string getTitle();
	std::string getDetails();
	void setDetails(std::string details);
	std::string getId();
	void addPreReq(std::string pre);
	std::vector<std::string> getRequirements();
	void ActivateQuest();
	void MakeVisible();
	bool isVisible();
	bool isActive();
	bool IsCompleted();
	void Draw();
	void Unload();
	void Update();
	void HandleEvent( Event &message);
};