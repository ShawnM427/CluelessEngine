#pragma once

#include <string>
#include "glm_math.h"

#include "anim/MeshBone.h"
#include "anim/PoseProviders.h"
#include "anim/MeshSkeletonPose.h"
#include "graphics/SkinnedMesh.h"

namespace clueless { namespace anim {
	

	class MeshSkeleton {
	public:
		MeshSkeleton(MeshBone* rootBone);
		void Apply(const PoseProvider *poseSource);
		
		uint16_t GetJointCount() const { return myBoneCount; }
		PoseData* GetCurrentPoseTransforms() const { return myPose.Poses; }

		void ApplyTo(glm::mat4* dataSet);

	private:
		Pose                   myPose;
		MeshBone              *myRootBone;
		uint16_t               myBoneCount;
	};

} }