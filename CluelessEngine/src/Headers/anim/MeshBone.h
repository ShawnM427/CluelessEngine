#pragma once

#include <string>
#include "glm_math.h"

#include "anim/PoseData.h"

#include "graphics/SkinnedMesh.h"

namespace clueless { namespace anim {

	class MeshBone {
		public:
			std::string Name;

			MeshBone(const MeshBone&) = delete;
			MeshBone(uint16_t boneId);
			MeshBone(MeshBone* parent, uint16_t boneId);
			virtual ~MeshBone();

			void AddChild(MeshBone* child);

			void Update(glm::mat4 *finalTransforms, const glm::mat4 *localPoseTransforms) const;

			uint16_t GetTotalChildCount() const;

			void ApplyToDataSet(PoseData* poses, glm::mat4 *dataSet);

			// These are carefully controlled members, plz don't touch
			uint16_t   BoneId;
			int        ParentId;

			glm::vec3  OriginPos;
			glm::quat  OriginRot;
			float      OriginScale;

			glm::mat4  CalculatedToWorld;

			MeshBone **Children;
			uint16_t   ChildCount;
			
		private:
			MeshBone();

			void __CalcTotalBones(uint16_t& count);
	};

} }