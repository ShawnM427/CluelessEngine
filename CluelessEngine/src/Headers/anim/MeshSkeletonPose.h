#pragma once

#include <cstdint>
#include "glm_math.h"

#include "anim/PoseProviders.h"

namespace clueless { namespace anim {

	struct Pose {

		PoseData *Poses;
		uint16_t  JointCount;

		Pose();
		Pose(uint16_t numJoints);
		~Pose();

		void BlendWith(const PoseProvider *provider);
		void BlendWith(const Pose *pose);
		void ReplaceWith(const PoseProvider *provider);
		void InterpolateBetween(const PoseProvider *left, const PoseProvider *right, float tVal);
		void InterpolateBetween(const Pose *left, const Pose *right, float tVal);
		void Zero();
	};

} }