#pragma once

#include "glm_math.h"

namespace clueless { namespace anim {

		struct PoseData {

			glm::vec3 Origin;
			glm::quat Rotation;
			float     Length;

		    operator glm::mat4() const;

			PoseData operator +(const PoseData& right) const;
			PoseData operator -(const PoseData& right) const;

			PoseData& operator +=(const PoseData& right);
			PoseData& operator -=(const PoseData& right);
		};

	} }