/*
Authors:
Shaun McKinnon  - 100642799
Shawn Matthews  - 100412327
Daniel Munusami -
Paul Puig       -
Selina Daley    -
*/

#pragma once
#include <stdint.h>
#include <string>

#include "glm_math.h"
#include "tinyxml2.h"

#include "LinearMath/btVector3.h"
#include "LinearMath/btQuaternion.h"
#include "LinearMath/btTransform.h"

#include "Transform.h"

namespace clueless { namespace anim {

		enum AnimTypes {
			ANIM_LINEAR = 0,
			ANIM_CATMUL = 1,
			ANIM_BEZIER = 2,

			ANIM_POS_X = 0,
			ANIM_POS_Y = 1,
			ANIM_POS_Z = 2,
			ANIM_ROT_ROLL = 3,
			ANIM_ROT_PITCH = 4,
			ANIM_ROT_YAW = 5,
			ANIM_SCALE_X = 6,
			ANIM_SCALE_Y = 7,
			ANIM_SCALE_Z = 8
		};

		enum AnimBehaviour {
			ANIM_REST = 0,
			ANIM_REPEAT = 1
		};

		/*
		Represents an instances transformation data
		*/
		/*    REPLACE WITH TRANSFORM


		struct PosRotScale {
			glm::vec3 Position;
			glm::quat Rotation;
			glm::vec3 Scale = glm::vec3(1.0f);

			PosRotScale() {
				Position = glm::vec3(0.0f);
				Rotation = glm::quat();
				Scale = glm::vec3(1.0f);
			}

			PosRotScale(glm::vec3 position) {
				Position = position;
				Rotation = glm::quat();
				Scale = glm::vec3(1.0f);
			}

			glm::mat4 GetTransform() const {
				glm::mat4 result;
				result = glm::translate(result, Position);
				result *= glm::mat4_cast(Rotation);
				result = glm::scale(result, Scale);
				return result;
			}

			void CopyFrom(btTransform& source) {
				CopyFromBullet(Position, source.getOrigin());
				CopyFromBullet(Rotation, source.getRotation());
			}
		};
		*/
		
		/*
		Represents a value at a time for a given animation channel
		*/
		struct Keyframe {
			float   time = 0;
			float   value = 0;
			// Typecode?
			// float, quat, etc
		};

		/*
		Represents a single segment of an animation curve
		*/
		struct AnimSegment {
			/* Represents the starting keyframe */
			Keyframe start;
			/* Represents the ending keyframe */
			Keyframe end;
			/* Represents the control points for a spline */
			float controls[2];
			/*
			Represents the interpolation type for this segment.
			Should be one of:
			ANIM_LINEAR
			ANIM_CATMUL
			ANIM_BEZIER
			*/
			uint8_t type;

			/*
			Reads an Animation Segment from an XML element node
			@param node The node to read from
			@returns An animation segment loaded from the node
			*/
			static AnimSegment ReadXml(tinyxml2::XMLElement *node) {
				using namespace tinyxml2;

				// Prep the result
				AnimSegment result;

				// Read in the type attribute, by default it is linear (lerp)
				result.type = node->IntAttribute("type", ANIM_LINEAR);

				// Attempt to get the starting keyframe node
				XMLElement *startKf = node->FirstChildElement("Start");
				if (startKf) {
					// If it exists, grab the time and value (will default to 0 if not present)
					result.start.time = startKf->FloatAttribute("time");
					result.start.value = startKf->FloatAttribute("value");
				}

				// Attempt to get the ending keyframe
				XMLElement *endKf = node->FirstChildElement("End");
				if (endKf) {
					// If it exists, grab the time and value (will default to 0 if not present)
					result.end.time = endKf->FloatAttribute("time");
					result.end.value = endKf->FloatAttribute("value");
				}

				// Attempt to get the controls node
				XMLElement *controls = node->FirstChildElement("Controls");
				if (controls) {
					// If it exists, grab the two control points
					result.controls[0] = node->FloatAttribute("first", 0);
					result.controls[1] = node->FloatAttribute("second", 0);
				}

				// Return the populated result
				return result;
			}

			/*
			Writes this animation segment to an XML file
			@param doc The document to write to
			@param parent The parent element to write to
			*/
			void WriteToXml(tinyxml2::XMLDocument &doc, tinyxml2::XMLNode& parent) {
				using namespace tinyxml2;

				// Build the container element and set type
				XMLElement *myElement = doc.NewElement("Segment");
				myElement->SetAttribute("type", type);

				// Build the start keyframe node
				XMLElement *myStartKf = doc.NewElement("Start");
				myStartKf->SetAttribute("time", start.time);
				myStartKf->SetAttribute("value", start.value);

				// Build the stop keyframe node
				XMLElement *myEndKf = doc.NewElement("End");
				myEndKf->SetAttribute("time", end.time);
				myEndKf->SetAttribute("value", end.value);

				// Build the controls node
				XMLElement *myControls = doc.NewElement("Controls");
				myControls->SetAttribute("first", controls[0]);
				myControls->SetAttribute("second", controls[1]);

				// Append the components of the segment to the container
				myElement->InsertEndChild(myStartKf);
				myElement->InsertEndChild(myEndKf);
				myElement->InsertEndChild(myControls);

				// Append the container to the parent node
				parent.InsertEndChild(myElement);
			}
		};

		/*
		Represents a single channel of animation data. This works on a single stream of floats in an animated structure
		*/
		struct AnimChannel {
			/* Stores the individual curve segments */
			AnimSegment* segments = nullptr;
			/* Represents the number of segments in the curve */
			uint16_t     SegCount = 0;
			/* The offset in floats into the data structure to modify */
			uint8_t      Offset = 0;

			AnimChannel() : segments(nullptr), SegCount(0), Offset(0) {}
			AnimChannel(uint16_t segCount, uint8_t offset) : SegCount(segCount), Offset(offset) {
				segments = new AnimSegment[SegCount];
			}

			float GetLength() const {
				return SegCount > 0 ? segments[SegCount - 1].end.time : 0;
			}

			void Unload() {
				delete[] segments;
			}

			void Resolve(float* data, float time);

			/*
			Returns the animation segment for a given time step from the start of the animation
			@param time The time offset from the start of the animation
			@returns The animation segment at time, or nullptr if one is not present
			*/
			AnimSegment* SegmentAt(const float time) const {
				// If we have no segments, simply return
				if (segments == nullptr)
					return nullptr;

				// Iterate over our segments
				for (int ix = 0; ix < SegCount; ix++) {
					// If the time is within the segments time range, return that segment
					if (time >= segments[ix].start.time && time < segments[ix].end.time)
						return &segments[ix];
				}

				// No segment returned, so return a nullptr
				return nullptr;
			}

			/*
			Reads an animation channel from a given XML node
			@param element The XML element to retreive data from
			@returns An animation channel read from the XML node
			*/
			static AnimChannel ReadXml(tinyxml2::XMLElement *element) {
				using namespace tinyxml2;

				// Prep the result
				AnimChannel result;
				// Start by grabbing the offset, default is 0
				result.Offset = element->IntAttribute("offset", 0);

				// We need to count the number of children to allocate memory, so we'll need some fields
				int count = 0;
				XMLElement *child;

				// Now we can iterate over all segments and count them
				for (child = element->FirstChildElement("Segment"); child; child = child->NextSiblingElement("Segment"))
					count++;

				// Store the segment count
				result.SegCount = count;

				// Only grab data if we have anything
				if (count) {
					// Allocate the memory
					result.segments = new AnimSegment[count];

					count = 0;
					for (child = element->FirstChildElement("Segment"); child; child = child->NextSiblingElement("Segment")) {
						result.segments[count] = AnimSegment::ReadXml(child);
						count++;
					}
				}
				// Otherwise set the segment count to null
				else {
					result.segments = nullptr;
				}

				// Return the loaded result
				return result;
			}

			void WriteToXml(tinyxml2::XMLDocument &doc, tinyxml2::XMLNode& parent, const uint8_t offset) {
				using namespace tinyxml2;

				XMLElement *myElement = doc.NewElement("Channel");
				myElement->SetAttribute("offset", offset);

				for (int ix = 0; ix < SegCount; ix++) {
					segments[ix].WriteToXml(doc, *myElement);
				}

				parent.InsertEndChild(myElement);
			}
		};

		struct Animation {
			// pos.x,y,z rot.x,y,z scal.x,y,z
			AnimChannel channels[9];
			std::string Name;

			Animation() {
				for (int ix = 0; ix < 9; ix++) {
					channels[ix].Offset = ix;
				}
			}

			void WriteToXML(tinyxml2::XMLDocument &doc, tinyxml2::XMLNode& parent) {
				using namespace tinyxml2;

				XMLElement *body = doc.NewElement("Animation");
				body->SetAttribute("name", Name.c_str());

				for (int ix = 0; ix < 9; ix++) {
					channels[ix].WriteToXml(doc, *body, ix);
				}

				doc.InsertEndChild(body);
			}

			void Save(const std::string& fileName) {
				using namespace tinyxml2;

				XMLDocument doc;
				XMLDeclaration *decl = doc.NewDeclaration();
				decl->SetValue("xml version=\"1.0\" encoding=\"\"");
				doc.LinkEndChild(decl);

				WriteToXML(doc, doc);

				doc.SaveFile(fileName.c_str());
			}

			static Animation* Load(const std::string& fileName) {
				using namespace tinyxml2;

				XMLDocument doc;
				doc.LoadFile(fileName.c_str());

				XMLElement *root = doc.FirstChildElement("Animation");

				if (root) {
					Animation* result = new Animation();
					result->Name = root->Attribute("name");


					XMLElement *channel = root->FirstChildElement("Channel");
					int ix = 0;

					do {
						AnimChannel channelIn = AnimChannel::ReadXml(channel);
						result->channels[channelIn.Offset] = channelIn;

						ix++;
						channel = channel->NextSiblingElement("Channel");
					} while (channel != nullptr && ix < 9);

					return result;
				}
				else {
					return nullptr;
				}

			}
		};

		class Animator {

		public:
			// catmull-rom matrix
			static const glm::mat4 catmulRomMatrix;
			static const glm::mat4 bezierMatrix;

			/* LERP Methods */
			// p(t) = (1 - t)P1 + P2t
			static float Linear(float point1, float point2, float time) {
				return (1 - time) * point1 + point2 * time;
			}

			static float CatmulRom(float controls[2], float inVal, float outVal, float t) {
				glm::vec4 u_time = { (t * t * t), (t * t), t, 1 };
				glm::vec4 points = { controls[0], inVal, outVal, controls[1] };

				return glm::dot(points, catmulRomMatrix * u_time);
			}

			static float Bezier(float controls[2], float inVal, float outVal, float t) {
				glm::vec4 u_time = { (t * t * t), (t * t), t, 1 };
				glm::vec4 points = { inVal, controls[0], controls[1], outVal };

				return glm::dot(points, bezierMatrix * u_time);
			}

			static AnimSegment* MakeCatmulSet(Keyframe* keyFrames, int numFrames, int& segmentCount) {
				if (numFrames < 2)
					return nullptr;

				AnimSegment* results = new AnimSegment[numFrames - 1];
				segmentCount = numFrames - 1;

				for (int ix = 0; ix < segmentCount; ix++) {
					results[ix].start = keyFrames[ix];
					results[ix].end = keyFrames[ix + 1];
					results[ix].controls[0] = (ix == 0 ? keyFrames[0] : keyFrames[ix - 1]).value;
					results[ix].controls[1] = (ix + 2 < numFrames ? keyFrames[ix + 2] : keyFrames[numFrames - 1]).value;
					results[ix].type = ANIM_CATMUL;
				}

				return results;
			}

			static void Animate(Transform& data, Animation& anim, float time, AnimBehaviour behaviour = ANIM_REST) {
				for (int cx = 0; cx < 9; cx++) {
					if (anim.channels[cx].SegCount > 0) {
						if (behaviour == ANIM_REPEAT) {
							time = fmod(time, anim.channels[cx].GetLength());
						}
						
						float* dataPtr = reinterpret_cast<float*>(&data);

						anim.channels[cx].Resolve(dataPtr, time);
					}
				}
			}

		};
} }