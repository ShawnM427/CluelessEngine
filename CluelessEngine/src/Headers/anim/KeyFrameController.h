#pragma once
#include <vector>
#include <iostream>
#include "AnimationMath.h"
#include "glm_math.h"
#include <cmath>

template <typename T>
struct SpeedControlTableRow
{
	float uValue;
	T p0; 
	float pairwiseDistance;
	float distanceOnCurve;
};
template <typename T>
struct SpeedControlTableRowAdaptive
{
	float u0;
	float u1;
	float uMid;
	T P0;
	T Pmid;
	T P1;
	float dist1;
	float dist2;
	float dist3;
};

template<typename T>
class KeyFrameController
{
	protected:
		int currentKeyFrame;
		int nextKeyFrame;
		float localTime;
		float timeToPlay;
		std::vector<T>myKeyFrames;
		std::vector<float> myKeyTimes;
		std::vector< SpeedControlTableRow<T> > lookupTable;
		std::vector< SpeedControlTableRowAdaptive<T> > adaptiveLookupTable;


	public:
		float curvePlaySpeed;
		bool paused;
		bool doesSpeedControl;

		KeyFrameController()
		{
			currentKeyFrame = 0;
			nextKeyFrame = 1;
			localTime = 0.0f;

			doesSpeedControl = true;
		}
		KeyFrameController(float time, bool speedControl)
		{
			currentKeyFrame = 0;
			nextKeyFrame = 1;
			localTime = time;
			doesSpeedControl = speedControl;
		}

		void CalculateLookupTableAdaptiveSampling(float tolerance)
		{
			if (myKeyFrames.size()>3)
			{
				float uStart = 0.0f;
				float uEnd = 1.0f;
				float uMid = (uStart+uEnd) * 0.5;
				float sample;
				lookupTable.clear();

				for (int i = 0; i < myKeyFrames.size()-3; i++)
				{
					while (uStart!=0.750f)
					{
						SpeedControlTableRowAdaptive<T> row;

						T P0 = AnimationMath::bezier(myKeyFrames[i], myKeyFrames[i + 1],
							myKeyFrames[i+2], myKeyFrames[i + 3],uStart);
						T P1 = AnimationMath::bezier(myKeyFrames[i], myKeyFrames[i + 1],
							myKeyFrames[i + 2], myKeyFrames[i + 3], uMid);
						T P2= AnimationMath::bezier(myKeyFrames[i], myKeyFrames[i + 1],
							myKeyFrames[i + 2], myKeyFrames[i + 3], uEnd);
						float distance1 = glm::distance(P1, P0);
						float distance2 = glm::distance(P1, P2);
						float distance3 = glm::distance(P0, P2);

						row.u0 = uStart;
						row.u1 = uEnd;
						row.uMid = uMid;
						row.P0 = P0;
						row.Pmid = P1;
						row.P1 = P2;
						row.dist1 = distance1;
						row.dist2 = distance2;
						row.dist3 = distance3;
						sample = distance1 + distance2 - distance3;
						if (sample >tolerance)
						{
							adaptiveLookupTable.push_back(row);
							uEnd = uMid;
							uMid = (uStart + uEnd) * 0.5;

						}
						else
						{
							uStart = uEnd;
							if (uStart==0.5f)
							{
								uEnd = 1.0f;
							}
							else
							{
								uEnd = 1.0f * 0.5;
							}
							uMid = (uStart + uEnd)*0.5;

						}
					}
					
				}

			}

		}

		void CalculateLookupTable(int samples)
		{
			if (myKeyFrames.size()>3)
			{
				lookupTable.clear();
				float sampleRate = 1.0f / (float)samples;
				for (int i = 0; i < myKeyFrames.size()-3; i++)
				{
					for (float j = 0.0; j <= 1.0f; j+=sampleRate)
					{
						SpeedControlTableRow<T> row;
						row.uValue = j;
						//can be catmull rom,bezier
						row.p0 = AnimationMath::lerp(myKeyFrames[i], myKeyFrames[i + 1], j);
						//row.p0 = AnimationMath::bezier(myKeyFrames[i], myKeyFrames[i+1],
						//	myKeyFrames[i + 2], myKeyFrames[i + 3],j);
						lookupTable.push_back(row);
					}
				}

			}
			int size = lookupTable.size();
			if (size <= 0)
			{
				std::cout << "Table Not created" << std::endl;
				return;
			}
			lookupTable[0].pairwiseDistance = 0.0;
			lookupTable[0].distanceOnCurve = 0.0;

			for (int i = 1; i < size; i++)
			{

				lookupTable[i].pairwiseDistance = glm::distance(lookupTable.at(1).p0,lookupTable.at(i-1).p0);
				lookupTable[i].distanceOnCurve = lookupTable.at(i-1).distanceOnCurve + lookupTable[i].pairwiseDistance;
			}

			float totalCurveLength = lookupTable[size - 1].distanceOnCurve;// length of the path = distance the last sample is from the beginning
			for (int i = 1; i < size; i++)
			{
				lookupTable[i].pairwiseDistance= lookupTable[i].distanceOnCurve/ totalCurveLength;
			}
		}
		~KeyFrameController()
		{

		}

		T Update(float dt)
		{
			if (myKeyFrames.size()<2)
			{
				return T();
			}
			if (doesSpeedControl)
			{
				return speedControlledUpdate(dt);
			}
			return noSpeedControlUpdate(dt);
		}
		void AddKey(T key,float time)
		{
			myKeyFrames.push_back(key);
			myKeyTimes.push_back(time);
		}

		T speedControlledUpdate(float dt)
		{
			localTime += dt;

			UpdateSegments();
			float t = AnimationMath::invLerp(localTime, myKeyTimes[currentKeyFrame], myKeyTimes[nextKeyFrame]);
			for (int i = 1; i < lookupTable.size(); i++)
			{
				if (lookupTable[i].pairwiseDistance >= t)
				{
					float p0 = lookupTable[i - 1].pairwiseDistance;
					float p1 = lookupTable[i].pairwiseDistance;
					float u = AnimationMath::invLerp(t, p0, p1);

					T sample0 = lookupTable[i - 1].p0;
					T sample1 = lookupTable[i].p0;

					return AnimationMath::lerp(sample0, sample1, u);
				}
			}

				return T();

			
		}

			
		
		T noSpeedControlUpdate(float dt)
		{
			localTime += dt;

			UpdateSegments();

			T p0 = myKeyFrames[currentKeyFrame];
			T p1 = myKeyFrames[nextKeyFrame];

			return AnimationMath::lerp<T>(p0, p1, localTime);
		}
		void UpdateSegments()
		{
			if (localTime >= myKeyTimes[nextKeyFrame])
			{
				currentKeyFrame++;
				nextKeyFrame++;
				if (nextKeyFrame >= myKeyFrames.size())
				{
					localTime = 0;
					currentKeyFrame= 0;
					nextKeyFrame= 1;
				}

			}
		}

};