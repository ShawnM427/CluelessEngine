#include <typeinfo>
#include <cstdint>

template <typename TBaseType>
class TypeMap {
public:
	TypeMap() :
		myData(nullptr),
		myDataSize(0),
		myKeys(nullptr),
		myAllocKeyCount(0),
		myKeyCount(0),
		myUsedData(0) {
	}
	~TypeMap() {
		// Free the allocated memory block
		delete[] myKeys;
		free(myData);
	}

	/*
	Creates a new default instance for each of the given types and adds it to the map.

	This will fail compilation if any of the types does not have a defualt constructor
	*/
	template <typename ... Args>
	void DefaultConstruct() {
		// We don't really need size, but it's kina cool to see what we can do
		int size = 0;
		// Stores number of arguments
		int count = 0;

		// Runs a lambda with reference capture to modify count and size
		[&](...) {}((size += sizeof(Args), count++)...);

		// Reserve space for structs
		myData = realloc(myData, myDataSize + size);
		myDataSize += size;

		// Reserve space on the vector for the keys to avoid copies
		myKeys = (KeyPair*)realloc(myKeys, (myKeyCount + count) * sizeof(KeyPair));
		myAllocKeyCount = myKeyCount + count;

		// Add a new default constructed value of the argument type
		auto list = { (Add<Args>(Args()))... };
	}

	/*
	Adds a new instance of the given type to the map, this will return false if a component with the given
	type already exists
	*/
	template <typename T>
	bool Add(T value) {
		// Type safety check, will throw a COMPILE TIME exception! :D
		if (static_cast<TBaseType*>(&value)) {
			// Grab the typeinfo as a ptr (basically an int so free hashing! :D)
			const std::type_info *type = &typeid(T);
			
			// Get the pointer to the data stored in the key vector
			KeyPair* ptr = myKeys;
			// Get the pointer of the last element
			KeyPair* last = ptr + myKeyCount;
			for (; ptr < last; ptr++) {
				// Check to see if key exists
				if (ptr->Key == type) {
					// Type already exists
					return false;
				}
			}

			// Key does not exist

			// Re-allocate the data buffer, extend it by the size of T
			if (myUsedData + sizeof(T) > myDataSize) {
				myDataSize += sizeof(T);
				myData = realloc(myData, myDataSize);
			}

			// Copy the data in the value to the end of the new data buffer
			memcpy(reinterpret_cast<char*>(myData) + myUsedData, &value, sizeof(T));

			// If we haven't pre-allocated the key list, expand it now
			if (myKeyCount + 1 > myAllocKeyCount) {
				myKeys = (KeyPair*)realloc(myKeys, (myKeyCount + 1) * sizeof(KeyPair));
				myAllocKeyCount++;
			}

			// Add the new keypair to the vector
			myKeys[myKeyCount] = KeyPair(type, myUsedData);
			myKeyCount++;

			// Increment our data size
			myUsedData = myUsedData + sizeof(T);

			return true;
		}
		return false;
	}

	/*
	Gets the component from the system with the given type if it exists.
	Note that this pointer will be invalidated if a new component is added to the system
	*/
	template <typename T>
	T* Get() {
		// Grab type
		const std::type_info *type = &typeid(T);
		// Same idea as above
		KeyPair* ptr = myKeys;
		KeyPair* last = ptr + myKeyCount;
		for (; ptr < last; ptr++) {
			if (ptr->Key == type) {
				// Some MAAAGIC. 
				// Basically we start by adding the keypair's data offset to the data block pointer
				// Then we reinterpret this pointer as the typeof T
				return (reinterpret_cast<T*>(reinterpret_cast<char*>(myData) + ptr->BaseOffset));
			}
		}
		// Return a nullptr if no object of the type was found
		return nullptr;
	}

	/*
	Gets the number of elements in the map
	*/
	uint32_t Count() const { return myKeyCount; }
	/*
	Gets a pointer to the component at the given index. Note that the pointer will be invalidated when new components are added
	*/
	template<typename T>
	T* GetAt(const uint32_t index) {
		return (reinterpret_cast<T*>(reinterpret_cast<char*>(myData) + (myKeys + index)->BaseOffset));
	}

	TBaseType* GetAt(const uint32_t index) {
		return (reinterpret_cast<TBaseType*>(reinterpret_cast<char*>(myData) + (myKeys + index)->BaseOffset));
	}

private:
	// Represents the pointer to a single typed entry in the map
	struct KeyPair {
		// Stores the type that is stored
		const std::type_info* Key;
		// Stores the offset from the start of the data array
		uint32_t        BaseOffset;

		KeyPair(const std::type_info* key, uint32_t offset) :
			Key(key),
			BaseOffset(offset) {

		}
	};
	// Stores the data keys
	KeyPair* myKeys;
	uint32_t  myKeyCount;
	uint32_t  myAllocKeyCount;

	// Declares a generic block of memory as well as how many bytes it takes up
	void     *myData;
	uint32_t  myDataSize;
	uint32_t  myUsedData;
};