#include <typeinfo>
#include <cstdint>
#include <cstring>

template <typename T>
class HashedMap {
	public:
		HashedMap() :
			myData(nullptr),
			myDataSize(0),
			myKeys(nullptr),
			myAllocKeyCount(0),
			myKeyCount(0),
			myUsedData(0) {
		}
		~HashedMap() {
			// Free the allocated memory block
			delete[] myKeys;
			free(myData);
		}

		/*
			Adds an un-named item to this hash map and generates a new hash for it
		*/
		uint64_t Add(T value) {
			uint64_t hash = (uint64_t)&value;
			if (!Add(value, hash))
				hash = 0;
			return hash;
		}
		/*
			Adds a new instance to the map, using a string name to generate the hash from. This will return
			the hashcode that was generated
		*/
		uint64_t Add(T value, const char name[8]) {
			uint64_t hash = *reinterpret_cast<uint64_t*>(&name);
			if (!Add(value, hash))
				hash = 0;
			return hash;
		}

		bool Add(T value, uint64_t hash) {
			// Get the pointer to the data stored in the key vector
			KeyPair* ptr = myKeys;
			// Get the pointer of the last element
			KeyPair* last = ptr + myKeyCount;
			for (; ptr < last; ptr++) {
				// Check to see if key exists
				if (ptr->Key == hash) {
					// Type already exists
					return false;
				}
			}

			// Key does not exist

			// Re-allocate the data buffer, extend it by the size of T
			if (myUsedData + sizeof(T) > myDataSize) {
				myDataSize += sizeof(T);
				myData = realloc(myData, myDataSize);
			}

			// Copy the data in the value to the end of the new data buffer
			memcpy(reinterpret_cast<char*>(myData) + myUsedData, &value, sizeof(T));

			// If we haven't pre-allocated the key list, expand it now
			if (myKeyCount + 1 > myAllocKeyCount) {
				myKeys = (KeyPair*)realloc(myKeys, (myKeyCount + 1) * sizeof(KeyPair));
				myAllocKeyCount++;
			}

			// Add the new keypair to the vector
			myKeys[myKeyCount] = KeyPair(hash, myUsedData);
			myKeyCount++;

			// Increment our data size
			myUsedData = myUsedData + sizeof(T);

			return true;
		}

		T* Get(const char name[8]) {
			uint64_t hash = *reinterpret_cast<uint64_t*>(name);
			return Get(hash);
		}

		T* Get(uint64_t hash) {
			KeyPair* ptr = myKeys;
			KeyPair* last = ptr + myKeyCount;
			for (; ptr < last; ptr++) {
				if (ptr->Key == hash) {
					// Some MAAAGIC. 
					// Basically we start by adding the keypair's data offset to the data block pointer
					// Then we reinterpret this pointer as the typeof T
					return (reinterpret_cast<T*>(reinterpret_cast<char*>(myData) + ptr->BaseOffset));
				}
			}
			// Return a nullptr if no object of the type was found
			return nullptr;
		}

		/*
		Gets the number of elements in the map
		*/
		uint32_t Count() const { return myKeyCount; }

		T* GetAt(const uint32_t index) {
			return (reinterpret_cast<T*>(reinterpret_cast<char*>(myData) + (myKeys + index)->BaseOffset));
		}

	private:
		// Represents the pointer to a single typed entry in the map
		struct KeyPair {
			// Stores the type that is stored
			uint64_t   Key;
			// Stores the offset from the start of the data array
			uint32_t   BaseOffset;

			KeyPair(uint64_t key, uint32_t offset) :
				Key(key),
				BaseOffset(offset) {

			}
		};
		// Stores the data keys
		KeyPair* myKeys;
		uint32_t  myKeyCount;
		uint32_t  myAllocKeyCount;

		// Declares a generic block of memory as well as how many bytes it takes up
		void     *myData;
		uint32_t  myDataSize;
		uint32_t  myUsedData;
};