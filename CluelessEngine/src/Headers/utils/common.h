#pragma once
#ifndef UTILS_H_INCLUDED
#define UTILS_H_INCLUDED

#include <stdexcept>
#include <string>

#define STRINGIZE_HELPER(x) #x
#define STRINGIZE(x) STRINGIZE_HELPER(x)
#define WARNING(desc) message(__FILE__ "(" STRINGIZE(__LINE__) ") : Warning: " #desc)

namespace clueless {

    template <typename T> inline constexpr int sgn(T val) {
        return (T(0) < val) - (val < T(0));
    }

    /**
        Reads a file from the disk into a character buffer

        @param filename The name of the file to read
        @return The contents of the file

        @throw runtime_error Thrown when the file could not be opened
    */
    char* readFile(const char* filename);

	template <typename T>
	T* readFile(const char* filename) {
		char* data = readFile(filename);
		return reinterpret_cast<T*>(data);
	}

    /**
        Gets the path part of a filename

        @param filename The name of the file to get the path for
        @param pathLength The length of the resulting text
    */
    char* getFilePath(const char* filename, int &pathLength);

    char* getFileExtension(const char* path, int &length);
    char* getFileExtension(const char* path);

    class io_exception : public std::runtime_error
    {
        public:
            io_exception(const std::string& message = "A file access operation has failed") : runtime_error(message) {}
    };

    class glfw_exception : public std::runtime_error
    {
        public:
            glfw_exception(const  std::string& message = "A GLFW exception has occured") : runtime_error(message){}
    };

    class gl_exception : public std::runtime_error
    {
        public:
            gl_exception(const  std::string& message) : runtime_error(message){}
    };

    class user_error :
        public std::runtime_error
    {
        public:
            user_error(const std::string& message) : std::runtime_error(message) {}
    };

    class index_error :
        public std::range_error
    {
        public:
            index_error(const std::string& message = "The index was outside the bounds of the array") : std::range_error(message) {}
    };

    class EmptyStackError :
        public std::runtime_error
    {
        public:
            EmptyStackError(const std::string& message = "Stack is empty") : std::runtime_error(message) {}
    };

}

#endif // UTILS_H_INCLUDED
