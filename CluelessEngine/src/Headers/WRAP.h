#pragma once
#include "game_logic/GameSystem.h"
#include "game_logic/GameObject.h"
#include "graphics/Texture.h"
//#include <imgui/imgui.h>
//#include <imgui/imgui_impl.h>
//#include <imgui/imgui_internal.h>

namespace clueless {namespace game_logic {namespace systems {
	class WRAP:public GameSystem
	{

	public:
		WRAP();

		~WRAP();

		static bool open;
		static bool forceQuest;
		void Init();
		void PostRender();



	};
}}}