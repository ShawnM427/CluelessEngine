#pragma once

#include <string>
#include <map>
#include <vector>
#include <functional>

#include "Graphics/common.h"

#define CMS_UNKOWN        0x00
#define CMS_TYPE_TEXTURE  0x01
#define CMS_TYPE_MODEL    0x02
#define CMS_TYPE_SHADER   0x03
#define CMS_TYPE_CUBEMAP  0x04
#define CMS_TYPE_FREEMESH 0x05
#define CMS_TYPE_TEXT     0x06
#define CMS_TYPE_SOUND    0x07
#define CMS_TYPE_COLLIDER 0x08
#define CMS_TYPE_USER     0x09
#define CMS_TYPE_MATERIAL 0x0A
#define CMS_TYPE_FONT     0x0B
#define CMS_TYPE_MIN      CMS_TYPE_TEXTURE
#define CMS_TYPE_MAX      CMS_TYPE_FONT
#define CMS_TYPE_COUNT    CMS_TYPE_MAX - CMS_TYPE_MIN + 1

#define RESOURCE_VALID_TOKEN 0x07800910

#define tType(type) (type - CMS_TYPE_MIN)

enum CmsType {
	TypeUnknown  = CMS_UNKOWN,
	TypeTexture  = CMS_TYPE_TEXTURE,  // Implemented
	TypeModel    = CMS_TYPE_MODEL,
	TypeShader   = CMS_TYPE_SHADER,   // Implemented
	TypeCubeMap  = CMS_TYPE_CUBEMAP,
	TypeFreeMesh = CMS_TYPE_FREEMESH,
	TypeText     = CMS_TYPE_TEXT,
	TypeSound    = CMS_TYPE_SOUND,    // Implemented
	TypeCollider = CMS_TYPE_COLLIDER,     
	TypeMaterial = CMS_TYPE_MATERIAL,  // TODO
	TypeFont     = CMS_TYPE_FONT,      // TODO
	TypeUser     = CMS_TYPE_USER
};


class CMS;

struct IResource {
	virtual ~IResource() {}
	virtual void Unload() = 0;
private:
	uint32_t __ValidationToken = RESOURCE_VALID_TOKEN;
};

// Token that can be used to retreive content from the CMS
// These are safe to pass around as ref, they are immutable to anything besides the CMS
// If any asshole (especially you, Future Shawn) decides to direct memory access these things,
// or try to cache them, then you are an idiot

struct ContentToken {
	// Delete any other constructor, we only want it to be visible to the CMS

	ContentToken() : TypeID(TypeUnknown), ResourceID(0) {} ;
	//ContentToken(const ContentToken&&) = delete; // fuck you and your copying shit

	/* Gets the TypeCode for this token */
	CmsType GetType() const { return TypeID; }
	/* Gets whether the resource is loaded */
	bool GetIsLoaded() const;
	
	bool IsNull() const {
		return (TypeID == CmsType::TypeUnknown) & (ResourceID == 0);
	}

private:
	friend class CMS;

	// Private constructor, only visible to the CMS
	ContentToken(CmsType typeID, uint32_t resourceID) : TypeID(typeID), ResourceID(resourceID) { }

	CmsType  TypeID;
	uint32_t ResourceID;
};

//structure to store content
struct ContentTag
{
	// The name of the content, this will be unique to the content type
	std::string Name;
	// Stores definition of content from xml(example texture definition)
	void* MetaData;
	// Stores the actual pointer to the underlying resource
	void* Resource;

	ContentTag()
	{
		Name = "default";
		MetaData = nullptr;
		Resource = nullptr;
	}

	ContentTag(const std::string& name, void* metaData)
	{
		Name = name;
		MetaData = metaData;

	}
};

//stores the bundle
struct Bundle
{
	// Stores the name of the bundle
	std::string Name;

	// Stores a list of all the content items for the bundle
	std::map<std::string, ContentToken> Content;

	// Stores whether this bundle is loaded or not
	bool IsLoaded = false;

	Bundle(std::string bundleName)
	{
		Name = bundleName;
	}

};

struct CubeMapMeta {
	std::string NegXPath;
	std::string PosXPath;
	std::string NegYPath;
	std::string PosYPath;
	std::string NegZPath;
	std::string PosZPath;

	CubeMapMeta() {
		NegXPath = "";
		PosXPath = "";
		NegYPath = "";
		PosYPath = "";
		NegZPath = "";
		PosZPath = "";
	}
};

// Stores the settings needed to configure a texture loaded as a resource
struct TextureMeta
{
	std::string FileName;
	GLenum FilterMode;
	GLenum WrapMode;
	bool   EnableAnisotropy;

	TextureMeta() {

		FilterMode = GL_LINEAR;
		WrapMode = GL_CLAMP;
		EnableAnisotropy = false;
	}

	TextureMeta(std::string fileName, GLenum filterMode = GL_LINEAR, GLenum wrapMode = GL_CLAMP,
		bool enableAnisotropy = false)
	{
		FileName = fileName;
		FilterMode = filterMode;
		WrapMode = wrapMode;
		EnableAnisotropy = enableAnisotropy;
	}
};

struct MaterialMeta {
	const char *TextureNames[4];
	std::string ShaderName;
};

// Stores settings needed to load in a sound effect as a resource
struct SoundMeta {
	std::string FileName;
	float Pan;
	float Pitch;
	float Gain;

	SoundMeta() {

		Pan   = 0.0f;
		Pitch = 1.0f;
		Gain  = 1.0f;
	}

	SoundMeta(std::string fileName, float pan, float pitch, float gain)
	{
		FileName = fileName;
		Pan   = pan;
		Pitch = pitch;
		Gain  = gain;
	}

};

struct ShaderMeta {
	std::string VsFileName;
	std::string FsFileName;
	std::string GsFileName;

	std::string VsSource;
	std::string FsSource;
	std::string GsSource;

	ShaderMeta() {

	}

	ShaderMeta(const std::string& vsFileName, const std::string& fsFileName) {
		VsFileName = vsFileName;
		FsFileName = fsFileName;
	}
};

struct TrueTypeFontMeta {
	std::string FileName;
	uint32_t    Size;
};

struct ModelMeta {
	std::string SourceFile;
	std::string NodeSelector;

    ModelMeta() {
		NodeSelector = "root";
	}

	ModelMeta(const std::string& sourceFile, const std::string nodeSelector) {
		SourceFile = sourceFile;
		NodeSelector = nodeSelector;
	}
};

struct TextMeta {
	std::string SourcePath;
	const char* Text;

	TextMeta() {

		Text = nullptr;
	}
};

/*
	Defines a generic use metadata that provides a function for loading and unloading a resource
	information from an inner metadata.
*/
struct UserMeta {
	std::function<void*(void*)> LoadContent;
	std::function<void*(void*)> UnloadContent;
	void*                       MetaData;

	UserMeta() {
		LoadContent = std::bind(&UserMeta::NoOp, this, std::placeholders::_1);
		UnloadContent = std::bind(&UserMeta::NoOp, this, std::placeholders::_1);
		MetaData = nullptr;
	}

	UserMeta(std::function<void*(void*)> loadFunc, std::function<void*(void*)> unloadFunc, void* metaData) {
		LoadContent = loadFunc;
		UnloadContent = unloadFunc;
		MetaData = metaData;
	}

	private:
		void* NoOp(void* meta) {
			return nullptr;
		}
};