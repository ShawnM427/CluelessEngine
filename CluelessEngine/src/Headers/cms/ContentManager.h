#pragma once
#include <iostream>
#include <string.h>

//base class for content management system
class ContentManager
{
public:
	/*Used to manage content from all systems
	*/
	ContentManager();

	/*function to create create a content 
	 @return bool of result if content is created or not*/
	virtual bool Create()=0;

	/* used to check is content is already created, so to not load it again
	@params names checks to see if anything with name is defined
	*/
	virtual bool Find(const std::string name) = 0;
	/* unloads content
	  @params name: delete content with name 
	*/
	virtual bool Del(const std::string name) = 0;

	//destructor for content manager
	~ContentManager();
};