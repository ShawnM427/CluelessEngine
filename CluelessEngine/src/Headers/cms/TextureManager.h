#pragma once
#include "ContentManager.h"
#include "graphics/Texture.h"
#include <iostream>
#include <string>
#include <map>

//handles all textures in game
class TextureManager:public ContentManager
{
public:

	//constructor to assign amount of textures needed to store
	TextureManager();
	
	//default create function,not implemented yet
	bool Create();

	/*overided Create function, that creates a texture
	@params t1: TextureDefinition struct passed to define a texture
	@params name: texture name, used to store in map so we dont redefined textures
	@returns A bool result stating if the texture was created or not
	*/
	virtual bool Create(clueless::graphics::TextureDefinition t1, const std::string name) ;

	/*overided Create function, that creates a texture stores it in the manger
	@params fileName:  char pointer for a Name of the file trying to open, for a texture
	@params filterMode: 
	@params wrapMode:
	@params name: Name for the texture, so we dont duplicate the same texture
	@returns A bool result stating if the texture was created or not
	*/
	virtual bool Create(const char *fileName, GLenum filterMode, 
		GLenum wrapMode,  const std::string name);

	/*used to get a texture already stored in the class 
	@params name: The name of the texture to get stored in manger
	@returns a texture, either the found one or the default texture.
	*/
	clueless::graphics::Texture* GetTexture(const std::string name);

	/* used to check if content is already created, so to not load it again
	@params name= checks to see if anything with a name is created 
	@returns bool result if the texture is found or not
	*/
	virtual bool Find(const std::string name);
	/* unloads content
	@params name: deletes whatever texture has the name 
	@returns bool result if content is unloaded or not
	*/
	virtual bool Del(const std::string name);
	~TextureManager();

private:
	std::map<std::string,clueless::graphics::Texture*> textures;
	unsigned int totalTextures = 0;
	unsigned int textureIndex = 0;

};