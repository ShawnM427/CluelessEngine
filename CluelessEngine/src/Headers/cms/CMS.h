#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "audio/SoundManager.h"
#include "tinyxml2.h"
#include <map>
#include <type_traits>
#include "Structures.h"

typedef ContentTag* ContentTagPtr;

class CMS {

private:
	// Only the CMS can manipulate tokens
	friend struct ContentToken;

	static ContentToken NullToken;

	// We should not be able to create an instance of the CMS
	CMS() = delete;
	
	// Stores all allocated tokens
	static std::vector<ContentToken> myTokens;

	// Stores the content collections and their counts
	static ContentTagPtr myContent[CMS_TYPE_COUNT];
	static uint32_t      myContentCounts[CMS_TYPE_COUNT];

	// Stores all of the bundles by name
	static std::map<std::string, Bundle*> myBundles;

	//util function to lower case input
	static std::string lowerType(std::string type);

	// Internal functions for loading and unloading
	static void __LoadResource(const ContentToken& token);
	static void __UnloadResource(const ContentToken& token);
	
	// XML utility functions
	static void xmlTextureRead(Bundle& bundle, tinyxml2::XMLElement *document);
	static void xmlSoundLoad(Bundle& bundle, tinyxml2::XMLElement *document);
	static void xmlShaderLoad(Bundle& bundle, tinyxml2::XMLElement *document);
	static void xmlCubeMapLoad(Bundle& bundle, tinyxml2::XMLElement *document);
	static void xmlModelLoad(Bundle& bundle, tinyxml2::XMLElement *document);
	static void xmlTextLoad(Bundle& bundle, tinyxml2::XMLElement *document);
	static void xmlMaterialLoad(Bundle& bundle, tinyxml2::XMLElement *document);
	static void xmlFontLoad(Bundle& bundle, tinyxml2::XMLElement *document);

public:	
	//Initalizises any resources needed for cms
	static void Init();
	//unloads resources by cms
	static void Unload();

	/*
		Gets a token for a resource with the given type and resource name
		@param type The CMS typecode for the resource to fetch
		@param name The name of the resource to fetch
		@returns A token for fetching the actual resource, or a nullptr if no resource is found
	*/
	static ContentToken& GetToken(CmsType type, const std::string& name);

	/*
		Redeems a token to get a resource, this will load the resource if required.
		The token will remain valid after use. You can hold onto the token to check
		if a redeemed resource has been invalidated or unloaded.
		@param token The token to redeem
		@typename T The type of resource to return, should be based on the type of the token
	*/
	template <typename T>
		static T* Redeem(const ContentToken& token);

	/*
		Performs both a token lookup and a token redeem, which is slow and fairly unsafe.
		Useful when in a quick pinch though.
	*/
	template <typename T>
		static T* RedeemDirect(CmsType type, const std::string& name);		

	/*
		Force unloads a single resource with the given token
		@param token The token for the resource to unload
	*/
	static void Unload(const ContentToken& token);

	/*
		Force loads a single resource with the given token
		@param token The token for the resource to load
	*/
	static void ForceLoad(const ContentToken& token);

	/*
		Register a new resource to the CMS. MetaData is optional, but *should* be provided wherever available. If the 
		type is TypeUser, then the metaData should be of type 
		@param type The typecode of the content to register
		@param name The name of the resource to register, must be unique to the CmsType
		@param resource The resource to register
		@param metaData The data required to reconstruct the resource
		@returns A new token that points to the content
	*/
	static ContentToken Register(CmsType type, const std::string& name, void* resource, void* metaData = nullptr);

	/*
		Loads a CMS manifest file from an XML document
		@param fileName The name of the file to load
		@returns True if the file was loaded, false if otherwise
	*/
	static bool LoadManifest(const std::string& fileName);

	/*
		Loads all of the resources in a bundle into memory
		@param bundleName The name of the bundle to load resources for
	*/
	static void LoadBundle(const std::string& bundleName);

	/*
		Unloads a bundle of resources from memory
		@param bundleName pointer to const string for the bundle name to unload
	*/
	static void UnloadBundle(const std::string& bundleName);

};

template<typename T>
T* CMS::Redeem(const ContentToken& token) {
	if (token.IsNull())
		return nullptr;

	if (!token.GetIsLoaded())
		__LoadResource(token);

	return reinterpret_cast<T*>(myContent[tType(token.TypeID)][token.ResourceID].Resource);
}

template<typename T>
T * CMS::RedeemDirect(CmsType type, const std::string& name) {
	ContentToken &token = GetToken(type, name);
	
	if (!token.IsNull()) {
		return Redeem<T>(token);
	}
	else
		return nullptr;
}