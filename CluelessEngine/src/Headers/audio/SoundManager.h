#ifndef SOUNDMANAGER_H
#define SOUNDMANAGER_H

#include <vector>
#include <string>

#include <gorilla/ga.h>
#include <gorilla/gau.h>

#include <audio/SoundEffect.h>
#include <audio/SoundInstance.h>

namespace clueless { namespace audio {

	/*
		Handles all the audio and sound effects
	*/
    class SoundManager
    {
        public:
			/* Initializes the sound manager */
            static void Init();

			/* 
				Registers a new sound effect with the audio manager
				@param sound The sound effect to register
			*/
            static void Add(SoundEffect* sound);
			/*
				Gets the sound effect with the given name
				@param name The name of the sound effect to get
				@returns    The pointer to the sound effect instance 
			*/
            static SoundEffect* Get(const std::string name); 
			/*
				Gets a new sound effect instance for the given name
				@param name The name of the sound effect to create
				@returns    A sound effect instance to play
			*/
            static SoundInstance* GetInstance(const std::string name);

			/* Updates the audio manager and all the sound instances */
            static void Update();
			/* Handles cleaning up the sound resources */
            static void Cleanup(); 

        private:
			// We have friends!
            friend class SoundEffect;
            friend class SoundInstance;

			// We cannot create new instances publicly
            SoundManager() {}

			// Stores a list of all the sounds
            static std::vector<SoundEffect*> __mySounds;

			// Internal Gorilla Audio handles
            static gau_Manager *__myManager;
            static ga_Mixer    *__myMixer;
    };

} }

#endif // SOUNDMANAGER_H
