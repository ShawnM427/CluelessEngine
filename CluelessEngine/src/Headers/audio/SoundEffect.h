#pragma once
#ifndef SOUNDEFFECT_H
#define SOUNDEFFECT_H

#include <string>

#include <gorilla/ga.h>
#include <gorilla/gau.h>

namespace clueless { namespace audio { class SoundEffect; } }

#include <audio/SoundInstance.h>

namespace clueless { namespace audio {

	/*
		Represents a sound effect that can be used to generate effect instances
	*/
    class SoundEffect
    {
        public:
			/*
				Loads a sound effect from a file
				@param name     The internal name of the sound effect to use
				@param fileName The name of the file to load the sound effect from
			*/
            SoundEffect(const std::string name, const std::string fileName);
			// Delete the copy constructor
			SoundEffect(const SoundEffect& other) = delete;

			/*
				Handles unloading this sound effect
			*/
			void Unload();

			/*
				Set the gain for all new instances created for this sound
				@param value The new gain for the sound effect
			*/
            void SetGain(const float value);
			/*
				Set the pitch for all new instances created for this sound
				@param value The new pitch for the sound effect
			*/
            void SetPitch(const float value);
			/*
				Set the pan for all new instances created for this sound
				@param value The new pan for the sound effect
			*/
            void SetPan(const float value);

			/*
				Makes a new sound effect instance from this sound. This MUST be cleaned
				up after use, or will result in a memory leak
				@returns a new instance of a sound effect
			*/
            SoundInstance* MakeInstance() const;

			/* Gets the name of the sound effect */
            inline const std::string GetName() const { return myName; }
			/* Gets the filename that this sound effect was loaded from */
            inline const std::string GetFilename() const { return myFilename; }

        private:
			// Sound instance is our friend...
            friend class SoundInstance;

			// Stores our names and whatnot
            std::string myName;
            std::string myFilename;

			// Settings
            float myGain, myPitch, myPan;

			// Stores the actual underlying Gorilla Audio sound effect
            ga_Sound* mySoundHandle;
    };

}}

#endif // SOUNDEFFECT_H
