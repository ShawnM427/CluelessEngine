#ifndef SOUNDINSTANCE_H
#define SOUNDINSTANCE_H

#include <gorilla/ga.h>
#include <gorilla/gau.h>

namespace clueless { namespace audio { class SoundInstance; } }

#include <audio/SoundEffect.h>

namespace clueless { namespace audio {

	/*
		Represents a single instance of a sound effect
	*/
    class SoundInstance
    {
        public:
			/*
				Creates a new sound effect instance from the given sound effect and audio settings
				@param sound The effect to create the instance for
				@param gain  The gain for the audio (how loud it is)
				@param pan   The pan (left-right) of the audio
				@param pitch The pitch of the audio
			*/
            SoundInstance(const SoundEffect* sound, const float gain = 1.0f, const float pan = 0.0f, const float pitch = 1.0f);
			SoundInstance(const SoundInstance& other) = delete;
			~SoundInstance();

			/* Plays this sound instance, can be command chained */
            SoundInstance* Play();
			/* Pauses this sound instance without seeking to beginning, can be command chained */
            SoundInstance* Pause();
			/* Start playing this sound instance in loop mode, can be command chained */
            SoundInstance* Loop();
			/* Stops this sound instance without and seeks to beginning, can be command chained */
            SoundInstance* Stop();

			/*
				Sets the sound effect handle for this sound effect. If the sound is currently playing, 
				this will only effect the next time the sound is played. This is useful for sound instance
				pooling, which may be implemented later
				@param sound The Sound Effect to set this instance to
				@returns The pointer to this, useful for command chaining
			*/
			SoundInstance* SetEffect(const SoundEffect* sound);

			/* Sets the new value for gain, can be comman chained */
            SoundInstance* SetGain(const float value);
			/* Sets the new value for pitch, can be comman chained */
            SoundInstance* SetPitch(const float value);
			/* Sets the new value for pan, can be comman chained */
            SoundInstance* SetPan(const float value);

			/* Gets whether or not this instance is currently playing */
            bool IsPlaying() const;

        private:
			// Stores the pointer back to the sound effect
            const SoundEffect* mySound;

			// Audio instance settings
            float    myGain, myPitch, myPan;
            gc_int32 mySeekOffset;
            bool     isKeepAlive;

			// Freind functions that are defined externally
            friend void setFlagAndDestroyOnFinish(ga_Handle* handle, void* context);
            friend void loopOnFinish(ga_Handle* handle, void* context);

			// Pointer to the Gorilla Audio handle
            ga_Handle* myHandle;
			gau_SampleSourceLoop* myLoopSrc;
    };

} }

#endif // SOUNDINSTANCE_H
