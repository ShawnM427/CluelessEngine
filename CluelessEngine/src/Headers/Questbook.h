
#include <iostream>
#include <vector>
#include "Quest.h"
#include "tinyxml2.h"
#include "Event.h"
#include "../Headers/ImGuiQuestSystem.h"
class Questbook {

private:

	static std::vector<Quest*>* MyGameQuests;
	static std::vector<Quest*>* MyActiveQuests;
	static std::vector<Quest*>* MyCompleteQuests;
	static std::vector<Quest*>* __LoadQuests(const char* file);
	static bool open;

	//static clueless::game_logic::systems::ImGuiQuestSystem myQuests2;

public:
	static void Init(const char* questFile);
	static void Update(Event &message);
	static void Draw();
	static void Unload();
};