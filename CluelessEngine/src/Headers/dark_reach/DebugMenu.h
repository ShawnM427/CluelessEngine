#pragma once

#include "graphics/window.h"

#include <imgui\imgui.h>

namespace dark_reach {

	struct DebugSettings {
		int         IsWireframeEnabled;
		int         DrawMode;
		bool        ShaderDirty;
		float       CameraZ;
		//PointLight *Light;

		DebugSettings() : IsWireframeEnabled(0), DrawMode(0), ShaderDirty(true), CameraZ(1.6f) {

		}
	};

	class DebugMenu
		{
		public:
			DebugMenu(DebugSettings *settings, clueless::graphics::Window *window);

			void Init();
			void PostFrame();
			void Unload();

			void HideCursor();

		private:
			//nk_context                 *myCtx;
			clueless::graphics::Window *myWindow;
			DebugSettings              *mySettings;
			const char                **myRenderModesText;
	};

}