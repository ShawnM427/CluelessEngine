#pragma once

namespace clueless { 

	struct GameTime {
		static float TotalTime;
		static float SceneTime;
		static float DeltaTime;
		static float FrameDeltaTime;
	};

}