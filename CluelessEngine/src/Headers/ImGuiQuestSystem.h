#pragma once
#include "game_logic/Scene.h"
#include "cms/Structures.h"

namespace clueless {namespace game_logic {namespace systems {

class ImGuiQuestSystem :public GameSystem
{


public:

	static bool open;
	static bool showPrompt;
	static std::string quest;
	static std::string questDetails;
	static std::string task;

    ContentToken myFontToken;

	ImGuiQuestSystem();

	~ImGuiQuestSystem();
	 void Init();
	 void PostRender();

private:
	

};

}}}