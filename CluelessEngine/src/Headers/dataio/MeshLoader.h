#ifndef MESH_LOADER_H
#define MESH_LOADER_H

#include "glm_math.h"

namespace clueless { namespace dataio {

    /*
        TODO:
         - Null-terminated strings instead?
         - Actual loader implementations
    */

    enum ModelFlags {
        ModelHasSkeleton   = 0x01,
        ModelHasAnimations = 0x02
    };

    enum MeshFlags {
        Mesh32Indices      = 0x01,
    };

    enum VertexFlags {
        VertexHasPosition  = 0x01,
        VertexHasTexture   = 0x02,
        VertexHasNormal    = 0x04,
        VertexHasTangent   = 0x08,
        VertexHasBinormal  = 0x10
    };

    struct MaterialTexture {
        uchar       BindingSlot;
        char*       Path;
        char*       TexParamName;
    };

    struct MaterialParam {
        char*       Name;
        uchar       TypeCode;
        void*       Data;
    };

    struct RenderMaterial {
        uint16_t    ID;
        char*       Name;
        uint16_t    ShaderID;

        uchar       TextureCount;
        MaterialTexture* Textures;

        uchar       ParamCount;
        MaterialParam*   Parameters;
    };

    struct MeshPart {
        glm::mat4   Transform;
        float       Volume;
        uint16_t    RenderMaterial;
        uint16_t    PhysicsMaterial;
        uchar       MeshFlags;
        uchar       VertexFlags;
        uint32_t    VertexCount;
        void*       VertexData;
        uint32_t    IndexCount;
        void*       IndexData;
    };

    struct MeshJoint {
        uchar       ID;
        char*       Name;              // Debug only?
        uchar       ParentID;
        glm::vec3   DefaultLocation;
        glm::quat   DefaultRotation;
    };

    struct JointPose {
        uchar       JointID;
        glm::vec3   Position;
        glm::quat   Rotation;
    };

    struct KeyFrame {
        float       Time;
        JointPose* Poses;
    };

    struct MeshAnimation {
        uchar       ID;
        char*       Name;
        uint16_t    FrameCount;
        KeyFrame*   Frames;
    };

    struct Mesh {
        uint16_t    ID;
        char*       Name;
        uchar       ModelFlags;
        uchar       PartCount;
        MeshPart*   Parts;
        uchar       JointCount;
        MeshPart*   Joints;
        uchar       AnimCount;
        MeshAnimation* Animations;
    };

}}

#endif // MESH_LOADER_H
