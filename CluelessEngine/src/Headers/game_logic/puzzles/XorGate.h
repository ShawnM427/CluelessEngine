#pragma once
class XorGate
{
public:
	//gate inputs
	XorGate inputs(bool in1, bool in2);
	//inputs of the gate
	bool input1, input2;
	//output of the gate
	bool output;
};

