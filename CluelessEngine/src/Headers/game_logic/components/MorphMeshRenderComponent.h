#pragma once

#include "game_logic/GameComponent.h"
#include "graphics/Model.h"
#include "graphics/Material.h"
#include "graphics/MorphMesh.h"

#include "Graphics/Shader.h"

namespace clueless { namespace game_logic {
		
	class MorphMeshRenderComponent : 
		public GameComponent
	{
		public:
			MorphMeshRenderComponent(GameObject *parent, graphics::MorphMesh *model, graphics::Material *material);
			~MorphMeshRenderComponent();
			
			graphics::MorphMesh* getMorphMesh();
			virtual void Render();

		private:
			graphics::MorphMesh *myModel;
			graphics::Material  *myMaterial;
	};

} }