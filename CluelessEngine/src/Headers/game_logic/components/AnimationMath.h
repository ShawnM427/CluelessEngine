/*Name: Stephen Richards
ID: 100458273
Date: November 6,2017
*/
#pragma once
#include <glm/vec3.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/quaternion.hpp>

struct Transform {
	glm::vec3 Position;
	glm::quat Orientation;
	glm::vec3 Scale;

	Transform() : Position(glm::vec3()), Orientation(glm::quat()), Scale(glm::vec3(1.0f)) { }
};

namespace glm {
	float distance(const Transform& left, const Transform& right);
}

namespace AnimationMath
{
	// Linear interpolation
	template <typename T>
	T lerp(T d0, T d1, float t)
	{
		return (1 - t) * d0 + d1 * t;
	}

	template <>
	Transform lerp<Transform>(Transform left, Transform right, float t);

	template <typename T>
	float invLerp(T d, T d0, T d1)
	{
		return (d - d0) / (d1 - d0);
	}

	template<typename T>
	T catmull(T p0, T p1, T p2, T p3, float u)
	{
		//build U
		glm::vec4 U = glm::vec4(u ^ 3, u ^ 2, U, 1.0f);



		//build catmull M
		glm::mat4 M = glm::mat4(1.0f);
		glm::row(M, 0, 0.5*glm::vec4(-1.0f,3.0f,-3.0f,1.0f));
		glm::row(M, 1, 0.5*glm::vec4(2.0f, -5.0f, 4.0f, -1.0f));
		glm::row(M, 2, 0.5*glm::vec4(-1.0f, 0.0f, 1.0f, 0.0f));
		glm::row(M, 3, 0.5*glm::vec4(0.0f, 2.0f, 0.0f, 0.0f));


		//build P
		glm::vec4 P = glm::vec4(p0, p1, p2, p3);

		return U*M*P;

		
	}
template <typename T>
inline T bezier(T p0, T t0, T t1, T p1, float u)
	{
		//build U
		glm::vec4 U = glm::vec4(u*u*u, u *u, u, 1.0f);



		//build Bezier curve
		glm::mat4 M = glm::mat4(1.0f);
		M=glm::row(M, 0, glm::vec4(-1.0f, 3.0f, -3.0f, 1.0f));
		M = glm::row(M, 1, glm::vec4(3.0f, -6.0f, 3.0f, 0.0f));
		M = glm::row(M, 2, glm::vec4(-3.0f, 3.0f, 0.0f, 0.0f));
		M = glm::row(M, 3, glm::vec4(1.0f, 0.0f, 0.0f, 0.0f));

		//build P
		glm::mat4 P = glm::mat4(1.0f);
		P = glm::row(P, 0, glm::vec4(p0.x,p0.y,p0.z,1.0f));
		P = glm::row(P, 1, glm::vec4(t0.x,t0.y,t0.z, 1.0f));
		P = glm::row(P, 2, glm::vec4(t1.x,t1.y,t1.z, 1.0f));
		P = glm::row(P, 3, glm::vec4(p1.x,p1.y,p1.z,1.0f));



		return U*(M*P);


	}
}

//#include "AnimationMath.cpp""