#pragma once

#include "game_logic/GameComponent.h"

namespace clueless { namespace game_logic { namespace components {

	class CameraController :
		public GameComponent
	{
	public:
	    glm::vec2 MouseDelta;
		glm::vec2 Sensitivity;
		glm::vec3 Forward;

		CameraController(GameObject *parent);
		~CameraController();

		bool GetMouseLook() const { return myMouseLook; }
		void SetMouseLook(bool value);
		void ToggleMouseLook() { SetMouseLook(!myMouseLook); }

		virtual void FixedUpdate() override;
		virtual void Update() override;

		float GetYaw() const { return myYaw; }
		void SetYaw(float yaw) { myYaw = yaw; }

	private:
		float myRotSpeed;
		float myPitch;
		float myYaw;
		float myRoll;
	    bool  myMouseLook;
	};



} } }