#pragma once

#include "game_logic/GameComponent.h"

namespace clueless { namespace game_logic { namespace components {

			class PhysicsInteractComponent :
				public GameComponent {

				public:
					PhysicsInteractComponent(GameObject * parent) : GameComponent(parent) { }

					void HandleMessage(const MessageEvent& message, GameComponent *dispatchedFrom = nullptr);

			};

} } }