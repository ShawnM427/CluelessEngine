#pragma once

#include "game_logic/GameComponent.h"
#include "graphics/Model.h"
#include "graphics/Material.h"

#include "Graphics/Shader.h"

namespace clueless { namespace game_logic {
		
	class MeshRenderComponent : 
		public GameComponent
	{
		public:
			MeshRenderComponent(GameObject *parent, graphics::Model *mesh, graphics::Material *material);
			~MeshRenderComponent();

			virtual void Render();

		private:
			graphics::Model    *myModel;
			graphics::Material *myMaterial;
	};

} }