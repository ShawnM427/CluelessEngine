#pragma once

#include "BulletCollision/CollisionDispatch/btGhostObject.h"
#include "game_logic/GameComponent.h"

#include <functional>

namespace clueless { namespace game_logic { namespace components {

	class TriggerColliderComponent :
		public GameComponent
	{
		public:
			short int FilterMask;

			TriggerColliderComponent(GameObject *parent) : GameComponent(parent), 
				myGhost(nullptr),
				myCallback(nullptr), 
				myIsTriggered(false),
				myTriggerOnEmpty(false),
				myIsEmpty(true) {}
			TriggerColliderComponent(GameObject *parent, btCollisionShape *shape);
			void AddedToScene();

			void FixedUpdate();	

			void SetCallback(void(*callback)(GameObject*)) {
				myCallback = callback;
			}

			void SetTriggerOnEmpty(bool trigger) { myTriggerOnEmpty = trigger; }


		private:
			void(*myCallback)(GameObject*);
			btPairCachingGhostObject  *myGhost;
			bool myIsTriggered;
			bool myTriggerOnEmpty;
			bool myIsEmpty;
	};

} } }