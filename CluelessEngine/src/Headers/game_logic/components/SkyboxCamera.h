#pragma once

#include "game_logic/ICamera.h"
#include "Skybox.h"

namespace clueless { namespace game_logic { namespace components {

	class SkyboxCamera :
		public ICamera
	{
	public:
		SkyboxCamera(GameObject *parent);
		~SkyboxCamera();

		virtual void PreRender();
		virtual void PostRender() {};

	protected:
		glm::mat4  myCameraSpaceToWorld;
	};

} } }