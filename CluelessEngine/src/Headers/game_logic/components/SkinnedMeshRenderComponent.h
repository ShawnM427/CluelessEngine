#pragma once

#include "game_logic/GameComponent.h"
#include "graphics/SkinnedMesh.h"

namespace clueless { namespace game_logic { namespace components {

	class SkinnedMeshRenderComponent :
		public GameComponent
	{
	public:
		SkinnedMeshRenderComponent(GameObject *parent, graphics::SkinnedMesh *mesh);
		~SkinnedMeshRenderComponent();

		virtual void Render();

	private:
		graphics::SkinnedMesh *myMesh;
	};

} } }