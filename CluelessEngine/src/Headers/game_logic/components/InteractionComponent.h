#pragma once

#include "game_logic/GameComponent.h"

namespace clueless { namespace game_logic { namespace components {

	class InteractionComponent :
		public GameComponent {
	public:
		InteractionComponent(GameObject* parent) : GameComponent(parent), myCallback(nullptr) {}
		virtual ~InteractionComponent() {};

		void SetCallback(void(*callback)(GameObject*)) {
			myCallback = callback;
		}

		void Invoke() {
			if (myCallback)
				myCallback(myObject);
		}

	private:
		void(*myCallback)(GameObject*);
	};

} } }