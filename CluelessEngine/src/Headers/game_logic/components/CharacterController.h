#pragma once

#include "game_logic/GameComponent.h"

#include "BulletCollision/CollisionDispatch/btGhostObject.h"
#include <LinearMath\btDefaultMotionState.h>

namespace clueless { namespace game_logic { namespace components {

	class CharacterController :
		public GameComponent
	{
		public:
			CharacterController(GameObject *parent);
			~CharacterController();

			static bool disabled;

			virtual void AddedToScene();
			
			virtual void PrePhysics();
			virtual void FixedUpdate();
			void SetMyState(btDefaultMotionState *state) { myMotion = state; };

		private:	
			btPairCachingGhostObject *myGhost;
			btCollisionShape	     *myShape;
			btRigidBody              *myBody;
			btDefaultMotionState     *myMotion;

			float myYaw;
	};

} } }