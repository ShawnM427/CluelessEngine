#pragma once

#include "game_logic/GameComponent.h"
#include "game_logic/GameObject.h"
#include "physics/RigidBody.h"

namespace clueless { namespace game_logic { namespace components {

	class RigidBodyComponent :
		public GameComponent
	{
		public:
		RigidBodyComponent(GameObject* parent, physics::RigidBody* body);

		virtual void AddedToScene();
		//virtual void PrePhysics();
		virtual void FixedUpdate();
		void SetTransform(Transform& value);

		physics::RigidBody * getBody();

		private:
		physics::RigidBody* myBody;
	};

} } }