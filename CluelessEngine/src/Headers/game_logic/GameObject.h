#pragma once
#define GAME_OBJ_H

#include <vector>

#include "Transform.h"
#include "GameComponent.h"
#include "utils/TypeMap.h"
#include "utils/HashedMap.h"

namespace clueless { namespace game_logic {

	class GameObject final
		: public GameComponent 
	{
		public:
			Transform  LocalTransform;

			GameObject() : LocalTransform(Transform()), myComponents(TypeMap<GameComponent>()) {}
			GameObject(GameObject* parent = nullptr); 
			virtual ~GameObject();

			virtual void OnStart();

			void Render();

			void AddedToScene();
			void RemovedFromScene();

			void PrePhysics();

			void HandleMessage(const MessageEvent& message, GameComponent *dispatchedFrom = nullptr);

			void Update();
			void FixedUpdate();
			void LateUpdate();

			void LookAt(glm::vec3 target);

			glm::mat4 GetWorldTransform();
			void      SetWorldTransform(glm::mat4 transform);

			glm::vec3 ToLocalSpace(const glm::vec3& worldValue);
			glm::vec3 ToLocalSpaceNorm(const glm::vec3& worldNorm);

			void RotateLocalSpace(const glm::vec3& euler);
			void RotateParentSpace(const glm::vec3& euler);

			template <typename T>
			void AddComponent(T component) {
				myComponents.Add(component);
			}

			template <> 
			void AddComponent<GameObject>(GameObject component) {
				myChildren.Add(component);
			}

			void AddChild(GameObject component, const char name[8]);

			template <typename T>
			T* GetComponent() {
				return myComponents.Get<T>();
			}

		protected:
			TypeMap<GameComponent> myComponents;
			HashedMap<GameObject>  myChildren;
	};

} }