#pragma once

#include "glm_math.h"
#include "GameObject.h"

namespace clueless { namespace game_logic {

	class ICamera : 
		public GameComponent 
	{
	public:
		ICamera(GameObject *parent) : GameComponent(parent) {}
		virtual ~ICamera() {};

		virtual void PreRender() = 0;
		virtual void PostRender() = 0;

		virtual void SetProjection(const glm::mat4& projection) { myProjection = projection; }

		virtual glm::mat4 GetView() const { return myView; }
		virtual glm::mat4 GetProjection() const { return myProjection; }

	protected:
		glm::mat4 myView;
		glm::mat4 myProjection;
	};

} }