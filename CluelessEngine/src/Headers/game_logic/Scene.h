#pragma once

#include <vector>

#include "GameTime.h"
#include "graphics/Shader.h"

#include "physics/PhysicsWorld.h"

#include "game_logic/GameObject.h"
#include "game_logic/GameSystem.h"
#include "game_logic/ICamera.h"

#include "graphics/Window.h"

namespace clueless { namespace game_logic {
	class Scene  {
	public:
		static void Init();
		static void Load();
		static void Start();
		static void Update();
		static void Unload();
		static void Cleanup();

		static void AddCollisionItem(physics::RigidBody *body);
		static void AddCollisionItem(btCollisionObject *body);

		static void SetActiveCamera(ICamera *camera) {
			myActiveCamera = camera;
		}
		static ICamera* GetActiveCamera() { return myActiveCamera; }

		template <typename T>
		static void AddSystem(T& system) {
			mySystems.Add(system);
		}
		static void AddObject(GameObject* object);
		static void RemoveObject(GameObject* object);

		static void SetWindow(graphics::Window *window) {
			myWindow = window;
		}
		static graphics::Window* GetWindow() { 
			return myWindow;
		}

		static physics::PhysicsWorld *GetPhysicsWorld() { return myPhysicsWorld; }

	protected:
		static TypeMap<GameSystem> mySystems;
		static std::vector<GameObject*> myGameObjects;

		static graphics::Window*	    myWindow;

		static ICamera		           *myActiveCamera;

		static physics::PhysicsWorld *myPhysicsWorld;

		static float myFrameDelta;
		static float myFixedFrameTime;

		template <typename T>
		static void DeleteList(std::vector<T*> list);
	};
} }