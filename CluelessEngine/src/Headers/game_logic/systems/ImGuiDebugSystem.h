#pragma once

#include "game_logic/GameSystem.h"

#include "graphics/FontRenderer.h"
#include "ui/TrueTypeFont.h"

namespace clueless { namespace game_logic { namespace systems {

	class ImGuiDebugSystem :
		public GameSystem
	{
		public:
			static bool IsOpen;

			ImGuiDebugSystem();
			~ImGuiDebugSystem();

			virtual void Init();

			virtual void PostRender();

		private:
			static ui::TrueTypeFont *myFont;
			static ContentToken      myShaderToken;
	};

} } }