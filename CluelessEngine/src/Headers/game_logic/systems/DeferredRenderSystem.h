#pragma once

#include "game_logic/GameSystem.h"
#include "game_logic/ICamera.h"

#include "graphics/GBuffer.h"
#include "graphics/Mesh.h"
#include "graphics/UniformBuffer.h"

#include "graphics/PointLightData.h"

#include "anim/Animate.h"

#include <vector>

#include "cms/CMS.h"

namespace clueless { namespace game_logic { namespace systems {
	
	class DeferredRenderSystem :
		public GameSystem
	{
		public:
			struct LightSpline {
				int               Index = 0;

				anim::AnimChannel *RedChannel = nullptr;
				anim::AnimChannel *GreenChannel = nullptr;
				anim::AnimChannel *BlueChannel = nullptr;
				anim::AnimChannel *Attenuation = nullptr;
			};

			virtual void PreRender();
			virtual void Render(GameObject *object);
			virtual void PostRender();
					
			// Inherited via GameSystem
			virtual void Init();
			virtual void FixedUpdate(GameObject * object) {};
			virtual void Update(GameObject * object) {};
			virtual void LateUpdate(GameObject * object) {};

			void AddLightSpline(LightSpline *spline);

			void SetActiveLights(int count) { myNumActiveLights = count; };

			graphics::PointLightData* GetLight(int index);

		private:
			int      myNumActiveLights;
			ICamera *myCamera;
			graphics::GBuffer *myBackBuffer;
			ContentToken       myPostShader;
			ContentToken       myForwardShader;
			graphics::UniformBuffer* myLightBuffer;

			LightSpline **myLightSplines = nullptr;
			uint16_t      myLightSplineCount = 0;

			graphics::Mesh *myQuad;
	};

} } }