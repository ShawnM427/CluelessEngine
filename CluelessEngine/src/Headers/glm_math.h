#pragma once

#define GLM_FORCE_SWIZZLE
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtx/matrix_interpolation.hpp>
#include <GLM/gtx/rotate_vector.hpp>

#include "LinearMath/btVector3.h"
#include "LinearMath/btQuaternion.h"
#include "LinearMath/btMatrixX.h"

glm::vec3 bt2glm(const btVector3& value);
btVector3 glm2bt(const glm::vec3& value);

glm::quat bt2glm(const btQuaternion& value);
btQuaternion glm2bt(const glm::quat& value);

glm::vec3 mult(const glm::vec3& left, const glm::mat4& right);

glm::vec2 mult(const glm::vec2& left, const glm::mat3& right);

void CopyFromBullet(glm::vec3& target, const btVector3& source);
void CopyFromBullet(glm::vec4& target, const btVector4& source);
void CopyFromBullet(glm::quat& target, const btQuaternion& source);

void CopyToBullet(const glm::vec3& source, btVector3& target);
void CopyToBullet(const glm::vec4& source, btVector4& target);
void CopyToBullet(const glm::quat& source, btQuaternion& target);
