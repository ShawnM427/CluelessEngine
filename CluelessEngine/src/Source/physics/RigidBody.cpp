#include "physics/RigidBody.h"

namespace clueless { namespace physics {

	RigidBody::RigidBody(btCollisionShape *shape, float mass, bool ownsShape) :
		RigidBody(shape, btVector3(), btQuaternion(), mass, ownsShape) { }

	RigidBody::RigidBody(btCollisionShape *shape, btVector3 position, float mass, bool ownsShape) : 
		RigidBody(shape, position, btQuaternion(btVector3(0, 0, 1), 0), mass, ownsShape) { }

	RigidBody::RigidBody(btCollisionShape *shape, btVector3 position, btQuaternion rotation, float mass, bool ownsShape) {
		myShape = shape;
		myMotionState = new btDefaultMotionState(btTransform(rotation, position));
		btVector3 inertia = btVector3(0, 0, 0);
		if (mass != 0)
			myShape->calculateLocalInertia(mass, inertia);
		myRigidBody = new btRigidBody(mass, myMotionState, myShape, inertia);
		
		myCollisionGroup = 0x01;
		myCollisionMask  = 0xFF;

		myTransform = Transform();
	}

	RigidBody::~RigidBody() {
		if (myWorld)
			myWorld->RemoveRigidBody(this);
		
		delete myMotionState;
		delete myRigidBody;

		if (ownsShape)
			delete myShape;
	}

	glm::vec3 RigidBody::GetPosition() const
	{
		static btTransform tempTransform = btTransform();
		static glm::vec3 tempResult = glm::vec3();
		myMotionState->getWorldTransform(tempTransform);
		CopyFromBullet(tempResult, tempTransform.getOrigin());
		return tempResult;
	}

	glm::quat RigidBody::GetOrientation()
	{
		static btTransform tempTransform = btTransform();
		static glm::quat tempResult = glm::quat();
		myMotionState->getWorldTransform(tempTransform);
		CopyFromBullet(tempResult, tempTransform.getRotation());
		return tempResult;
	}

	void RigidBody::SetOrientation(const glm::quat & value) {
		myRigidBody->getWorldTransform().setRotation(btQuaternion(value.x, value.y, value.z, value.w));
	}

	void RigidBody::SetTransform(Transform & value) {

		
		static btTransform  tempTransform = btTransform();
		
		tempTransform.setOrigin(glm2bt(value.Position));
		tempTransform.setRotation(glm2bt(value.Orientation));
		myRigidBody->setWorldTransform(tempTransform);
		myMotionState->setWorldTransform(tempTransform);


		myTransform = value;
		//myMotionState->setWorldTransform(tempTransform);

		
	}

	void RigidBody::SetTransform(glm::mat4 & value) {

		static btTransform  tempTransform = btTransform();
		static glm::vec3 scale, translate, skew;
		static glm::vec4 perspective;
		static glm::quat orientation;
		glm::decompose(value, scale, orientation, translate, skew, perspective);

		tempTransform.setOrigin(glm2bt(translate));
		tempTransform.setRotation(glm2bt(orientation));

		myRigidBody->setWorldTransform(tempTransform);
		myMotionState->setWorldTransform(tempTransform);


		myTransform.Position = translate;
		myTransform.Orientation = orientation;
		//myMotionState->setWorldTransform(tempTransform);
	}

	Transform RigidBody::GetTransform()
	{
		static btTransform tempTransform = btTransform();
		myMotionState->getWorldTransform(tempTransform);

		CopyFromBullet(myTransform.Position, tempTransform.getOrigin());
		CopyFromBullet(myTransform.Orientation, tempTransform.getRotation());

		return myTransform;
	}

	void RigidBody::ApplyImpulse(const glm::vec3 & impulse) {
		myRigidBody->applyCentralImpulse(btVector3(impulse.x, impulse.y, impulse.z));
	}

} }
