#include "Task.h"
#include <iostream>
Task::Task()
{

}

Task::Task(taskImportance ti, std::string title, std::string details,
	std::string complete, EventTypes type)
{
	myImportance = ti;
	myTitle = title;
	myDetails = details;
	myCompletedDetails = complete;
	myTaskType = type;
}
Task::~Task()
{

}

void Task::setFilterMask(int mask)
{
	FilterMask = mask;
}

std::string Task::getDetails()
{
	return myDetails;
}

void Task::CompleteTask()
{
	myComplete = true;
}
taskImportance Task::CheckTaskImportance()
{
	return myImportance;
}
void Task::SetTaskDetails(std::string details)
{
	myDetails = details;
}
std::string Task::ViewTaskDetails()
{

	return myDetails;
}
void Task::SetTaskCompleteDetails(std::string complete)
{
	myCompletedDetails = complete;
}
std::string Task::GetCompleteDetails()
{
	return myCompletedDetails;
}
bool Task::isComplete()
{
	return myComplete;
}
bool Task::IsVisible()
{
	return myVisible;
}
bool Task::IsActive()
{
	return myActive;
}
void Task::Draw()
{
	std::cout <<myDetails<< std::endl;
}
void Task::Update()
{

}

void Task::SetTaskId(std::string id)
{
	this->id = id;
}
void Task::setMyEventType(EventTypes type)
{
	myTaskType = type;
}

void Task::setId(std::string id)
{
	myId = id;
}



void Task:: HandleEvent(Event &message)
{
	if (message.type==myTaskType)
	{
		switch (myTaskType)
		{
			case Puzzle:
			{
				PuzzleStart puzzle = message.GetData<PuzzleStart>();
				if (puzzle.flags==COMPLETE)
				{
					if (puzzle.id==1)
					{
						myComplete = true;
					}
				}
			}
			break;
			case InventoryPickup:
			{
				Inventory myTask = message.GetData<Inventory>();
			}
				break;
			case ColliderEmpty:
			{
				ColliderE &myCol = message.GetData<ColliderE>();
				if (myCol.flags==PICKEDUP)
				{
					myComplete = true;
				}
				break;
			}
			case ColliderEnter:
			{
				ColliderEn &myCol = message.GetData<ColliderEn>();
				if (myCol.flags ==Player)
				{
					if (myCol.id==FilterMask)
					{
						myComplete = true;
					}
					
				}
				break;
			}
		}
		
	}
}
