#include "Quest.h"

Quest::Quest()
{
	myTitle = "Deafult";
	myDetails = "details";
}


Quest::Quest(std::string title, std::string details, std::string completeDetails,
	QuestLevel questImportance)
{
	myTitle = title;
	myDetails = details;
	myCompleteDetails = completeDetails;
	myQuestLevel = questImportance;
}
Quest::~Quest()
{

}


std::vector<std::string> Quest:: getRequirements()
{
	return requirements;
}

std::string Quest:: getId()
{
	return id;
}

void Quest::addPreReq(std::string pre)
{
	requirements.push_back(pre);
}

void Quest::setDetails(std::string details)
{
	this->myDetails = details;
}
void Quest::SetId(std::string id)
{
	this->id = id;
}

void Quest::setTitle(std::string title)
{
	this->myTitle = title;
}


void Quest::AddTask(Task* task)
{
	myTasks.push_back(task);
}
void Quest::ActivateQuest()
{
	myActive = true;
	myVisible = true;

}
bool Quest::IsCompleted()
{
	return myComplete;
}

std::string Quest::getTitle()
{
	return myTitle;
}

void Quest::MakeVisible()
{
	myVisible = true;
}
bool Quest::isVisible()
{
	return myVisible;
}
bool Quest::isActive()
{
	return myActive;
}
void Quest::__CompleteQuest()
{
	myComplete = true;
}
void Quest::Draw()
{

	std::cout << myTitle << std::endl;
	std::cout << "-----------------------" << std::endl;
	std::cout << myDetails << std::endl;
	for (int i = 0; i < myTasks.size(); i++)
	{
		std::cout << i + 1 << ".";
		myTasks.at(i)->Draw();
	}
}

std::vector<Task*>  Quest::getTasks()
{
	return myTasks;
}

void Quest::Unload()
{

}

std::string Quest::getDetails()
{
	return myDetails;
}

void Quest::Update()
{
	for (int i = 0; i < myTasks.size(); i++)
	{
		if (!myTasks[i]->isComplete())
		{
			return;
		}
	} 
	__CompleteQuest();
}

void  Quest::HandleEvent(Event &message)
{
	for (int i = 0; i < myTasks.size(); i++)
	{
		myTasks[i]->HandleEvent(message);
	}
}
