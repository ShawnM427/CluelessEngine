/**
    Implementation file for the instanced mesh class
    @date   July 27, 2017
    @author Shawn Matthews - 100412327
    @author Shaun McKinnon - 100642799
    @author Paul Puig      - 100656910
*/
#include <iostream>

#include <graphics/common.h>
#include <utils/common.h>

#include <graphics/InstancedMesh.h>
#include <graphics/VertexDeclaration.h>
#include <utils/Logger.h>


namespace clueless { namespace graphics {

    // Implementation of mesh constructor
    InstancedMesh::InstancedMesh(const MeshData& data, const VertexDeclaration vDecl, const uint numInstances, const int attribLocation) {
        // Update our local fields
        myVertexCount = data.VertexCount;
        myIndexCount  = data.IndexCount;
        myInstanceCount = numInstances;

        // Generate our vertex array binding
        glGenVertexArrays(1, &myVao);

        // Generate some buffers for our vertices and indices
        glGenBuffers(1, &myVbo);
        glGenBuffers(1, &myIbo);
        glGenBuffers(1, &myInstanceBuffer);

        // Make sure our creation was successful
        if (myVao == 0 || myVbo == 0 || myIbo == 0 || myInstanceBuffer == 0) {
            // Otherwise delete any objects we DID make
            glDeleteBuffers(1, &myVbo);
            glDeleteBuffers(1, &myIbo);
            glDeleteBuffers(1, &myInstanceBuffer);
            glDeleteVertexArrays(1, &myVao);

            // and throw an exception
            throw gl_exception("Failed to generate VAO, VBO, or IBO");
        }

        // Bind our VAO, let's us tie commands to it
        glBindVertexArray(myVao);

        // Bind and buffer our vertex data
        glBindBuffer(GL_ARRAY_BUFFER, myVbo);
        glBufferData(GL_ARRAY_BUFFER, myVertexCount * vDecl.ElementSize, data.Vertices, GL_STATIC_DRAW);

        // Apply our vertex layout
        vDecl.Apply();

        // Bind and buffer our index data
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, myIbo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, myIndexCount * sizeof(USHORT), data.Indices, GL_STATIC_DRAW);

        glBindVertexArray(0);

        myInstanceData = new InstanceData[numInstances];

        glBindBuffer(GL_ARRAY_BUFFER, myInstanceBuffer);
        glBufferData(GL_ARRAY_BUFFER, numInstances * sizeof(InstanceData), myInstanceData, GL_DYNAMIC_DRAW);

        glBindVertexArray(myVao);

        for(int i = 0; i < 4; i ++) {
            glEnableVertexAttribArray(attribLocation + i);
            glVertexAttribPointer(attribLocation + i, 4, GL_FLOAT, GL_FALSE, sizeof(InstanceData), (void*)(i * sizeof(float) * 4));
            glVertexAttribDivisor(attribLocation + i, 1);
        }

        glEnableVertexAttribArray(attribLocation + 4);
        glVertexAttribPointer(attribLocation + 4, 4, GL_FLOAT, GL_FALSE, sizeof(InstanceData), (void*)(4 * sizeof(float) * 4));
        glVertexAttribDivisor(attribLocation + 4, 1);

        glBindBuffer(GL_ARRAY_BUFFER, myInstanceBuffer);

        // Bind the default vertex array so ours isn't accidentally modified
        glBindVertexArray(0);

        FILE_LOG(logINFO) << "Creating instanced mesh " << myVao << " (VBO: " << myVbo << ", IBO: " << myIbo  << ", INST: " << myInstanceBuffer << ")";
    }

    // Implementation of destructor
    InstancedMesh::~InstancedMesh() {
        // Delete our VAO
        glDeleteVertexArrays(1, &myVao);

        // Delete our buffers
        glDeleteBuffers(1, &myVbo);
        glDeleteBuffers(1, &myIbo);
        glDeleteBuffers(1, &myInstanceBuffer);

        FILE_LOG(logINFO)  << "Deleting instanced mesh " << myVao << " (VBO: " << myVbo << ", IBO: " << myIbo  << ", INST: " << myInstanceBuffer << ")" << std::endl;
    }

    // Implementation of mesh draw
    void InstancedMesh::Draw(const GLenum primitiveMode, const int count) const {
        // Bind our VAO for use, we want to render from it
        glBindVertexArray(myVao);
        // Draw our elements in indexed mode
        glDrawElementsInstanced(primitiveMode, myIndexCount, GL_UNSIGNED_SHORT, 0, count);
    }

    //updates instance based on count
    void InstancedMesh::UpdateInstances(const int count) {
        glBindBuffer(GL_ARRAY_BUFFER, myInstanceBuffer);
        glBufferSubData(GL_ARRAY_BUFFER, 0, count * sizeof(InstanceData), myInstanceData);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

    //returns instance data
    InstanceData* InstancedMesh::GetInstanceData(int& count) {
        count = myInstanceCount;
        return myInstanceData;
    }
}}











