/**
    Implementation of a glyph renderer using our OpenGL rendering engine

    @date   Aug 03, 2017
    @author Shawn Matthews - 100412327
    @author Shaun Mkinnon  -
    @author Paul Puig      -
*/

#include "graphics/FontRenderer.h"

#include <fstream>
#include <stdexcept>
#include <iostream>

#include "utils/common.h"
#include "utils/Logger.h"
#include "graphics/common.h"

namespace clueless { namespace graphics {

	using namespace clueless::ui;

    // The number of characters that can be written in a batch
    const int FontRenderer::BATCH_SIZE = 100;

	 Mesh*    FontRenderer::myMesh;
	 Shader*  FontRenderer::myShader;

	VertexPositionColorTexture*  FontRenderer::myVertexData;
	uint16_t*                    FontRenderer::myIndexData;

	void FontRenderer::__Init() {
		FILE_LOG(logDEBUG) << "Initializing font renderer";

		myVertexData = new VertexPositionColorTexture[FontRenderer::BATCH_SIZE * 4];
		myIndexData = new uint16_t[FontRenderer::BATCH_SIZE * 6];

		MeshData emptyMesh = MeshData(myVertexData, BATCH_SIZE * 4, myIndexData, BATCH_SIZE * 6, &VertPosColTexDecl);
		myMesh = new Mesh(emptyMesh, GL_TRIANGLES, GL_DYNAMIC_DRAW);

		const char* vsSource = R"LIT(#version 410
            layout (location = 0) in vec3 vertexPosition;
            layout (location = 1) in vec4 vertexColor;
            layout (location = 2) in vec2 vertexTexture;	
            layout (location = 0) out vec4 fragmentColor;
            layout (location = 1) out vec2 fragmentTexture;
            uniform mat4 xTransform;	
            void main() {
                gl_Position = xTransform * vec4(vertexPosition, 1);
                fragmentColor = vertexColor;
                fragmentTexture = vertexTexture;
            })LIT";

		const char* fsSource = R"LIT(#version 410
            uniform sampler2D xSampler;
            layout (location = 0) in vec4 fragColor;
            layout (location = 1) in vec2 fragUv;            	
            out vec4 frag_color;            	
            void main() {
                frag_color = fragColor * texture2D(xSampler, fragUv).a;
            })LIT";

		myShader = new Shader();
		myShader->LoadShaderPartFromSource(GL_VERTEX_SHADER, vsSource);
		myShader->LoadShaderPartFromSource(GL_FRAGMENT_SHADER, fsSource);
		myShader->Link();

		FILE_LOG(logDEBUG) << "Done initilaizing font renderer";
	}

    MeshData FontRenderer::Bake(const TextureFont& font, const char* text, const glm::vec2 position, const glm::vec4 color, const float scale) {
		if (!myMesh)
			__Init();

        int length = 0;
        while (text[length] != '\0' && text[length] != ' ' && text[length] != '\n' && text[length] != '\r')
            length ++;

        MeshData result;
        result.Vertices = new VertexPositionColorTexture[FontRenderer::BATCH_SIZE * 4];
        result.Indices  = new uint16_t[FontRenderer::BATCH_SIZE * 6];

        __GenerateData(font, text, position, color, (VertexPositionColorTexture*)result.Vertices, (uint16_t*)result.Indices, length, scale);
        result.VertexCount = length * 4;
        result.IndexCount  = length * 6;
		result.VDecl = &VertPosColTexDecl;
        return result;
    }

    void FontRenderer::Render(const TextureFont& font, const char* text, const glm::vec2 position, const glm::vec4 color, const float scale){
		if (!myMesh)
			__Init();

        // TODO: bind texture & custom shader
		myShader->Enable();
		myShader->SetUniform("xTransform", glm::orthoLH(0.0f, 800.0f, 600.0f, 0.0f, 1.0f, -1.0f));
		myShader->SetUniform("xSampler", 0);
        font.GetTexture()->Bind(0U);

        int length = -1;
        __GenerateData(font, text, position, color, myVertexData, myIndexData, length, scale);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		if (length > 0) {
			myMesh->Update(myVertexData, length * 4, myIndexData, length * 6);
			myMesh->Draw(length * 6);
		}

		glDisable(GL_BLEND);
    }

    void FontRenderer::__GenerateData(const TextureFont& font, const char* text, const glm::vec2 position, const glm::vec4 color,
                                      VertexPositionColorTexture* vertices, uint16_t* indices, int& length, const float scale) {
		if (!myMesh)
			__Init();

        length = 0;
        while (text[length] != '\0')
            length ++;
		
        float multiplier = scale;

        // The number of quads ACTUALLY drawn Shit-For-Brains
        int quads = 0;

		glm::vec3 originPos = glm::vec3(position.x, position.y, 0.0f);

		static GlyphInfo glyph;

		GpuColor gpuCol;
		gpuCol.R = color.r * 255;
		gpuCol.G = color.g * 255;
		gpuCol.B = color.b * 255;
		gpuCol.A = color.a * 255;

		float xOff{ 0 }, yOff{ 0 };

        for(int i = 0; i < length; i ++) {
			glyph = font.GetGlyph(text[i], xOff, yOff);
			xOff = glyph.OffsetX;
			yOff = glyph.OffsetY;

            if (text[i] == '\n') 
			{
                yOff += font.GetLineHeight() * multiplier;
				xOff = 0;
            } else if (text[i] == '\r') {
				xOff = 0;
            } else if (text[i] == '\t') {
				float xOffTemp{ 0 }, yOffTemp{ 0 };
				glyph = font.GetGlyph(' ', xOffTemp, yOffTemp);
				xOff += glyph.OffsetX * 4;
            } else {

                myVertexData[quads * 4 + 0].Position = originPos + glyph.Positions[0] * multiplier;
                myVertexData[quads * 4 + 0].Texture = glyph.UVs[0];
				myVertexData[quads * 4 + 0].Color = gpuCol;

				myVertexData[quads * 4 + 1].Position = originPos + glyph.Positions[1] * multiplier;
				myVertexData[quads * 4 + 1].Texture = glyph.UVs[1];
				myVertexData[quads * 4 + 1].Color = gpuCol;

				myVertexData[quads * 4 + 2].Position = originPos + glyph.Positions[2] * multiplier;
				myVertexData[quads * 4 + 2].Texture = glyph.UVs[2];
				myVertexData[quads * 4 + 2].Color = gpuCol;

				myVertexData[quads * 4 + 3].Position = originPos + glyph.Positions[3] * multiplier;
				myVertexData[quads * 4 + 3].Texture = glyph.UVs[3];
				myVertexData[quads * 4 + 3].Color = gpuCol;


                myIndexData[quads * 6 + 0] = quads * 4 + 0;
                myIndexData[quads * 6 + 1] = quads * 4 + 1;
                myIndexData[quads * 6 + 2] = quads * 4 + 2;

                myIndexData[quads * 6 + 3] = quads * 4 + 0;
                myIndexData[quads * 6 + 4] = quads * 4 + 2;
                myIndexData[quads * 6 + 5] = quads * 4 + 3;
				
                quads++;
            }
        }

        length = quads;
    }
}}

















