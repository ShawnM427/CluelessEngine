#include "graphics/GBuffer.h"

#include "utils/common.h"
#include "utils/Logger.h"

namespace clueless { namespace graphics {

	GBuffer::GBuffer(const uint32_t width, const uint32_t height, const GLenum wrapMode, const bool multisample)
    {
        myWidth  = width;
        myHeight = height;
        __Create(width, height, wrapMode, multisample);
    }

    void GBuffer::__Create(const uint32_t width, const uint32_t height, const GLenum wrapMode, const bool multisample) {
        glGenFramebuffers(1, &myFrameBufferHandle);
        glBindFramebuffer(GL_FRAMEBUFFER, myFrameBufferHandle);
		
		__CreateTexture(myRenderedTexture, width, height, wrapMode, GL_COLOR_ATTACHMENT0, GL_RGB, GL_RGB, GL_UNSIGNED_BYTE);
		__CreateTexture(myNormalTexture, width, height, wrapMode, GL_COLOR_ATTACHMENT1, GL_RGB16F, GL_RGB, GL_FLOAT);
		__CreateTexture(myEmmisiveTexture, width, height, wrapMode, GL_COLOR_ATTACHMENT2, GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE);
		__CreateTexture(mySpecularTexture, width, height, wrapMode, GL_COLOR_ATTACHMENT3, GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE);
		__CreateTexture(myWorldSpaceTexture, width, height, wrapMode, GL_COLOR_ATTACHMENT4, GL_RGB16F, GL_RGB, GL_FLOAT);
		__CreateTexture(myDepthTexture, width, height, wrapMode, GL_DEPTH_ATTACHMENT, GL_DEPTH_COMPONENT, GL_DEPTH_COMPONENT, GL_FLOAT);
		
        //glGenRenderbuffers(1, &myDepthRenderBuffer);
        //glBindRenderbuffer(GL_RENDERBUFFER, myDepthRenderBuffer);
        //glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
        //glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, myDepthRenderBuffer);

        // Set the list of draw buffers.
        GLenum DrawBuffers[5] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT4 };
        glDrawBuffers(5, DrawBuffers);

        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
            Cleanup();
			FILE_LOG(logERROR) << "Failed to build rendertarget: " << glCheckFramebufferStatus(GL_FRAMEBUFFER);
            //throw gl_exception("Failed to build render target");
        }

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

	void GBuffer::__CreateTexture(GLuint& texHandle, const uint32_t width, const uint32_t height, const GLenum wrapMode, const GLenum bindingLoc, 
		const GLenum internalFormat, const GLenum format, const GLenum type, const bool multisample) {
		glGenTextures(1, &texHandle);

		if (multisample) {
			glEnable(GL_MULTISAMPLE);
			glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, texHandle);
			glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, 4, internalFormat, width, height, false);
			glTexParameteri(GL_TEXTURE_2D_MULTISAMPLE, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D_MULTISAMPLE, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D_MULTISAMPLE, GL_TEXTURE_WRAP_S, wrapMode);
			glTexParameteri(GL_TEXTURE_2D_MULTISAMPLE, GL_TEXTURE_WRAP_T, wrapMode);

			glFramebufferTexture2D(GL_FRAMEBUFFER, bindingLoc, GL_TEXTURE_2D_MULTISAMPLE, texHandle, 0);
		}
		else {
			glBindTexture(GL_TEXTURE_2D, texHandle);
			glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format, type, nullptr);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapMode);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapMode);

			glFramebufferTexture(GL_FRAMEBUFFER, bindingLoc, texHandle, 0);
		}
	}

    void GBuffer::Unbind() {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    void GBuffer::Cleanup() {
        glDeleteRenderbuffers(1, &myDepthRenderBuffer);
        glDeleteTextures(1, &myRenderedTexture);
		glDeleteTextures(1, &myNormalTexture);
        glDeleteFramebuffers(1, &myFrameBufferHandle);
    }

    void GBuffer::Bind() const {
        glBindFramebuffer(GL_FRAMEBUFFER, myFrameBufferHandle);
        glViewport(0, 0, myWidth, myHeight);
    }

} }
