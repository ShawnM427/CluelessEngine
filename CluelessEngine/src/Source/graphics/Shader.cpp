/**
    Implementation file of our shader implementation, which lets us use GLSL shader programs in our rendering engine
    Controls the intersection traffic lights
    @date   July 27, 2017
    @author Shawn Matthews - 100412327
    @author Shaun McKinnon - 100642799
    @author Paul Puig      - 100656910
*/

#include <iostream>

#include <graphics/Shader.h>
#include <utils/common.h>

#include "glm_math.h"

#include "utils/Logger.h"


namespace clueless { namespace graphics {

	GLuint Shader::__BoundShaderId = 0;

	/**
	Creates a new shader instance of the given type

	@param type The type of the shader, should be GL_VERTEX_SHADER, GL_FRAGMENT_SHADER, GL_GEOMETRY_SHADER, GL_TESS_CONTROL_SHADER, or GL_TESS_EVALUATION_SHADER
	@param source The source code for the shader, this should be after any pre-processing

	@throw gl_exception Thrown if the shader part failed to compile
	*/
	GLuint Shader::__CreateShader(const GLenum type, const char* source) {
		// Create the new shader part
		GLuint result = glCreateShader(type);

		// Set the source and compile
		glShaderSource(result, 1, &source, NULL);
		glCompileShader(result);

		// Check our compile status
		GLint compileStatus = 0;
		glGetShaderiv(result, GL_COMPILE_STATUS, &compileStatus);

		// If we failed to compile
		if (compileStatus == GL_FALSE) {
			// Get the size of the error log
			GLint logSize = 0;
			glGetShaderiv(result, GL_INFO_LOG_LENGTH, &logSize);

			// Create a new character buffer for the log
			char* log = new char[logSize];

			// Get the log
			glGetShaderInfoLog(result, logSize, &logSize, log);

			FILE_LOG(logWARNING) << "Failed to compile shader part: \n" << log;

			// Delete the broken shader result
			glDeleteShader(result);
		}

		// Return the compiled shader part
		return result;
	}

	Shader::Shader() {

		myProgramHandle = glCreateProgram();

	}

	Shader::~Shader() {
		if (myProgramHandle != 0) {
			Unload();
		}
	}

	void Shader::LoadShaderPart(GLenum type, const char* fileName) {

		FILE_LOG(logDEBUG) << "Starting load from " << fileName;
		char* source = readFile(fileName);
		LoadShaderPartFromSource(type, source);
		delete[] source;
		FILE_LOG(logDEBUG) << "Done";
	}

	void Shader::LoadShaderPartFromSource(GLenum type, const char* source) {

		if (myShaderParts[type] != 0) {
			glDetachShader(myProgramHandle, myShaderParts[type]);
			glDeleteShader(myShaderParts[type]);
		}

		GLuint part = Shader::__CreateShader(type, source);
		glAttachShader(myProgramHandle, part);

		myShaderParts[type] = part;
	}

	void Shader::Link() {

		// Link the program, connects the vertex and fragment shaders
		glLinkProgram(myProgramHandle);

		// Iterate over any shader parts we have and clean them up
		for (std::map<GLenum, GLuint>::iterator it = myShaderParts.begin(); it != myShaderParts.end(); ++it) {
			glDetachShader(myProgramHandle, it->second);
			glDeleteShader(it->second);
		}

		// Get whether the link was successfull
		GLint success = 0;
		glGetProgramiv(myProgramHandle, GL_LINK_STATUS, &success);

		// If not, we need to grab the log and throw an exception
		if (success == GL_FALSE) {
			// Get the length of the log
			GLint length = 0;
			glGetProgramiv(myProgramHandle, GL_INFO_LOG_LENGTH, &length);

			// Read the log from openGL
			char* log = new char[length];
			glGetProgramInfoLog(myProgramHandle, length, &length, log);
			FILE_LOG(logERROR) << "Shader failed to link:\n" << log;

			// Delete the partial program
			glDeleteProgram(myProgramHandle);
			delete[] log;

			isLoaded = false;
		}

		__PerformIntrospection();

		isLoaded = true;
	}

	void Shader::__PerformIntrospection() {
		GLint numUniforms;
		glGetProgramiv(myProgramHandle, GL_ACTIVE_UNIFORMS, &numUniforms);

		GLint* nameSizes = new GLint[numUniforms];
		GLuint* uniformLocs = new GLuint[numUniforms];

		for (int ix = 0; ix < numUniforms; ix++)
			uniformLocs[ix] = ix;

		glGetActiveUniformsiv(myProgramHandle, numUniforms, uniformLocs, GL_UNIFORM_NAME_LENGTH, nameSizes);

		for (int ix = 0; ix < numUniforms; ix++) {
			char* name = new char[nameSizes[ix]];
			int length;
			glGetActiveUniformName(myProgramHandle, ix, nameSizes[ix], &length, name);
			GLint loc = glGetUniformLocation(myProgramHandle, name);
			myUniformLocs.insert(std::pair<char*, GLint>(name, loc));
			delete[] name;
		}

		for (auto it : myUniformLocs) {
			std::cout << it.first << ", " << it.second << std::endl;
		}

		delete[] nameSizes;
		delete[] uniformLocs;
	}

	void Shader::Enable() const {
		if (myProgramHandle != __BoundShaderId) {
			glUseProgram(myProgramHandle);
		}
	}

	void Shader::Unload() {
		for (std::map<GLenum, GLuint>::iterator it = myShaderParts.begin(); it != myShaderParts.end(); ++it) {
			glDetachShader(myProgramHandle, it->second);
			glDeleteShader(it->second);
		}
		myShaderParts.clear();

		glDeleteProgram(myProgramHandle);
		myProgramHandle = 0;
	}

	void Shader::AddAttribute(uint32_t index, const std::string & name) {
		glBindAttribLocation(myProgramHandle, index, name.c_str());
	}

	int Shader::GetAttribLocation(const std::string & name) const {
		return glGetAttribLocation(myProgramHandle, name.c_str());
	}

	int Shader::GetUniformLoc(const std::string & name) {
		return myUniformLocs[name];
	}

	int Shader::GetUniformArraySize(const std::string & name)
	{
		const GLchar* uniformName = name.c_str();
		GLuint uniformIdx = 0;
		uniformIdx = glGetUniformBlockIndex(myProgramHandle, uniformName);

		if (uniformIdx != GL_INVALID_INDEX) {
			GLchar *tempName = new GLchar[name.length()];
			GLint uniformSize = 0;
			glGetActiveUniformBlockiv(myProgramHandle, uniformIdx, GL_UNIFORM_BLOCK_DATA_SIZE, &uniformSize);
			return uniformSize;
		}
		else {
			return 0;
		}
	}

	void Shader::SetBufferBindingPoint(const std::string & name, GLuint bindPoint) {
		GLuint bindIndex;
		bindIndex = glGetUniformBlockIndex(myProgramHandle, name.c_str());
		glUniformBlockBinding(myProgramHandle, bindIndex, bindPoint);
	}

	void Shader::SetUniform(const std::string& name, const glm::mat4 value, bool transpose) {
		SetUniform(myUniformLocs[name], value, transpose);
	}
	void Shader::SetUniform(const std::string& name, const glm::mat3 value, bool transpose) {
		SetUniform(myUniformLocs[name], value, transpose);
	}

	void Shader::SetUniform(const int location, const glm::mat4 value, bool transpose) {
		glUniformMatrix4fv(location, 1, transpose, glm::value_ptr(value));
	}
	void Shader::SetUniform(const int location, const glm::mat3 value, bool transpose) {
		glUniformMatrix3fv(location, 1, transpose, glm::value_ptr(value));
	}
	void Shader::SetUniform(const int location, const glm::vec4 value) {
		glUniform4fv(location, 1, glm::value_ptr(value));
	}
	void Shader::SetUniform(const int location, const glm::vec3 value) {
		glUniform3fv(location, 1, glm::value_ptr(value));
	}
	void Shader::SetUniform(const int location, const glm::vec2 value) {
		glUniform2fv(location, 1, glm::value_ptr(value));
	}
	void Shader::SetUniform(const int location, const float value) {
		glUniform1f(location, value);
	}
	void Shader::SetUniform(const int location, const int value) {
		glUniform1i(location, value);
	}
	void Shader::SetUniform(const int location, const uint64_t value) {
		glUniform1ui(location, value);
	}

} }
