#include "graphics/PrimitiveBatch3D.h"

namespace clueless { namespace graphics {

	PrimitiveBatch3D::PrimitiveVert3D *PrimitiveBatch3D::myTriVertices;
	PrimitiveBatch3D::PrimitiveVert3D *PrimitiveBatch3D::myLineVertices;

	glm::mat4        PrimitiveBatch3D::ViewProj = glm::mat4();
	
	uint32_t         PrimitiveBatch3D::myBufferCounts[2];
	GLuint           PrimitiveBatch3D::myVertexBuffers[2];
	GLuint           PrimitiveBatch3D::myVaos[2];

	glm::mat4        PrimitiveBatch3D::myCurrent;

	Shader          *PrimitiveBatch3D::myShader;
	Stack<glm::mat4> PrimitiveBatch3D::myTransformStack;

	void PrimitiveBatch3D::Init() {
		myTransformStack.Push(glm::mat4());
		myCurrent = myTransformStack.Peek();
		myBufferCounts[0] = 0;
		myBufferCounts[1] = 0;

		const char *vs_source = R"LIT(
			#version 440
			layout(location = 0) uniform mat4 xViewProj;

			layout(location = 0) in vec3 Position;
			layout(location = 1)in vec4 Color;

			out vec4 Frag_Color;
			void main()
			{
				Frag_Color = Color;
				gl_Position = xViewProj * vec4(Position,1);
			}
		)LIT";

		const char *fs_source =
			R"LIT(#version 330
				in vec4 Frag_Color;
				out vec4 Out_Color;
				void main()
				{
					Out_Color = Frag_Color;
				}
			)LIT";

		myShader = new Shader();
		myShader->LoadShaderPartFromSource(GL_VERTEX_SHADER, vs_source);
		myShader->LoadShaderPartFromSource(GL_FRAGMENT_SHADER, fs_source);
		myShader->Link();

		myTriVertices = new PrimitiveVert3D[RENDER3D_TRIS_SIZE];
		myLineVertices = new PrimitiveVert3D[RENDER3D_LINES_SIZE];

		glGenVertexArrays(2, myVaos);
		glGenBuffers(2, myVertexBuffers);

		glBindVertexArray(myVaos[0]);

		glBindBuffer(GL_ARRAY_BUFFER, myVertexBuffers[0]);
		glBufferData(GL_ARRAY_BUFFER, RENDER3D_TRIS_SIZE * sizeof(PrimitiveVert3D), myTriVertices, GL_DYNAMIC_DRAW);
			
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(PrimitiveVert3D), (GLvoid*)(0));
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(PrimitiveVert3D), (GLvoid*)(3 * sizeof(float)));
			
		glBindVertexArray(myVaos[1]);

		glBindBuffer(GL_ARRAY_BUFFER, myVertexBuffers[1]);
		glBufferData(GL_ARRAY_BUFFER, RENDER3D_LINES_SIZE * sizeof(PrimitiveVert3D), myLineVertices, GL_DYNAMIC_DRAW);

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(PrimitiveVert3D), (GLvoid*)(0));
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(PrimitiveVert3D), (GLvoid*)(3 * sizeof(float)));
						
		glBindVertexArray(0);
	}

	void PrimitiveBatch3D::PushTransform(const glm::mat4& transform) {
		myTransformStack.Push(myTransformStack.Peek() * transform);
		myCurrent = myTransformStack.Peek();
	}

	void PrimitiveBatch3D::PushOverride(const glm::mat4& transform) {
		myTransformStack.Push(transform);
		myCurrent = myTransformStack.Peek();
	}

	void PrimitiveBatch3D::PopTransform() {
		if (myTransformStack.Size() > 1)
			myTransformStack.Pop();

		myCurrent = myTransformStack.Peek();
	}

	void PrimitiveBatch3D::PushLine(const glm::vec3& pos1, const glm::vec3& pos2, const glm::vec4& color) {
	
		myLineVertices[myBufferCounts[1] + 0].Position = mult(pos1, myCurrent);
		myLineVertices[myBufferCounts[1] + 0].Color = color;
		myLineVertices[myBufferCounts[1] + 1].Position = mult(pos2, myCurrent);
		myLineVertices[myBufferCounts[1] + 1].Color = color;

		myBufferCounts[1] += 2;

		if (myBufferCounts[1] >= RENDER3D_LINES_SIZE)
			Flush();
	}

	void PrimitiveBatch3D::PushLine(const glm::vec3& pos1, const glm::vec3& pos2, const glm::vec4& fromColor, const glm::vec4& toColor) {

		myLineVertices[myBufferCounts[1] + 0].Position = mult(pos1, myCurrent);
		myLineVertices[myBufferCounts[1] + 0].Color = fromColor;
		myLineVertices[myBufferCounts[1] + 1].Position = mult(pos2, myCurrent);
		myLineVertices[myBufferCounts[1] + 1].Color = toColor;

		myBufferCounts[1] += 2;

		if (myBufferCounts[1] >= RENDER3D_LINES_SIZE)
			Flush();
	}

	void PrimitiveBatch3D::PushTriangle(const glm::vec3& pos1, const glm::vec3& pos2, const glm::vec3& pos3, const glm::vec4& color) {

		myTriVertices[myBufferCounts[0] + 0].Position = mult(pos1, myCurrent);
		myTriVertices[myBufferCounts[0] + 0].Color    = color;
		myTriVertices[myBufferCounts[0] + 1].Position = mult(pos2, myCurrent);
		myTriVertices[myBufferCounts[0] + 1].Color = color;
		myTriVertices[myBufferCounts[0] + 2].Position = mult(pos3, myCurrent);
		myTriVertices[myBufferCounts[0] + 2].Color = color;

		myBufferCounts[0] += 3;

		if (myBufferCounts[0] >= RENDER3D_TRIS_SIZE)
			Flush();
	}

	void PrimitiveBatch3D::PushSphere(const glm::vec3& center, const glm::vec3& size, const glm::vec4& color) {

	}

	void PrimitiveBatch3D::Flush() {
		
		myShader->Enable();
		myShader->SetUniform(0, ViewProj);

		if (myBufferCounts[0] > 0) {
			glBindVertexArray(myVaos[0]);
			glBindBuffer(GL_ARRAY_BUFFER, myVertexBuffers[0]);
			glBufferSubData(GL_ARRAY_BUFFER, 0, myBufferCounts[0] * sizeof(PrimitiveVert3D), myTriVertices);
			glDrawArrays(GL_TRIANGLES, 0, myBufferCounts[0]);
			myBufferCounts[0] = 0;
		}

		if (myBufferCounts[1] > 0) {
			glBindVertexArray(myVaos[1]);
			glBindBuffer(GL_ARRAY_BUFFER, myVertexBuffers[1]);
			glBufferSubData(GL_ARRAY_BUFFER, 0, myBufferCounts[1] * sizeof(PrimitiveVert3D), myLineVertices);
			glDrawArrays(GL_LINES, 0, myBufferCounts[1]);
			myBufferCounts[1] = 0;
		}
	}

} }