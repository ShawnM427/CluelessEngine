#include "graphics/RenderTarget2D.h"

#include "utils/common.h"

namespace clueless { namespace graphics {

    RenderTarget2D::RenderTarget2D(const uint32_t width, const uint32_t height, const GLenum wrapMode)
    {
        myWidth  = width;
        myHeight = height;
        __Create(width, height, wrapMode);
    }

    void RenderTarget2D::__Create(const uint32_t width, const uint32_t height, const GLenum wrapMode) {
        glGenFramebuffers(1, &myFrameBufferHandle);
        glBindFramebuffer(GL_FRAMEBUFFER, myFrameBufferHandle);

        glGenTextures(1, &myRenderedTexture);

        glBindTexture(GL_TEXTURE_2D, myRenderedTexture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapMode);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapMode);

        glGenRenderbuffers(1, &myDepthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, myDepthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, myDepthRenderBuffer);

        glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, myRenderedTexture, 0);
        // Set the list of draw buffers.
        GLenum DrawBuffers[1] = {GL_COLOR_ATTACHMENT0};
        glDrawBuffers(1, DrawBuffers);

        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
            Cleanup();
            throw gl_exception("Failed to build render target");
        }

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    void RenderTarget2D::Unbind() {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    void RenderTarget2D::Cleanup() {
        glDeleteRenderbuffers(1, &myDepthRenderBuffer);
        glDeleteTextures(1, &myRenderedTexture);
        glDeleteFramebuffers(1, &myFrameBufferHandle);
    }

    void RenderTarget2D::Bind() const {
        glBindFramebuffer(GL_FRAMEBUFFER, myFrameBufferHandle);
        glViewport(0, 0, myWidth, myHeight);
    }

} }
