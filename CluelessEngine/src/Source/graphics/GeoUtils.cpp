/**
    Namespace utilities that generates geometry
    @date   July 27, 2017
    @author Shawn Matthews - 100412327
    @author Shaun McKinnon - 100642799
    @author Paul Puig      - 100656910
*/

#include <cmath>

#include <graphics/GeoUtils.h>
#include <graphics/VertexLayouts.h>


namespace clueless { namespace graphics { namespace geo_utils {

	uint pack(glm::vec4 color) {
		glm::u8vec4 vec = (glm::u8vec4)(color * 255.0f);
		uint result = vec.r << 24 | vec.g << 16 | vec.b << 8 | vec.a;
		return result;
	}

    MeshData CreateSprite(const glm::vec2 halfSize, const float minTexX, const float minTexY, const float maxTexX, const float maxTexY) {
        // Create an array of vertices
        VertexPositionTexture *vertices = new VertexPositionTexture[4];

        // Set our center vertex to be at the offset, and set it's UV coord to the center of the texture
        vertices[0].Position = glm::vec3(-halfSize.x, -halfSize.y, 1.0f);
        vertices[0].Texture = glm::vec2( minTexX,  minTexY);

        vertices[1].Position = glm::vec3( halfSize.x, -halfSize.y, 1.0f);
        vertices[1].Texture = glm::vec2( maxTexX,  minTexY);

        vertices[2].Position = glm::vec3( halfSize.x,  halfSize.y, 1.0f);
        vertices[2].Texture = glm::vec2( maxTexX,  maxTexY);

        vertices[3].Position = glm::vec3(-halfSize.x,  halfSize.y, 1.0f);
        vertices[3].Texture = glm::vec2( minTexX,  maxTexY);

        // Create our index array
        USHORT *indices = new USHORT[6];

        indices[0] = 0;
        indices[1] = 1;
        indices[2] = 2;

        indices[3] = 0;
        indices[4] = 2;
        indices[5] = 3;

        // Create and return the mesh structure
        return MeshData(vertices, 4, indices, 6, &VertPosTexDecl);
    }


    /**
        Creates an indexed circle with the given segment count, radius, color, and offset

        @param segments The number of triangles in the circle, higher is better precision
        @param radius   The radius of the circle to create
        @param color    The color for the circle
        @param offset   The offset for the circle

        @return A MeshData containing all the vertices and indices to render the circle
    */
    MeshData CreateCircleFan(const int segments, const float radius, const glm::vec4 color, const glm::vec2 offset, float texSize) {
        // Create an array of vertices
		VertexPositionColorTextureNormal *vertices = new VertexPositionColorTextureNormal[segments + 1];

        /*
             /---\
            /\ | /\
            |_\|/_|   This would be a circle with 8 segments
            | /|\ |   arc is the angle per segment
            \/ | \/
             \_|_/

        */

        // Determine the size of each arc, basically we divide the total angle (2pi)
        double arc = M_PI * 2 / segments;

        // Set our center vertex to be at the offset, and set it's UV coord to the center of the texture
        vertices[0].Position = glm::vec3(offset, 0.0f);
        vertices[0].Texture  = glm::vec2(0.5f);
        vertices[0].Color.R  = color.r * 255.0f;
        vertices[0].Color.G  = color.g * 255.0f;
        vertices[0].Color.B  = color.b * 255.0f;
        vertices[0].Color.A  = color.a * 255.0f;
		vertices[0].Normal   = glm::vec3(0.0f, 0.0f, 1.0f);

        float uvMult = (texSize == -1 ? 1.0f : radius / texSize);

        // Iterate over each segment in our circle
        for(int i = 1; i < segments + 1; i ++) {
            // Determine the position of the vertex, using some basic trig
            vertices[i].Position.x = offset.x + cos(arc * i) * radius;
            vertices[i].Position.y = offset.y + sin(arc * i) * radius;
            vertices[i].Position.z = 0.0f;

            // Copy the Color over
            vertices[i].Color      = vertices[0].Color;
			vertices[i].Normal     = vertices[0].Normal;

            // Determine our texture UV coords with some more basic trig
            vertices[i].Texture.x = 0.5f + (cos(arc * i) * uvMult / 2.0f);
            vertices[i].Texture.y = 0.5f + (sin(arc * i) * uvMult / 2.0f);
        }

        // Create our index array
        USHORT *indices = new USHORT[segments * 3];

        // Iterate over our segments, from 0 to our second-to last
        for(int seg = 1; seg < segments; seg ++) {
            // Each segment has the first vertex at the center
            indices[seg * 3 + 0] = 0;
            // The other two vertices are at the segment index, and the next segment index
            indices[seg * 3 + 1] = seg;
            indices[seg * 3 + 2] = seg + 1;
        }

        // The last segment (which we store in the first segment slot (tricksy little buggers)
        // is comprised of the center vertex, and the first and last edge vertices
        indices[0] = 0;
        indices[1] = segments;
        indices[2] = 1;

        // Create and return the mesh structure
        return MeshData(vertices, segments + 1, indices, segments * 3, &VertPosColTexNormDecl);
    }

	MeshData CreateTextureCube(const glm::vec3 size, const glm::vec3 offset, const glm::vec4 color, const TexturingMode mode, bool inverted) {
		VertexPositionColorTextureNormal *vertices = new VertexPositionColorTextureNormal[6 * 4]; // 6 faces, 4 verts a peice 
		USHORT *indices = new USHORT[6 * 2 * 3]; // 6 faces, 2 tris per face, 3 indices per tri
		
		static glm::vec3 halfSize;
		static uint colorCode = 0;
		halfSize = size / 2.0f;
		
		colorCode = pack(color);
		
		vertices[0 ].Position = offset + glm::vec3(-halfSize.x,  halfSize.y, -halfSize.z);
		vertices[0 ].Color.Value = colorCode;
		vertices[1 ].Position = offset + glm::vec3(-halfSize.x, -halfSize.y, -halfSize.z);
		vertices[1 ].Color.Value = colorCode;
		vertices[2 ].Position = offset + glm::vec3( halfSize.x, -halfSize.y, -halfSize.z);
		vertices[2 ].Color.Value = colorCode;
		vertices[3 ].Position = offset + glm::vec3( halfSize.x,  halfSize.y, -halfSize.z);
		vertices[3 ].Color.Value = colorCode;

		vertices[0].Normal = vertices[1].Normal = vertices[2].Normal = vertices[3].Normal = glm::vec3(0.0f, 0.0f, -1.0f);

		vertices[4 ].Position = offset + glm::vec3(-halfSize.x, -halfSize.y, -halfSize.z);
		vertices[4 ].Color.Value = colorCode;
		vertices[5 ].Position = offset + glm::vec3(-halfSize.x, -halfSize.y,  halfSize.z);
		vertices[5 ].Color.Value = colorCode;
		vertices[6 ].Position = offset + glm::vec3( halfSize.x, -halfSize.y,  halfSize.z);
		vertices[6 ].Color.Value = colorCode;
		vertices[7 ].Position = offset + glm::vec3( halfSize.x, -halfSize.y, -halfSize.z);
		vertices[7 ].Color.Value = colorCode;

		vertices[4].Normal = vertices[5].Normal = vertices[6].Normal = vertices[7].Normal = glm::vec3(0.0f, -1.0f, 0.0f);
		
		vertices[8 ].Position = offset + glm::vec3(-halfSize.x, -halfSize.y,  halfSize.z);
		vertices[8 ].Color.Value = colorCode;
		vertices[9 ].Position = offset + glm::vec3(-halfSize.x,  halfSize.y,  halfSize.z);
		vertices[9 ].Color.Value = colorCode;
		vertices[10].Position = offset + glm::vec3( halfSize.x,  halfSize.y,  halfSize.z);
		vertices[10].Color.Value = colorCode;
		vertices[11].Position = offset + glm::vec3( halfSize.x, -halfSize.y,  halfSize.z);
		vertices[11].Color.Value = colorCode;

		vertices[8].Normal = vertices[9].Normal = vertices[10].Normal = vertices[11].Normal = glm::vec3(0.0f, 0.0f, 1.0f);
		
		vertices[12].Position = offset + glm::vec3( halfSize.x,  halfSize.y, -halfSize.z);
		vertices[12].Color.Value = colorCode;
		vertices[13].Position = offset + glm::vec3( halfSize.x,  halfSize.y,  halfSize.z);
		vertices[13].Color.Value = colorCode;
		vertices[14].Position = offset + glm::vec3(-halfSize.x,  halfSize.y,  halfSize.z);
		vertices[14].Color.Value = colorCode;
		vertices[15].Position = offset + glm::vec3(-halfSize.x,  halfSize.y, -halfSize.z);
		vertices[15].Color.Value = colorCode;

		vertices[12].Normal = vertices[13].Normal = vertices[14].Normal = vertices[15].Normal = glm::vec3(0.0f, 1.0f, 0.0f);
		
		vertices[16].Position = offset + glm::vec3(-halfSize.x,  halfSize.y, -halfSize.z);
		vertices[16].Color.Value = colorCode;
		vertices[17].Position = offset + glm::vec3(-halfSize.x,  halfSize.y,  halfSize.z);
		vertices[17].Color.Value = colorCode;
		vertices[18].Position = offset + glm::vec3(-halfSize.x, -halfSize.y,  halfSize.z);
		vertices[18].Color.Value = colorCode;
		vertices[19].Position = offset + glm::vec3(-halfSize.x, -halfSize.y, -halfSize.z);
		vertices[19].Color.Value = colorCode;

		vertices[16].Normal = vertices[17].Normal = vertices[18].Normal = vertices[19].Normal = glm::vec3(-1.0f, 0.0f, 0.0f);

		vertices[20].Position = offset + glm::vec3( halfSize.x, -halfSize.y, -halfSize.z);
		vertices[20].Color.Value = colorCode;
		vertices[21].Position = offset + glm::vec3( halfSize.x, -halfSize.y,  halfSize.z);
		vertices[21].Color.Value = colorCode;
		vertices[22].Position = offset + glm::vec3( halfSize.x,  halfSize.y,  halfSize.z);
		vertices[22].Color.Value = colorCode;
		vertices[23].Position = offset + glm::vec3( halfSize.x,  halfSize.y, -halfSize.z);
		vertices[23].Color.Value = colorCode;

		vertices[20].Normal = vertices[21].Normal = vertices[22].Normal = vertices[23].Normal = glm::vec3(1.0f, 0.0f, 0.0f);

		switch (mode) {
			case TextureModeFace:
				for (int ix = 0; ix < 6 * 4; ix += 4) {
					vertices[ix + 0].Texture = glm::vec2(0, 0);
					vertices[ix + 1].Texture = glm::vec2(0, 1);
					vertices[ix + 2].Texture = glm::vec2(1, 1);
					vertices[ix + 3].Texture = glm::vec2(1, 0);
				}
				break;
		}

		for (int ix{0}, vx{0}; ix < 6 * 2 * 3; ix += 6, vx+=4) {
			indices[ix + 0] = vx + 0;
			indices[ix + 1] = vx + (inverted ? 1 : 2);
			indices[ix + 2] = vx + (inverted ? 2 : 1);
			indices[ix + 3] = vx + 0;
			indices[ix + 4] = vx + (inverted ? 2 : 3);
			indices[ix + 5] = vx + (inverted ? 3 : 2);
		}

		// Create and return the mesh structure
		return MeshData(vertices, 6 * 4, indices, 6 * 2 * 3, &VertPosColTexNormDecl);
	}
}}}
