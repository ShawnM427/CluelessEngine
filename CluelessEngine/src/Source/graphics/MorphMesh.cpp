#include "graphics/MorphMesh.h"

#include <stdexcept>
#include <algorithm>
#include "glm_math.h"

namespace clueless { namespace graphics {
	

	MorphMesh::MorphMesh() :
		myKeys(nullptr), 
		myNumKeys(0), 
		myActiveKeys(0), 
		myTime(0), 
		myLoopType(anim::LoopType_Stop), 
		myMesh(nullptr), 
		mySpeedMult(1.0f),
		myTotalTime(0.0f),
		myCurrentKey(0),
		myNextKey(1),
		myActiveData(MeshData())
	{
	}

	MorphMesh::~MorphMesh() {
		if (myKeys)
			delete[] myKeys;

		myNumKeys = 0;
		myActiveKeys = 0;
		myTime = 0;
	}


	void MorphMesh::SetLoopMode(anim::LoopType value)
	{
		myLoopType = value;
	}

	void MorphMesh::AllocateKeys(int count) {
		if (count > myNumKeys) {
			if (myKeys) {
				MorphKey *newData = new MorphKey[count];
				memcpy(newData, myKeys, sizeof(MorphKey) * myNumKeys);
				delete[] myKeys;
				myKeys = newData;
				myNumKeys = count;
			}
			else {
				myKeys = new MorphKey[count];
				myNumKeys = count;
			}
		}
	}

	void MorphMesh::InsertKey(float time, MeshData data) {
		if (myActiveKeys == 0) {
			myActiveData.VertexCount = data.VertexCount;
			myActiveData.IndexCount  = data.IndexCount;
			myActiveData.VDecl       = data.VDecl;
			myActiveData.Vertices = malloc(data.VDecl->ElementSize * data.VertexCount);
			myActiveData.Indices = (uint16_t*)malloc(data.IndexCount * sizeof(uint16_t));
			memcpy(myActiveData.Vertices, data.Vertices, data.VertexCount * data.VDecl->ElementSize);
			memcpy(myActiveData.Indices, data.Indices, data.IndexCount * sizeof(uint16_t));

			myMesh = new Mesh(myActiveData);
		}
		else if (myActiveData.VertexCount != data.VertexCount | myActiveData.IndexCount != data.IndexCount) {
			FILE_LOG(logERROR) << "Attempted to build morph mesh with mis-aligned meshes";
			throw std::exception("Cannot build Morph Mesh with different sized meshes. Vertex and index counts MUST line up");
		}

		if (myNumKeys < myActiveKeys + 1)
			AllocateKeys(myNumKeys + 1);

		myKeys[myActiveKeys].Time = time;
		myKeys[myActiveKeys].Data = data;

		myActiveKeys++;

		myTotalTime = time;
	}

	void MorphMesh::pauseLoop(bool result)
	{
		pause = result;
	 }

	void MorphMesh::Update(float deltaTime) {
		

		if (pause == false)
		{
			myTime += deltaTime * mySpeedMult;


			if (myTime >= myTotalTime) {
				switch (myLoopType) {
				case anim::LoopType_Reverse:
					mySpeedMult *= -1.0f;
					myTime += deltaTime * mySpeedMult;
					break;
				case anim::LoopType_Repeat:
					myTime -= myTotalTime;
					break;
				default:
					mySpeedMult = 0.0f;
					myTime = myTotalTime;
					break;
				}
			}
			if (myTime < 0) {
				switch (myLoopType) {
				case anim::LoopType_Reverse:
					mySpeedMult *= -1.0f;
					myTime += deltaTime * mySpeedMult;
					break;
				case anim::LoopType_Repeat:
					myTime += myTotalTime;
					break;
				default:
					mySpeedMult = 1.0f;
					myTime = 0.0f;
					break;
				}
			}

			if (mySpeedMult > 0.0f) {
				if (myTime > myKeys[myNextKey].Time) {
					myCurrentKey++;
				}
			}
			else {
				if (myTime < myKeys[myCurrentKey].Time) {
					myCurrentKey--;
				}
			}

			myCurrentKey = myCurrentKey >= myActiveKeys ? 0 : myCurrentKey < 0 ? myActiveKeys - 1 : myCurrentKey;

			myNextKey = myCurrentKey + 1;
			myNextKey = myNextKey >= myActiveKeys ? 0 : myNextKey < 0 ? myActiveKeys - 1 : myNextKey;

			MorphKey t1 = myKeys[myCurrentKey];
			MorphKey t2 = myKeys[myNextKey];

			float tVal = (myTime - t1.Time) / abs(t2.Time - t1.Time);
			const VertexDeclaration * vDecl = myActiveData.VDecl;

			int positionOff = vDecl->Introspect(VType_Position).Offset;
			int normalOff = vDecl->Introspect(VType_Normal).Offset;
			int tangentOff = vDecl->Introspect(VType_Normal).Offset;
			int biTangentOff = vDecl->Introspect(VType_Normal).Offset;
			int colorOff = vDecl->Introspect(VType_Color).Offset;

			void *t1Vert, *t2Vert, *tResVert;
			int vOff;

			if (tVal < 0 | tVal > 1)
				FILE_LOG(logWARNING) << "fffff";

			for (int ix = 0; ix < myActiveData.VertexCount; ix++) {
				vOff = vDecl->ElementSize * ix;
				t1Vert = (char*)t1.Data.Vertices + vOff;
				t2Vert = (char*)t2.Data.Vertices + vOff;
				tResVert = (char*)myActiveData.Vertices + vOff;

				__TryMix<glm::vec3>(positionOff, tVal, t1Vert, t2Vert, tResVert, vDecl);
				__TryMix<glm::vec3, true>(normalOff, tVal, t1Vert, t2Vert, tResVert, vDecl);
				__TryMix<glm::vec3, true>(tangentOff, tVal, t1Vert, t2Vert, tResVert, vDecl);
				__TryMix<glm::vec3, true>(biTangentOff, tVal, t1Vert, t2Vert, tResVert, vDecl);
				__TryMix<glm::vec4>(colorOff, tVal, t1Vert, t2Vert, tResVert, vDecl);
			}

			myMesh->Update(myActiveData.Vertices, myActiveData.VertexCount, myActiveData.Indices, myActiveData.IndexCount);
		}
	}

	void MorphMesh::Draw() const {
		if (myMesh)
			myMesh->Draw();
	}

} }