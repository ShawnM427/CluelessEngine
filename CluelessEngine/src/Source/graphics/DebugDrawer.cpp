#include "graphics/DebugDrawer.h"

#include "graphics/PrimitiveBatch3D.h"

namespace clueless { namespace graphics {

	DebugDrawer DebugDrawer::Singleton;

	void DebugDrawer::drawLine(const btVector3 & from, const btVector3 & to, const btVector3 & fromColor, const btVector3 & toColor) {

		PrimitiveBatch3D::PushLine(bt2glm(from), bt2glm(to), glm::vec4(bt2glm(fromColor), 1.0f), glm::vec4(bt2glm(toColor), 1.0f));
	}

	void DebugDrawer::drawLine(const btVector3 & from, const btVector3 & to, const btVector3 & color) {
		PrimitiveBatch3D::PushLine(bt2glm(from), bt2glm(to), glm::vec4(bt2glm(color), 1.0f));
	}

	void DebugDrawer::drawSphere(const btVector3 & p, btScalar radius, const btVector3 & color) {

	}

	void DebugDrawer::drawTriangle(const btVector3 & a, const btVector3 & b, const btVector3 & c, const btVector3 & color, btScalar alpha) {
		PrimitiveBatch3D::PushTriangle(bt2glm(a), bt2glm(b), bt2glm(c), glm::vec4(bt2glm(color), alpha));
	}

	void DebugDrawer::drawContactPoint(const btVector3 & PointOnB, const btVector3 & normalOnB, btScalar distance, int lifeTime, const btVector3 & color) {

	}

	void DebugDrawer::reportErrorWarning(const char * warningString) {

	}

	void DebugDrawer::draw3dText(const btVector3 & location, const char * textString) {

	}

} }

