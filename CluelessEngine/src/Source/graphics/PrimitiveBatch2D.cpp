#include "graphics/PrimitiveBatch2D.h"

namespace clueless {
	namespace graphics {

		PrimitiveBatch2D::PrimitiveVert2D *PrimitiveBatch2D::myTriVertices;
		PrimitiveBatch2D::PrimitiveVert2D *PrimitiveBatch2D::myLineVertices;

		glm::mat4        PrimitiveBatch2D::ViewProj = glm::mat4();

		uint32_t         PrimitiveBatch2D::myBufferCounts[2];
		GLuint           PrimitiveBatch2D::myVertexBuffers[2];
		GLuint           PrimitiveBatch2D::myVaos[2];

		glm::mat3        PrimitiveBatch2D::myCurrent;

		Shader          *PrimitiveBatch2D::myShader;
		Stack<glm::mat3> PrimitiveBatch2D::myTransformStack;

		void PrimitiveBatch2D::Init() {
			myTransformStack.Push(glm::mat3());
			myCurrent = myTransformStack.Peek();
			myBufferCounts[0] = 0;
			myBufferCounts[1] = 0;

			const char *vs_source = R"LIT(
			#version 440
			layout(location = 0) uniform mat4 xViewProj;

			layout(location = 0) in vec2 Position;
			layout(location = 1) in vec4 Color;

			out vec4 Frag_Color;
			void main()
			{
				Frag_Color = Color;
				gl_Position = xViewProj * vec4(Position, 0 ,1);
			}
		)LIT";

			const char *fs_source =
				R"LIT(#version 330
				in vec4 Frag_Color;
				out vec4 Out_Color;
				void main()
				{
					Out_Color = Frag_Color;
				}
			)LIT";

			myShader = new Shader();
			myShader->LoadShaderPartFromSource(GL_VERTEX_SHADER, vs_source);
			myShader->LoadShaderPartFromSource(GL_FRAGMENT_SHADER, fs_source);
			myShader->Link();

			myTriVertices = new PrimitiveVert2D[RENDER2D_TRIS_SIZE];
			myLineVertices = new PrimitiveVert2D[RENDER2D_LINES_SIZE];

			glGenVertexArrays(2, myVaos);
			glGenBuffers(2, myVertexBuffers);

			glBindVertexArray(myVaos[0]);

			glBindBuffer(GL_ARRAY_BUFFER, myVertexBuffers[0]);
			glBufferData(GL_ARRAY_BUFFER, RENDER2D_TRIS_SIZE * sizeof(PrimitiveVert2D), myTriVertices, GL_DYNAMIC_DRAW);

			glEnableVertexAttribArray(0);
			glEnableVertexAttribArray(1);

			glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(PrimitiveVert2D), (GLvoid*)(0));
			glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(PrimitiveVert2D), (GLvoid*)(2 * sizeof(float)));

			glBindVertexArray(myVaos[1]);

			glBindBuffer(GL_ARRAY_BUFFER, myVertexBuffers[1]);
			glBufferData(GL_ARRAY_BUFFER, RENDER2D_LINES_SIZE * sizeof(PrimitiveVert2D), myLineVertices, GL_DYNAMIC_DRAW);

			glEnableVertexAttribArray(0);
			glEnableVertexAttribArray(1);

			glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(PrimitiveVert2D), (GLvoid*)(0));
			glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(PrimitiveVert2D), (GLvoid*)(2 * sizeof(float)));

			glBindVertexArray(0);
		}

		void PrimitiveBatch2D::PushTransform(const glm::mat3& transform) {
			myTransformStack.Push(myTransformStack.Peek() * transform);
			myCurrent = myTransformStack.Peek();
		}

		void PrimitiveBatch2D::PushOverride(const glm::mat3& transform) {
			myTransformStack.Push(transform);
			myCurrent = myTransformStack.Peek();
		}

		void PrimitiveBatch2D::PopTransform() {
			if (myTransformStack.Size() > 1)
				myTransformStack.Pop();

			myCurrent = myTransformStack.Peek();
		}

		void PrimitiveBatch2D::PushLine(const glm::vec2& pos1, const glm::vec2& pos2, const glm::vec4& color) {

			myLineVertices[myBufferCounts[1] + 0].Position = mult(pos1, myCurrent);
			myLineVertices[myBufferCounts[1] + 0].Color = color;
			myLineVertices[myBufferCounts[1] + 0].TexPower = 0.0f;
			myLineVertices[myBufferCounts[1] + 1].Position = mult(pos2, myCurrent);
			myLineVertices[myBufferCounts[1] + 1].Color = color;
			myLineVertices[myBufferCounts[1] + 1].TexPower = 0.0f;

			myBufferCounts[1] += 2;

			if (myBufferCounts[1] >= RENDER2D_LINES_SIZE)
				Flush();
		}

		void PrimitiveBatch2D::PushLine(const glm::vec2& pos1, const glm::vec2& pos2, const glm::vec4& fromColor, const glm::vec4& toColor) {

			myLineVertices[myBufferCounts[1] + 0].Position = mult(pos1, myCurrent);
			myLineVertices[myBufferCounts[1] + 0].Color = fromColor;
			myLineVertices[myBufferCounts[1] + 0].TexPower = 0.0f;
			myLineVertices[myBufferCounts[1] + 1].Position = mult(pos2, myCurrent);
			myLineVertices[myBufferCounts[1] + 1].Color = toColor;
			myLineVertices[myBufferCounts[1] + 1].TexPower = 0.0f;

			myBufferCounts[1] += 2;

			if (myBufferCounts[1] >= RENDER2D_LINES_SIZE)
				Flush();
		}

		void PrimitiveBatch2D::PushTriangle(const glm::vec2& pos1, const glm::vec2& pos2, const glm::vec2& pos3, const glm::vec4& color) {

			myTriVertices[myBufferCounts[0] + 0].Position = mult(pos1, myCurrent);
			myTriVertices[myBufferCounts[0] + 0].Color = color;
			myTriVertices[myBufferCounts[0] + 0].TexPower = 0.0f;
			myTriVertices[myBufferCounts[0] + 1].Position = mult(pos2, myCurrent);
			myTriVertices[myBufferCounts[0] + 1].Color = color;
			myTriVertices[myBufferCounts[0] + 1].TexPower = 0.0f;
			myTriVertices[myBufferCounts[0] + 2].Position = mult(pos3, myCurrent);
			myTriVertices[myBufferCounts[0] + 2].Color = color;
			myTriVertices[myBufferCounts[0] + 2].TexPower = 0.0f;

			myBufferCounts[0] += 3;

			if (myBufferCounts[0] >= RENDER2D_TRIS_SIZE)
				Flush();
		}

		void PrimitiveBatch2D::PushEllipse(const glm::vec2& center, const glm::vec2& size, const glm::vec4& color, uint8_t segmentRatio) {
			uint32_t count = segmentRatio * size.length() / 2.0f * 90;

			static glm::vec2 pos1 = glm::vec2();
			static glm::vec2 pos2 = glm::vec2();

			float radianTick = (M_PI * 2.0) / count;

			uint32_t ix2;
			for (uint32_t ix = 0, ix2 = 1; ix < count - 1; ix++, ix2++) {
				pos1.x = center.x + cos(ix * radianTick);
				pos1.y = center.y + sin(ix * radianTick);
				pos2.x = center.x + cos(ix2 * radianTick);
				pos2.y = center.y + sin(ix2 * radianTick);
				PushTriangle(center, pos1, pos2, color);
			}
			pos1.x = center.x + cos((count - 1) * radianTick);
			pos1.y = center.y + sin((count - 1) * radianTick);
			pos2.x = center.x + cos(0 * radianTick);
			pos2.y = center.y + sin(0 * radianTick);
			PushTriangle(center, pos1, pos2, color);
		}

		void PrimitiveBatch2D::Flush() {

			myShader->Enable();
			myShader->SetUniform(0, ViewProj);

			if (myBufferCounts[0] > 0) {
				glBindVertexArray(myVaos[0]);
				glBindBuffer(GL_ARRAY_BUFFER, myVertexBuffers[0]);
				glBufferSubData(GL_ARRAY_BUFFER, 0, myBufferCounts[0] * sizeof(PrimitiveVert2D), myTriVertices);
				glDrawArrays(GL_TRIANGLES, 0, myBufferCounts[0]);
				myBufferCounts[0] = 0;
			}

			if (myBufferCounts[1] > 0) {
				glBindVertexArray(myVaos[1]);
				glBindBuffer(GL_ARRAY_BUFFER, myVertexBuffers[1]);
				glBufferSubData(GL_ARRAY_BUFFER, 0, myBufferCounts[1] * sizeof(PrimitiveVert2D), myLineVertices);
				glDrawArrays(GL_LINES, 0, myBufferCounts[1]);
				myBufferCounts[1] = 0;
			}
		}

	}
}