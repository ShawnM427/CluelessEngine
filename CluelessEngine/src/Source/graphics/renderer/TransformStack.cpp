#pragma once
#ifndef TRANSFORM_STACK_CPP
#define TRANSFORM_STACK_CPP

#include "graphics/renderer/TransformStack.h"

namespace clueless { namespace graphics { namespace renderer {

	template <typename T>
	TransformStack<T>::TransformStack() {
		myQueue.Push(T());
	}

	template<typename T>
	TransformStack<T>::~TransformStack() {
		myQueue.Pop();
	}

	template<typename T>
	void TransformStack<T>::Push(T & transform, bool overwrite) {
		if (overwrite)
			myQueue.Push(transform);
		else
			myQueue.Push(transform * myQueue.Peek());  
	}

	template<typename T>
	void TransformStack<T>::Pop() {
		myQueue.Pop();
	}

	template<typename T>
	const T & TransformStack<T>::Current() const {
		return myQueue.Peek();
	}
		
}}}

#endif