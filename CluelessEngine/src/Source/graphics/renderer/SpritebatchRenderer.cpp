#include <graphics/renderer/SpritebatchRenderer.h>

#include <graphics/VertexLayouts.h>

namespace clueless { namespace graphics { namespace renderer {

    SpritebatchRenderer::SpritebatchRenderer()
    {
        //ctor
    }

    SpritebatchRenderer::~SpritebatchRenderer()
    {
        //dtor
    }

    void SpritebatchRenderer::Begin(){}

    void SpritebatchRenderer::Submit(const Renderable2D* renderable){}

    void SpritebatchRenderer::End(){}

    void SpritebatchRenderer::Flush(){}

} } }
