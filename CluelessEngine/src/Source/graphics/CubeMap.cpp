
#include "graphics/CubeMap.h"

#include <iostream>

#include <stb_image.h>

#include "utils/Logger.h"
#include "utils/common.h"

namespace clueless {	namespace graphics {

	CubeMap::~CubeMap() {
		Unload();
	}

	CubeMap::CubeMap(const char** fileNames, GLenum filterMode, GLenum wrapMode) {
		__Create(fileNames, filterMode, GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE, wrapMode);
	}
	
	void CubeMap::Unload() {
		if (myTextureHandle != 0) {
			// We want to delete our texture from the GPU
			FILE_LOG(logINFO) << "Destroying texture: " << myTextureHandle;
			glDeleteTextures(1, &myTextureHandle);
			myTextureHandle = 0;
		}
	}

	void CubeMap::Bind(GLenum slot) const {
		glActiveTexture(GL_TEXTURE0 + slot);
		glBindTexture(GL_TEXTURE_CUBE_MAP, myTextureHandle);

	}

	void CubeMap::__Create(const char** fileNames,
		const GLenum filterMode,
		const GLenum internalPixelType,
		const GLenum pixelFormat,
		const GLenum pixelType,
		const GLenum wrapMode) {
			glGenTextures(1, &myTextureHandle);
			glBindTexture(GL_TEXTURE_CUBE_MAP, myTextureHandle);

			__LoadSide(fileNames[0], GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, internalPixelType, pixelFormat, pixelType);
			__LoadSide(fileNames[1], GL_TEXTURE_CUBE_MAP_POSITIVE_Z, internalPixelType, pixelFormat, pixelType);
			__LoadSide(fileNames[2], GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, internalPixelType, pixelFormat, pixelType);
			__LoadSide(fileNames[3], GL_TEXTURE_CUBE_MAP_POSITIVE_Y, internalPixelType, pixelFormat, pixelType);
			__LoadSide(fileNames[4], GL_TEXTURE_CUBE_MAP_NEGATIVE_X, internalPixelType, pixelFormat, pixelType);
			__LoadSide(fileNames[5], GL_TEXTURE_CUBE_MAP_POSITIVE_X, internalPixelType, pixelFormat, pixelType);

			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		}

		void CubeMap::__LoadSide(const char* fileName, GLenum side,
			const GLenum internalPixelType,
			const GLenum pixelFormat,
			const GLenum pixelType) {

			int x, y, n;
			int force_channels = 4;
			unsigned char*  image_data = stbi_load(
				fileName, &x, &y, &n, force_channels);
			if (!image_data) {
				FILE_LOG(logERROR) << "Could not load " << fileName;
				return;
			}
			// non-power-of-2 dimensions check
			if ((x & (x - 1)) != 0 || (y & (y - 1)) != 0) {
				FILE_LOG(logWARNING) << "Image " << fileName << " is not power-of-2 dimensions\n";
			}

			// copy image data into 'target' side of cube map
			glTexImage2D(side, 0, internalPixelType, x, y, 0, pixelFormat, pixelType, image_data);
			free(image_data);
	}
} }