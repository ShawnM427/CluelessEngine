#include "graphics/Model.h"
#include "graphics/VertexLayouts.h"

namespace clueless { namespace graphics {

	using namespace std;

	Model::Model() {
		myName = "unknown";
	}

	Model::~Model() {

	}

	void Model::Draw()
	{
		for (GLuint i = 0; i < myMeshes.size(); i++)
			myMeshes[i].Draw();
	}
	
	void Model::Unload() {
		for (GLuint i = 0; i < this->myMeshes.size(); i++)
			myMeshes[i].Unload();
	}

	void Model::__HandleNode(const aiScene *scene, aiNode* node, Model& model) {
		for (GLuint i = 0; i < node->mNumMeshes; i++)
		{
			// the node object only contains indices to index the actual objects in the scene. 
			// the scene contains all the data, node is just to keep stuff organized (like relations between nodes).
			aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];

			FullVertex* vertices = new FullVertex[mesh->mNumVertices];
			USHORT*     indices = new USHORT[mesh->mNumFaces * 3];

			// Walk through each of the mesh's vertices
			for (unsigned int i = 0; i < mesh->mNumVertices; i++)
			{
				FullVertex vertex;
				glm::vec3 vector; // we declare a placeholder vector since assimp uses its own vector class that doesn't directly convert to glm's vec3 class so we transfer the data to this placeholder glm::vec3 first.
								  // positions
				vector.x = mesh->mVertices[i].x;
				vector.y = mesh->mVertices[i].y;
				vector.z = mesh->mVertices[i].z;
				vertex.Position = vector;
				// normals
				vector.x = mesh->mNormals[i].x;
				vector.y = mesh->mNormals[i].y;
				vector.z = mesh->mNormals[i].z;
				vertex.Normal = vector;
				// texture coordinates
				if (mesh->mTextureCoords[0]) // does the mesh contain texture coordinates?
				{
					glm::vec2 vec;
					// a vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't 
					// use models where a vertex can have multiple texture coordinates so we always take the first set (0).
					vec.x = mesh->mTextureCoords[0][i].x;
					vec.y = mesh->mTextureCoords[0][i].y;
					vertex.Texture = vec;
				}
				else
					vertex.Texture = glm::vec2(0.0f, 0.0f);
				//// tangent
				if (mesh->mTangents) {
					vector.x = mesh->mTangents[i].x;
					vector.y = mesh->mTangents[i].y;
					vector.z = mesh->mTangents[i].z;
					vertex.Tangent = vector;
				}
				else {
					vertex.Tangent = glm::vec3(0.0f);
				}
				//// bitangent
				if (mesh->mTangents) {
					vector.x = mesh->mBitangents[i].x;
					vector.y = mesh->mBitangents[i].y;
					vector.z = mesh->mBitangents[i].z;
					vertex.BiTangent = vector;
				}
				else {
					vertex.BiTangent = glm::vec3(0.0f);
				}

				vertices[i] = vertex;
			}
			// now walk through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
			for (GLuint i = 0; i < mesh->mNumFaces; i++)
			{
				aiFace face = mesh->mFaces[i];
				// retrieve all indices of the face and store them in the indices vector
				for (GLuint j = 0; j < face.mNumIndices; j++)
					indices[i * 3 + j] = face.mIndices[j];
			}

			USHORT vCount, iCount;
			vCount = mesh->mNumVertices;
			iCount = mesh->mNumFaces * 3;
			MeshData data(vertices, vCount, indices, iCount, &FullVertexDecl);
			Mesh resultMesh(data);
			
			model.myMeshes.push_back(resultMesh);
		}

		for (int ix = 0; ix < node->mNumChildren; ix++) {
			__HandleNode(scene, node->mChildren[ix], model);
		}
	}

	Model* Model::LoadFromNode(const aiScene *scene, aiNode *node)
	{
		Model *result = new Model();

		// process each mesh located at the current node
		result->myName = node->mName.C_Str();

		__HandleNode(scene, node, *result);

		return result;
	}
} }
