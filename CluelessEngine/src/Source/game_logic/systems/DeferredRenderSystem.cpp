#include "game_logic/systems/DeferredRenderSystem.h"

#include "GraphicsConfig.h"

#include "graphics/Shader.h"
#include "graphics/Mesh.h"
#include "graphics/GeoUtils.h"

#include "game_logic/Scene.h"

namespace clueless { namespace game_logic { namespace systems {

	using namespace graphics;

	void DeferredRenderSystem::Init() {
		myBackBuffer = new GBuffer(GraphicsConfig::BackBufferWidth, GraphicsConfig::BackBufferHeight);

		MeshData quadData = geo_utils::CreateSprite(glm::vec2(1.0f, 1.0f), 0.0f, 0.0f, 1.0f, 1.0f);
		myQuad = new Mesh(quadData);

		myPostShader = CMS::GetToken(CmsType::TypeShader, "DeferredPost");
		myForwardShader = CMS::GetToken(CmsType::TypeShader, "DeferredForward");

		Shader* shader = CMS::Redeem<Shader>(myPostShader);

		myLightBuffer = new UniformBuffer();

		int numLights = shader->GetBlockElementCount<PointLightData>("xLights");
		shader->SetBufferBindingPoint("xLights", 1);		
		myLightBuffer->Init<PointLightData>(1U, numLights);

	}

	void DeferredRenderSystem::AddLightSpline(LightSpline *spline) {
		LightSpline **newArray = new LightSpline*[myLightSplineCount + 1];
		if (myLightSplines) {
			for (int ix = 0; ix < myLightSplineCount; ix++)
				newArray[ix] = myLightSplines[ix];
			delete[] myLightSplines;
		}
		myLightSplines = newArray;
		myLightSplines[myLightSplineCount] = spline;
		myLightSplineCount++;
	}

	PointLightData* DeferredRenderSystem::GetLight(int index)
	{
		if (index < 0 || index >= myLightBuffer->ElementCount())
			return nullptr;
		else
			return myLightBuffer->GetData<PointLightData>(index);
	}
	
	void DeferredRenderSystem::PreRender() {
		myBackBuffer->Bind();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

		glEnable(GL_DEPTH_TEST);

		CMS::Redeem<Shader>(myForwardShader)->Enable(); 
	}

	void DeferredRenderSystem::Render(GameObject * object) {
		object->Render();
	}

	void DeferredRenderSystem::PostRender() {

		GBuffer::Unbind();

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

		Shader* shader = CMS::Redeem<Shader>(myPostShader);
		
		glPolygonMode(GL_FRONT, GL_FILL);

		shader->Enable();
		shader->SetUniform("xNumActiveLights", myNumActiveLights);
		shader->SetUniform("xAmbientLight", glm::vec3(0.1f));

		for (int ix = 0; ix < myLightSplineCount; ix++) {
			LightSpline *spline = myLightSplines[ix];
			PointLightData *light = GetLight(ix);
			if (light) {
				if (spline->RedChannel)
					spline->RedChannel->Resolve(&light->Specular.r, fmod(GameTime::TotalTime, spline->RedChannel->GetLength()));
				if (spline->GreenChannel)
					spline->GreenChannel->Resolve(&light->Specular.g, fmod(GameTime::TotalTime, spline->GreenChannel->GetLength()));
				if (spline->BlueChannel)
					spline->BlueChannel->Resolve(&light->Specular.b, fmod(GameTime::TotalTime, spline->BlueChannel->GetLength()));
				if (spline->Attenuation)
					spline->Attenuation->Resolve(&light->Attenuation, fmod(GameTime::TotalTime, spline->Attenuation->GetLength()));
			}
		}

		myLightBuffer->Update();

		shader->SetUniform("xColorTransform", glm::mat4());
		shader->SetUniform("xSampler", 9);
		shader->SetUniform("xNormals", 10);
		shader->SetUniform("xDepth", 11);
		shader->SetUniform("xEmissive", 12);
		shader->SetUniform("xSpecular", 13);
		shader->SetUniform("xWorldSpace", 14);
		shader->SetUniform("xTexSize", glm::vec2(GraphicsConfig::ScreenWidth, GraphicsConfig::ScreenHeight));
		shader->SetUniform("xView", Scene::GetActiveCamera()->GetView());

		glActiveTexture(GL_TEXTURE9);
		glBindTexture(GL_TEXTURE_2D, myBackBuffer->GetTextureHandle());

		glActiveTexture(GL_TEXTURE10);
		glBindTexture(GL_TEXTURE_2D, myBackBuffer->GetNormalTextureHandle());

		glActiveTexture(GL_TEXTURE11);
		glBindTexture(GL_TEXTURE_2D, myBackBuffer->GetDepthTexture());

		glActiveTexture(GL_TEXTURE12);
		glBindTexture(GL_TEXTURE_2D, myBackBuffer->GetEmissiveTexture());

		glActiveTexture(GL_TEXTURE13);
		glBindTexture(GL_TEXTURE_2D, myBackBuffer->GetSpecularTexture());

		glActiveTexture(GL_TEXTURE14);
		glBindTexture(GL_TEXTURE_2D, myBackBuffer->GetWorldSpaceTexture());

		glViewport(0, 0, GraphicsConfig::ScreenWidth, GraphicsConfig::ScreenHeight);

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_ALWAYS);

		myQuad->NoMaterialDraw();

		glDepthFunc(GL_LESS);

		/*
		glBindFramebuffer(GL_READ_FRAMEBUFFER, myBackBuffer->GetFboHandle());
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
		glBlitFramebuffer(0, 0, myBackBuffer->GetWidth(), myBackBuffer->GetHeight(), 0, 0, GraphicsConfig::ScreenWidth, GraphicsConfig::ScreenHeight,
			GL_DEPTH_BUFFER_BIT, GL_NEAREST);
		*/ 
	}

} } }
