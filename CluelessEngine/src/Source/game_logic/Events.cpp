#include <iostream>
#include <functional>
#include <map>
#include <vector>

#include "game_logic/Events.h"
#include "utils/Logger.h"
#include "globals.h"

namespace clueless {

    // Re-declare the static members because c++ is weird like that
    Events Events::mySingleton;
    std::map<const char*, std::vector<std::function<void()>>> Events::myEvents;

    void Events::RegisterEventCallback(const char* key, std::function<void()> callback) {
        Events::myEvents[key].push_back(callback);
    }

    void Events::InvokeEvent(const char* key) {
        std::vector<std::function<void()>> callbacks = Events::myEvents[key];

        if (callbacks.size() == 0) {
            FILE_LOG(logWARNING) << "Invoked callback with name " << key << std::endl;
        }

        for(uint i = 0; i < callbacks.size(); i ++) {
            callbacks[i]();
        }
    }

    void Events::DumpEventList() {
        for(auto const &pair : Events::myEvents) {
            std::cout << pair.first << std::endl;
        }
    }

}
