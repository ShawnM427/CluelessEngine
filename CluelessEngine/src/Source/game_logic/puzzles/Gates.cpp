#include "game_logic/puzzles/Gates.h"

Gates::Gates(bool in1, bool in2)
{
	this->in1 = in1;
	this->in2 = in2;
}
bool Gates::getIn1() {
	return in1;
}
bool Gates::getIn2() {
	return in2;
}
bool Gates::getOutput()
{
	return false;
}


Gates::~Gates()
{
}
