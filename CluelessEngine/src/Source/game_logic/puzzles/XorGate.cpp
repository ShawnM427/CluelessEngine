#include "game_logic/puzzles/XorGate.h"

XorGate XorGate::inputs(bool in1, bool in2){
	input1 = in1;
	input2 = in2;
	if (in1 == false && in2 == false) {
		output = false;
	}
	else if (in1 == true && in2 == true) {
		output = false;
	}
	else {
		output = true;
	}
	return XorGate();
}
