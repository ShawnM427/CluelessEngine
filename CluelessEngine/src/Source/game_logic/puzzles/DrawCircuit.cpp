#include <iostream>
#include <string>
#include "game_logic/puzzles/DrawCircuit.h"

DrawCircuit::DrawCircuit(int num)
{
	circuitNum = num;
}
Circuits c;
//gives puzzle information
void DrawCircuit::Draw(bool in1, bool in2, bool in3, bool in4, bool in5)
{
	if (circuitNum == 1) {
		puzzle1(in1, in2, in3, in4, in5);
	}
	else if (circuitNum == 2) {
		puzzle2(in1, in2, in3, in4, in5);
	}
	else if (circuitNum == 3) {
		puzzle3(in1, in2, in3, in4);
	}
}

void DrawCircuit::puzzle1(bool in1, bool in2, bool in3, bool in4, bool in5)
{
	c.circuit1(in1, in2, in3, in4, in5);
	//shows states for switches
	for (int i = 0; i < c.switches.size(); i++) {
		std::cout << "switch" << i + 1 << " input: " << c.switches[i].input << std::endl;
		std::cout << "switch" << i + 1 << " output: " << c.switches[i].output << std::endl;
	}
	//shows states for the And gates
	for (int i = 0; i < c.and.size(); i++) {
		std::cout << "And" << i + 1 << " input: " << c.and[i].input1 << std::endl;
		std::cout << "And" << i + 1 << " input2: " << c.and[i].input2 << std::endl;
		std::cout << "And" << i + 1 << " output: " << c.and[i].output << std::endl;
	}
	//states for the Or gates
	std::cout << "Or1" << " input: " << c. or [0].input1 << std::endl;
	std::cout << "Or1" << " input2: " << c. or [0].input2 << std::endl;
	std::cout << "Or1" << " output: " << c. or [0].output << std::endl;
	//states for the Xor gates
	for (int i = 0; i < c.xor.size(); i++) {
		std::cout << "Xor" << i + 1 << " input: " << c.xor[i].input1 << std::endl;
		std::cout << "Xor" << i + 1 << " input2: " << c.xor[i].input2 << std::endl;
		std::cout << "Xor" << i + 1 << " output: " << c.xor[i].output << std::endl;
	}
	//
	std::cout << "Diode" << " output: " << c.diode[0].finalOutput << std::endl;
	if (c.diode[0].finalOutput == true)
		complete = true;
	else
		complete = false;
}

void DrawCircuit::puzzle2(bool in1, bool in2, bool in3, bool in4, bool in5)
{
	c.circuit2(in1, in2, in3, in4, in5);
	//shows states for switches
	for (int i = 0; i < c.switches.size(); i++) {
		std::cout << "switch" << i + 1 << " input: " << c.switches[i].input << std::endl;
		std::cout << "switch" << i + 1 << " output: " << c.switches[i].output << std::endl;
	}
	//shows states for the And gates
	for (int i = 0; i < c.and.size(); i++) {
		std::cout << "And" << i + 1 << " input: " << c.and[i].input1 << std::endl;
		std::cout << "And" << i + 1 << " input2: " << c.and[i].input2 << std::endl;
		std::cout << "And" << i + 1 << " output: " << c.and[i].output << std::endl;
	}
	//states for the Or gates
	std::cout << "Or1" << " input: " << c. or [0].input1 << std::endl;
	std::cout << "Or1" << " input2: " << c. or [0].input2 << std::endl;
	std::cout << "Or1" << " output: " << c. or [0].output << std::endl;
	//states for the Xor gates
	for (int i = 0; i < c.xor.size(); i++) {
		std::cout << "Xor" << i + 1 << " input: " << c.xor[i].input1 << std::endl;
		std::cout << "Xor" << i + 1 << " input2: " << c.xor[i].input2 << std::endl;
		std::cout << "Xor" << i + 1 << " output: " << c.xor[i].output << std::endl;
	}
	//
	for (int i = 0; i < c.diode.size(); i++) {
		std::cout << std::endl;
		std::cout << "Diode" << i + 1 << " output: " << c.diode[i].finalOutput << std::endl;
	}
	
	if (c.diode[0].finalOutput == true && c.diode[1].finalOutput == true && c.diode[2].finalOutput == true)
		complete = true;
	else
		complete = false;
}

void DrawCircuit::puzzle3(bool in1, bool in2, bool in3, bool in4)
{
	c.circuit3(in1, in2, in3, in4);
	//shows states for switches
	for (int i = 0; i < c.switches.size(); i++) {
		std::cout << "switch" << i + 1 << " input: " << c.switches[i].input << std::endl;
		std::cout << "switch" << i + 1 << " output: " << c.switches[i].output << std::endl;
	}
	//shows states for the And gates
	for (int i = 0; i < c.and.size(); i++) {
		std::cout << "And" << i + 1 << " input: " << c.and[i].input1 << std::endl;
		std::cout << "And" << i + 1 << " input2: " << c.and[i].input2 << std::endl;
		std::cout << "And" << i + 1 << " output: " << c.and[i].output << std::endl;
	}
	//states for the Or gates
	std::cout << "Or1" << " input: " << c. or [0].input1 << std::endl;
	std::cout << "Or1" << " input2: " << c. or [0].input2 << std::endl;
	std::cout << "Or1" << " output: " << c. or [0].output << std::endl;
	//states for the Xor gates
	for (int i = 0; i < c.xor.size(); i++) {
		std::cout << "Xor" << i + 1 << " input: " << c.xor[i].input1 << std::endl;
		std::cout << "Xor" << i + 1 << " input2: " << c.xor[i].input2 << std::endl;
		std::cout << "Xor" << i + 1 << " output: " << c.xor[i].output << std::endl;
	}
	//states of the diodes
	for (int i = 0; i < c.diode.size(); i++) {
		std::cout << std::endl;
		std::cout << "Diode" << i + 1 << " output: " << c.diode[i].finalOutput << std::endl;
	}

	if (c.diode[0].finalOutput == true && c.diode[1].finalOutput == true && c.diode[2].finalOutput == true)
		complete = true;
	else
		complete = false;
}
