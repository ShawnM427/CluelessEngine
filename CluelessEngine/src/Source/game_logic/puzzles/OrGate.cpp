#include "game_logic/puzzles/OrGate.h"

OrGate OrGate::inputs(bool in1, bool in2)
{
	input1 = in1;
	input2 = in2;
	if (in1 == false && in2 == false) {
		output = false;
	}
	else if (in1 == true && in2 == true) {
		output = true;
	}
	else {
		output = true;
	}
	return OrGate();
}
