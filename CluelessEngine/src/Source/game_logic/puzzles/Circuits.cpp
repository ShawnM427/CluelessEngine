#include <iostream>
#include <string>
#include "game_logic/puzzles/Circuits.h"

Circuits::Circuits()
{

}
Circuits::~Circuits()
{

}


void Circuits::circuit1(bool st1, bool st2, bool st3, bool st4, bool st5) {
	//switches
	Switch s1, s2, s3, s4, s5;
	//diode
	Diode d1;
	//and gate
	andGate and1, and2, and3, and4, and5;
	//or gate
	OrGate or1;
	//xor gate
	XorGate xor1, xor2;

	//setting the switches inputs
	s1.SetSwitch(st1);//true
	s2.SetSwitch(st2);//true
	s3.SetSwitch(st3);
	s4.SetSwitch(st4);
	s5.SetSwitch(st5);//true

	//sending inputs into the gates
	and1.inputs(s1.output, s2.output);
	xor1.inputs(s1.output, s3.output);
	or1.inputs(s2.output, s3.output);
	and2.inputs(s4.output, s5.output);
	and3.inputs(and1.output, xor1.output);
	xor2.inputs(and2.output, s5.output);
	and4.inputs(or1.output, xor2.output);
	and5.inputs(and4.output, and3.output);

	//getting the final output
	d1.finalOutput = and5.output;

	//storing the objects into arrays
	switches = { s1,s2,s3,s4,s5 };
	and = { and1,and2,and3,and4,and5 };
	or = { or1 };
	xor = { xor1,xor2 };
	diode = { d1 };

}

void Circuits::circuit2(bool st1, bool st2, bool st3, bool st4, bool st5) {
	//switches
	Switch s1, s2, s3, s4, s5;
	//diode
	Diode d1, d2, d3;
	//and gates
	andGate and1, and2, and3, and4;
	//or gate
	OrGate or1;
	//xor gates
	XorGate xor1, xor2;

	//setting switch inputs
	s1.SetSwitch(st1);//
	s2.SetSwitch(st2);//
	s3.SetSwitch(st3);
	s4.SetSwitch(st4);//
	s5.SetSwitch(st5);

	//giving outputs to the gates
	and1.inputs(s2.output, s4.output);
	xor1.inputs(and1.output, s4.output);
	or1.inputs(s1.output, s5.output);
	xor2.inputs(xor1.output, s5.output);
	and2.inputs(s1.output, s3.output);
	and3.inputs(and2.output, xor1.output);
	and4.inputs(or1.output, xor2.output);

	//getting the final outputs
	d1.finalOutput = and4.output;
	d2.finalOutput = and3.output;
	d3.finalOutput = xor2.output;

	//stores objects into arrays
	switches = { s1,s2,s3,s4,s5 };
	and = { and1,and2,and3,and4 };
	or = { or1 };
	xor = { xor1,xor2 };
	diode = { d1, d2, d3 };
}

void Circuits::circuit3(bool st1, bool st2, bool st3, bool st4) {
	Switch s1, s2, s3, s4;
	Diode d1, d2, d3;
	andGate and1;
	OrGate  or1;
	XorGate xor1;

	s1.SetSwitch(st1);
	s2.SetSwitch(st2);
	s3.SetSwitch(st3);
	s4.SetSwitch(st4);

	xor1.inputs(s1.output, s2.output);
	or1.inputs(s3.output, s4.output);
	and1.inputs(xor1.output, or1.output);

	d1.finalOutput = xor1.output;
	d2.finalOutput = and1.output;
	d3.finalOutput = or1.output;

	switches = { s1,s2,s3,s4 };
	and = { and1 };
	or = { or1 };
	xor = { xor1 };
	diode = { d1,d2,d3 };
}

void Circuits::circuit4(bool s1, bool s2, bool s3, bool s4, bool s5) {

}

void Circuits::circuit5(bool sw1, bool sw2, bool sw3, bool sw4)
{
	Switch s1, s2, s3, s4;
	Diode d1;
	andGate and1, and2;
	OrGate or1;
	XorGate xor1;

	s1.SetSwitch(sw1);
	s2.SetSwitch(sw2);
	s3.SetSwitch(sw3);
	s4.SetSwitch(sw4);

	and1.inputs(s2.output, s3.output);
	xor1.inputs(s1.output, and1.output);
	or1.inputs(and1.output, s4.output);
	and2.inputs(xor1.output, or1.output);

	d1.finalOutput = and2.output;

	switches = { s1,s2,s3,s4 };
	and = { and1, and2 };
	xor = { xor1 };
	or = { or1 };
	diode = { d1 };
}
