#include "game_logic/puzzles/Diode.h"

//aka the final output of the puzzle, defines if puzzle is done or not
void Diode::diode(bool in1) {
	if (in1 == false) {
		finalOutput = false;
	}
	else {
		finalOutput = true;
	}
}
