#include "game_logic/components/PhysicsInteractComponent.h"

#include "graphics/PrimitiveBatch3D.h"
#include "ui/input/InputManager.h"

#include "game_logic/components/InteractionComponent.h"

#include "physics/EventStructs.h"

namespace clueless { namespace game_logic { namespace components {

	void PhysicsInteractComponent::HandleMessage(const MessageEvent& message, GameComponent *dispatchedFrom) {
		if (message.TypeCode == EVENT_PHYSICS_INTERACT) {
			physics::PhysicsInteract data = *message.GetMeta<physics::PhysicsInteract>();

			GameObject *object = static_cast<GameObject*>(data.Object->getUserPointer());
			InteractionComponent *interaction = nullptr;

			if (object)
				interaction = object->GetComponent<InteractionComponent>();

			if (interaction) {
				interaction->Invoke();
			}
			else {
				if (data.Object->getInternalType() == btCollisionObject::CO_RIGID_BODY) {
					btRigidBody *body = (btRigidBody*)data.Object;
					if (!body->isStaticObject()) {
						body->activate(true);
						body->applyImpulse(glm2bt(data.HitNormal) * -10.0f * GameTime::FrameDeltaTime, glm2bt(data.HitPointLocal));
					}
				}
			}
		}

		GameComponent::HandleMessage(message, dispatchedFrom);
	}


} } }
