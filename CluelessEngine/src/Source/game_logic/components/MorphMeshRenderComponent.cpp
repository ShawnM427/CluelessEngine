#include "game_logic/components/MorphMeshRenderComponent.h"

#include "game_logic/Scene.h";

namespace clueless { namespace game_logic{
	using namespace graphics;

	MorphMeshRenderComponent::MorphMeshRenderComponent(GameObject * parent, MorphMesh * mesh, Material* material) :
		GameComponent(parent), myModel(mesh), myMaterial(material) {
	}


	MorphMeshRenderComponent::~MorphMeshRenderComponent() { }
	
	graphics::MorphMesh* MorphMeshRenderComponent:: getMorphMesh()
	{
		return myModel;
	}


	void MorphMeshRenderComponent::Render() {
		myModel->Update(GameTime::FrameDeltaTime);

		if (myMaterial != nullptr) {
			myMaterial->Apply();
			myMaterial->SetModelTransform(glm::mat4());
			myMaterial->SetWorldTransform(myObject->GetWorldTransform());
			myMaterial->SetProjectionMatrix(Scene::GetActiveCamera()->GetProjection());
			myMaterial->SetViewMatrix(Scene::GetActiveCamera()->GetView());
		}

		myModel->Draw();
	}
} }
