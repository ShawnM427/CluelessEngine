#include "game_logic/components/RigidBodyComponent.h"

#include "game_logic/Scene.h"

namespace clueless { namespace game_logic { namespace components {

	RigidBodyComponent::RigidBodyComponent(GameObject* parent, physics::RigidBody* body) :
		GameComponent(parent)
	{
		myBody = body;

		btTransform transform;
		glm::mat4 parentTrans = parent->GetWorldTransform();
		myBody->SetTransform(parentTrans);
	}
	
	void RigidBodyComponent::AddedToScene() {
		myBody->GetInternalBody()->setUserPointer(myObject);
		Scene::AddCollisionItem(myBody);  
	}

	//void RigidBodyComponent::PrePhysics() {
		//if (myObject)
		//	myBody->SetTransform(myObject->GetWorldTransform());
	//}


	physics::RigidBody* RigidBodyComponent::getBody()
	{
		return myBody;
	}

	void RigidBodyComponent::FixedUpdate() {
		glm::vec3 Scale = myObject->LocalTransform.Scale;

		if (myObject)
			myObject->SetWorldTransform(myBody->GetTransform());

		myObject->LocalTransform.Scale = Scale;
	}

	void RigidBodyComponent::SetTransform(Transform & value) {
		if (myObject)
			myBody->SetTransform(value);
	}

} } } 