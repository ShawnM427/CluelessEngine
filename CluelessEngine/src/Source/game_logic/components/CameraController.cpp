#include "game_logic/components/CameraController.h"

#include "graphics/PrimitiveBatch3D.h"
#include "ui/input/InputManager.h"

#include "physics/EventStructs.h"

namespace clueless { namespace game_logic { namespace components {

	using namespace ui::input;

	CameraController::CameraController(GameObject * parent) :
		myRoll(0.0f), myPitch(0.0f), myYaw(11.0f),
		Sensitivity(0.05f, 0.05f),
		GameComponent(parent), myRotSpeed(20.0f) {

	}

	CameraController::~CameraController() {

	}

	void CameraController::SetMouseLook(bool value) {
		myMouseLook = value;
		InputManager::SetCursorMode(myMouseLook ? GLFW_CURSOR_DISABLED : GLFW_CURSOR_NORMAL);
	}

	void CameraController::FixedUpdate() {
		btDynamicsWorld *world = Scene::GetPhysicsWorld()->GetInternalWorld();

		glm::vec3 from = myObject->LocalTransform.Position;
		glm::vec3 to = myObject->LocalTransform.Position + Forward * 2.0f;
		btCollisionWorld::ClosestRayResultCallback RayCallback(glm2bt(from), glm2bt(to));
		world->rayTest(RayCallback.m_rayFromWorld, RayCallback.m_rayToWorld, RayCallback);

		if (RayCallback.hasHit()) {
			if (InputManager::GetMouseButton(GLFW_MOUSE_BUTTON_LEFT) == ButtonState::ButtonHeld) {
				physics::PhysicsInteract data;
				data.HitNormal = bt2glm(RayCallback.m_hitNormalWorld);
				data.HitPointWorld = bt2glm(RayCallback.m_hitPointWorld);
				data.HitPointLocal = bt2glm(RayCallback.m_collisionObject->getWorldTransform().inverse() * RayCallback.m_hitPointWorld);
				data.Object = RayCallback.m_collisionObject;

				Dispatch(MessageEvent(EVENT_PHYSICS_INTERACT, data, MessageHandle_Dispatch2Way));
			}
		}
	}

	void CameraController::Update() {

		static float prevX = 0;
		static float prevY = 0;

		float changeX = 0;
		float changeY = 0;

		if (!myRotSpeed)
			myRotSpeed = 0.1f;

		MouseDelta = InputManager::GetMouseDelta();

		float mainSpeed = GameTime::DeltaTime * 2.0f;
		float strafeSpeed = GameTime::DeltaTime * 1.5f;

		//if (abs(mouseDelta.x) > 1000.0f | abs(mouseDelta.y) > 1000.0f) { // hack to prevent jumps 
		//	mouseDelta.x = 0;
		//	mouseDelta.y = 0;
		//}

		if (myMouseLook) {
			changeX += MouseDelta.x * Sensitivity.x;
			changeY -= MouseDelta.y * Sensitivity.y;
		}

		if (InputManager::GetKeyState(GLFW_KEY_LEFT) != ButtonState::ButtonReleased)
			changeX -= 1.0f;

		if (InputManager::GetKeyState(GLFW_KEY_RIGHT) != ButtonState::ButtonReleased)
			changeX += 1.0f;

		if (InputManager::GetKeyState(GLFW_KEY_UP) != ButtonState::ButtonReleased)
			changeY += 1.0f;

		if (InputManager::GetKeyState(GLFW_KEY_DOWN) != ButtonState::ButtonReleased)
			changeY -= 1.0f;

		if ((abs(changeX) >= 200.0f) || (abs(changeY) >= 200.0f)) { // hack to prevent jumps 
			changeX = 0;
			changeY = 0;
		}

		myPitch += (0.2f * changeY) * GameTime::FrameDeltaTime;
		myYaw -= (0.2f * changeX) * GameTime::FrameDeltaTime;


		if (myPitch <=-90.0f)
		{
			myPitch = -88.0f;
		}

		if (myPitch >= 90.0f)
		{
			myPitch = 88.0f;
		}

		glm::quat rot = glm::quat();

		rot *= glm::angleAxis(glm::radians(myYaw),   glm::vec3(0.0f, 0.0f, 1.0f));
		rot *= glm::angleAxis(glm::radians(myPitch), glm::vec3(0.0f, 1.0f, 0.0f));
		rot *= glm::angleAxis(glm::radians(myRoll),  glm::vec3(1.0f, 0.0f, 0.0f));
		rot *= glm::quat(glm::vec3(glm::radians(90.0f), 0.0f, glm::radians(90.0f)));
		
		myObject->LocalTransform.Orientation = rot;// = glm::inverse(transform);

		glm::vec3 forward = glm::vec3(0.0f, 0.0f, -1.0f);
		glm::vec3 up      = glm::vec3(0.0f, 1.0f, 0.0f);
		glm::vec3 right   = glm::vec3(1.0f, 0.0f, 0.0f);
		
		forward = rot * forward;
		up      = rot * up;
		right   = rot * right;

		Forward = forward;

	}

} } }
