#include "game_logic/components/SkinnedMeshRenderComponent.h"

#include "game_logic/Scene.h"

namespace clueless { namespace game_logic { namespace components {

	SkinnedMeshRenderComponent::SkinnedMeshRenderComponent(GameObject * parent, graphics::SkinnedMesh * mesh) :
		GameComponent(parent), myMesh(mesh)
	{ }

	SkinnedMeshRenderComponent::~SkinnedMeshRenderComponent() {

	}

	void SkinnedMeshRenderComponent::Render() {
		myMesh->Draw(
			myObject->GetWorldTransform(), 
			Scene::GetActiveCamera()->GetView(), 
			Scene::GetActiveCamera()->GetProjection());
	}

} } }