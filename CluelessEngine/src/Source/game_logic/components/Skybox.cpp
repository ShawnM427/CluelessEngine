#include "game_logic/components/Skybox.h"

#include "graphics/GeoUtils.h"

#include "cms/CMS.h"

#include "game_logic/Scene.h"

namespace clueless { namespace game_logic { namespace components {

	using namespace clueless;
	using namespace clueless::graphics;
	
	void Skybox::Load() {
		FILE_LOG(logDEBUG) << "\tSky Shader";
		myShader = CMS::GetToken(CmsType::TypeShader, "Skybox");
		
		myCubeMap = CMS::GetToken(TypeCubeMap, "space_purple");

		{
			MeshData skyboxData = geo_utils::CreateTextureCube(glm::vec3(1.0f), glm::vec3(0.0f), glm::vec4(1.0f), geo_utils::TextureModeFace, true);
			myGeometry = new Mesh(skyboxData);
		}
	}

	void Skybox::Render() {
		glDisable(GL_DEPTH_TEST);

		CubeMap* cubeMap = CMS::Redeem<CubeMap>(myCubeMap);
		
		if (cubeMap)
			cubeMap->Bind(31U);

		Shader* shader = CMS::Redeem<Shader>(myShader);
		if (shader) {
			ICamera* cam = Scene::GetActiveCamera();
			shader->Enable();
			shader->SetUniform("xSampler", 31);
			shader->SetUniform("xTransform", cam->GetProjection() * glm::mat4(glm::mat3(cam->GetView())));
		}

		myGeometry->NoMaterialDraw();

		glEnable(GL_DEPTH_TEST);
	}

	void Skybox::Unload() {
		// TODO: unload geometry if not tied to CMS
		if (myGeometry != nullptr) {
			delete myGeometry;
			myGeometry = nullptr;
		}
	}

} } }