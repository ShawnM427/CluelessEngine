#include "ImGuiQuestSystem.h"
#include <imgui/imgui.h>
#include <imgui/imgui_impl.h>
#include <imgui/imgui_internal.h>
#include "ui/input/InputManager.h"
#include "game_logic/components/CameraController.h"

#include "graphics/FontRenderer.h"

#include "ui/TextureFont.h"

#include <string>

namespace clueless {
	namespace game_logic {
		namespace systems {

			using namespace ui::input; 

			using namespace components;

			bool ImGuiQuestSystem::open = false;
			bool ImGuiQuestSystem::showPrompt = true;


			std::string ImGuiQuestSystem::quest = "Testing 123";

			std::string ImGuiQuestSystem::questDetails = "blah";

			std::string ImGuiQuestSystem::task = "test 2";

			ImGuiQuestSystem::ImGuiQuestSystem()
			{
				myFontToken = CMS::GetToken(TypeFont, "font");
			}
			

			ImGuiQuestSystem::~ImGuiQuestSystem()
			{

			}

			void ImGuiQuestSystem::Init()
			{
						
			}

			void ImGuiQuestSystem::PostRender()
			{

				ICamera* cam = Scene::GetActiveCamera();
				CameraController *controller = cam->GetParent()->GetComponent<CameraController>();

				if (showPrompt) {		
					ui::TextureFont *font = CMS::Redeem<ui::TextureFont>(myFontToken);
					if (font)
						graphics::FontRenderer::Render(*font, "Press <Tab> to open W.R.A.P", glm::vec2(100, 520), glm::vec4(1.0f), 0.75f);
				}

				//ImGui::NewFrame();
				if (open) {

					if (ImGui::Begin("QuestBook", &open, ImGuiWindowFlags_AlwaysAutoResize)) {
						ImGui::Text(quest.c_str());
						ImGui::Separator();
						ImGui::Text(questDetails.c_str());
						ImGui::Text("Tasks");
						ImGui::Text(task.c_str());
						
					}
					ImGui::End();
				}
			}

		}
	}
}