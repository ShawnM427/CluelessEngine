#include "anim/AnimationStructs.h"

namespace clueless { namespace anim {

		void ChannelMeta::WriteToFile(std::fstream & stream) {
			Write(stream, (uint32_t)0);
			Write(stream, NameLength);
			Write(stream, Name, NameLength);
			Write(stream, NumKeyframes);
			Write(stream, KeyFrameType);
			Write(stream, KeyFrameSize);
		}

		void ChannelMeta::ReadFromFile(std::fstream & stream)
		{
			Read(stream, DataOffset);
			Read(stream, NameLength);
			Name = new char[NameLength];
			Read(stream, Name, NameLength);
			Read(stream, NumKeyframes);
			Read(stream, KeyFrameType);
			Read(stream, KeyFrameSize);
		}

		void AnimationHeader::WriteToFile(std::fstream & stream) {
			Write(stream, HeaderCode, 4);
			Write(stream, MajorVersion);
			Write(stream, MinorVersion);
			uint32_t headerSizeLoc = stream.tellg();
			Write(stream, (uint32_t)0);
			Write(stream, TotalLength);
			Write(stream, NameLength);
			Write(stream, Name, NameLength);
			Write(stream, ChannelCount);

			uint32_t *headerLocs = new uint32_t[ChannelCount];

			for (int ix = 0; ix < ChannelCount; ix++) {
				headerLocs[ix] = stream.tellg();
				ChannelMetas[ix].WriteToFile(stream);
			}

			uint32_t endofHeaders = stream.tellg();
			stream.seekg(headerSizeLoc);
			Write(stream, endofHeaders);
			stream.seekg(endofHeaders);

			for (int ix = 0; ix < ChannelCount; ix++) {
				uint32_t offset = stream.tellg();
				stream.seekg(headerLocs[ix]);
				Write(stream, offset);
				stream.seekg(offset);
				for (int jx = 0; jx < ChannelMetas[ix].NumKeyframes; jx++) {
					Write(stream, ChannelMetas[ix].Data[jx].Time);
					Write(stream, ChannelMetas[ix].Data[jx].Value, ChannelMetas[ix].KeyFrameSize);
				}
			}

			delete[] headerLocs;
		}

		void AnimationHeader::ReadFromFile(std::fstream & stream) {
			Read(stream, HeaderCode, 4);

			if (HeaderCode[0] != 'A' || HeaderCode[1] != 'N' ||
				HeaderCode[2] != 'I' || HeaderCode[3] != 'M')
				return;

			Read(stream, MajorVersion);
			Read(stream, MinorVersion);

			// Todo: switch on version
			Read(stream, DataOffset);
			Read(stream, TotalLength);
			Read(stream, NameLength);
			Name = new char[NameLength];
			Read(stream, Name, NameLength);
			Read(stream, ChannelCount);

			ChannelMetas = new ChannelMeta[ChannelCount];

			for (int ix = 0; ix < ChannelCount; ix++) {
				ChannelMetas[ix].ReadFromFile(stream);
				ChannelMetas[ix].Data = new ChannelKeyframe[ChannelMetas[ix].NumKeyframes];
			}

			for (int ix = 0; ix < ChannelCount; ix++) {
				uint32_t offset = ChannelMetas[ix].DataOffset;
				stream.seekg(offset);
				for (int jx = 0; jx < ChannelMetas[ix].NumKeyframes; jx++) {
					Read(stream, ChannelMetas[ix].Data[jx].Time);
					ChannelMetas[ix].Data[jx].Value = malloc(ChannelMetas[ix].KeyFrameSize);
					Read(stream, ChannelMetas[ix].Data[jx].Value, ChannelMetas[ix].KeyFrameSize);
				}
			}
		}

}
}
