#include "anim/PoseProviders.h"

namespace clueless { namespace anim {

	PoseData PoseFrame::GetJointTransform(uint16_t jointId) const
	{
		return PoseTransforms[jointId];
	}


	PoseData PartialPoseFrame::GetJointTransform(uint16_t jointId) const
	{
		for (int ix = 0; ix < JointCount; ix++)
			if (JointIds[ix] == jointId)
				return PoseTransforms[ix];
		return PoseData();
	}

}
}