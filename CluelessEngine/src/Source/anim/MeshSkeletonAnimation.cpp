#include "anim/MeshSkeletonAnimation.h"

#include "anim/AnimationMath.h"

namespace clueless { namespace anim {

	MeshSkeletonAnimation::MeshSkeletonAnimation() :
		Frames(nullptr),
		FrameCount(0),
		Name("<unknown>"),
		myCurrentFrame(0),
		myNextFrame(0)
	{ }


	MeshSkeletonAnimation::MeshSkeletonAnimation(PoseProvider_ptr * frames, int frameCount) :
		MeshSkeletonAnimation()
	{
		assert(frameCount > 0);
		assert(frames);
		assert(frames[0]);

		JointCount = frames[0]->JointCount;

		for (int ix = 0; ix < frameCount; ix++) {
			assert(frames[ix]);
			assert(frames[ix]->JointCount == JointCount);
		}

		Frames     = frames;
		FrameCount = frameCount;
	}

	MeshSkeletonAnimation::~MeshSkeletonAnimation() {

	}

	void MeshSkeletonAnimation::Update(float dt) {
		myTime += dt * mySpeedMult;

	}

	PoseProvider* MeshSkeletonAnimation::GetCurrentFrame() const {
		return Frames[myCurrentFrame];
	}

	PoseProvider * MeshSkeletonAnimation::GetNextFrame() const {
		return Frames[myNextFrame];
	}

	PoseData MeshSkeletonAnimation::GetJointTransform(uint16_t jointId) const {
		assert(jointId >= 0);
		assert(jointId < JointCount);
		if (myCurrentFrame != myNextFrame) {
			float tVal = (myTime - GetCurrentFrame()->Time) / (GetNextFrame()->Time - GetCurrentFrame()->Time);
			PoseData left = GetCurrentFrame()->GetJointTransform(jointId);
			PoseData right = GetNextFrame()->GetJointTransform(jointId);
			return interpolate(left, right, tVal);
		}
		else {
			return GetCurrentFrame()->GetJointTransform(jointId);
		}
	}

}}