#include "anim/PoseData.h"

namespace clueless { namespace anim {

	PoseData::operator glm::mat4() const {
		return glm::translate(Origin) * glm::mat4_cast(Rotation) *  glm::scale(glm::vec3(Length));
	}


	PoseData PoseData::operator +(const PoseData& right) const {
		PoseData result;
		result.Origin   = Origin + right.Origin;
		result.Rotation = Rotation * right.Rotation;
		result.Length = Length;// + right.Length;
		return result;
	}
	PoseData PoseData::operator -(const PoseData& right) const {
		PoseData result;
		result.Origin   = Origin - right.Origin;
		result.Rotation = Rotation * glm::conjugate(right.Rotation);
		result.Length = Length;// +right.Length;
		return result;
	}

	PoseData& PoseData::operator+=(const PoseData & right) {
		Origin   += right.Origin;
		Rotation *= right.Rotation;
		//Length   += right.Length;
		return *this;
	}

	PoseData& PoseData::operator-=(const PoseData & right) {
		Origin   -= right.Origin;
		Rotation *= glm::conjugate(right.Rotation);
		//Length   -= right.Length;
		return *this;
	}

} }