#include "anim/MeshSkeleton.h"

#include <assert.h> 

namespace clueless { namespace anim {

	MeshSkeleton::MeshSkeleton(MeshBone * rootBone) {
		myRootBone = rootBone;

		if (myRootBone) {
			myBoneCount = myRootBone->GetTotalChildCount();
			myPose = Pose(myBoneCount);
		}
		else {
			myBoneCount = 0;
		}
	}

	void MeshSkeleton::Apply(const PoseProvider * poseSource) {
		myPose.ReplaceWith(poseSource);
	}

	void MeshSkeleton::ApplyTo(glm::mat4 * dataSet) {
		myRootBone->ApplyToDataSet(myPose.Poses, dataSet);
	}
	

}}
