#include "anim/HTRLoader.h"
#include <iostream>

namespace clueless { namespace anim {

	#define MAX_LINE_LENGTH 128

	HTRLoader::HTRLoader() {

	}

	bool HTRLoader::LoadHTR(std::string fileName, MeshSkeleton *&skeleton, MeshSkeletonAnimation *&animation)
	{
		myFrameCount = 0;
		myRootBone = nullptr;
		myBones.clear();
		myJointDescriptors.clear();

		FILE* file;
		file = fopen(fileName.c_str(), "r");

		char* loc;
		char buffer[MAX_LINE_LENGTH];
		int bufferSize = 0;

		if (file)
		{
			// Find line with header
			// fgets() will read to new line OR MAX_LINE_LENGTH, which ever happens first
			// Returns pointer to buffer OR null pointer for end of file
			while (fgets(buffer, MAX_LINE_LENGTH, file) != NULL)
			{
				// Get location of "Header" in file
				loc = strstr(buffer, "Header");

				// Process headers
				if (loc)
				{
					if (!processHeaderSection(file, loc))
						break;

					if (!processSegementNameSection(file, loc))
						break;

					if (!processBasePositionSection(file, loc)) // TODO: IMPLEMENT THIS FUNCTION
						break;

					if (!processAnimationSection(file, loc))
						break;
				}
			}

			fclose(file);	// Remember to close file
			
			CreateBones();
			GetRootBone();
			skeleton = new MeshSkeleton(myRootBone);
			animation = packAnimation();

			return true;
		}
		else
		{
			std::cout << "LOAD HTR ERROR: " << fileName << " not found or cannot be opened." << std::endl;
			assert(0);
			return false;
		}
	}

	JointDescriptor* HTRLoader::GetJointDescriptorByName(std::string jointName)
	{
		int numJoints = myJointDescriptors.size();

		for (int i = 0; i < numJoints; i++)
		{
			if (jointName == myJointDescriptors[i].jointName)
				return &myJointDescriptors[i];
		}

		return nullptr;
	}

	MeshBone* HTRLoader::GetBoneByName(std::string jointName)
	{
		int numJoints = myBones.size();

		for (int i = 0; i < numJoints; i++)
		{
			if (jointName == myBones[i]->Name)
				return myBones[i];
		}

		return nullptr;
	}

	MeshBone * HTRLoader::GetRootBone()

	{
		if (myRootBone == nullptr)
		{
			for (int i = 0; i < myBones.size(); i++)
			{
				if (myBones[i]->ParentId == -1)
					myRootBone = *(myBones.data() + i);
			}
		}
		return myRootBone;
	}

	void HTRLoader::CreateBones()
	{
		// Todo:
		// Create all of the Bones
		// Loop through each Joint descriptor and create game object for children
		for (uint16_t i = 0; i < myJointDescriptors.size(); i++)
		{
			MeshBone *newBone = new MeshBone(i);
			newBone->Name = myJointDescriptors[i].jointName;

			//newJoint->m_pJointAnimation = &jointDescriptors[i];
			myBones.push_back(newBone);
		}

		// Now that we have GameObjects for each joint, we need to set up
		// the kinematic linkage

		MeshBone* child;
		MeshBone* parent;

		// Set up the hierarchy 
		for (int i = 0; i < myJointDescriptors.size(); i++)
		{
			JointDescriptor* jd = &myJointDescriptors[i];

			child = GetBoneByName(jd->jointName);
			parent = GetBoneByName(jd->jointParent);

			if (child == nullptr || parent == nullptr)
				continue;

			child->ParentId = parent->BoneId;
			parent->AddChild(child);

		}
	}
	
	std::vector<MeshBone*> HTRLoader::GetBones()
	{
		return myBones;
	}

	MeshSkeletonAnimation* HTRLoader::packAnimation()
	{

		MeshSkeletonAnimation::PoseProvider_ptr *frames = new MeshSkeletonAnimation::PoseProvider_ptr[myFrameCount];

		uint16_t numJoints = myJointDescriptors.size();

		for (int ix = 0; ix < myFrameCount; ix++) {
			PoseFrame* poseFrame = new PoseFrame();

			poseFrame->JointCount     = numJoints - 1;
			poseFrame->PoseTransforms = new PoseData[numJoints - 1];
			for (int jx = 1; jx < numJoints; jx++) {
				poseFrame->PoseTransforms[jx-1].Origin   = myJointDescriptors[jx-1].jointPositions[ix];
				poseFrame->PoseTransforms[jx-1].Rotation = myJointDescriptors[jx-1].jointRotations[ix];
				poseFrame->PoseTransforms[jx-1].Length   = myJointDescriptors[jx-1].jointScales[ix];
			}

			frames[ix] = poseFrame;
		}

		return new MeshSkeletonAnimation(frames, myFrameCount);
	}

	bool HTRLoader::processHeaderSection(FILE* fp, char* loc)
	{
		char buffer[MAX_LINE_LENGTH];

		// File currently on line with "Header"
		// Go to the next line since it actually has data
		goToNextValidLineInFile(fp, buffer);

		// Keep processing headers until we make it to the next section
		// Once that happens, we will begin processing the new section
		do
		{
			// Allocate memory for values
			char identifierBuff[64];
			char valueBuff[64];

			// Split the current line (buffer)
			sscanf(buffer, "%s %s", identifierBuff, valueBuff);

			// Figure out which string identifier goes with which member variable in the class
			processHeader(std::string(identifierBuff), std::string(valueBuff));

			// Increment to next valid line
			goToNextValidLineInFile(fp, buffer);

			// Check if current line is the start of the next section
			// strstr returns null if second param isn't found in first
			loc = strstr(buffer, "SegmentNames");

		} while (loc == NULL);

		return true;
	}

	bool HTRLoader::processHeader(std::string identifier, std::string value)
	{
		if (identifier == "FileType")
		{
			HeaderData.fileType = value;
			return true;
		}
		else if (identifier == "DataType")
		{
			HeaderData.dataType = value;
			return true;
		}
		else if (identifier == "FileVersion")
		{
			// remember fileVersion is an int
			// atoi converts a string to an int
			HeaderData.fileVersion = std::atoi(value.c_str());
			return true;
		}
		else if (identifier == "NumSegments")
		{
			HeaderData.numSegments = std::atoi(value.c_str());
			return true;
		}
		else if (identifier == "NumFrames")
		{
			HeaderData.numFrames = std::atoi(value.c_str());
			return true;
		}
		else if (identifier == "DataFrameRate")
		{
			HeaderData.fps = std::atoi(value.c_str());
			return true;
		}
		else if (identifier == "EulerRotationOrder")
		{
			if (value == "XYZ")
				HeaderData.rotationOrder = RotationOrder::XYZ;
			else if (value == "ZYX")
				HeaderData.rotationOrder = RotationOrder::ZYX;
			else
			{
				std::cout << "HTR Parse Warning: File has unsupported rotation order of: " << value << std::endl;
			}

			// put conditions for the other rotation orders here...
		}
		else if (identifier == "CalibrationUnits")
		{
			if (value == "mm")
			{
				HeaderData.calibrationUnits = CalibrationUnits::Millimeters;
				HeaderData.unitsScaleFactor = 0.1;
			}
			else if (value == "cm")
			{
				HeaderData.calibrationUnits = CalibrationUnits::Centimeters;
				HeaderData.unitsScaleFactor = 1.0f;
			}
			else
			{
				std::cout << "HTR Parse Warning: File has unsupported units: " << value << std::endl;
			}
		}
		else if (identifier == "RotationUnits")
		{
			HeaderData.rotationUnits = value;
		}
		else if (identifier == "GlobalAxisofGravity")
		{

		}
		else if (identifier == "BoneLengthAxis")
		{

		}
		else if (identifier == "ScaleFactor")
		{

		}
		else
		{
			std::cout << "HTR Parse Error: Could not identify HEADER: " << identifier << " with value: " << value << std::endl;
			return false;
		}

	}

	bool HTRLoader::processSegementNameSection(FILE * fp, char* loc)
	{
		char buffer[MAX_LINE_LENGTH];

		// Process segment names
		// File currently on line with "SegmentNames"
		goToNextValidLineInFile(fp, buffer);
		do
		{
			// Allocate memory for values
			char childBuff[64];
			char parentBuff[64];

			// Parse values from current line 
			sscanf(buffer, "%s %s", childBuff, parentBuff);

			// Create new joint descriptor
			JointDescriptor jd;

			// Set joint descriptor values
			jd.jointName = childBuff;
			jd.jointParent = parentBuff;

			// Store the new joint
			myJointDescriptors.push_back(jd);

			// Increment to next valid line
			goToNextValidLineInFile(fp, buffer);

			// Check if current line is the start of the next section
			// strstr returns null if second param isn't found in first
			loc = strstr(buffer, "BasePosition");

		} while (loc == NULL);

		return true;
	}

	bool HTRLoader::processBasePositionSection(FILE* fp, char* loc)
	{
		char buffer[MAX_LINE_LENGTH];

		// Process base positions
		// File currently on line with "BasePosition"
		// increment to next line with actual data
		goToNextValidLineInFile(fp, buffer);
		do
		{
			char jointNameBuff[64];
			float tx, ty, tz;
			float rx, ry, rz;
			float boneLength;

			// TODO:
			// 'buffer' contains the current line
			// Parse this line and store the data in the temporary variables above
			// I'd recommend the "sscanf" function.
			sscanf(buffer, "%s %f %f %f %f %f %f %f %f", jointNameBuff, &tx, &ty, &tz, &rx, &ry, &rz, &boneLength);

			switch (HeaderData.calibrationUnits) {
				case CalibrationUnits::Millimeters:
					tx /= 1000;
					ty /= 1000;
					tz /= 1000;
					break;
				case CalibrationUnits::Centimeters:
					tx /= 100;
					ty /= 100;
					tz /= 100;
					break;
			}

			// Find the joint descriptor by name
			JointDescriptor* jd = GetJointDescriptorByName(jointNameBuff);

			// Make sure we got it
			if (!jd)
			{
				std::cout << "HTR Parse ERROR: Could not find JointDescriptor with name: " << std::string(jointNameBuff) << std::endl;
				return false;
			}

			// store the parsed values in the appropriate locations
			jd->jointBasePosition = glm::vec3(tx, ty, tz) * HeaderData.unitsScaleFactor; // Note the scale factor multiplication. See 'unitsScaleFactor' definition for details.
			jd->jointBaseRotation = createQuaternion(rx, ry, rz);
			jd->boneLength = boneLength;

			// Increment to next valid line
			goToNextValidLineInFile(fp, buffer);

			// Check if current line is the start of the next section
			// strstr returns null if second param isn't found in first
			loc = strstr(buffer, myJointDescriptors[0].jointName.c_str());
		} while (loc == NULL);
	}

	bool HTRLoader::processAnimationSection(FILE* fp, char* loc)
	{
		char buffer[MAX_LINE_LENGTH];

		// Process each joint's poses at each frame
		for (int i = 0; i < myJointDescriptors.size(); i++)
		{
			goToNextValidLineInFile(fp, buffer);
			do
			{
				// TODO: 
				// - create temporary variables to store the data
				// - parse the line (I recommend sscanf)
				// - store the data in the appropriate location
				int    frameId;
				float  tX, tY, tZ;
				float  rX, rY, rZ;
				float  scale;

				sscanf(buffer, "%i %f %f %f %f %f %f %f", &frameId, &tX, &tY, &tZ, &rX, &rY, &rZ, &scale);
				
				switch (HeaderData.calibrationUnits) {
					case CalibrationUnits::Millimeters:
						tX /= 1000;
						tY /= 1000;
						tZ /= 1000;
						break;
					case CalibrationUnits::Centimeters:
						tX /= 100;
						tY /= 100;
						tZ /= 100;
						break;
				}

				myJointDescriptors[i].jointPositions.push_back(glm::vec3(tX, tY, tZ));
				myJointDescriptors[i].jointRotations.push_back(createQuaternion(rX, rY, rZ));
				myJointDescriptors[i].jointScales.push_back(scale);

				myJointDescriptors[i].numFrames = frameId;

				myFrameCount = frameId > myFrameCount ? frameId : myFrameCount;

				// Increment to next valid line
				goToNextValidLineInFile(fp, buffer);

				// Check if current line is the start of the next section
				// Remember strstr returns null if second param isn't found in first
				int nextJointIndex = i + 1;

				if (nextJointIndex < myJointDescriptors.size())
					loc = strstr(buffer, myJointDescriptors[nextJointIndex].jointName.c_str());
				else
				{
					//break;
					loc = strstr(buffer, "EndOfFile");
				}
			} while (loc == NULL);
		}

		return true;
	}

	void HTRLoader::goToNextValidLineInFile(FILE* fp, char* buffer)
	{
		fgets(buffer, MAX_LINE_LENGTH, fp);

		// Move stream to next valid line
		// Remember: fgets will split on tabs
		// Remember: '#' is comment symbol
		while (buffer[0] == '\t' || buffer[0] == '#')
			fgets(buffer, MAX_LINE_LENGTH, fp);
	}

	glm::quat HTRLoader::createQuaternion(float rx, float ry, float rz)
	{
		// If rotation units is degrees, convert to radians
		if (HeaderData.rotationUnits == "Degrees")
		{
			rx = glm::radians(rx);
			ry = glm::radians(ry);
			rz = glm::radians(rz);
		}

		// Construct unit quaternions for each axis
		// The reason why we create 3 quaternions for each axis is so we can control
		// the multiplication order.
		glm::quat qX = glm::angleAxis(rx, glm::vec3(1.0f, 0.0f, 0.0f));
		glm::quat qY = glm::angleAxis(ry, glm::vec3(0.0f, 1.0f, 0.0f));
		glm::quat qZ = glm::angleAxis(rz, glm::vec3(0.0f, 0.0f, 1.0f));

		// Check for rotation order
		if (HeaderData.rotationOrder == RotationOrder::XYZ)
			return qX * qY * qZ;
		else if (HeaderData.rotationOrder == RotationOrder::ZYX)
			return qZ * qY * qX;
		// ... put other rotation orders here ...
		else
		{
			std::cout << "HTR Parse Warning: File has unsupported rotation order" << std::endl;
		}

		// Return XYZ by default
		return qZ * qY * qX;
	}

} }