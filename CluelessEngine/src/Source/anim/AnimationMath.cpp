#ifndef ANIM_MATH
#define ANIM_MATH
 
#include "anim/AnimationMath.h"

namespace glm {
	float distance(const clueless::Transform& left, const clueless::Transform& right) {
		return glm::distance(left.Position, right.Position) +
			glm::distance((float)left.Orientation.length(), (float)right.Orientation.length()) +
			glm::distance(left.Scale, right.Scale);
	}
}
namespace clueless { namespace anim {

	template <>
	Transform lerp<Transform>(Transform left, Transform right, float t) {
		Transform result;
		result.Position = lerp(left.Position, right.Position, t);
		result.Orientation = glm::slerp(left.Orientation, right.Orientation, t);
		result.Scale = lerp(left.Scale, right.Scale, t);
		return result;
	}

	PoseData interpolate(const PoseData & left, const PoseData & right, float tVal) {
		PoseData result;
		result.Origin = glm::mix(left.Origin, right.Origin, tVal);
		result.Length = glm::mix(left.Length, right.Length, tVal);
		result.Rotation = glm::slerp(left.Rotation, right.Rotation, tVal);
		return result;
	}

} }

#endif