#include "anim/MeshBone.h"

namespace clueless { namespace anim {


	MeshBone::MeshBone() :
		ParentId(-1),
		Children(nullptr),
		ChildCount(0),
		BoneId(0)
	{
	}
	
	MeshBone::MeshBone(uint16_t boneId) :
		ParentId(-1),
		Children(nullptr),
		ChildCount(0),
		BoneId(boneId)
	{
	}

	MeshBone::MeshBone(MeshBone *parent, uint16_t boneId) :
		ParentId(parent->BoneId),
		Children(nullptr),
		ChildCount(0),
		BoneId(boneId) {

	}

	MeshBone::~MeshBone() {
		if (Children) {
			free(Children);
		}
		ParentId = -1;
		Children = nullptr;
		ChildCount = 0;
	}

	void MeshBone::AddChild(MeshBone* child) {
		ChildCount++;

		MeshBone** newArray = new MeshBone*[ChildCount];

		if (Children) {
			for (int ix = 0; ix < ChildCount - 1; ix++)
				newArray[ix] = Children[ix];
			delete[] Children;
		}
		newArray[ChildCount - 1] = child;

		Children = newArray;
	}

	void MeshBone::Update(glm::mat4 *finalTransforms, const glm::mat4 *localPoseTransforms) const {
		if (ParentId != -1)
			finalTransforms[BoneId] = localPoseTransforms[BoneId] * finalTransforms[ParentId];
		else
			finalTransforms[BoneId] = localPoseTransforms[BoneId];

		for (int ix = 0; ix < ChildCount; ix++) {
			Children[ix]->Update(finalTransforms, localPoseTransforms);
		}

	}

	uint16_t MeshBone::GetTotalChildCount() const
	{
		uint16_t result = ChildCount;

		if (Children) {
			for (int ix = 0; ix < ChildCount; ix++)
				Children[ix]->__CalcTotalBones(result);
		}

		return result;
	}

	void MeshBone::ApplyToDataSet(PoseData *poses, glm::mat4 *dataSet) {
		if (ParentId != -1)
			dataSet[BoneId] = (poses[ParentId] + poses[BoneId]);
		else
			dataSet[BoneId] = poses[BoneId];

		for (int ix = 0; ix < ChildCount; ix++)
			Children[ix]->ApplyToDataSet(poses, dataSet);
	}

	void MeshBone::__CalcTotalBones(uint16_t & count) {
		count += ChildCount;

		if (Children) {
			for (int ix = 0; ix < ChildCount; ix++)
				Children[ix]->__CalcTotalBones(count);
		}
	}
	
} }