#include "anim/MeshSkeletonPose.h"

#include "anim/AnimationMath.h"

namespace clueless { namespace anim {


	Pose::Pose() :
		Poses(nullptr),
		JointCount(0)
	{ }

	Pose::Pose(uint16_t numJoints) {
		Poses = new PoseData[numJoints];
		JointCount = numJoints;
	}

	Pose::~Pose() {
		if (Poses)
			delete[] Poses;

		Poses = nullptr;
		JointCount = 0;
	}

	void Pose::BlendWith(const PoseProvider *provider) {
		for (int ix = 0; ix < JointCount; ix++)
			Poses[ix] += provider->GetJointTransform(ix);
	}

	void Pose::BlendWith(const Pose * pose) {
		assert(pose->JointCount == JointCount);

		for (int ix = 0; ix < JointCount; ix++)
			Poses[ix] += pose->Poses[ix];
	}

	void Pose::ReplaceWith(const PoseProvider * provider) {
		for (int ix = 0; ix < JointCount; ix++)
			Poses[ix] = provider->GetJointTransform(ix);
	}

	void Pose::InterpolateBetween(const PoseProvider *left, const PoseProvider *right, float tVal) {
		for (int ix = 0; ix < JointCount; ix++)
			Poses[ix] = interpolate(left->GetJointTransform(ix), right->GetJointTransform(ix), tVal);
	}

	void Pose::InterpolateBetween(const Pose *left, const Pose *right, float tVal) {
		assert(left->JointCount == right->JointCount & left->JointCount == JointCount);

		for (int ix = 0; ix < JointCount; ix++)
			Poses[ix] = interpolate(left->Poses[ix], right->Poses[ix], tVal);
	}

	void Pose::Zero() {
		for (int ix = 0; ix < JointCount; ix++)
			Poses[ix] = PoseData();
	}

}}