#include "ui/BitmapFont.h"

#include <fstream>
#include <stdexcept>
#include <iostream>

#include "utils/common.h"
#include "utils/Logger.h"
#include "graphics/common.h"

namespace clueless { namespace ui {

	using namespace clueless::graphics;

	BitmapFont::BitmapFont(const char* fontFileName) {

		using namespace std;

		myGlyphs = new Glyph[BitmapFont::CHAR_COUNT];
		myKernings = new char*[BitmapFont::CHAR_COUNT];

		for (int ix = 0; ix < BitmapFont::CHAR_COUNT; ix++) {
			myKernings[ix] = new char[BitmapFont::CHAR_COUNT];
			memset(myKernings[ix], 0, BitmapFont::CHAR_COUNT);
		}

		ifstream inputFile;
		inputFile.open(fontFileName, ios::in | ios::binary);

		if (inputFile.good()) {

			char* verify = new char[4];

			inputFile.read(verify, 4);

			if (verify[0] != 'B' || verify[1] != 'M' || verify[2] != 'F') {
				inputFile.close();
				throw runtime_error("Could not validate file");
			}

			if (verify[3] != 3) {
				inputFile.close();
				throw runtime_error("Incorrect font file version, should be 3");
			}

			long headerEnd = inputFile.tellg();
			long fileLength = headerEnd;
			inputFile.seekg(0, ios::end);
			fileLength = (long)inputFile.tellg() - fileLength;
			inputFile.seekg(headerEnd, ios::beg);

			while (inputFile.tellg() < fileLength) {
				char blockType = 0;
				int  blockSize = 0;
				inputFile.read(&blockType, 1);
				inputFile.read(reinterpret_cast<char*>(&blockSize), 4);

				int blockStartLoc = inputFile.tellg();

				char *blockData;
				blockData = new char[blockSize];
				inputFile.read(blockData, blockSize);

				switch (blockType) {
				case 1: // INFO BLOCK
				{
					myFontSize = *reinterpret_cast<int*>(blockData);
					myFontName = new char[blockSize - 14];
					memcpy(myFontName, blockData + 14, blockSize - 14);
				}

				break;

				case 2: // COMMON BLOCK
				{
					myLineHeight = *reinterpret_cast<short*>(blockData);
					myLineBase = *reinterpret_cast<short*>(blockData + 2);
					myScaleX = *reinterpret_cast<short*>(blockData + 4);
					myScaleY = *reinterpret_cast<short*>(blockData + 6);
				}
				break;

				case 3: // PAGES BLOCK
				{
					char* fileName;

					int nullIx = 0;
					while (blockData[nullIx] != '\0')
						nullIx++;
					nullIx++;

					int pathLength = 0;
					const char* path = getFilePath(fontFileName, pathLength);

					fileName = new char[pathLength + nullIx];
					memcpy(fileName, path, pathLength);
					memcpy(fileName + pathLength, blockData, nullIx);

					__LoadTextureFile(fileName);
				}

				break;

				case 4:  // GLYPH BLOCK
				{
					int numChars = blockSize / 20;

					for (int ix{ 0 }, off{ 0 }; ix < numChars; ix++) {
						off = ix * 20;
						int character = *reinterpret_cast<int*>(blockData + off + 0);

						Glyph toAdd;
						toAdd.X = *reinterpret_cast<short*>(blockData + off + 4);
						toAdd.Y = *reinterpret_cast<short*>(blockData + off + 6);
						toAdd.Width = *reinterpret_cast<short*>(blockData + off + 8);
						toAdd.Height = *reinterpret_cast<short*>(blockData + off + 10);
						toAdd.XOff = *reinterpret_cast<short*>(blockData + off + 12);
						toAdd.YOff = *reinterpret_cast<short*>(blockData + off + 14);
						toAdd.XAdvance = *reinterpret_cast<short*>(blockData + off + 16);

						myGlyphs[character] = toAdd;
					}
				}
				break;

				case 5:  // KERNING BLOCK
				{
					int numPairs = blockSize / 10;

					for (int ix{ 0 }, off{ 0 }; ix < numPairs; ix++) {
						off = ix * 10;
						char left = *reinterpret_cast<int*>(blockData + 0 + off);
						char right = *reinterpret_cast<int*>(blockData + 4 + off);
						myKernings[left][right] = *reinterpret_cast<short*>(blockData + 8 + off);
					}
				}
				break;
				}

				delete[] blockData;

				if (blockStartLoc + blockSize >= fileLength)
					inputFile.seekg(0, ios::end);
				else {
					inputFile.seekg(blockStartLoc + blockSize);
				}

			}
			inputFile.close();
		}
		else {
			throw runtime_error("Failed to open file");
		}

		GLenum error = glGetError();
		if (error != 0)
			std::cout << error << std::endl;
	}

	BitmapFont::~BitmapFont() {
		delete myTexture;
		delete myGlyphs;
		delete myKernings;
	}

	void BitmapFont::GetCharMetrics(int codepoint, int *xAdvance, int *xOff, int *yOff, int* gWidth, int *gHeight) const {
		*xAdvance = myGlyphs[codepoint].XAdvance;
		*xOff     = myGlyphs[codepoint].XOff;
		*yOff     = myGlyphs[codepoint].YOff;
		*gWidth   = myGlyphs[codepoint].Width;
		*gHeight  = myGlyphs[codepoint].Height;
	}
	int  BitmapFont::GetKerning(int char1, int char2) const {
		return myKernings[char1][char2];
	}
	void BitmapFont::GetCharBounds(int inChar, float *x, float *y, float *x2, float* y2) const {
		*x  = myGlyphs[inChar].X / (float)myTexture->GetWidth();
		*y  = myGlyphs[inChar].Y / (float)myTexture->GetHeight();
		*x2 = (myGlyphs[inChar].X + myGlyphs[inChar].Width) / (float)myTexture->GetWidth();
		*y2 = (myGlyphs[inChar].Y + myGlyphs[inChar].Height) / (float)myTexture->GetWidth();
	}

	int BitmapFont::GetCharXAdvance(const char codepoint) const {
		return myGlyphs[codepoint].XAdvance + myGlyphs[codepoint].XOff;
	}

	void BitmapFont::__LoadTextureFile(const char* fileName) {

		myTexture = new Texture(fileName);

		GLenum error = glGetError();
		if (error != 0)
			throw gl_exception("Failed to create texture");
	}
	
	#ifdef DEBUG
	void BitmapFont::DBG_DumpKernings() const {

		std::stringstream stream;

		for (int ix = 0; ix < BitmapFont::CHAR_COUNT; ix++)
			for (int kx = 0; kx < BitmapFont::CHAR_COUNT; kx++)
				stream << (char)ix << " - " << (char)kx << ": " << (char)(myKernings[ix][kx]) << std::endl;

		FILE_LOG(logINFO) << stream.str();
	}
	#endif

} }