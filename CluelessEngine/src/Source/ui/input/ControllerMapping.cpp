#include "ui/input/ControllerMapping.h"

namespace clueless { namespace ui { namespace input {

	// Utility function for making the known mappings
    std::map<std::string, ControllerMapping> __MakeKnownMappings() {

        std::map<std::string, ControllerMapping> result;

		/* Hard-coded implementation for xBox 360 controller*/
        ControllerMapping xBox = ControllerMapping("Xbox 360 Controller");
        xBox.AddAxisLookup("LSTICK_X",       0, "Left Stick - X");
        xBox.AddAxisLookup("LSTICK_Y",       1, "Left Stick - Y");
        xBox.AddAxisLookup("RSTICK_X",       2, "Right Stick - X");
        xBox.AddAxisLookup("RSTICK_Y",       3, "Right Stick - Y");
        xBox.AddAxisLookup("LTRIGGER",       4, "Left Trigger");
        xBox.AddAxisLookup("RTRIGGER",       5, "Right Trigger");

        xBox.AddButtonLookup("A",            0, "A");
        xBox.AddButtonLookup("B",            1, "B");
        xBox.AddButtonLookup("X",            2, "X");
        xBox.AddButtonLookup("Y",            3, "Y");
        xBox.AddButtonLookup("LBUMPER",      4, "Left Bumper");
        xBox.AddButtonLookup("RBUMPER",      5, "Right Bumper");
        xBox.AddButtonLookup("BACK",         6, "Back");
        xBox.AddButtonLookup("START",        7, "Start");
        xBox.AddButtonLookup("LSTICK_HAT",   8, "Left Stick - Click");
        xBox.AddButtonLookup("RSTICK_HAT",   9, "Right Stick - Click");
        xBox.AddButtonLookup("DPAD_UP",     10, "D-Pad Up");
        xBox.AddButtonLookup("DPAD_RIGHT",  11, "D-Pad Right");
        xBox.AddButtonLookup("DPAD_DOWN",   12, "D-Pad Down");
        xBox.AddButtonLookup("DPAD_LEFT",   13, "D-Pad Left");

		/* Hard-coded implementation for the Saitek x52 */
        ControllerMapping x52 = ControllerMapping("Saitek X52 Flight Control System");
        x52.AddAxisLookup("STICK_ROLL",   0);
        x52.AddAxisLookup("STICK_PITCH",  1);
        x52.AddAxisLookup("THROTTLE",     2);
        x52.AddAxisLookup("THUMBWHEEL_I", 3);
        x52.AddAxisLookup("THUMBWHEEL_E", 4);
        x52.AddAxisLookup("STICK_YAW",    5);
        x52.AddAxisLookup("SLIDER",       6);

        x52.AddButtonLookup("TRIGGER_HALF",         0);
        x52.AddButtonLookup("HAT_MISSILE",          1);
        x52.AddButtonLookup("HAT_A",                2);
        x52.AddButtonLookup("HAT_B",                3);
        x52.AddButtonLookup("HAT_C",                4);
        x52.AddButtonLookup("THROTTLE_D",           6);
        x52.AddButtonLookup("THROTTLE_E",           7);
        x52.AddButtonLookup("TOGGLE_1_UP",          8);
        x52.AddButtonLookup("TOGGLE_1_DOWN",        9);
        x52.AddButtonLookup("TOGGLE_2_UP",         10);
        x52.AddButtonLookup("TOGGLE_2_DOWN",       11);
        x52.AddButtonLookup("TOGGLE_3_UP",         12);
        x52.AddButtonLookup("TOGGLE_3_DOWN",       13);
        x52.AddButtonLookup("TRIGGER_FULL",        14);
        x52.AddButtonLookup("HAT_DPAD_1_UP",       15);
        x52.AddButtonLookup("HAT_DPAD_1_RIGHT",    16);
        x52.AddButtonLookup("HAT_DPAD_1_DOWN",     17);
        x52.AddButtonLookup("HAT_DPAD_1_LEFT",     18);
        x52.AddButtonLookup("THROTTLE_DPAD_UP",    19);
        x52.AddButtonLookup("THROTTLE_DPAD_RIGHT", 20);
        x52.AddButtonLookup("THROTTLE_DPAD_DOWN",  21);
        x52.AddButtonLookup("THROTTLE_DPAD_LEFT",  22);
        x52.AddButtonLookup("MONITOR_FUNC",        26);
        x52.AddButtonLookup("MONITOR_DOWN",        27);
        x52.AddButtonLookup("MONITOR_UP",          28);
        x52.AddButtonLookup("THROTTLE_I",          29);
        x52.AddButtonLookup("HAT_DPAD_2_UP",       34);
        x52.AddButtonLookup("HAT_DPAD_2_RIGHT",    35);
        x52.AddButtonLookup("HAT_DPAD_2_DOWN",     36);
        x52.AddButtonLookup("HAT_DPAD_2_LEFT",     37);

        result["Xbox 360 Controller"] = xBox;
        result["Saitek X52 Flight Control System"] = x52;

        return result;
    }

	// Initialize the known maps to our calculated known maps
    std::map<std::string, ControllerMapping> ControllerMapping::myKnownMaps = __MakeKnownMappings();

	// Handles registering a new joystick type
    void ControllerMapping::__AddMap(const char* name, const ControllerMapping newMapping) {
        ControllerMapping::myKnownMaps[name] = newMapping;
    }

	// Handles getting a controller mapping from a given name
    const ControllerMapping& ControllerMapping::GetKnownMapping(const char* name) {
        return ControllerMapping::myKnownMaps[name];
    }

	// Create a new named mapping
    ControllerMapping::ControllerMapping(const char* name) {
        myName = name;
    }

	// Create a new un-named mapping
    ControllerMapping::ControllerMapping() {
        myName = "< Unknown >";
    }

	// Prints the mapping to the stream
    void ControllerMapping::Print(std::ostream& stream) {
        stream << myName << ":" << std::endl;
        stream << "Axes:" << std::endl;
        for(auto const &ent : myAxisLookups) {
            stream << "\t" << (ent.second.Location - 1) << " - " << ent.second.ReadableName << std::endl;
        }
        stream << "Buttons:" << std::endl;
        for(auto const &ent : myButtonLookups) {
            stream << "\t" << (ent.second.Location - 1) << " - " << ent.second.ReadableName << std::endl;
        }
    }

}}}
