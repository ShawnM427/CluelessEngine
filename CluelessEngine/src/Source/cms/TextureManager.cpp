#include "cms/TextureManager.h"
#include "graphics/texture.h"


TextureManager::TextureManager()
{
	
}
/*Function to create a texture and add it to texture manager

@params t1: is a textureDefinition to create a texture 
	    name: is the name, of the texture, what we are calling it
*/
bool TextureManager::Create(clueless::graphics::TextureDefinition t1, const std::string name)
{
	if (Find(name))
	{
		std::cout << "Cannot create texture, texture already creadted" << std::endl;
		return false;
	}
	//create a new texture
	clueless::graphics::Texture *temp = new clueless::graphics::Texture(t1);
	//add it to the manager
	textures.insert(std::pair<std::string, clueless::graphics::Texture*>(name,temp));
	return true;
}

//default create doesnt work yet TO BE IMPLEMNTED
bool TextureManager::Create()
{
	return false;
}
bool TextureManager::Create(const char *fileName, GLenum filterMode,
	GLenum wrapMode,  const std::string name)
{
	if (Find(name))
	{
		std::cout << "Cannot create texture, texture already creadted" << std::endl;
		return false;
	}
	//create a new texture
	clueless::graphics::Texture *temp = new clueless::graphics::Texture(fileName,filterMode,
		wrapMode);
	//add it to the manager
	textures.insert(std::pair<std::string, clueless::graphics::Texture*>(name, temp));
	return true;
}
/* used to get any kind of content
*/
clueless::graphics::Texture* TextureManager::GetTexture(const std::string name)
{
	if (Find(name))
	{
		return  textures.find(name)->second;
	}
	else
	{
		clueless::graphics::Texture* t1;
		return t1;
	}
}
/* used to check is content is already created, so to not load it again
@params T*= checks to see if anything type T is defined in memory location
*/
bool TextureManager::Find(const std::string name)
{
	if (textures.count(name)==1)
	{
		return true;
	}

	return false;
}
/* unloads content
@params type T* deletes the pointer to a certain type
*/
bool TextureManager::Del(const std::string name)
{
	if (Find(name))
	{
		textures.erase(name);
		return true;
	}
	return false;
}


TextureManager::~TextureManager()
{
	textures.clear();
}