#include "dark_reach/game.h"

#include "glm_math.h"

#include "graphics/common.h"
#include "graphics/VertexDeclaration.h"
#include "graphics/Mesh.h"
#include "graphics/Shader.h"
#include "graphics/MeshData.h"
#include "graphics/GeoUtils.h"
#include "graphics/VertexLayouts.h"
#include "graphics/Window.h"

#include "graphics/Texture.h"
#include "graphics/CubeMap.h"
#include "graphics/FontRenderer.h"
#include "graphics/RenderTarget2D.h"

#include "utils/common.h"
#include "utils/Logger.h"

#include "audio/SoundManager.h"
#include "audio/SoundEffect.h"
#include "audio/SoundInstance.h"

#include "utils/common.h"

#include "openvr/openvr.hpp"

#include "ui/input/InputManager.h"
#include "ui/input/ControllerMapping.h"

#include "ui/input/InputSources.h"

#include "stb_truetype.h"

#include "globals.h"

namespace dark_reach {

	using namespace std;
	using namespace clueless::graphics;
	using namespace clueless::audio;
	using namespace clueless::ui::input;

	Game::Game(int argc, char *argv[]) {

	}

	Game::~Game() {

	}

	void Game::Init() {
		__LoadSettings();
		__Init();
		__LoadContent();
	}

	void Game::Run() {

	}

	void Game::Stop() {

	}


	void Game::__Init() {

	}

	void Game::__LoadSettings() {

	}

	void Game::__LoadContent() {

	}

	void Game::__Draw(double gameTime, double elapsedTime) {

	}

	void Game::__Update(double gameTime, double elapsedTime) {

	}

}