#include "dark_reach/DebugMenu.h"

#include "graphics/common.h"

#include "glm_math.h"

namespace dark_reach {
	
	DebugMenu::DebugMenu(DebugSettings *settings, clueless::graphics::Window *window) {
		myWindow = window;
		mySettings = settings;
	}

	void DebugMenu::Init() {
		//myCtx = nk_glfw3_init(myWindow->GetHandle(), NK_GLFW3_INSTALL_CALLBACKS);

		//{
		//	struct nk_font_atlas *atlas;
		//	nk_glfw3_font_stash_begin(&atlas);
		//	/*struct nk_font *droid = nk_font_atlas_add_from_file(atlas, "../../../extra_font/DroidSans.ttf", 14, 0);*/
		//	struct nk_font *roboto = nk_font_atlas_add_from_file(atlas, "res/fonts/Roboto-Medium.ttf", 16, 0);
		//	/*struct nk_font *future = nk_font_atlas_add_from_file(atlas, "../../../extra_font/kenvector_future_thin.ttf", 13, 0);*/
		//	/*struct nk_font *clean = nk_font_atlas_add_from_file(atlas, "../../../extra_font/ProggyClean.ttf", 12, 0);*/
		//	/*struct nk_font *tiny = nk_font_atlas_add_from_file(atlas, "../../../extra_font/ProggyTiny.ttf", 10, 0);*/
		//	/*struct nk_font *cousine = nk_font_atlas_add_from_file(atlas, "../../../extra_font/Cousine-Regular.ttf", 13, 0);*/
		//	nk_glfw3_font_stash_end();
		//	nk_style_load_all_cursors(myCtx, atlas->cursors);
		//	nk_style_set_font(myCtx, &roboto->handle);
		//}

		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_CULL_FACE);

		glEnable(GL_MULTISAMPLE);

		myRenderModesText = new const char*[7]{
			"Phong",
			"Normals",
			"Depth",
			"Emissive",
			"Specular",
			"Albedo",
			"World"
		};
	}

	void DebugMenu::PostFrame() {
		/*
		if (nk_begin(myCtx, "Debug Settings", nk_rect(0, 0, 230, 400),
			NK_WINDOW_BORDER | NK_WINDOW_MINIMIZABLE | NK_WINDOW_TITLE)) {

			if (nk_tree_push(myCtx, NK_TREE_TAB, "Render options", NK_MINIMIZED)) {
				int pDrawMode = mySettings->DrawMode;
				nk_combobox(myCtx, myRenderModesText, 7, &mySettings->DrawMode, 30, nk_vec2(200, 180));
				mySettings->ShaderDirty |= pDrawMode != mySettings->DrawMode;

				nk_checkbox_label(myCtx, "Wireframe", &mySettings->IsWireframeEnabled);

				nk_label(myCtx, "Camera Z:", NK_LEFT);
				nk_slider_float(myCtx, 1.6f, &mySettings->CameraZ, 5.1f, 0.1f);

				nk_tree_pop(myCtx);
			}
			*/

			/*
			if (nk_tree_push(myCtx, NK_TREE_TAB, "Lighting", NK_MINIMIZED)) {
				nk_layout_row_dynamic(myCtx, 20, 2);

				nk_labelf(myCtx, NK_LEFT, "Power: %f", mySettings->Light->SpecularPower);
				float pSpecPow = mySettings->Light->SpecularPower;
				nk_slider_float(myCtx, 1, &mySettings->Light->SpecularPower, 50, 1.0f);
				mySettings->ShaderDirty |= pSpecPow != mySettings->Light->SpecularPower;

				nk_label(myCtx, "Spec. Color", NK_LEFT);
				nk_layout_row_dynamic(myCtx, 80, 1);
				glm::vec3 pSpecCol = mySettings->Light->Specular;
				nk_color pSpecConverted;
				pSpecConverted.r = pSpecCol.r * 255;
				pSpecConverted.g = pSpecCol.g * 255;
				pSpecConverted.b = pSpecCol.b * 255;
				nk_color col = nk_color_picker(myCtx, pSpecConverted, nk_color_format::NK_RGB);
				mySettings->Light->Specular = glm::vec3(col.r / 255.0f, col.g / 255.0f, col.b / 255.0f);
				mySettings->ShaderDirty |= pSpecCol != mySettings->Light->Specular;

				nk_layout_row_dynamic(myCtx, 20, 1);

				nk_label(myCtx, "Position:", NK_TEXT_ALIGN_MIDDLE);

				mySettings->Light->Position.x = nk_propertyf(myCtx, "X", -100.0f, mySettings->Light->Position.x, 100.0f, 0.1f, 1.0f);
				mySettings->Light->Position.y = nk_propertyf(myCtx, "Y", -100.0f, mySettings->Light->Position.y, 100.0f, 0.1f, 1.0f);
				mySettings->Light->Position.z = nk_propertyf(myCtx, "Z", -100.0f, mySettings->Light->Position.z, 100.0f, 0.1f, 1.0f);

				if (nk_button_label(myCtx, "Light to Camera"))
					mySettings->Light->Position = glm::vec4(myCamera->Position, 1.0f);

				nk_tree_pop(myCtx);
			}
			*/


		//}
		//nk_end(myCtx);
	}

	void DebugMenu::Unload() {
		//nk_glfw3_shutdown();

		delete myRenderModesText;
	}

	void DebugMenu::HideCursor() {
		//nk_style_hide_cursor(myCtx);
	}

}