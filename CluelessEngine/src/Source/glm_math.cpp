#include "glm_math.h"

glm::vec3 bt2glm(const btVector3 & value)
{
	return glm::vec3(value.x(), value.y(), value.z());
}

btVector3 glm2bt(const glm::vec3 & value)
{
	return btVector3(value.x, value.y, value.z);
}

glm::quat bt2glm(const btQuaternion & value)
{	
	glm::quat quaternion(value.getW(), -value.getX(), -value.getY(), -value.getZ());
	glm::vec3 angles = -glm::eulerAngles(quaternion);
	angles.z *= -1;
	return glm::quat(angles);
}

btQuaternion glm2bt(const glm::quat & value)
{
	return btQuaternion(value.x, value.y, value.z, value.w);
}

glm::vec3 mult(const glm::vec3 & left, const glm::mat4 & right)
{
	return glm::vec3(
		left.x * right[0][0] + left.y * right[1][0] + left.z * right[2][0] + right[3][0],
	    left.x * right[0][1] + left.y * right[1][1] + left.z * right[2][1] + right[3][1],
		left.x * right[0][2] + left.y * right[1][2] + left.z * right[2][2] + right[3][2]);
}

glm::vec2 mult(const glm::vec2 & left, const glm::mat3 & right)
{
	return glm::vec3(
		left.x * right[0][0] + left.y * right[1][0] + right[2][0],
		left.x * right[0][1] + left.y * right[1][1] + right[2][1],
		left.x * right[0][2] + left.y * right[1][2] + right[2][2]);
}

void CopyFromBullet(glm::vec3& target, const btVector3& source) {
	target.x = source.x();
	target.y = source.y();
	target.z = source.z();
}

void CopyFromBullet(glm::vec4& target, const btVector4& source) {
	target.x = source.x();
	target.y = source.y();
	target.z = source.z();
	target.w = source.w();
}

void CopyFromBullet(glm::quat& target, const btQuaternion& source) {
	target.x = -source.x();
	target.y = -source.y();
	target.z = -source.z();
	target.w = source.w();
}

void CopyToBullet(const glm::vec3& source, btVector3& target) {
	target.setX(source.x);
	target.setY(source.y);
	target.setZ(source.z);
}
void CopyToBullet(const glm::vec4& source, btVector4& target) {
	target.setX(source.x);
	target.setY(source.y);
	target.setZ(source.z);
	target.setW(source.w);
}
void CopyToBullet(const glm::quat& source, btQuaternion& target) {
	target.setX(source.x);
	target.setY(source.y);
	target.setZ(source.z);
	target.setW(source.w);
}