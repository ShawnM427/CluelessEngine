#include "../Headers/WRAP.h"

//#include <imgui/imgui.h>
//#include <imgui/imgui_impl.h>
//#include <imgui/imgui_internal.h>

#include "ui/input/InputManager.h"
#include "game_logic/components/RigidBodyComponent.h";
#include "game_logic/components/TriggerColliderComponent.h";




#include "game_logic/Scene.h"
#include "game_logic/components/CameraController.h"
#include "ImGuiQuestSystem.h"
#include "imgui_tabs.h"




#include <iostream>
namespace clueless {
	namespace game_logic {
		namespace systems {
			using namespace ui::input;
			using namespace components;
			bool WRAP::open = false;
			bool WRAP::forceQuest = true;
			WRAP::WRAP()
			{
			}

			WRAP::~WRAP()
			{

			}

			void  WRAP::Init()
			{

			}

			void WRAP::PostRender()
			{
				if (open)
				{
					ICamera* cam = Scene::GetActiveCamera();
					CameraController *controller = cam->GetParent()->GetComponent<CameraController>();
					if (ImGui::Begin("WRAP", nullptr, ImGuiWindowFlags_NoResize)) {

						ImGuiWindow *window = ImGui::GetCurrentWindow();
						ImGui::SetWindowSize(ImVec2(500.0f, 300.0f));
						ImGui::BeginTabBar("Version 2.3b");
						
						
						ImGui::DrawTabsBackground();


						if (ImGui::AddTab("Files")) {

							ImGui::Columns(2, ("Files"));
							ImGui::SetColumnWidth(0, 250.0f);
							if (ImGui::TreeNode("Files"))
							{
								if (ImGui::TreeNode("C:\\"))
								{

									if (ImGui::TreeNode("Audio"))
									{
										if (ImGui::TreeNode("Crew_Members"))
										{
											if (ImGui::TreeNode("Alex_Peran"))
											{

												for (int i = 0; i < 5; i++)
													if (ImGui::TreeNode((void*)(intptr_t)i, "0%d", i))
													{
														ImGui::Button("Play");
														ImGui::TreePop();
													}

												ImGui::TreePop();
											}

											ImGui::TreePop();
										}


										ImGui::TreePop();
									}

									ImGui::TreePop();
								}
								ImGui::TreePop();
							}
							ImGui::NextColumn();
							ImGui::Button("Blad", ImVec2(50.0f, 50.0f));
						
						}
						if (ImGui::AddTab("Map")) {

							ImGuiWindow* window = ImGui::GetCurrentWindow();
							ImRect bb(window->DC.CursorPos, window->DC.CursorPos + ImVec2(300, 180));
							ImGui::ItemSize(bb);
							ImDrawList* drawList = ImGui::GetWindowDrawList();

							if (!ImGui::ItemAdd(bb, NULL))
							{


							}
							drawList->AddRect(ImVec2(bb.Min.x + 1.0f, bb.Min.y + 1.0f),
								ImVec2(bb.Max.x - 1.0f, bb.Max.y - 1.0f), ImColor(0.0f, 0.0f, 255.0f, 255.0f),0.0f,-1,4.0f);
							drawList->AddCircleFilled(ImVec2(bb.Min.x + 50.0f, bb.Min.y + 50.0f)
								, 10.0f, ImColor(255.0f, 0.0f, 0.0f, 255.0f));
						}
						

						if (ImGui::AddTab("Quests")) {
							ImGui::Text(ImGuiQuestSystem::quest.c_str());
							ImGui::Separator();
							ImGui::Text(ImGuiQuestSystem::questDetails.c_str());
							ImGui::Text("Tasks");
							ImGui::Text(ImGuiQuestSystem::task.c_str());
						}

						if (ImGui::AddTab("Options")) {
							ImGui::Text("Mouse Sensitivity:");
							ImGui::SameLine();
							ImGui::DragFloat2("Sensitivity", &controller->Sensitivity[0], 0.001f, 0.0f, 1.0f);
							if (ImGui::Button("Quit"))
							{
								Scene::GetWindow()->Close();
							}
							
						}


						if (forceQuest)
						{
							ImGui::SetActiveTabOfCurrentTabBar(2);
							forceQuest = false;
						}
						

						ImGui::EndTabBar();

						

						ImGui::End();
					}
					
				}

			}




}}}