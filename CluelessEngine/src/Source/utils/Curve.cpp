#include "utils/Curve.h"

#include "cmath"

namespace clueless {

	Curve::Curve(CurveKeyFrame* frames, int length) {
		myFrames = new CurveKeyFrame[length];
		myFrameCount = length;
		memcpy(myFrames, frames, length * sizeof(CurveKeyFrame));
	}

	float Curve::Sample(float t) {
		for (int ix = 0; ix < myFrameCount; ix++) {
			if (myFrames[ix].Time >= t) {
				if (ix == 0 ) {
					return myFrames[ix].Value;
				}
				else {
					switch (myFrames[ix - 1].Mode) {
						case SplineCurve:
							return Spline((t - myFrames[ix-1].Time) / (myFrames[ix].Time - myFrames[ix - 1].Time), myFrames[ix - 1].Value, myFrames[ix].Value);
						case LinearCurve:
							return myFrames[ix - 1].Value + (t - myFrames[ix - 1].Time) / (myFrames[ix].Time - myFrames[ix - 1].Time) * (myFrames[ix].Value - myFrames[ix - 1].Value);
						default:
							return myFrames[ix].Value;

					}
				}
			}
		}
		return myFrames[myFrameCount - 1].Value;
	}

	float Curve::Spline(float t, float inX, float outX) {
		float invT = 1.0f - t;
		float invTPow3 = invT * invT * invT;
		float invTPow2 = invT * invT;
		return (invTPow3 * inX) + (3 * invTPow2 * t * inX) + (3 * invT * t * t * outX) + (t * t * t * outX);
	}

}