#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <fstream>
#include "cms/CMS.h"
#include "tinyxml2.h"
 
#include "glm_math.h"

#include "graphics/common.h"
#include "graphics/VertexDeclaration.h"  
#include "graphics/Mesh.h" 
#include "graphics/Model.h" 
#include "graphics/Material.h"
#include "graphics/Shader.h"
#include "graphics/MeshData.h" 
#include "graphics/GeoUtils.h"
#include "graphics/VertexLayouts.h"
#include "graphics/Window.h"
#include "graphics/MorphMesh.h"
#include "graphics/PrimitiveBatch3D.h" 

#include "graphics/Texture.h"
#include "graphics/CubeMap.h"
#include "graphics/FontRenderer.h"
#include "graphics/RenderTarget2D.h"
#include "graphics/GBuffer.h"

#include "ui/BitmapFont.h"
#include "ui/TrueTypeFont.h"

#include "utils/common.h"
#include "utils/Logger.h"

#include "audio/SoundManager.h"
#include "audio/SoundEffect.h"
#include "audio/SoundInstance.h"

#include "utils/common.h"

#include "ui/input/InputManager.h"
#include "ui/input/ControllerMapping.h"

#include "ui/input/InputSources.h"

#include "stb_truetype.h"

#include "globals.h"

#include "dark_reach/Game.h"

#include "game_logic/Scene.h"

#include "utils/Curve.h"

#include "anim/animate.h"

#include "dark_reach/DebugMenu.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "btBulletDynamicsCommon.h"
#include "BulletDynamics/Character/btKinematicCharacterController.h"
#include "BulletCollision/CollisionDispatch/btGhostObject.h"

#include "game_logic/components/MeshRenderComponent.h"
#include "game_logic/components/MorphMeshRenderComponent.h"
#include "game_logic/components/SkyboxCamera.h"
#include "game_logic/components/CameraController.h"
#include "game_logic/components/TriggerColliderComponent.h"
#include "game_logic/components/PhysicsInteractComponent.h"
#include "game_logic/components/CharacterController.h"
#include "game_logic/components/RigidBodyComponent.h"
#include "game_logic/components/SkinnedMeshRenderComponent.h"
#include "game_logic/systems/DeferredRenderSystem.h"
#include "game_logic/systems/ImGuiDebugSystem.h"
#include "../Headers/WRAP.h"

#include "anim/HTRLoader.h"

#include "Questbook.h"
#include "../Headers/Event.h"
#include "../Headers/ImGuiPuzzleSystem.h"

#include <map>



#include "physics/PhysicsWorld.h" 

extern "C"  
{
	__declspec(dllexport) unsigned long NvOptimusEnablement = 0x00000001;
	__declspec(dllexport) int AmdPowerXpressRequestHighPerformance = 1;
}

using namespace std;
using namespace clueless;
using namespace clueless::graphics;
using namespace clueless::audio;
using namespace clueless::ui::input;
using namespace clueless::ui;
using namespace clueless::game_logic;
using namespace clueless::anim;
using namespace clueless::physics;
using namespace clueless::game_logic::systems;
using namespace clueless::game_logic::components;

using namespace dark_reach;

ColliderEn col;

Event message;
bool toolEnable = false;
bool enable=false;

//Global Sound handler
std::map<std::string, SoundInstance*> mySounds;

void ResetGame()
{
	//reset quest system
	Questbook::Unload();
	Questbook::Init("res/quest.xml");

	//reset  wrap and imquest
	ImGuiQuestSystem::showPrompt = true;
	WRAP::forceQuest = true;

	//reset start beep
	std::map<std::string, SoundInstance*>::iterator sound = (mySounds.find("Beep"));
	if (sound != mySounds.end())
	{
		sound->second->Loop();
	}

	//reset player state
	CameraController* ctrl = Scene::GetActiveCamera()->GetParent()->GetComponent<CameraController>();
	CharacterController* ctrl2 = Scene::GetActiveCamera()->GetParent()->GetComponent<CharacterController>();
	ctrl->SetYaw(11.0f);
	ctrl2->SetMyState(new btDefaultMotionState(btTransform(btQuaternion(0.0f, 0.0f, 0.0f), btVector3(18.0f, 0.0f, 0.0f))));

}



void BuildScene() {
	Scene::Init();

	Assimp::Importer importer;
	
	//floor
	RigidBody* floor = new RigidBody(new btBoxShape(btVector3(100.0f, 100.0f, 0.5f)), btVector3(0.0f, 0.0f, -2.0f), 0.0f);
	Scene::AddCollisionItem(floor);
	
	//roof
	RigidBody* roof = new RigidBody(new btBoxShape(btVector3(100.0f, 100.0f, 0.25f)), btVector3(0.0f, 0.0f, 1.75f), 0.0f);
	Scene::AddCollisionItem(roof);


	// Right wall
	RigidBody* wall1 = new RigidBody(new btBoxShape(btVector3(8.0f, 0.25f, 1.5f)), btVector3(12.0f, -5.25f, 0.0f));
	Scene::AddCollisionItem(wall1);

	// Left wall
	RigidBody* wall2 = new RigidBody(new btBoxShape(btVector3(8.0f, 0.25f, 1.5f)), btVector3(12.0f, 5.5f, 0.0f));
	Scene::AddCollisionItem(wall2);

	// Seperator wall
	RigidBody* wall3 = new RigidBody(new btBoxShape(btVector3(0.125f, 3.6f, 1.5f)), btVector3(9.5f, 1.4f, 0.0f));
	Scene::AddCollisionItem(wall3);

	//Seperator other side of wall

	RigidBody* wallSepLeft = new RigidBody(new btBoxShape(btVector3(0.125f, 1.0f, 1.5f)), btVector3(9.5f, -5.0f, 0.0f));
	Scene::AddCollisionItem(wallSepLeft);

	//Viewer Wall
	RigidBody* wallViwer = new RigidBody(new btBoxShape(btVector3(0.125f, 6.0f, 1.5f)), btVector3(19.5f, 0.0f, 0.0f));
	Scene::AddCollisionItem(wallViwer);

	// Right angled wall
	RigidBody* wall4 = new RigidBody(new btBoxShape(btVector3(2.0f, 0.25f, 1.5f)), btVector3(3.2f, -3.9f, 0.0f));
	wall4->SetOrientation(glm::quat(glm::vec3(0.0f, 0.0f, glm::radians(-62.0f))));
	Scene::AddCollisionItem(wall4);

	// Left angled wall
	RigidBody* wall5 = new RigidBody(new btBoxShape(btVector3(2.0f, 0.25f, 1.5f)), btVector3(3.2f, 3.9f, 0.0f));
	wall5->SetOrientation(glm::quat(glm::vec3(0.0f, 0.0f, glm::radians(62.0f))));
	Scene::AddCollisionItem(wall5);

	// Elevator back
	RigidBody* wall6 = new RigidBody(new btBoxShape(btVector3(0.25f, 2.25f, 1.5f)), btVector3(-2.625f, 0.0f, 0.0f));
	Scene::AddCollisionItem(wall6);

	// Elevator left
	RigidBody* wall7 = new RigidBody(new btBoxShape(btVector3(2.25f, 0.25f, 1.5f)), btVector3(0.0f, 2.45f, 0.0f));
	Scene::AddCollisionItem(wall7);

	// Elevator right
	RigidBody* wall8 = new RigidBody(new btBoxShape(btVector3(2.25f, 0.25f, 1.5f)), btVector3(0.0f, -2.45f, 0.0f));
	Scene::AddCollisionItem(wall8);

	// Bridge stairs
	RigidBody* stairBack = new RigidBody(new btBoxShape(btVector3(0.925f, 2.75f, 0.25f)), btVector3(10.575f, 1.75f, -1.25f));
	Scene::AddCollisionItem(stairBack);
	RigidBody* stairFront= new RigidBody(new btBoxShape(btVector3(3.33f, 1.58f, 0.25f)), btVector3(13.33f, -0.08f, -1.25f));
	Scene::AddCollisionItem(stairFront);

	// desk1
	RigidBody* desk1 = new RigidBody(new btBoxShape(btVector3(0.6f, 1.1f, 0.6f)), btVector3(8.5f, 0.0f, -0.9f));
	Scene::AddCollisionItem(desk1);

	//desk2
	RigidBody* desk2 = new RigidBody(new btBoxShape(btVector3( 0.6f, 1.1f,0.6f)), btVector3(8.5f, 3.3f, -0.9f));
	Scene::AddCollisionItem(desk2);
	
	RigidBody* ball = new RigidBody(new btSphereShape(0.2f), 1.0f);
	ball->SetGroup(COL_BALL);   

	//wrench rigidbody
	RigidBody* ball3 = new RigidBody(new btBoxShape(btVector3(0.1f, 0.1f, 0.1f)));

	// Body collider
	RigidBody* bodyTest = new RigidBody(new btBoxShape(btVector3(0.5f, 0.5f, 0.5f)));

	//Communication Desk
	RigidBody* commDesk = new RigidBody(new btBoxShape(btVector3(1.1f, 0.6f, 0.6f)), btVector3(0.15f, -7.1f, 0.6f));

	//door collider
	RigidBody* door= new RigidBody(new btBoxShape(btVector3(1.0f,1.0f, 2.0f)), btVector3(0.0f, 0.0f, -1.5f));
	//door->SetGroup(COL_INTERACTIVE);

	RigidBody* debris = new RigidBody(new btBoxShape(btVector3(0.8f, 0.8f, 1.0f)), 60.0f);
	debris->SetGroup(COL_DEBRIS);
	//Scene::AddCollisionItem(debris);

	RigidBody* hatch = new RigidBody(new btBoxShape(btVector3(0.5f, 0.5f, 0.125f)), btVector3(0.0f, 0.0f, -1.45f));
	
	//console communication rigidbody
	RigidBody* console1 = new RigidBody(new btBoxShape(btVector3(0.5f, 0.5f, 0.125f)));

	//Elevator rigidbody
	RigidBody* elevaotrBody = new RigidBody(new btBoxShape(btVector3(0.1f, 1.75f, 1.5f)));

	//console3 rigidbody
	RigidBody* console3 = new RigidBody(new btBoxShape(btVector3(0.5f, 0.5f, 0.125f)));
	
	RigidBody* deadPerson2 = new RigidBody(new btBoxShape(btVector3(0.5f, 0.5f, 0.5f)));

	//shape for panel collisions
	btCompoundShape *shape = new btCompoundShape();
	btTransform local(btQuaternion(), btVector3(0.0f, 0.0f, 0.0f));
	local.setOrigin(btVector3(0.0f, 0.0f, 0.0f));
	shape->addChildShape(local, new btBoxShape(btVector3(0.4f, 0.1f, 0.5f)));
	local.setRotation(btQuaternion(btVector3(0.0f, 0.0f, glm::radians(45.0f)), 90.0f));
	local.setOrigin(btVector3(0.7f, -0.30f, 0.0f));
	shape->addChildShape(local, new btBoxShape(btVector3(0.4f, 0.1f, 0.5f)));
	local.setRotation(btQuaternion(btVector3(0.0f, 0.0f, glm::radians(-45.0f)), 90.0f));
	local.setOrigin(btVector3(-0.7f, -0.30f, 0.0f));
	shape->addChildShape(local, new btBoxShape(btVector3(0.4f, 0.1f, 0.5f)));

	btCompoundShape *shape2 = new btCompoundShape();
	local.setRotation(btQuaternion());
	local.setOrigin(btVector3(0.0f, 0.0f, 0.0f));
	shape2->addChildShape(local, new btBoxShape(btVector3(0.4f, 0.1f, 0.5f)));
	local.setRotation(btQuaternion(btVector3(0.0f, 0.0f, glm::radians(45.0f)), 90.0f));
	local.setOrigin(btVector3(0.7f, -0.30f, 0.0f));
	shape2->addChildShape(local, new btBoxShape(btVector3(0.4f, 0.1f, 0.5f)));
	local.setRotation(btQuaternion(btVector3(0.0f, 0.0f, glm::radians(-45.0f)), 90.0f));
	local.setOrigin(btVector3(-0.7f, -0.30f, 0.0f));
	shape2->addChildShape(local, new btBoxShape(btVector3(0.4f, 0.1f, 0.5f)));


	RigidBody* console2 = new RigidBody(shape,false);

	RigidBody* console4 = new RigidBody(shape2);

	RigidBody * deadBody = new RigidBody(new btBoxShape(btVector3(0.5f, 0.5f, 0.75f)));
	RigidBody * deadH = new RigidBody(new btSphereShape(0.1f), 0.4f);
	deadH->SetGroup (COL_INTERACTIVE);
	RigidBody * deadHi = new RigidBody(new btSphereShape(0.1f), 0.4f);
	deadHi->SetGroup(COL_INTERACTIVE);
	RigidBody * deadLA = new RigidBody(new btSphereShape(0.1f), 0.4f);
	deadLA->SetGroup(COL_INTERACTIVE);
	RigidBody * deadLL = new RigidBody(new btSphereShape(0.1f), 0.4f);
	deadLL->SetGroup(COL_INTERACTIVE);
	RigidBody * deadRA = new RigidBody(new btSphereShape(0.1f), 0.4f);
	deadRA->SetGroup(COL_INTERACTIVE);
	RigidBody * deadRL = new RigidBody(new btSphereShape(0.1f), 0.4f);
	deadRL->SetGroup(COL_INTERACTIVE);
	RigidBody * deadT = new RigidBody(new btSphereShape(0.2f),0.4f);
	deadT->SetGroup(COL_INTERACTIVE);


	
	//ship in scene
    const aiScene *sceneFloor = importer.ReadFile("res/models/Bridge/Floor/Floor.fbx", aiProcess_FlipUVs);
    Model *sceneFloorModel = Model::LoadFromNode(sceneFloor, sceneFloor->mRootNode);

    const aiScene *sceneWalls = importer.ReadFile("res/models/Bridge/Walls/Walls.fbx", aiProcess_FlipUVs);
    Model *sceneWallsModel = Model::LoadFromNode(sceneWalls, sceneWalls->mRootNode);

    const aiScene *sceneStairs = importer.ReadFile("res/models/Bridge/Stairs/Stairs.fbx", aiProcess_FlipUVs);
    Model *sceneStairsModel = Model::LoadFromNode(sceneStairs, sceneStairs->mRootNode);

    const aiScene *sceneRoof = importer.ReadFile("res/models/Bridge/Ceiling/Ceiling.fbx", aiProcess_FlipUVs);
    Model *sceneRoofModel = Model::LoadFromNode(sceneRoof, sceneRoof->mRootNode);

	//ball in scene
	const aiScene *ballScene = importer.ReadFile("res/models/sphere.obj", aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
	Model *ballModel = Model::LoadFromNode(ballScene, ballScene->mRootNode);

	//wrench in scene
	const aiScene *wrenchScene = importer.ReadFile("res/models/wrench.obj", aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
	Model *wrenchModel = Model::LoadFromNode(wrenchScene, wrenchScene->mRootNode);

	//desk in scene
	const aiScene *deskScene = importer.ReadFile("res/models/Bridge/Desk/Desk.fbx", aiProcess_FlipUVs);
	Model *deskModel = Model::LoadFromNode(deskScene, deskScene->mRootNode);

	//console in scene
   const aiScene *consoleScene = importer.ReadFile("res/models/Bridge/Console/Console.fbx", aiProcess_FlipUVs);
	Model *consoleModel = Model::LoadFromNode(consoleScene, consoleScene->mRootNode);
	
	const aiScene *bodyScene = importer.ReadFile("res/models/Bridge/Dead_Bodies/DeadGuy_01_low.fbx", aiProcess_FlipUVs);
	Model *bodyModel = Model::LoadFromNode(bodyScene, bodyScene->mRootNode);

	//Debris in scene
	const aiScene *debrisScene = importer.ReadFile("res/models/Bridge/Debris/Debris.fbx", aiProcess_FlipUVs);
	Model *debrisModel = Model::LoadFromNode(debrisScene, debrisScene->mRootNode);

	const aiScene *hatchScene = importer.ReadFile("res/models/Bridge/Hatch/Hatch.fbx", aiProcess_FlipUVs);
	Model *hatchModel = Model::LoadFromNode(hatchScene, hatchScene->mRootNode);

	const aiScene *deadPersons = importer.ReadFile("res/models/Bridge/dead-body-01/dead_body_01_low.fbx", aiProcess_FlipUVs);
	Model *person = Model::LoadFromNode(deadPersons, deadPersons->mRootNode);

	//dead dismembered body
	const aiScene *deadHead = importer.ReadFile("res/models/Bridge/dead-body-02/head/head.fbx", aiProcess_FlipUVs);
	Model *deadHeadModel = Model::LoadFromNode(deadHead, deadHead->mRootNode);
	const aiScene *deadHips = importer.ReadFile("res/models/Bridge/dead-body-02/hips/hips.fbx", aiProcess_FlipUVs);
	Model *deadHipsModel = Model::LoadFromNode(deadHips, deadHips->mRootNode);
	const aiScene *deadLeftArm = importer.ReadFile("res/models/Bridge/dead-body-02/left-arm/left-arm.fbx", aiProcess_FlipUVs);
	Model *deadLeftArmModel = Model::LoadFromNode(deadLeftArm, deadLeftArm->mRootNode);
	const aiScene *deadLeftLeg = importer.ReadFile("res/models/Bridge/dead-body-02/left-leg/left_leg.fbx", aiProcess_FlipUVs);
	Model *deadLeftLegModel = Model::LoadFromNode(deadLeftLeg, deadLeftLeg->mRootNode);
	const aiScene *deadRightArm = importer.ReadFile("res/models/Bridge/dead-body-02/right-arm/right-arm.fbx", aiProcess_FlipUVs);
	Model *deadRightArmModel = Model::LoadFromNode(deadRightArm, deadRightArm->mRootNode);
	const aiScene *deadRightLeg = importer.ReadFile("res/models/Bridge/dead-body-02/right-leg/right_leg.fbx", aiProcess_FlipUVs);
	Model *deadRightLegModel = Model::LoadFromNode(deadRightLeg, deadRightLeg->mRootNode);
	const aiScene *deadTorso = importer.ReadFile("res/models/Bridge/dead-body-02/torso/torso.fbx", aiProcess_FlipUVs);
	Model *deadTorsoModel = Model::LoadFromNode(deadTorso, deadTorso->mRootNode);


	
	//MATERIALS /TEXTURES

	MorphMesh* gooMesh = new MorphMesh();
	gooMesh->SetLoopMode(anim::LoopType_Reverse);

	gooMesh->AllocateKeys(4);

	char path[33];
	memcpy(path, "res/models/Bridge/Goo/goo_00.obj", 33);
	for (int ix = 0; ix < 4; ix++) {
		sprintf(path + 26, "%02d", ix);
		path[28] = '.';
		const aiScene *scene = importer.ReadFile(path, aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
		gooMesh->InsertKey(ix * 0.5f, MeshData::LoadFromScene(scene, 0));
	}

	//elevators morphs
	const aiScene *morph1 = importer.ReadFile("res/models/Bridge/Elevator/Elevator.fbx", aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
	MeshData morph1Data = MeshData::LoadFromScene(morph1, 0);

	const aiScene *morph2 = importer.ReadFile("res/models/Bridge/Elevator/Elevator_open.fbx", aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
	MeshData morph2Data = MeshData::LoadFromScene(morph2, 0);

	//door morphs
	const aiScene *morph3 = importer.ReadFile("res/models/Bridge/Door/Door.fbx", aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
	MeshData morph3Data = MeshData::LoadFromScene(morph3, 0);

	const aiScene *morph4 = importer.ReadFile("res/models/Bridge/Door/Door_open.fbx", aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
	MeshData morph4Data = MeshData::LoadFromScene(morph4, 0);





	//elevator morph targets
	MorphMesh* morphMesh = new MorphMesh();
	morphMesh->SetLoopMode(anim::LoopType_Reverse);
	morphMesh->pauseLoop(true);
	morphMesh->AllocateKeys(2);
	morphMesh->InsertKey(0.0f, morph1Data);
	morphMesh->InsertKey(1.0f, morph2Data);

	//door morph targets
	MorphMesh* morphMesh2= new MorphMesh();
	morphMesh2->SetLoopMode(anim::LoopType_Reverse);
	morphMesh2->pauseLoop(true);
	morphMesh2->AllocateKeys(2);
	morphMesh2->InsertKey(0.0f, morph3Data);
	morphMesh2->InsertKey(1.0f, morph4Data);

	// Lazy-load materials here cause I'm a lazy peice of shit and can't be bothered to use the tokens I wrote
	Material *material        = CMS::RedeemDirect<Material>(CmsType::TypeMaterial, "bricks");
	Material *materialFloor   = CMS::RedeemDirect<Material>(CmsType::TypeMaterial, "floor");
	Material *materialWalls   = CMS::RedeemDirect<Material>(CmsType::TypeMaterial, "walls");
	Material *materialDebris  = CMS::RedeemDirect<Material>(CmsType::TypeMaterial, "debris");
	Material *materialStairs  = CMS::RedeemDirect<Material>(CmsType::TypeMaterial, "stairs");
	Material *materialHatch   = CMS::RedeemDirect<Material>(CmsType::TypeMaterial, "hatch");
	Material *materialCeiling = CMS::RedeemDirect<Material>(CmsType::TypeMaterial, "ceiling");
	Material *materialConsole = CMS::RedeemDirect<Material>(CmsType::TypeMaterial, "console");
	Material *materialDead    = CMS::RedeemDirect<Material>(CmsType::TypeMaterial, "dead");
	Material *materialElevator= CMS::RedeemDirect<Material>(CmsType::TypeMaterial, "elevator");
	Material *materialDoor    = CMS::RedeemDirect<Material>(CmsType::TypeMaterial, "door");
	Material *materialGoo     = CMS::RedeemDirect<Material>(CmsType::TypeMaterial, "goo");
	//dead person materials
	Material *materialHead = CMS::RedeemDirect<Material>(CmsType::TypeMaterial, "deadhead");
	Material *materialHips = CMS::RedeemDirect<Material>(CmsType::TypeMaterial, "deadhips");
	Material *materialLeftArm = CMS::RedeemDirect<Material>(CmsType::TypeMaterial, "deadleftarm");
	Material *materialLeftLeg = CMS::RedeemDirect<Material>(CmsType::TypeMaterial, "deadleftleg");
	Material *materialRightArm = CMS::RedeemDirect<Material>(CmsType::TypeMaterial, "deadrightarm");
	Material *materialRightLeg = CMS::RedeemDirect<Material>(CmsType::TypeMaterial, "deadrightleg");
	Material *materialTorso = CMS::RedeemDirect<Material>(CmsType::TypeMaterial, "deadtorso");


	//wrap system
	WRAP wrap = WRAP();
	Scene::AddSystem(wrap);
	
	DeferredRenderSystem renderer = DeferredRenderSystem();
	renderer.Init();
	
	PointLightData* myLightData = renderer.GetLight(0);
	myLightData->Position = glm::vec4(5.5f, 0.0f, 1.25f, 1.0f);
	myLightData->Diffuse = glm::vec3(0.2f, 0.2f, 0.2f); 
	myLightData->Specular = glm::vec3(0.6f, 0.6f, 0.3f);
	myLightData->Attenuation = 1.0f;

	myLightData = renderer.GetLight(1);
	myLightData->Position = glm::vec4(12.25f, 0.0f, 1.0f, 1.0f);
	myLightData->Diffuse = glm::vec3(0.2f, 0.2f, 0.2f);
	myLightData->Specular = glm::vec3(0.1f,0.1f, 0.1f);
	myLightData->Attenuation = 1.0f;

	myLightData = renderer.GetLight(2);
	myLightData->Position = glm::vec4(15.5f, 0.0f, 0.00f, 1.0f);
	myLightData->Diffuse = glm::vec3(0.4f, 0.4f, 0.4f);
	myLightData->Specular = glm::vec3(0.6f, 0.6f, 0.3f);
	myLightData->Attenuation = 1.0f;

	myLightData = renderer.GetLight(3);
	myLightData->Position = glm::vec4(18.0f, 3.5f, 0.00f, 1.0f);
	myLightData->Diffuse = glm::vec3(0.2f, 0.2f, 0.2f);
	myLightData->Specular = glm::vec3(0.6f, 0.6f, 0.3f);
	myLightData->Attenuation = 1.0f;

	myLightData = renderer.GetLight(4);
	myLightData->Position = glm::vec4(18.0f, -3.5f, 0.00f, 1.0f);
	myLightData->Diffuse = glm::vec3(0.2f, 0.2f, 0.2f);
	myLightData->Specular = glm::vec3(0.6f, 0.6f, 0.3f);
	myLightData->Attenuation = 1.0f;

	myLightData = renderer.GetLight(5);
	myLightData->Position = glm::vec4(0.0f, 0.0f, 0.00f, 1.0f);
	myLightData->Diffuse = glm::vec3(0.2f, 0.2f, 0.2f);
	myLightData->Specular = glm::vec3(0.6f, 0.6f, 0.3f);
	myLightData->Attenuation = 1.0f;

	renderer.SetActiveLights(6);
	
	AnimChannel *attChannel = new AnimChannel();

	Keyframe* frames = new Keyframe[120];
	for (int ix = 0; ix < 120; ix++) {
		frames[ix].time = ix * 0.1f;
		frames[ix].value = 0.5f + RANDF / 2.0f;
	}
	frames[119].value = frames[0].value;

	int segCount{ 0 };
	attChannel->segments = Animator::MakeCatmulSet(frames, 8, segCount);
	attChannel->SegCount = segCount;

	for (int ix = 0; ix < 6; ix++) {
		DeferredRenderSystem::LightSpline *flickerTest = new DeferredRenderSystem::LightSpline();
		flickerTest->Attenuation = attChannel;
		flickerTest->Index = ix;
		renderer.AddLightSpline(flickerTest);
	}

	Scene::AddSystem(renderer); 

	//debug system
	ImGuiDebugSystem debugUI = ImGuiDebugSystem();
	debugUI.Init();

	Scene::AddSystem(debugUI);
	
	//main camera
	GameObject *mainCam = new GameObject(nullptr);
	mainCam->LocalTransform = Transform();
	mainCam->LocalTransform.Position = glm::vec3(0.0f, 0.0f, 1.8f);
	mainCam->LookAt(glm::vec3(-10.0f, 0.0f, 1.0f));

	//Physics and player 
	PhysicsInteractComponent interact = PhysicsInteractComponent(mainCam);
	mainCam->AddComponent(interact);
	SkyboxCamera camController = SkyboxCamera(mainCam);
	mainCam->AddComponent(camController);
	CameraController controller = CameraController(mainCam);
	mainCam->AddComponent(controller);
	CharacterController cContrl = CharacterController(mainCam);
	mainCam->AddComponent(cContrl);;
	Scene::AddObject(mainCam);
	Scene::SetActiveCamera(mainCam->GetComponent<SkyboxCamera>());

	//trigger box shapes for events
	btBoxShape *triggerShape = new btBoxShape(btVector3(2.25f, 2.25f, 1.5f));
	btBoxShape *triggerShape2 = new btBoxShape(btVector3(0.0f,0.0f,0.0f));
	btBoxShape *triggerShape3=new btBoxShape(btVector3(1.0f, 1.0f, 1.0f));
	btBoxShape *triggerShape4 = new btBoxShape(btVector3(0.5f, 1.5f,2.0f));
	btBoxShape *triggerShape5 = new btBoxShape(btVector3(0.6f, 0.6f, 0.4f));
	btBoxShape *triggerDebris = new btBoxShape(btVector3(1.0f, 1.0f, 2.0f));
	btBoxShape *triggerWrench = new btBoxShape(btVector3(0.5f, 0.5f, 0.8f));

	//GAMEOBJECTS


	//build sounds for the scene
	SoundManager::Add(new SoundEffect("Beep", "res/sounds/Beep2.wav"));
	SoundManager::Add(new SoundEffect("puzzle", "res/sounds/puzzle.wav"));
	SoundManager::Add(new SoundEffect("space", "res/sounds/space.wav"));
	SoundManager::Add(new SoundEffect("alarm", "res/sounds/bridge-alarm.wav"));
	SoundManager::Add(new SoundEffect("pickup", "res/sounds/pickup.wav"));
	SoundManager::Get("puzzle")->SetGain(0.25f);
	SoundManager::Get("Beep")->SetGain(0.5f);
	SoundManager::Get("space")->SetGain(0.05f);
	SoundManager::Get("alarm")->SetGain(0.05f);
	SoundManager::Get("pickup")->SetGain(0.5f);


	SoundInstance * mySound = SoundManager::GetInstance("Beep");
	SoundInstance * mySound2 = SoundManager::GetInstance("puzzle");
	SoundInstance * mySound3 = SoundManager::GetInstance("space");
	SoundInstance * mySound4 = SoundManager::GetInstance("alarm");
	SoundInstance * mySound5 = SoundManager::GetInstance("pickup");


	mySound->Loop();
	mySound4->Loop();
	mySound3->Loop();
	//add to global sounds
	mySounds.insert(std::pair<std::string, SoundInstance*>("Beep", mySound));
	mySounds.insert(std::pair<std::string, SoundInstance*>("puzzle", mySound2));
	mySounds.insert(std::pair<std::string, SoundInstance*>("space", mySound3));
	mySounds.insert(std::pair<std::string, SoundInstance*>("alarm", mySound4));
	mySounds.insert(std::pair<std::string, SoundInstance*>("pickup", mySound5));






	//back of ship
	GameObject *shipFloor= new GameObject(nullptr);
	MeshRenderComponent meshRender = MeshRenderComponent(shipFloor, sceneFloorModel, materialFloor);
	shipFloor->AddComponent(meshRender);
	Scene::AddObject(shipFloor);

	GameObject *goo = new GameObject(nullptr);
	goo->LocalTransform.Position = glm::vec3(0.0f, 0.0f, -1.5f);
	MorphMeshRenderComponent gooMorph = MorphMeshRenderComponent(goo, gooMesh, materialGoo);
	goo->AddComponent(gooMorph);
	Scene::AddObject(goo);
	

	//Elevator Door
	GameObject *morphTest = new GameObject(nullptr);
	morphTest->LocalTransform.Position = glm::vec3(2.3f, 0.0f, 0.0f);
	MorphMeshRenderComponent testMorphMeshRender = MorphMeshRenderComponent(morphTest, morphMesh, materialElevator);
	morphTest->AddComponent(testMorphMeshRender);
	Scene::AddObject(morphTest);

	//Morph Door
	GameObject *morphTest2 = new GameObject(nullptr);
	morphTest2->LocalTransform.Position = glm::vec3(0.025f,0.0f, 0.0f);
	MorphMeshRenderComponent testMorphMeshRender2 = MorphMeshRenderComponent(morphTest2, morphMesh2, materialDoor);
	morphTest2->AddComponent(testMorphMeshRender2);
	Scene::AddObject(morphTest2);
	
	HTRLoader loader;
	SkinnedMesh           *skinnedMesh;
	MeshSkeleton          *skeleton = nullptr;
	MeshSkeletonAnimation *animation = nullptr;

	//loader.LoadHTR("res/animations/simple_rig.htr", skeleton, animation);
	// Game Colliders

	//skeleton->Apply(animation);
	//skeleton->ApplyTo(skinnedMesh->GetBoneData());

	//skinnedMesh = new SkinnedMesh(skeleton->GetJointCount());
	//const aiScene *animatedScene = importer.ReadFile("res/models/animatedTest.fbx", aiProcess_FlipUVs);
	//skinnedMesh->LoadFromScene(animatedScene, 0, CMS::GetToken(CmsType::TypeMaterial, "SkinnedTest"));

	//GameObject *testAnim = new GameObject(nullptr);
	//testAnim->LocalTransform.Position = glm::vec3(11.0f, 0.0f, 0.0f);
	//SkinnedMeshRenderComponent animRender = SkinnedMeshRenderComponent(testAnim, skinnedMesh);
	//testAnim->AddComponent(animRender);
	//Scene::AddObject(testAnim);

	//Ship walls
	GameObject *shipWalls = new GameObject(nullptr);
	MeshRenderComponent frontMesh = MeshRenderComponent(shipWalls, sceneWallsModel, materialWalls);
	shipWalls->AddComponent(frontMesh);
	Scene::AddObject(shipWalls);
	
	//Ship stairs
	GameObject *shipStairs = new GameObject(nullptr);
	MeshRenderComponent stairsMesh = MeshRenderComponent(shipStairs, sceneStairsModel, materialStairs);
	shipStairs->AddComponent(stairsMesh);
	Scene::AddObject(shipStairs);

	//Roof
	GameObject *shipRoof = new GameObject(nullptr);
	MeshRenderComponent roofMesh = MeshRenderComponent(shipRoof, sceneRoofModel, materialCeiling);
	shipRoof->AddComponent(roofMesh);
	Scene::AddObject(shipRoof);

	//ball object
	GameObject *ballObject = new GameObject(nullptr);
	ballObject->LocalTransform.Position = glm::vec3(3.1f, 0.0f, -1.0f);
	MeshRenderComponent meshRender2 = MeshRenderComponent(ballObject, deadHeadModel, materialHead);
	ballObject->AddComponent(meshRender2); 
	RigidBodyComponent ballCollider = RigidBodyComponent(ballObject, ball);
	ballObject->AddComponent(ballCollider);
	Scene::AddObject(ballObject);

	//questbook initiate
	Questbook::Init("res/quest.xml");

	ImGuiQuestSystem questSystem;
	questSystem.Init();
	Scene::AddSystem(questSystem);

	//desk1 object
	GameObject *deskObject = new GameObject(nullptr);
	deskObject->LocalTransform.Position = glm::vec3(8.5f, 3.3f, -0.9f);
	deskObject->LocalTransform.Orientation = glm::quat(glm::vec3(0.0f, 0.0f, glm::radians(-90.0f)));
	MeshRenderComponent deskMesh2 = MeshRenderComponent(deskObject,consoleModel, materialConsole);
	deskObject->AddComponent(deskMesh2);
	Scene::AddObject(deskObject);

	//desk2 object
	GameObject *deskObject2 = new GameObject(nullptr);
	deskObject2->LocalTransform.Position = glm::vec3(8.5f, 0.0f, -0.9f);
	deskObject2->LocalTransform.Orientation = glm::quat(glm::vec3(0.0f, 0.0f, glm::radians(-90.0f)));
	MeshRenderComponent deskMesh = MeshRenderComponent(deskObject2, consoleModel, materialConsole);
	deskObject2->AddComponent(deskMesh);
	Scene::AddObject(deskObject2);

	//communication desk
	GameObject *commDeskObject = new GameObject(nullptr);
	commDeskObject->LocalTransform.Position = glm::vec3(18.0f,3.6f, -0.85f);
	commDeskObject->LocalTransform.Orientation = glm::quat(glm::vec3(0.0f, 0.0f, glm::radians(90.0f)));
	MeshRenderComponent deskMesh3 = MeshRenderComponent(commDeskObject,consoleModel, materialConsole);
	commDeskObject->AddComponent(deskMesh3);
	RigidBodyComponent commCol = RigidBodyComponent(commDeskObject, commDesk);
	commDeskObject->AddComponent(commCol);
	TriggerColliderComponent myTrigger2 = TriggerColliderComponent(commDeskObject, triggerShape3);
	myTrigger2.FilterMask = COL_PLAYER;
	myTrigger2.SetCallback([](GameObject*) {
		col.flags = Player;
		col.id = COL_PLAYER;
		message.data = (void*)&col;
		message.type = ColliderEnter;
		ImGuiPuzzleSystem::puzzleNum = 3;
		ImGuiPuzzleSystem::openPuzzle = true;
		CameraController* ctrl = Scene::GetActiveCamera()->GetParent()->GetComponent<CameraController>();
		ctrl->ToggleMouseLook();
		CharacterController::disabled = true;
		enable = true;
	});
	commDeskObject->AddComponent(myTrigger2);
	Scene::AddObject(commDeskObject);

	//Forward Desk
	GameObject *forwardDesk = new GameObject(nullptr);
	forwardDesk->LocalTransform.Position = glm::vec3(18.0f, -3.6f, -0.85f);
	forwardDesk->LocalTransform.Orientation = glm::quat(glm::vec3(0.0f, 0.0f, glm::radians(90.0f)));
	MeshRenderComponent forwardMesh3 = MeshRenderComponent(forwardDesk, consoleModel, materialConsole);
	forwardDesk->AddComponent(forwardMesh3);
	RigidBodyComponent commCol2 = RigidBodyComponent(forwardDesk,console2);
	forwardDesk->AddComponent(commCol2);
	Scene::AddObject(forwardDesk);

	//Captain Desk
	GameObject *captainDesk = new GameObject(nullptr);
	captainDesk->LocalTransform.Position = glm::vec3(15.5f, 0.0f, -0.2f);
	captainDesk->LocalTransform.Orientation = glm::quat(glm::vec3(0.0f, 0.0f, glm::radians(90.0f)));
	MeshRenderComponent captainMesh3 = MeshRenderComponent(captainDesk, consoleModel, materialConsole);
	captainDesk->AddComponent(captainMesh3);
	RigidBodyComponent capCol = RigidBodyComponent(captainDesk, console4 );
	captainDesk->AddComponent(capCol);
	Scene::AddObject(captainDesk );

	//dead person 1
	GameObject * deadGuy = new GameObject(nullptr);
	deadGuy->LocalTransform.Position = glm::vec3(15.3f, -5.1f, -1.5f);
	MeshRenderComponent deadMesh = MeshRenderComponent(deadGuy, person, materialDead);
	deadGuy->AddComponent(deadMesh);
	RigidBodyComponent deadPer = RigidBodyComponent(deadGuy, deadBody);
	deadGuy->AddComponent(deadPer);
	Scene::AddObject(deadGuy);



	//debris object
	GameObject *debrisObject = new GameObject(nullptr);
	debrisObject->LocalTransform.Position = glm::vec3(3.8f, 3.3f, 0.0f);
	debrisObject->LocalTransform.Orientation = glm::quat(glm::vec3(0, 0, glm::radians(-24.0f)));
	RigidBodyComponent debrisCol = RigidBodyComponent(debrisObject, debris);
	debrisObject->AddComponent(debrisCol);
	MeshRenderComponent debrisMesh = MeshRenderComponent(debrisObject, debrisModel, materialDebris);
	debrisObject->AddComponent(debrisMesh);

	Scene::AddObject(debrisObject);

	//debris off latch trigger, right side

	GameObject* debrisArea = new GameObject(nullptr);
	debrisArea->LocalTransform.Position = glm::vec3(3.8f,3.3f, 0.0f);
	TriggerColliderComponent debrisTrigger = TriggerColliderComponent(debrisArea, triggerDebris);

	debrisTrigger.FilterMask = COL_DEBRIS;
	debrisTrigger.SetTriggerOnEmpty(true);
	debrisTrigger.SetCallback([](GameObject*) {
		if (enable == true)
		{
			col.flags = Player;
			col.id = COL_DEBRIS;
			message.data = (void*)&col;
			message.type = ColliderEnter;
		}
		
	});
	debrisArea->AddComponent(debrisTrigger);
	Scene::AddObject(debrisArea);

	//debris off latch trigger, left side
	
	//wrench object
	GameObject *wrenchObject = new GameObject(nullptr);
	wrenchObject->LocalTransform.Position = glm::vec3(10.0f, 4.0f, -1.0f);
	MeshRenderComponent meshRender4 = MeshRenderComponent(wrenchObject, wrenchModel, material);
	wrenchObject->AddComponent(meshRender4);
	RigidBodyComponent wrenchCollider = RigidBodyComponent(wrenchObject, ball3);
	wrenchObject->AddComponent(wrenchCollider);
	TriggerColliderComponent wrenchTrigger = TriggerColliderComponent(wrenchObject, triggerWrench);
	wrenchTrigger.FilterMask = COL_PLAYER;
	wrenchTrigger.SetCallback([](GameObject* obj) {
		if (enable == true)
		{
			Scene::RemoveObject(obj);
			SoundManager::GetInstance("pickup")->Play();
			col.flags = Player;
			col.id = COL_TOOL;
			message.data = (void*)&col;
			message.type = ColliderEnter;
			toolEnable = true;
		}
	});
	wrenchObject->AddComponent(wrenchTrigger);
	Scene::AddObject(wrenchObject);


	//Hatch object
	GameObject *hatchObject = new GameObject(nullptr);
	hatchObject->LocalTransform.Position = glm::vec3(3.9f, 3.32f, -1.5f);
	hatchObject->LocalTransform.Orientation=(glm::quat(glm::vec3(0.0f, 0.0f, glm::radians(0.0f))));
	MeshRenderComponent hatchMesh = MeshRenderComponent(hatchObject, hatchModel, materialHatch);
	hatchObject->AddComponent(hatchMesh);
	Scene::AddObject(hatchObject);

	//hatch object trigger
	GameObject* hatchTri = new GameObject(nullptr);
	hatchTri->LocalTransform.Position = glm::vec3(3.9, 3.32, -1.5f);
	TriggerColliderComponent hatchTrigger = TriggerColliderComponent(hatchTri, triggerShape5);
	hatchTrigger.FilterMask = COL_PLAYER;
	hatchTrigger.SetCallback([](GameObject*) {
		col.flags = Player;
		col.id = COL_PLAYER;
		message.data = (void*)&col;
		message.type = ColliderEnter;
		enable = true;

	});
	hatchTri->AddComponent(hatchTrigger);
	Scene::AddObject(hatchTri);


	//Split door in scene
	GameObject *doorOne = new GameObject(nullptr);
	doorOne->LocalTransform.Position = glm::vec3(9.0f,-3.1f,0.0f);
	RigidBodyComponent doorCollider = RigidBodyComponent(doorOne, door);
	doorOne->AddComponent(doorCollider);
	TriggerColliderComponent myTrigger = TriggerColliderComponent(doorOne,triggerShape3);
	myTrigger.FilterMask = COL_PLAYER;
	myTrigger.SetCallback([](GameObject*) {
		if (enable)
		{
			col.flags = Player;
			col.id = COL_PLAYER;
			message.data = (void*)&col;
			message.type = ColliderEnter;
			ImGuiDebugSystem::IsOpen = false;
			ImGuiQuestSystem::open = false;
			WRAP::open = false;
			ImGuiPuzzleSystem::puzzleNum = 1;
			ImGuiPuzzleSystem::openPuzzle = true;

			
			CameraController* ctrl = Scene::GetActiveCamera()->GetParent()->GetComponent<CameraController>();
			ctrl->ToggleMouseLook();
			CharacterController::disabled = true;
			enable = false;
		}
	});
	doorOne->AddComponent(myTrigger);
	TriggerColliderComponent *fetched = doorOne->GetComponent<TriggerColliderComponent>();
	Scene::AddObject(doorOne);


	//elevator Door object
	GameObject *elevatorGameObject = new GameObject(nullptr);
	elevatorGameObject->LocalTransform.Position = glm::vec3(2.5f, 0.0f, 0.0f);
	RigidBodyComponent eleRigid = RigidBodyComponent(elevatorGameObject, elevaotrBody);
	elevatorGameObject->AddComponent(eleRigid);
	TriggerColliderComponent elevatorTrigger = TriggerColliderComponent(elevatorGameObject, triggerShape4);
	elevatorTrigger.FilterMask = COL_PLAYER;
	elevatorTrigger.SetCallback([](GameObject*) {
		col.flags = Player;
		col.id = COL_PLAYER;
		message.data = (void*)&col;
		message.type = ColliderEnter;
		std::map<std::string, SoundInstance*>::iterator sound4 = mySounds.find("puzzle");
		ImGuiDebugSystem::IsOpen = false;
		WRAP::open = false;
		ImGuiPuzzleSystem::puzzleNum = 2;
		ImGuiPuzzleSystem::openPuzzle = true;
		CameraController* ctrl = Scene::GetActiveCamera()->GetParent()->GetComponent<CameraController>();
		ctrl->ToggleMouseLook();
		CharacterController::disabled = true;
	});

	elevatorGameObject->AddComponent(elevatorTrigger);
	Scene::AddObject(elevatorGameObject);


	//GameObject for dead persons
	GameObject *deadHipsObject = new GameObject(nullptr);
	deadHipsObject->LocalTransform.Position = glm::vec3(0.7f, 0.1f, -1.2f);
	RigidBodyComponent rigHips = RigidBodyComponent(deadHipsObject, deadHi);
	deadHipsObject->AddComponent(rigHips);
	MeshRenderComponent dHips = MeshRenderComponent(deadHipsObject, deadHipsModel, materialHips);
	deadHipsObject->AddComponent(dHips);
	Scene::AddObject(deadHipsObject);

	GameObject *deadLeftArmObject = new GameObject(nullptr);
	deadLeftArmObject->LocalTransform.Position = glm::vec3(1.1f, 0.0f, -0.0f);
	RigidBodyComponent rigLA = RigidBodyComponent(deadLeftArmObject, deadLA);
	deadLeftArmObject->AddComponent(rigLA);
	MeshRenderComponent dLeftArm = MeshRenderComponent(deadLeftArmObject, deadLeftArmModel, materialLeftArm);
	deadLeftArmObject->AddComponent(dLeftArm);
	Scene::AddObject(deadLeftArmObject);

	GameObject *deadLeftLegObject = new GameObject(nullptr);
	deadLeftLegObject->LocalTransform.Position = glm::vec3(0.7f, -0.3f, -0.6f);
	RigidBodyComponent rigLL = RigidBodyComponent(deadLeftArmObject, deadLL);
	deadLeftLegObject->AddComponent(rigLL);
	MeshRenderComponent dLeftLeg= MeshRenderComponent(deadLeftLegObject, deadLeftLegModel, materialLeftLeg);
	deadLeftLegObject->AddComponent(dLeftLeg);
	Scene::AddObject(deadLeftLegObject);

	GameObject *deadRightArmObject = new GameObject(nullptr);
	deadRightArmObject->LocalTransform.Position = glm::vec3(1.0f, -0.5f, -0.7f);
	RigidBodyComponent rigRA = RigidBodyComponent(deadRightArmObject, deadRA);
	deadRightArmObject->AddComponent(rigRA);
	MeshRenderComponent dRightArm = MeshRenderComponent(deadRightArmObject, deadRightArmModel, materialRightArm);
	deadRightArmObject->AddComponent(dRightArm);
	Scene::AddObject(deadRightArmObject);


	GameObject *deadRightLegObject = new GameObject(nullptr);
	deadRightLegObject->LocalTransform.Position = glm::vec3(1.0f, 0.25f, -0.8f);
	RigidBodyComponent rigRL = RigidBodyComponent(deadRightLegObject, deadRL);
	deadRightLegObject->AddComponent(rigRL);
	MeshRenderComponent dRightLeg = MeshRenderComponent(deadRightLegObject, deadRightLegModel, materialRightLeg);
	deadRightLegObject->AddComponent(dRightLeg);
	Scene::AddObject(deadRightLegObject);

	GameObject *deadTorsoObject = new GameObject(nullptr);
	deadTorsoObject->LocalTransform.Position = glm::vec3(1.0f, 0.5f, -1.0f);
	RigidBodyComponent rigTO = RigidBodyComponent(deadTorsoObject, deadT);
	deadTorsoObject->AddComponent(rigTO);
	MeshRenderComponent dTorso = MeshRenderComponent(deadTorsoObject, deadTorsoModel, materialTorso);
	deadTorsoObject->AddComponent(dTorso);
	Scene::AddObject(deadTorsoObject);

	//puzzle system
	ImGuiPuzzleSystem puzzles;
	puzzles.Init();
	puzzles.sound = mySound2;
	puzzles.alarmSound = mySound4;
	puzzles.soundSet = &mySounds;
	(*puzzles.soundSet)["alarm"]->Loop();
	//add objects associated with puzzle systen
	puzzles.AddGameObject("door", doorOne);
	puzzles.AddGameObject("communications", commDeskObject);
	puzzles.AddGameObject("elevator", elevatorGameObject);
	puzzles.AddGameObject("debris", debrisObject);
	puzzles.AddGameObject("elevatorAnimation", morphTest);
	puzzles.AddGameObject("doorAnimation", morphTest2);
	Scene::AddSystem(puzzles);


}

void InitGameLevelControls() {

	CameraController* ctrl = Scene::GetActiveCamera()->GetParent()->GetComponent<CameraController>();

	if (ctrl)
		ctrl->SetMouseLook(true);
	
	InputManager::AddKeyEventCallback([](void*, int key, int scanCode, ButtonState state, int) {

		CameraController* ctrl = Scene::GetActiveCamera()->GetParent()->GetComponent<CameraController>();

		if (key == GLFW_KEY_F1 && state == ButtonPressed) {
			if (ctrl)
				ctrl->ToggleMouseLook();
		}
		else if (key == GLFW_KEY_F2 && state == ButtonPressed) {
			ImGuiDebugSystem::IsOpen = !ImGuiDebugSystem::IsOpen;
			ctrl->SetMouseLook(!ImGuiDebugSystem::IsOpen);

			ImGuiQuestSystem::open = false;
			ImGuiPuzzleSystem::openPuzzle = false;
		}
		else if (key == GLFW_KEY_F3 && state == ButtonPressed) {
			ResetGame();

		}
		else if (key==GLFW_KEY_E && state==ButtonPressed)
		{
			if (toolEnable)
			{
				col.flags = Player;
				col.id = COL_TOOL_USE;
				message.data = (void*)&col;
				message.type = ColliderEnter;
				Questbook::Update(message);
			}
		}
        else if (key == GLFW_KEY_TAB && state == ButtonPressed)
		{
			std::map<std::string, SoundInstance*>::iterator sound = (mySounds.find("Beep"));
			if (sound!=mySounds.end())
			{
				if (sound->second->IsPlaying())
					sound->second->Stop();
				ImGuiQuestSystem::showPrompt = false;
			}
			if (WRAP::open)
			{
				WRAP::open = false;
				CharacterController::disabled = false;
			}
			else
			{
				WRAP::open = true;
				CharacterController::disabled = true;
			}
			ctrl->SetMouseLook(!CharacterController::disabled);
			
		}
	});

	InputManager::AddKeyEventCallback([](void*, int key, int scanCode, ButtonState state, int) {
		if (key == GLFW_KEY_ESCAPE) {
			Scene::GetWindow()->Close();
		}
	});
}




int main(int argc, char *argv[]){ 
    FILE* file = fopen("logs.txt", "w");
    Output2FILE::Stream() = file;
	
    uint32_t windowWidth{960}, windowHeight{540};
    Window myWindow = Window(windowWidth, windowHeight);
	Scene::SetWindow(&myWindow);

	{
		TrueTypeFont *font = new TrueTypeFont("res/fonts/Roboto-Medium.ttf", 64);
		CMS::Register(CmsType::TypeFont, "font",font);
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		FontRenderer::Render(*font, "Loading...", glm::vec2(300, 520), glm::vec4(1.0f));
		glfwSwapBuffers(myWindow.GetHandle());
	}
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    FILE_LOG(logDEBUG) << "Loading cms...";

	double startTime = glfwGetTime();
	
	CMS::LoadManifest("res/manifest.xml");
	CMS::LoadBundle("space");
	CMS::LoadBundle("common");
	CMS::LoadBundle("shaders");

	double endTime = glfwGetTime();
	FILE_LOG(logINFO) << "Loaded bundles in " << endTime - startTime << " seconds";

    //BitmapFont* font = new BitmapFont("res/debugFont.fnt");
	//TrueTypeFont *font = new TrueTypeFont("res/fonts/Roboto-Medium.ttf", 32);

    FILE_LOG(logDEBUG) << "Done";

    FILE_LOG(logDEBUG) << "Starting audio loading...";
	
    FILE_LOG(logDEBUG) << "Done";        
				
	// Set up alpha blending
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	// Enable backface culling
	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);
	// Clear to black
	glClearColor(0, 0, 0, 1);

	BuildScene();

	InitGameLevelControls();

	Scene::Load();

	PrimitiveBatch3D::Init();
	
	myWindow.TriggerResize();

	myWindow.InitRenderLoop();


	col.flags = Object;
	message.data = (void*)&col;
	message.type = ColliderEnter;
    /* Loop until the user closes the window */
	FILE_LOG(logINFO) << "Starting main render loop";
	
    while (!myWindow.Closing())
    {
		myWindow.Update();

		GameTime::TotalTime = myWindow.GetFrameTime(); 
		GameTime::DeltaTime = myWindow.GetFrameDelta();
		Questbook::Update(message);

 		Questbook::Draw();
		
		Scene::Update();
    }

	Scene::Unload();
	Scene::Cleanup();
		
    return 0;
}

void InputControllerExample() {

	ControllerMapping mapping;

	const char* name = InputManager::GetJoystickName(0);
	if (name != 0) {
		mapping = ControllerMapping::GetKnownMapping(name);
		mapping.Print(cout);

		InputManager::AddJoystickButtonCallback([&](void*, int joystick, int button, ButtonState newState) {
			if (newState == ButtonPressed) {

			}
		});
	}

	struct InputController {
		std::unordered_map<std::string, AxisInputSource*>   Axes;
		std::unordered_map<std::string, ButtonInputSource*> Buttons;
	};

	AxisInputSource* p1_forward = new JoystickAxisSource(GLFW_JOYSTICK_1, 1);
	AxisInputSource* p1_strafe = new JoystickAxisSource(GLFW_JOYSTICK_1, 0);
	AxisInputSource* p1_pitch = new JoystickAxisSource(GLFW_JOYSTICK_1, 3);
	AxisInputSource* p1_pan = new JoystickAxisSource(GLFW_JOYSTICK_1, 2);
	AxisInputSource* p1_up = new NormalizedJoystickAxisSource(GLFW_JOYSTICK_1, 5);
	AxisInputSource* p1_down = new NormalizedJoystickAxisSource(GLFW_JOYSTICK_1, 4);

	AxisInputSource* kb_forward = new KeyboardAxisSource(GLFW_KEY_W);
	AxisInputSource* kb_back = new KeyboardAxisSource(GLFW_KEY_S);
	AxisInputSource* kb_left = new KeyboardAxisSource(GLFW_KEY_A);
	AxisInputSource* kb_right = new KeyboardAxisSource(GLFW_KEY_D);
	AxisInputSource* kb_up = new KeyboardAxisSource(GLFW_KEY_SPACE);
	AxisInputSource* kb_down = new KeyboardAxisSource(GLFW_KEY_LEFT_CONTROL);

	AxisInputSource* m_vert = new MouseDeltaYAxisSource();
	AxisInputSource* m_horiz = new MouseDeltaXAxisSource();

	ButtonInputSource* p1_rGradeU = new JoystickButtonSource(GLFW_JOYSTICK_1, mapping.GetButtonLoc("X"));
	ButtonInputSource* p1_rGradeD = new JoystickButtonSource(GLFW_JOYSTICK_1, mapping.GetButtonLoc("B"));

	ButtonInputSource* p1_gGradeU = new JoystickButtonSource(GLFW_JOYSTICK_1, mapping.GetButtonLoc("DPAD_DOWN"));
	ButtonInputSource* p1_gGradeD = new JoystickButtonSource(GLFW_JOYSTICK_1, mapping.GetButtonLoc("DPAD_UP"));
	ButtonInputSource* p1_bGradeU = new JoystickButtonSource(GLFW_JOYSTICK_1, mapping.GetButtonLoc("DPAD_LEFT"));
	ButtonInputSource* p1_bGradeD = new JoystickButtonSource(GLFW_JOYSTICK_1, mapping.GetButtonLoc("DPAD_RIGHT"));
	ButtonInputSource* p1_regrets = new JoystickButtonSource(GLFW_JOYSTICK_1, mapping.GetButtonLoc("START"));

	AxisAccumulator* forward_accumulator = new AxisAccumulator(new AxisInputSource*[3]{
		p1_forward,
		kb_forward,
		kb_back
	}, new float[3]{ 1.0f, 1.0f, -1.0f }, 3);

	AxisAccumulator* strafe_accumulator = new AxisAccumulator(new AxisInputSource*[3]{
		p1_strafe,
		kb_left,
		kb_right
	}, new float[3]{ 1.0f, -1.0f, 1.0f }, 3);

	AxisAccumulator* vertical_accumulator = new AxisAccumulator(new AxisInputSource*[4]{
		p1_up,
		p1_down,
		kb_up,
		kb_down
	}, new float[4]{ 1.0f, -1.0f, 1.0f, -1.0f }, 4);

	AxisAccumulator* pitch_accumulator = new AxisAccumulator(new AxisInputSource*[3]{
		p1_pitch,
		m_vert
	}, new float[2]{ 1.0f, -1.0f }, 2, -25.0f, 25.0f);

	AxisAccumulator* pan_accumulator = new AxisAccumulator(new AxisInputSource*[3]{
		p1_pan,
		m_horiz
	}, new float[2]{ 1.0f, 1.0f }, 2, -25.0f, 25.0f);

	InputController cameraController = InputController();
	cameraController.Axes["forward"] = forward_accumulator;
	cameraController.Axes["strafe"] = strafe_accumulator;
	cameraController.Axes["vertical"] = vertical_accumulator;

	cameraController.Axes["pitch"] = pitch_accumulator;
	cameraController.Axes["pan"] = pan_accumulator;
}
