﻿namespace ManifestPacker
{
    public class ShaderItem
    {
        public string Name;

        public string FsSourcePath;
        public string VsSourcePath;
        public string GsSourcePath;

        public ShaderItem(string name) {
            Name = name;
        }
    }
}