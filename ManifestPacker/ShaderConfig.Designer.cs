﻿namespace ManifestPacker
{
    partial class ShaderConfig
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ctrlVsSource = new ManifestPacker.FileConfigItem();
            this.ctrlFsSource = new ManifestPacker.FileConfigItem();
            this.ctrlGsSource = new ManifestPacker.FileConfigItem();
            this.SuspendLayout();
            // 
            // ctrlVsSource
            // 
            this.ctrlVsSource.DefaultExt = "";
            this.ctrlVsSource.Dock = System.Windows.Forms.DockStyle.Top;
            this.ctrlVsSource.FileFilter = "";
            this.ctrlVsSource.FileName = "";
            this.ctrlVsSource.LabelText = "VS Source:";
            this.ctrlVsSource.Location = new System.Drawing.Point(0, 0);
            this.ctrlVsSource.Name = "ctrlVsSource";
            this.ctrlVsSource.Size = new System.Drawing.Size(371, 30);
            this.ctrlVsSource.TabIndex = 0;
            // 
            // ctrlFsSource
            // 
            this.ctrlFsSource.DefaultExt = "";
            this.ctrlFsSource.Dock = System.Windows.Forms.DockStyle.Top;
            this.ctrlFsSource.FileFilter = "";
            this.ctrlFsSource.FileName = "";
            this.ctrlFsSource.LabelText = "FS Source:";
            this.ctrlFsSource.Location = new System.Drawing.Point(0, 30);
            this.ctrlFsSource.Name = "ctrlFsSource";
            this.ctrlFsSource.Size = new System.Drawing.Size(371, 30);
            this.ctrlFsSource.TabIndex = 1;
            // 
            // ctrlGsSource
            // 
            this.ctrlGsSource.DefaultExt = "";
            this.ctrlGsSource.Dock = System.Windows.Forms.DockStyle.Top;
            this.ctrlGsSource.FileFilter = "";
            this.ctrlGsSource.FileName = "";
            this.ctrlGsSource.LabelText = "GS Source:";
            this.ctrlGsSource.Location = new System.Drawing.Point(0, 60);
            this.ctrlGsSource.Name = "ctrlGsSource";
            this.ctrlGsSource.Size = new System.Drawing.Size(371, 30);
            this.ctrlGsSource.TabIndex = 2;
            // 
            // ShaderConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ctrlGsSource);
            this.Controls.Add(this.ctrlFsSource);
            this.Controls.Add(this.ctrlVsSource);
            this.Name = "ShaderConfig";
            this.Size = new System.Drawing.Size(371, 94);
            this.ResumeLayout(false);

        }

        #endregion

        private FileConfigItem ctrlVsSource;
        private FileConfigItem ctrlFsSource;
        private FileConfigItem ctrlGsSource;
    }
}
