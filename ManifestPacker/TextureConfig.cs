﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ManifestPacker
{
    public partial class TextureConfig : UserControl
    {
        private TextureItem myItem;

        public TextureItem Item
        {
            get
            {
                if (myItem != null)
                {
                    myItem.SourcePath = ctrlSource.FileName;
                    myItem.WrapMode = ((TextItem)cmbWrap.SelectedItem).InternalText;
                    myItem.SampleMode = ((TextItem)cmbSampleState.SelectedItem).InternalText;
                    myItem.AnisoEnabled = chkAniso.Checked;
                }

                return myItem;
            }
            set {
                myItem = value;

                if (value != null)
                {
                    ctrlSource.FileName = myItem.SourcePath;
                    chkAniso.Checked = myItem.AnisoEnabled;

                    for (int ix = 0; ix < cmbWrap.Items.Count; ix++)
                    {
                        if (((TextItem)cmbWrap.Items[ix]).InternalText == value.WrapMode)
                            cmbWrap.SelectedIndex = ix;
                    }

                    for (int ix = 0; ix < cmbSampleState.Items.Count; ix++)
                    {
                        if (((TextItem)cmbSampleState.Items[ix]).InternalText == value.SampleMode)
                            cmbSampleState.SelectedIndex = ix;
                    }
                }
            }
        }
        public TextureConfig()
        {
            InitializeComponent();

            cmbWrap.Items.Add(new TextItem("Clamp to Edge", "edge"));
            cmbWrap.Items.Add(new TextItem("Repeat", "repeat"));
            cmbWrap.Items.Add(new TextItem("Mirrored Repeat", "mrepeat"));
            cmbWrap.Items.Add(new TextItem("Clamp to Border", "border"));

            cmbWrap.SelectedIndex = 0;


            cmbSampleState.Items.Add(new TextItem("Linear", "linear"));
            cmbSampleState.Items.Add(new TextItem("Nearest", "nearest"));
            cmbSampleState.Items.Add(new TextItem("Mip Near Map Near", "nmipmapn"));
            cmbSampleState.Items.Add(new TextItem("Mip Linear Map Near", "lmipmapn"));
            cmbSampleState.Items.Add(new TextItem("Mip Linear Map Linear", "lmipmapl"));
            cmbSampleState.Items.Add(new TextItem("Mip Near Map Linear", "nmipmapl"));
            
            cmbSampleState.SelectedIndex = 0;
        }
    }

    class TextItem
    {
        public string VisibleText;
        public string InternalText;

        public TextItem(string visible, string internalText)
        {
            VisibleText = visible;
            InternalText = internalText;
        }


        public override string ToString()
        {
            return VisibleText;
        }
    }
}
