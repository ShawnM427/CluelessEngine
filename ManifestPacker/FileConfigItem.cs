﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace ManifestPacker
{

    public partial class FileConfigItem : UserControl
    {
        public string FileFilter
        {
            get { return dlgBrowseFile.Filter; }
            set { dlgBrowseFile.Filter = value; }
        }
        public string DefaultExt
        {
            get { return dlgBrowseFile.DefaultExt; }
            set { dlgBrowseFile.DefaultExt = value; }
        }
        public string FileName
        {
            get { return txtFileName.Text; }
            set { txtFileName.Text = value; }
        }
        public string LabelText
        {
            get { return lblFileName.Text; }
            set { lblFileName.Text = value; }
        }
     
        public FileConfigItem()
        {
            InitializeComponent();
        }

        private void btnBrowseFile_Click(object sender, EventArgs e) {

            dlgBrowseFile.InitialDirectory = Settings.WorkingDirectory;
            DialogResult result = dlgBrowseFile.ShowDialog();

            if (result == DialogResult.OK) {
                string path = dlgBrowseFile.FileName;
                path = path.Replace('\\', '/');

                if (path.Contains(Settings.WorkingDirectory)) {
                    string relPath = path.Replace(Settings.WorkingDirectory + "/", "");
                    relPath = relPath.Replace('\\', '/');

                    FileName = relPath;
                } else {
                    MessageBox.Show("Path must be within working directory");
                }
            }

        }
    }
}
