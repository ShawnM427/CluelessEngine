﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ManifestPacker
{
    public partial class ModelConfig : UserControl
    {
        private ModelItem myItem;

        public ModelItem Item
        {
            get
            {
                if (myItem != null)
                {
                    myItem.SourceFile = ctrlSource.FileName;
                    myItem.Selector = txtSelector.Text;
                }

                return myItem;
            }
            set {
                myItem = value;

                if (value != null)
                {
                    ctrlSource.FileName = myItem.SourceFile;
                    txtSelector.Text = myItem.Selector;
                }
            }
        }
        public ModelConfig()
        {
            InitializeComponent();
        }
    }
    
}
