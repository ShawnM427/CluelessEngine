﻿namespace ManifestPacker
{
    public class SoundEffectItem
    {
        public string Name;

        public string Source;
        public float Gain;
        public float Pitch;
        public float Pan;

        public SoundEffectItem(string name)
        {
            Name = name;

            Gain  = 1.0f;
            Pan   = 0.0f;
            Pitch = 1.0f;
        }
    }
}