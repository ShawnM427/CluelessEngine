﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ManifestPacker
{
    public partial class CubeMapConfig : UserControl
    {
        private CubeMapItem myItem;

        public CubeMapItem Item
        {
            get
            {
                if (myItem != null) {
                     myItem.PosXSource = ctrlPosX.FileName;
                     myItem.NegXSource = ctrlNegX.FileName;
                                       
                     myItem.NegYSource = ctrlNegY.FileName;
                     myItem.PosYSource = ctrlPosY.FileName;
                                       
                     myItem.NegZSource = ctrlNegZ.FileName;
                     myItem.PosZSource = ctrlPosZ.FileName;
                }                      

                return myItem;
            }
            set
            {
                myItem = value;

                if (myItem != null)
                {
                    ctrlNegX.FileName = myItem.NegXSource;
                    ctrlPosX.FileName = myItem.PosXSource;

                    ctrlNegY.FileName = myItem.NegYSource;
                    ctrlPosY.FileName = myItem.PosYSource;

                    ctrlNegZ.FileName = myItem.NegZSource;
                    ctrlPosZ.FileName = myItem.PosZSource;
                }
            }
        }
        public CubeMapConfig()
        {
            InitializeComponent();
        }
    }
}
