﻿namespace ManifestPacker
{
    public class TextureItem
    {
        public string Name;

        public string SourcePath;
        public string WrapMode;
        public string SampleMode;
        public bool  AnisoEnabled;

        public TextureItem(string name)
        {
            Name = name;
            SourcePath = "";
            WrapMode = "edge";
            SampleMode = "linear";
            AnisoEnabled = false;
        }
    }
}