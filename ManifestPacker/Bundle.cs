﻿using System;
using System.Collections.Generic;

namespace ManifestPacker
{
    internal class Bundle {

        public Bundle()
        {
            TextureItems = new List<TextureItem>();
            ModelItems   = new List<ModelItem>();
            CubeMapItems = new List<CubeMapItem>();
            ShaderItems  = new List<ShaderItem>();
            SoundItems   = new List<SoundEffectItem>(); 
        }

        public string Name;

        public List<TextureItem>     TextureItems;
        public List<ModelItem>       ModelItems;
        public List<CubeMapItem>     CubeMapItems;
        public List<ShaderItem>      ShaderItems;
        public List<SoundEffectItem> SoundItems;

        internal void AddTypelessResource(object item)
        {
            if (item.GetType() == typeof(TextureItem))
                TextureItems.Add((TextureItem)item);
            else if (item.GetType() == typeof(ModelItem))
                ModelItems.Add((ModelItem)item);
            else if (item.GetType() == typeof(CubeMapItem))
                CubeMapItems.Add((CubeMapItem)item);
            else if (item.GetType() == typeof(TextureItem))
                TextureItems.Add((TextureItem)item);
            else if (item.GetType() == typeof(ShaderItem))
                ShaderItems.Add((ShaderItem)item);
            else if (item.GetType() == typeof(SoundEffectItem))
                SoundItems.Add((SoundEffectItem)item);
        }

        internal void RemoveGeneric(object item)
        {
            if (item.GetType() == typeof(TextureItem))
                TextureItems.Remove((TextureItem)item);
            else if (item.GetType() == typeof(ModelItem))
                ModelItems.Remove((ModelItem)item);
            else if (item.GetType() == typeof(CubeMapItem))
                CubeMapItems.Remove((CubeMapItem)item);
            else if (item.GetType() == typeof(TextureItem))
                TextureItems.Remove((TextureItem)item);
            else if (item.GetType() == typeof(ShaderItem))
                ShaderItems.Remove((ShaderItem)item);
            else if (item.GetType() == typeof(SoundEffectItem))
                SoundItems.Remove((SoundEffectItem)item);
        }
    }
}