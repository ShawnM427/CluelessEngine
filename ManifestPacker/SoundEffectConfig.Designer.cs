﻿namespace ManifestPacker
{
    partial class SoundEffectConfig
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ctrlSource = new ManifestPacker.FileConfigItem();
            this.trkPitch = new System.Windows.Forms.TrackBar();
            this.pnlPitch = new System.Windows.Forms.Panel();
            this.lblPitch = new System.Windows.Forms.Label();
            this.pnlGain = new System.Windows.Forms.Panel();
            this.trkGain = new System.Windows.Forms.TrackBar();
            this.lblGain = new System.Windows.Forms.Label();
            this.pnlPan = new System.Windows.Forms.Panel();
            this.trkPan = new System.Windows.Forms.TrackBar();
            this.lblPan = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.trkPitch)).BeginInit();
            this.pnlPitch.SuspendLayout();
            this.pnlGain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trkGain)).BeginInit();
            this.pnlPan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trkPan)).BeginInit();
            this.SuspendLayout();
            // 
            // ctrlSource
            // 
            this.ctrlSource.DefaultExt = "";
            this.ctrlSource.Dock = System.Windows.Forms.DockStyle.Top;
            this.ctrlSource.FileFilter = "WAV|*.wav|OGG Vorbis|*.ogg";
            this.ctrlSource.FileName = "";
            this.ctrlSource.LabelText = "Source:";
            this.ctrlSource.Location = new System.Drawing.Point(0, 0);
            this.ctrlSource.Name = "ctrlSource";
            this.ctrlSource.Size = new System.Drawing.Size(420, 30);
            this.ctrlSource.TabIndex = 8;
            // 
            // trkPitch
            // 
            this.trkPitch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trkPitch.Location = new System.Drawing.Point(83, 0);
            this.trkPitch.Maximum = 200;
            this.trkPitch.Name = "trkPitch";
            this.trkPitch.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.trkPitch.Size = new System.Drawing.Size(332, 49);
            this.trkPitch.TabIndex = 9;
            this.trkPitch.TickFrequency = 20;
            this.trkPitch.Value = 100;
            this.trkPitch.MouseUp += new System.Windows.Forms.MouseEventHandler(this.trkPitch_MouseUp);
            // 
            // pnlPitch
            // 
            this.pnlPitch.Controls.Add(this.trkPitch);
            this.pnlPitch.Controls.Add(this.lblPitch);
            this.pnlPitch.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlPitch.Location = new System.Drawing.Point(0, 35);
            this.pnlPitch.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.pnlPitch.Name = "pnlPitch";
            this.pnlPitch.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.pnlPitch.Size = new System.Drawing.Size(420, 49);
            this.pnlPitch.TabIndex = 12;
            // 
            // lblPitch
            // 
            this.lblPitch.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblPitch.Location = new System.Drawing.Point(0, 0);
            this.lblPitch.Name = "lblPitch";
            this.lblPitch.Size = new System.Drawing.Size(83, 49);
            this.lblPitch.TabIndex = 10;
            this.lblPitch.Text = "Pitch:";
            this.lblPitch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlGain
            // 
            this.pnlGain.Controls.Add(this.trkGain);
            this.pnlGain.Controls.Add(this.lblGain);
            this.pnlGain.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlGain.Location = new System.Drawing.Point(0, 133);
            this.pnlGain.Name = "pnlGain";
            this.pnlGain.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.pnlGain.Size = new System.Drawing.Size(420, 49);
            this.pnlGain.TabIndex = 13;
            // 
            // trkGain
            // 
            this.trkGain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trkGain.Location = new System.Drawing.Point(83, 0);
            this.trkGain.Maximum = 200;
            this.trkGain.Name = "trkGain";
            this.trkGain.Size = new System.Drawing.Size(332, 49);
            this.trkGain.TabIndex = 9;
            this.trkGain.TickFrequency = 20;
            this.trkGain.Value = 100;
            this.trkGain.MouseUp += new System.Windows.Forms.MouseEventHandler(this.trkGain_MouseUp);
            // 
            // lblGain
            // 
            this.lblGain.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblGain.Location = new System.Drawing.Point(0, 0);
            this.lblGain.Name = "lblGain";
            this.lblGain.Size = new System.Drawing.Size(83, 49);
            this.lblGain.TabIndex = 10;
            this.lblGain.Text = "Gain:";
            this.lblGain.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlPan
            // 
            this.pnlPan.Controls.Add(this.trkPan);
            this.pnlPan.Controls.Add(this.lblPan);
            this.pnlPan.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlPan.Location = new System.Drawing.Point(0, 84);
            this.pnlPan.Name = "pnlPan";
            this.pnlPan.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.pnlPan.Size = new System.Drawing.Size(420, 49);
            this.pnlPan.TabIndex = 13;
            // 
            // trkPan
            // 
            this.trkPan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trkPan.Location = new System.Drawing.Point(83, 0);
            this.trkPan.Maximum = 100;
            this.trkPan.Minimum = -100;
            this.trkPan.Name = "trkPan";
            this.trkPan.Size = new System.Drawing.Size(332, 49);
            this.trkPan.TabIndex = 9;
            this.trkPan.TickFrequency = 20;
            this.trkPan.MouseUp += new System.Windows.Forms.MouseEventHandler(this.trkPan_MouseUp);
            // 
            // lblPan
            // 
            this.lblPan.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblPan.Location = new System.Drawing.Point(0, 0);
            this.lblPan.Name = "lblPan";
            this.lblPan.Size = new System.Drawing.Size(83, 49);
            this.lblPan.TabIndex = 10;
            this.lblPan.Text = "Pan:";
            this.lblPan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(420, 5);
            this.panel1.TabIndex = 14;
            // 
            // SoundEffectConfig
            // 
            this.Controls.Add(this.pnlGain);
            this.Controls.Add(this.pnlPan);
            this.Controls.Add(this.pnlPitch);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.ctrlSource);
            this.Name = "SoundEffectConfig";
            this.Size = new System.Drawing.Size(420, 178);
            ((System.ComponentModel.ISupportInitialize)(this.trkPitch)).EndInit();
            this.pnlPitch.ResumeLayout(false);
            this.pnlPitch.PerformLayout();
            this.pnlGain.ResumeLayout(false);
            this.pnlGain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trkGain)).EndInit();
            this.pnlPan.ResumeLayout(false);
            this.pnlPan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trkPan)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private FileConfigItem ctrlSource;
        private System.Windows.Forms.TrackBar trkPitch;
        private System.Windows.Forms.Panel pnlPitch;
        private System.Windows.Forms.Label lblPitch;
        private System.Windows.Forms.Panel pnlGain;
        private System.Windows.Forms.TrackBar trkGain;
        private System.Windows.Forms.Label lblGain;
        private System.Windows.Forms.Panel pnlPan;
        private System.Windows.Forms.TrackBar trkPan;
        private System.Windows.Forms.Label lblPan;
        private System.Windows.Forms.Panel panel1;
    }
}
