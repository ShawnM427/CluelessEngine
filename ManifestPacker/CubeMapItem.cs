﻿namespace ManifestPacker
{
    public class CubeMapItem
    {
        public string Name;

        public string NegXSource;
        public string PosXSource;
        public string NegYSource;
        public string PosYSource;
        public string NegZSource;
        public string PosZSource;

        public CubeMapItem(string name)
        {
            Name = name;
        }
    }
}