﻿namespace ManifestPacker
{
    partial class ModelConfig
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSelector = new System.Windows.Forms.Label();
            this.ctrlSource = new ManifestPacker.FileConfigItem();
            this.txtSelector = new System.Windows.Forms.TextBox();
            this.pnlSelector = new System.Windows.Forms.Panel();
            this.pnlSelector.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSelector
            // 
            this.lblSelector.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblSelector.Location = new System.Drawing.Point(0, 0);
            this.lblSelector.Name = "lblSelector";
            this.lblSelector.Size = new System.Drawing.Size(80, 20);
            this.lblSelector.TabIndex = 4;
            this.lblSelector.Text = "Selector:";
            this.lblSelector.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ctrlSource
            // 
            this.ctrlSource.DefaultExt = "";
            this.ctrlSource.Dock = System.Windows.Forms.DockStyle.Top;
            this.ctrlSource.FileFilter = "OBJ|*.obj|DAE|*.dae|FBX|*.fbx";
            this.ctrlSource.FileName = "";
            this.ctrlSource.LabelText = "Source:";
            this.ctrlSource.Location = new System.Drawing.Point(0, 0);
            this.ctrlSource.Name = "ctrlSource";
            this.ctrlSource.Size = new System.Drawing.Size(362, 30);
            this.ctrlSource.TabIndex = 8;
            // 
            // txtSelector
            // 
            this.txtSelector.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSelector.Location = new System.Drawing.Point(80, 0);
            this.txtSelector.Name = "txtSelector";
            this.txtSelector.Size = new System.Drawing.Size(282, 20);
            this.txtSelector.TabIndex = 9;
            // 
            // pnlSelector
            // 
            this.pnlSelector.Controls.Add(this.txtSelector);
            this.pnlSelector.Controls.Add(this.lblSelector);
            this.pnlSelector.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSelector.Location = new System.Drawing.Point(0, 30);
            this.pnlSelector.Name = "pnlSelector";
            this.pnlSelector.Size = new System.Drawing.Size(362, 20);
            this.pnlSelector.TabIndex = 10;
            // 
            // ModelConfig
            // 
            this.Controls.Add(this.pnlSelector);
            this.Controls.Add(this.ctrlSource);
            this.Name = "ModelConfig";
            this.Size = new System.Drawing.Size(362, 56);
            this.pnlSelector.ResumeLayout(false);
            this.pnlSelector.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblSelector;
        private FileConfigItem ctrlSource;
        private System.Windows.Forms.TextBox txtSelector;
        private System.Windows.Forms.Panel pnlSelector;
    }
}
