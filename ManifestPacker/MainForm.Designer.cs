﻿namespace ManifestPacker
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Package");
            this.trvPackage = new System.Windows.Forms.TreeView();
            this.cmsPackage = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsiAddBundle = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsBundle = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsiAddTexture = new System.Windows.Forms.ToolStripMenuItem();
            this.tsiAddShader = new System.Windows.Forms.ToolStripMenuItem();
            this.tsiAddModel = new System.Windows.Forms.ToolStripMenuItem();
            this.tsiAddCubeMap = new System.Windows.Forms.ToolStripMenuItem();
            this.tsiAddSoundEffect = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsiBundleRename = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsCollection = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsiAddItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMainMenu = new System.Windows.Forms.MenuStrip();
            this.tsiFile = new System.Windows.Forms.ToolStripMenuItem();
            this.tsiLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.tsiSave = new System.Windows.Forms.ToolStripMenuItem();
            this.tsiSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.tsiNew = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsiProjectSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlMainContent = new System.Windows.Forms.Panel();
            this.grpModel = new System.Windows.Forms.GroupBox();
            this.ctrlModel = new ManifestPacker.ModelConfig();
            this.grpSoundEffect = new System.Windows.Forms.GroupBox();
            this.ctrlSoundConfig = new ManifestPacker.SoundEffectConfig();
            this.grpCubeMap = new System.Windows.Forms.GroupBox();
            this.ctrlCubeMap = new ManifestPacker.CubeMapConfig();
            this.grpShader = new System.Windows.Forms.GroupBox();
            this.ctrlShaderConfig = new ManifestPacker.ShaderConfig();
            this.grpTexture = new System.Windows.Forms.GroupBox();
            this.ctrlTexture = new ManifestPacker.TextureConfig();
            this.dlgOpenManifest = new System.Windows.Forms.OpenFileDialog();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker2 = new System.ComponentModel.BackgroundWorker();
            this.dlgSaveManifest = new System.Windows.Forms.SaveFileDialog();
            this.cmsItemMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsiItemRename = new System.Windows.Forms.ToolStripMenuItem();
            this.tsiItemDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsPackage.SuspendLayout();
            this.cmsBundle.SuspendLayout();
            this.cmsCollection.SuspendLayout();
            this.mnuMainMenu.SuspendLayout();
            this.pnlMainContent.SuspendLayout();
            this.grpModel.SuspendLayout();
            this.grpSoundEffect.SuspendLayout();
            this.grpCubeMap.SuspendLayout();
            this.grpShader.SuspendLayout();
            this.grpTexture.SuspendLayout();
            this.cmsItemMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // trvPackage
            // 
            this.trvPackage.Dock = System.Windows.Forms.DockStyle.Left;
            this.trvPackage.LabelEdit = true;
            this.trvPackage.Location = new System.Drawing.Point(0, 24);
            this.trvPackage.Name = "trvPackage";
            treeNode1.Name = "tvnPackage";
            treeNode1.Text = "Package";
            this.trvPackage.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1});
            this.trvPackage.Size = new System.Drawing.Size(216, 485);
            this.trvPackage.TabIndex = 0;
            this.trvPackage.BeforeLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.ValidateCanEditLabel);
            this.trvPackage.AfterLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.ValidateLabelEdit);
            // 
            // cmsPackage
            // 
            this.cmsPackage.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsiAddBundle});
            this.cmsPackage.Name = "cmsPackage";
            this.cmsPackage.Size = new System.Drawing.Size(137, 26);
            // 
            // tsiAddBundle
            // 
            this.tsiAddBundle.Name = "tsiAddBundle";
            this.tsiAddBundle.Size = new System.Drawing.Size(136, 22);
            this.tsiAddBundle.Text = "Add Bundle";
            this.tsiAddBundle.Click += new System.EventHandler(this.AddBundleClicked);
            // 
            // cmsBundle
            // 
            this.cmsBundle.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsiAddTexture,
            this.tsiAddShader,
            this.tsiAddModel,
            this.tsiAddCubeMap,
            this.tsiAddSoundEffect,
            this.toolStripMenuItem2,
            this.tsiBundleRename});
            this.cmsBundle.Name = "cmsBundle";
            this.cmsBundle.Size = new System.Drawing.Size(167, 142);
            // 
            // tsiAddTexture
            // 
            this.tsiAddTexture.Name = "tsiAddTexture";
            this.tsiAddTexture.Size = new System.Drawing.Size(166, 22);
            this.tsiAddTexture.Text = "Add Texture";
            // 
            // tsiAddShader
            // 
            this.tsiAddShader.Name = "tsiAddShader";
            this.tsiAddShader.Size = new System.Drawing.Size(166, 22);
            this.tsiAddShader.Text = "Add Shader";
            // 
            // tsiAddModel
            // 
            this.tsiAddModel.Name = "tsiAddModel";
            this.tsiAddModel.Size = new System.Drawing.Size(166, 22);
            this.tsiAddModel.Text = "Add Model";
            // 
            // tsiAddCubeMap
            // 
            this.tsiAddCubeMap.Name = "tsiAddCubeMap";
            this.tsiAddCubeMap.Size = new System.Drawing.Size(166, 22);
            this.tsiAddCubeMap.Text = "Add CubeMap";
            // 
            // tsiAddSoundEffect
            // 
            this.tsiAddSoundEffect.Name = "tsiAddSoundEffect";
            this.tsiAddSoundEffect.Size = new System.Drawing.Size(166, 22);
            this.tsiAddSoundEffect.Text = "Add Sound Effect";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(163, 6);
            // 
            // tsiBundleRename
            // 
            this.tsiBundleRename.Name = "tsiBundleRename";
            this.tsiBundleRename.Size = new System.Drawing.Size(166, 22);
            this.tsiBundleRename.Text = "Rename";
            this.tsiBundleRename.Click += new System.EventHandler(this.BundleRename);
            // 
            // cmsCollection
            // 
            this.cmsCollection.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsiAddItem});
            this.cmsCollection.Name = "cmsCollection";
            this.cmsCollection.Size = new System.Drawing.Size(124, 26);
            // 
            // tsiAddItem
            // 
            this.tsiAddItem.Name = "tsiAddItem";
            this.tsiAddItem.Size = new System.Drawing.Size(123, 22);
            this.tsiAddItem.Text = "Add Item";
            this.tsiAddItem.Click += new System.EventHandler(this.AddCollectionItemClicked);
            // 
            // mnuMainMenu
            // 
            this.mnuMainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsiFile});
            this.mnuMainMenu.Location = new System.Drawing.Point(0, 0);
            this.mnuMainMenu.Name = "mnuMainMenu";
            this.mnuMainMenu.Size = new System.Drawing.Size(625, 24);
            this.mnuMainMenu.TabIndex = 3;
            this.mnuMainMenu.Text = "menuStrip1";
            // 
            // tsiFile
            // 
            this.tsiFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsiLoad,
            this.tsiSave,
            this.tsiSaveAs,
            this.tsiNew,
            this.toolStripMenuItem1,
            this.tsiProjectSettings});
            this.tsiFile.Name = "tsiFile";
            this.tsiFile.Size = new System.Drawing.Size(37, 20);
            this.tsiFile.Text = "File";
            // 
            // tsiLoad
            // 
            this.tsiLoad.Name = "tsiLoad";
            this.tsiLoad.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.tsiLoad.Size = new System.Drawing.Size(186, 22);
            this.tsiLoad.Text = "Load";
            this.tsiLoad.Click += new System.EventHandler(this.tsiLoad_Click);
            // 
            // tsiSave
            // 
            this.tsiSave.Name = "tsiSave";
            this.tsiSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.tsiSave.Size = new System.Drawing.Size(186, 22);
            this.tsiSave.Text = "Save";
            this.tsiSave.Click += new System.EventHandler(this.tsiSave_Click);
            // 
            // tsiSaveAs
            // 
            this.tsiSaveAs.Name = "tsiSaveAs";
            this.tsiSaveAs.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.tsiSaveAs.Size = new System.Drawing.Size(186, 22);
            this.tsiSaveAs.Text = "Save As";
            this.tsiSaveAs.Click += new System.EventHandler(this.tsiSaveAs_Click);
            // 
            // tsiNew
            // 
            this.tsiNew.Name = "tsiNew";
            this.tsiNew.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsiNew.Size = new System.Drawing.Size(186, 22);
            this.tsiNew.Text = "New";
            this.tsiNew.Click += new System.EventHandler(this.tsiNew_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(183, 6);
            // 
            // tsiProjectSettings
            // 
            this.tsiProjectSettings.Name = "tsiProjectSettings";
            this.tsiProjectSettings.Size = new System.Drawing.Size(186, 22);
            this.tsiProjectSettings.Text = "Settings";
            // 
            // pnlMainContent
            // 
            this.pnlMainContent.Controls.Add(this.grpModel);
            this.pnlMainContent.Controls.Add(this.grpSoundEffect);
            this.pnlMainContent.Controls.Add(this.grpCubeMap);
            this.pnlMainContent.Controls.Add(this.grpShader);
            this.pnlMainContent.Controls.Add(this.grpTexture);
            this.pnlMainContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMainContent.Location = new System.Drawing.Point(216, 24);
            this.pnlMainContent.Name = "pnlMainContent";
            this.pnlMainContent.Padding = new System.Windows.Forms.Padding(3);
            this.pnlMainContent.Size = new System.Drawing.Size(409, 485);
            this.pnlMainContent.TabIndex = 4;
            // 
            // grpModel
            // 
            this.grpModel.Controls.Add(this.ctrlModel);
            this.grpModel.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpModel.Location = new System.Drawing.Point(3, 710);
            this.grpModel.Name = "grpModel";
            this.grpModel.Size = new System.Drawing.Size(403, 77);
            this.grpModel.TabIndex = 6;
            this.grpModel.TabStop = false;
            this.grpModel.Text = "Model";
            this.grpModel.Visible = false;
            // 
            // ctrlModel
            // 
            this.ctrlModel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctrlModel.Item = null;
            this.ctrlModel.Location = new System.Drawing.Point(3, 16);
            this.ctrlModel.Name = "ctrlModel";
            this.ctrlModel.Size = new System.Drawing.Size(397, 58);
            this.ctrlModel.TabIndex = 0;
            // 
            // grpSoundEffect
            // 
            this.grpSoundEffect.Controls.Add(this.ctrlSoundConfig);
            this.grpSoundEffect.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpSoundEffect.Location = new System.Drawing.Point(3, 519);
            this.grpSoundEffect.Name = "grpSoundEffect";
            this.grpSoundEffect.Size = new System.Drawing.Size(403, 191);
            this.grpSoundEffect.TabIndex = 5;
            this.grpSoundEffect.TabStop = false;
            this.grpSoundEffect.Text = "Sound Effect";
            this.grpSoundEffect.Visible = false;
            // 
            // ctrlSoundConfig
            // 
            this.ctrlSoundConfig.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctrlSoundConfig.Item = null;
            this.ctrlSoundConfig.Location = new System.Drawing.Point(3, 16);
            this.ctrlSoundConfig.Name = "ctrlSoundConfig";
            this.ctrlSoundConfig.Size = new System.Drawing.Size(397, 172);
            this.ctrlSoundConfig.TabIndex = 0;
            // 
            // grpCubeMap
            // 
            this.grpCubeMap.Controls.Add(this.ctrlCubeMap);
            this.grpCubeMap.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpCubeMap.Location = new System.Drawing.Point(3, 283);
            this.grpCubeMap.Name = "grpCubeMap";
            this.grpCubeMap.Size = new System.Drawing.Size(403, 236);
            this.grpCubeMap.TabIndex = 5;
            this.grpCubeMap.TabStop = false;
            this.grpCubeMap.Text = "Cube Map";
            this.grpCubeMap.Visible = false;
            // 
            // ctrlCubeMap
            // 
            this.ctrlCubeMap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctrlCubeMap.Item = null;
            this.ctrlCubeMap.Location = new System.Drawing.Point(3, 16);
            this.ctrlCubeMap.Name = "ctrlCubeMap";
            this.ctrlCubeMap.Size = new System.Drawing.Size(397, 217);
            this.ctrlCubeMap.TabIndex = 0;
            // 
            // grpShader
            // 
            this.grpShader.Controls.Add(this.ctrlShaderConfig);
            this.grpShader.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpShader.Location = new System.Drawing.Point(3, 143);
            this.grpShader.Name = "grpShader";
            this.grpShader.Size = new System.Drawing.Size(403, 140);
            this.grpShader.TabIndex = 1;
            this.grpShader.TabStop = false;
            this.grpShader.Text = "Shader";
            this.grpShader.Visible = false;
            // 
            // ctrlShaderConfig
            // 
            this.ctrlShaderConfig.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctrlShaderConfig.Item = null;
            this.ctrlShaderConfig.Location = new System.Drawing.Point(3, 16);
            this.ctrlShaderConfig.Name = "ctrlShaderConfig";
            this.ctrlShaderConfig.Size = new System.Drawing.Size(397, 121);
            this.ctrlShaderConfig.TabIndex = 0;
            // 
            // grpTexture
            // 
            this.grpTexture.Controls.Add(this.ctrlTexture);
            this.grpTexture.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpTexture.Location = new System.Drawing.Point(3, 3);
            this.grpTexture.Name = "grpTexture";
            this.grpTexture.Size = new System.Drawing.Size(403, 140);
            this.grpTexture.TabIndex = 0;
            this.grpTexture.TabStop = false;
            this.grpTexture.Text = "Texture";
            this.grpTexture.Visible = false;
            // 
            // ctrlTexture
            // 
            this.ctrlTexture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctrlTexture.Item = null;
            this.ctrlTexture.Location = new System.Drawing.Point(3, 16);
            this.ctrlTexture.Name = "ctrlTexture";
            this.ctrlTexture.Size = new System.Drawing.Size(397, 121);
            this.ctrlTexture.TabIndex = 0;
            // 
            // dlgOpenManifest
            // 
            this.dlgOpenManifest.DefaultExt = "xml";
            this.dlgOpenManifest.FileName = "manifest.xml";
            this.dlgOpenManifest.Filter = "XML Files|*.xml";
            // 
            // dlgSaveManifest
            // 
            this.dlgSaveManifest.DefaultExt = "xml";
            this.dlgSaveManifest.Filter = "XML file|*.xml";
            this.dlgSaveManifest.Title = "Select location to save to";
            // 
            // cmsItemMenu
            // 
            this.cmsItemMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsiItemRename,
            this.tsiItemDelete});
            this.cmsItemMenu.Name = "cmsItemMenu";
            this.cmsItemMenu.Size = new System.Drawing.Size(118, 48);
            // 
            // tsiItemRename
            // 
            this.tsiItemRename.Name = "tsiItemRename";
            this.tsiItemRename.Size = new System.Drawing.Size(117, 22);
            this.tsiItemRename.Text = "Rename";
            this.tsiItemRename.Click += new System.EventHandler(this.ItemRenameClicked);
            // 
            // tsiItemDelete
            // 
            this.tsiItemDelete.Name = "tsiItemDelete";
            this.tsiItemDelete.Size = new System.Drawing.Size(117, 22);
            this.tsiItemDelete.Text = "Delete";
            this.tsiItemDelete.Click += new System.EventHandler(this.ItemDeleteClick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 509);
            this.Controls.Add(this.pnlMainContent);
            this.Controls.Add(this.trvPackage);
            this.Controls.Add(this.mnuMainMenu);
            this.MainMenuStrip = this.mnuMainMenu;
            this.Name = "MainForm";
            this.Text = "Manifest Packer";
            this.cmsPackage.ResumeLayout(false);
            this.cmsBundle.ResumeLayout(false);
            this.cmsCollection.ResumeLayout(false);
            this.mnuMainMenu.ResumeLayout(false);
            this.mnuMainMenu.PerformLayout();
            this.pnlMainContent.ResumeLayout(false);
            this.grpModel.ResumeLayout(false);
            this.grpSoundEffect.ResumeLayout(false);
            this.grpCubeMap.ResumeLayout(false);
            this.grpShader.ResumeLayout(false);
            this.grpTexture.ResumeLayout(false);
            this.cmsItemMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView trvPackage;
        private System.Windows.Forms.ContextMenuStrip cmsPackage;
        private System.Windows.Forms.ToolStripMenuItem tsiAddBundle;
        private System.Windows.Forms.ContextMenuStrip cmsBundle;
        private System.Windows.Forms.ToolStripMenuItem tsiAddTexture;
        private System.Windows.Forms.ToolStripMenuItem tsiAddShader;
        private System.Windows.Forms.ToolStripMenuItem tsiAddModel;
        private System.Windows.Forms.ToolStripMenuItem tsiAddCubeMap;
        private System.Windows.Forms.ToolStripMenuItem tsiAddSoundEffect;
        private System.Windows.Forms.ContextMenuStrip cmsCollection;
        private System.Windows.Forms.ToolStripMenuItem tsiAddItem;
        private System.Windows.Forms.MenuStrip mnuMainMenu;
        private System.Windows.Forms.ToolStripMenuItem tsiFile;
        private System.Windows.Forms.ToolStripMenuItem tsiSave;
        private System.Windows.Forms.ToolStripMenuItem tsiLoad;
        private System.Windows.Forms.ToolStripMenuItem tsiNew;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem tsiBundleRename;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tsiProjectSettings;
        private System.Windows.Forms.Panel pnlMainContent;
        private System.Windows.Forms.GroupBox grpTexture;
        private TextureConfig ctrlTexture;
        private System.Windows.Forms.OpenFileDialog dlgOpenManifest;
        private System.Windows.Forms.GroupBox grpShader;
        private ShaderConfig ctrlShaderConfig;
        private System.Windows.Forms.GroupBox grpCubeMap;
        private CubeMapConfig ctrlCubeMap;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox grpSoundEffect;
        private System.ComponentModel.BackgroundWorker backgroundWorker2;
        private SoundEffectConfig ctrlSoundConfig;
        private System.Windows.Forms.SaveFileDialog dlgSaveManifest;
        private System.Windows.Forms.ToolStripMenuItem tsiSaveAs;
        private System.Windows.Forms.GroupBox grpModel;
        private ModelConfig ctrlModel;
        private System.Windows.Forms.ContextMenuStrip cmsItemMenu;
        private System.Windows.Forms.ToolStripMenuItem tsiItemRename;
        private System.Windows.Forms.ToolStripMenuItem tsiItemDelete;
    }
}

