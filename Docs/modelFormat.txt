================= RENDER MATERIALS ================
	uint16 ID
	uint16 MATERIAL_NAME_LENGTH
	char*  MATERIAL_NAME
	uint16 SHADER_ID
	byte   TEX_COUNT

	TEXTURE_SLOT {
		byte    BIND_SLOT
		uint16  TEXTURE_PATH_LENGTH
		char*   TEXTURE_PATH
		uint16  TEXTURE_PARAM_NAME_LENGTH
		char*   TEXTURE_PARAM_NAME
	} [TEX_COUNT]

	byte   PARAM_COUNT <-- ???
	SHADER_PARAM {
		uint16  PARAM_NAME_LENGTH
		char*   PARAM_NAME
		byte    TYPECODE
		void*   DATA
	} [PARAM_COUNT]

================= PHYSICS MATERIALS ================= 
	uint16  ID
	uint16  MATERIAL_NAME_LENGTH
	char*   MATERIAL_NAME
	float32 DENSITY
	float32 FRICTION
	float32 ELASTICITY
	
	-- TODO: Audio Mappings -- 
 
================= MESH ================= 
	uint16 ID
	uint16 NAME_LENGTH
	char*  NAME
	byte   MODEL_FLAGS
	byte   PART_COUNT
	MESH_PART {
		MATRIX  TRANSFORM
		float32 VOLUME         <-- Used for physics calculations
		uint16  RENDER_MATERIAL
		uint16  PHYSICS_MATERIAL
		byte    MESH_FLAGS
		byte    VERTEX_FLAGS
		uint32  VERTEX_COUNT
		vertex* VERTEX_DATA    <-- Determined from VERTEX_FLAGS and MODEL_FLAGS
		uint32  INDEX_COUNT
		ixtype* INDEX_DATA
	} [PART_COUNT]

	(byte  JOINT_COUNT <-- Optional (HAS_SKELETON)
	MESH_JOINT {
		byte    JOINT_ID
		byte    PARENT_ID
		VEC3    DEFAULT_LOCATION
		QUATERN DEFAULT_ROTATION
	} [JOINT_COUNT])

	(byte  ANIMATION_COUNT <-- Optional (HAS_ANIMATIONS)
	MESH_ANIMATION {
		byte    ANIMATION_ID
		uint16  ANIMATION_NAME_LENGTH
		char*   ANIMATION_NAME
		uint16  KEYFRAME_COUNT
		ANIM_KEYFRAME {
			float32 TIME
			JOINT_FRAME {   
				VEC3    POSTION
				QUATERN ROTATION
			}[JOINT_COUNT]
		} [KEYFRAME_COUNT]
	} [ANIMATION_COUNT])
	

================= FLAGS ================= 
	
MODEL_FLAGS:
 - These are binary flags that indicate how a model is used and generated
 0x01 - HAS_SKELETON
 0x02 - HAS_ANIMATIONS
 0x04 - 
 0x08 - 
 0x10 - 
 0x20 -     
 0x40 -       
 0x80 - 
 
MESH_FLAGS:
 - These are binary flags that indicate how a mesh is used/created
 0x01 - USE_32_BIT_INDEXES
 0x02 - 
 0x04 - 
 0x08 - 
 0x10 - PRIMITIVE_MODE
 0x20 - PRIMITIVE_MODE    
 0x40 - PRIMITIVE_MODE      
 0x80 - PRIMITIVE_MODE
 
VERTEX_FLAGS:
 - These are binary flags that indicate what attributes of the vertex are specified
 0x01 - Position
 0x02 - TextureUV
 0x04 - Normal
 0x08 - Tangent
 0x10 - Binormal
 0x20 - 
 0x40 -      UNUSED
 0x80 -      UNUSED